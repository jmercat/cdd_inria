/**
 * \file Domain.cpp
 * \brief Definition of the class coor and Domain.
 * \version 1
 * \copyright ???
 * \author Jean Mercat (Inria/UBordeaux)
 * 
 * Domain is a set of nodes that have 2D coordinates organized in a 
 * partitionned space. A partition is called a bucket and contains a 
 * subset of the nodes. A node can switch from a bucket to another 
 * without too much memory movement. 
 * 
 * Some of the functions are inlined and therefore defined in header file.
 * 
 */

#include "Domain.h"

using namespace std;


/**
 * 
 * Default constructor.
 *
 */
coor::coor()
{
  memset(c,0,2*sizeof(double));
}

/**
 * 
 * Default constructor sets tag and coordinates to 0.
 *
 */
coorTag::coorTag() : coor(), tag(0)
{}

/**
 * \param co coordinates of the node to be constructed.
 * 
 * Constructor with packed coordinates.
 *
 */
coor::coor(double co[2])
{
  memcpy(this->c,co,2*sizeof(double));
}

/**
 * \param co coordinates of the node to be constructed.
 * 
 * Constructor with packed coordinates, sets tag to 1.
 *
 */
coorTag::coorTag(double co[2]) : coor(co), tag(1)
{}

/**
 * \param x  x coordinate of the node to be constructed.
 * \param y  y coordinate of the node to be constructed.
 * 
 * Constructor with separate coordinates.
 *
 */
coor::coor(double x, double y)
{
  c[0] = x;
  c[1] = y;
}

/**
 * \param x  x coordinate of the node to be constructed.
 * \param y  y coordinate of the node to be constructed.
 * 
 * Constructor with separate coordinates, sets tag to 1.
 *
 */
coorTag::coorTag(double x, double y) : coor(x,y), tag(1)
{}


/**
 * \param co coor structure to be copied
 * 
 * Copy constructor.
 *
 */
coor::coor(const coor& co)
{
  memcpy(this->c, co.c, 2*sizeof(double));
}

/**
 * \param co coor structure to be copied
 * 
 * Copy constructor.
 *
 */
coorTag::coorTag(const coorTag& co) : coor(co), tag(co.tag)
{}


/**
 * 
 * Destructor, sets tag to -1.
 *
 */
coorTag::~coorTag()
{
  tag = -1;
}


coor& coor::operator=(const coor& co)
{
  memcpy(this->c, co.c, 2*sizeof(double));
  return *this;
}

coorTag& coorTag::operator=(const coorTag& co)
{
  memcpy(this->c, co.c, 2*sizeof(double));
  tag = co.tag;
  return *this;
}

/**
 * 
 * Operator += add coordinate to coordinate.
 *
 */
coor& coor::operator+=(const coor& co)
{
  c[0]+=co.c[0];
  c[1]+=co.c[1];
  return *this;
}

/**
 * 
 * Operator -= substract coordinate to coordinate.
 *
 */
coor& coor::operator-=(const coor& co)
{
  c[0]-=co.c[0];
  c[1]-=co.c[1];
  return *this;
}

coor& coor::operator*=(const double& r)
{
  c[0]*=r;
  c[1]*=r;
  return *this;
}

void coorTag::setTag(const char t)
{
  tag = t;
}

void coor::setTag(const char t)
{}


/**
 * \param sigma standard deviation of the gaussian.
 * \param mu center of the gaussian along the y=x axis. 
 * 
 * Set coordinates to gaussian random value with parameters \a sigma and \a mu.
 *
 */
void coor::randg(std::default_random_engine& generator, double sigma, double mu)
{
  
  normal_distribution<double> gaussian(mu,sigma);
  uniform_real_distribution<double> uniform(0,2*3.14159265358979323846);
  double g,u;
  g = gaussian(generator);
  u = uniform(generator);
  c[0] = g*cos(u);
  c[1] = g*sin(u);
  //~ cout << "gaussian " << c[0] << " " << c[1] << " " << gaussian(generator) << endl;
  setTag(1);
}

/**
 * 
 * Set coordinates to uniform random value within [0,1]. 
 *
 */
void coor::rande(std::default_random_engine& generator)
{
  uniform_real_distribution<double> uniform(0,1);

  c[0] = uniform(generator);
  c[1] = uniform(generator);

  setTag(1);
}


/////////////////////////////// Domain ////////////////////////////////

/**
 * 
 * Default constructor sets number of buckets and nodes to 0, and 
 * pointers to NULL.
 *
 */
Domain::Domain() : nx(0), ny(0), npAll(0),
bucket(NULL), nbDel(NULL)
{    
  gettimeofday(&tm, NULL);
  std::default_random_engine generatorTemp(tm.tv_usec);
  swap(generator, generatorTemp);
}

/**
 * \param nx number of buckets along x direction.
 * \param ny number of buckets along y direction.
 * 
 * Constructor sets number of buckets to given values and nodes to 0. 
 * Allocates the bucket list.
 *
 */
Domain::Domain(int nx, int ny) : nx(nx), ny(ny), npAll(0)
{
  safeAlloc();
  memset(nbDel, 0, nx*ny*sizeof(int));
  gettimeofday(&tm, NULL);
  std::default_random_engine generatorTemp(tm.tv_usec);
  swap(generator, generatorTemp);
}

/**
 * \param d domain to copy
 *
 * Copy constructor.
 *
 */
Domain::Domain(const Domain& d) : nx(d.nx), ny(d.ny), npAll(d.npAll)
{
  safeAlloc();
  memcpy(nbDel, d.nbDel, nx*ny*sizeof(int));
  for (int j = 0; j< ny; j++)
    for (int i = 0; i< nx; i++)
    {
      bucket[i+j*nx] = d.bucket[i+j*nx];
    }
  gettimeofday(&tm, NULL);
  std::default_random_engine generatorTemp(tm.tv_usec);
  swap(generator, generatorTemp);
}

Domain::~Domain()
{
  delete[] bucket;
  delete[] nbDel;
  bucket = NULL;
  nx = 0;
  ny = 0;
  npAll = 0;
}

/**
 * 
 * Allocation with verification of success of: bucket and list of number
 * of deleted elements in each bucket,
 * 
 */
void Domain::safeAlloc()
{
  try 
  {
    bucket = new vector<coorTag>[nx*ny];
  }catch (std::bad_alloc&)
  {
    std::cerr << "Domain : bucket allocation failed" <<std::endl;
    exit(EXIT_FAILURE);
  }
  
  try 
  {
    nbDel = new int[nx*ny];
  }catch (std::bad_alloc&)
  {
    std::cerr << "Domain : nbDel allocation failed" <<std::endl;
    delete[] bucket;
    exit(EXIT_FAILURE);
  }
}


/**
 *\param name name of the output file, if it contains '.' all right of
 * it will be deleted. 
 *  
 * Outputs domain coordinates in ".node" format (not standard) either 
 * for analysis by another script or an importation later on.
 * 
 */
void Domain::outputNode(string name)
{
  name =  name.substr(0,name.find("."));
  name += ".node";
    
  ofstream file;
  if( !file.is_open()) 
  {
    file.open(const_cast <char*> (name.c_str()),ios::out | ios::trunc);
    if (!file.is_open())
      cout << " ** UNABLE TO OPEN " << name.c_str() << endl;
  }
  
  file << npAll <<"  " << 2 <<"  " << 1000 << "  " << 1. << endl;
  double x, y;
  for (int j = 0; j< ny; j++)
    for (int i = 0; i< nx; i++)
      for(unsigned int k = 0; k<bucket[i+nx*j].size(); ++k)
        if(bucket[i+nx*j][k].getTag()>0)
        {
          x = bucket[i+nx*j][k](0);
          y = bucket[i+nx*j][k](1);
          file << i << "  " << x<< "  " << y << "  " << 0. << endl;
        }
      
  file << "# end of file" << endl;
  file.close();
}

/**
 *\param name name of the input file.
 *  
 * Inputs domain coordinates from ".node" format (non standard).
 * 
 */
void Domain::inputNode(string name)
{
  ifstream file(name.c_str());

  if (file.is_open())
  {
    int no;
    float x,y;
    double s;
    coorTag vertex;
    file >> npAll >> x >> y >> s;
    cout << npAll << " " << nx << " " << ny << endl;
    // read the file
    for(int i = 0; i <npAll; i++)
    {
      //insert point in domain
      file >> no >> x >> y >> s;
      int posi = max(min(int(x*nx),nx-1),0);
      int posj = max(min(int(y*ny),ny-1),0); 
      vertex.set(x,y);
      vertex.setTag(1);
      bucket[posi+nx*posj].push_back(vertex);
    }
    file.close();
  }
}


Domain& Domain::operator=(const Domain& d) 
{
  if (this != &d)
  {
    if (!bucket)
    {
      safeAlloc();
    }
    else if ( nx*ny != d.nx*d.ny)
    {
      delete[] bucket;
      delete[] nbDel;
      safeAlloc();
      nbDel = new int[d.nx*d.ny];
    }
    nx = d.nx;
    ny = d.ny;
    memcpy(nbDel, d.nbDel, nx*ny*sizeof(int));

    for (int i = 0; i< nx; i++)
      for (int j = 0; j< ny; j++)
      {
        bucket[i+j*nx] = d.bucket[i+j*nx];

      }
  }
  return *this;
}


/**
 *\param i index of the bucket in linear storage.
 *  
 * Outputs bucket of index i as it is stored linearly.
 * 
 */
vector<coorTag>& Domain::operator()(int i)
{
  return bucket[i];
}

/**
 *\param i index of the bucket.
 *\param j index of the bucket.
 *  
 * Outputs bucket of index i,j seen as a 2D grid.
 * 
 */
vector<coorTag>& Domain::operator()(int i, int j)
{
  return bucket[i+j*nx];
}

/**
 *\param i index of the bucket.
 *\param j index of the bucket.
 *\param k index of the node.
 *  
 * Outputs node of index i,j,k.
 * 
 */
coorTag& Domain::operator()(int i, int j, int k)
{
  return bucket[i+j*nx][k];
}


/**
 *\param i potential index of the bucket.
 *\param j potential index of the bucket.
 *  
 * Tells if index i,j correspond to a bucket or are out of bounds.
 * 
 */
bool Domain::isBucket(int posi, int posj)
{
  return (posi >= 0 && posj >= 0 && posi < nx && posj < ny);
}

/**
 *\param vertex coor structure of the vertex to be added.
 *  
 * Inputs a vertex in the Domain at the back of the correct bucket.
 * 
 */
void Domain::addVertex(const coorTag& vertex)
{
  double x = vertex(0);
  double y = vertex(1);
  int posi = max(min(int(x*nx),nx-1),0);
  int posj = max(min(int(y*ny),ny-1),0); 
  bucket[posi+nx*posj].push_back(vertex);
  
  bucket[posi+nx*posj].back().setTag(1);
  ++npAll;
}

/**
 *\param vertex coor structure of the vertex to be added.
 *  
 * Inputs a vertex in the Domain at the first empty space in the
 * correct bucket.
 * 
 */
void Domain::addPackedVertex(const coorTag& vertex)
{
  double x = vertex(0);
  double y = vertex(1);
  int posi = max(min(int(x*nx),nx-1),0);
  int posj = max(min(int(y*ny),ny-1),0); 
  int indB = posi + nx*posj;
  if (nbDel[indB] > 0)
  {
    for (int k = bucket[indB].size()-1; k>=0; --k)//more chance of finding it close to the end
    {
      if (bucket[indB][k].getTag() <=0)
      {
        bucket[indB][k] = vertex;
        --nbDel[indB];
      }
    }
  }else
  {
    bucket[posi+nx*posj].push_back(vertex);
  }
  ++npAll;
}

/**
 *\param posi index of the bucket.
 *\param posj index of the bucket.
 *\param ind index of the vertex.
 *  
 * Delete a vertex from the Domain. Only thing done is setting the tag
 * of the vertex to -1. However if the structures has too much holes, 
 * (stored coordinates with tag -1), it is packed, meaning that all the
 * vertices with tag -1 will be deleted and other vertices are stored
 * contiguously.
 * 
 */
void Domain::delVertex(int posi, int posj, int ind)
{
  int indB = posi + nx*posj;
  bucket[indB][ind].setTag(-1);
  ++nbDel[indB];
  --npAll;
  if (((double) (nbDel[indB]))/bucket[indB].size()>pDelMax)
  {
    packVerticesPop(posi, posj);
  }
}

int Domain::getNx() const
{
  return nx;
}

int Domain::getNy() const
{
  return ny;
}

/**
 *\param i index of the bucket in linear format.
 *  
 * Returns the size of the bucket of index i. Be carefull, this is NOT 
 * the actual number of vertices in the bucket because some of them have
 * a tag that is not set to 1 and therefore are not actual vertices.
 * 
 */
int Domain::getNp(int i) const
{
  return bucket[i].size();
}

/**
 *\param i index of the bucket.
 *\param j index of the bucket.
 *  
 * Returns the size of the bucket of index i,j. Be carefull, this is NOT 
 * the actual number of vertices in the bucket because some of them have
 * a tag that is not set to 1 and therefore are not actual vertices.
 * 
 */
int Domain::getNp(int i, int j) const
{
  return bucket[i+nx*j].size();
}

/**
 *  
 * Returns the total number of actual vertices. This is NOT the sum of 
 * the size of the buckets. It only includes actual vertices: with tag=1.
 * 
 */
int Domain::getNpAll() const
{
  return npAll;
}

/**
 * \param indB index of the bucket.
 * \param ind index of the vertex.
 *  
 * Returns the index of the next vertex with tag < 0. If not existing,
 * returns size of the bucket.
 * 
 */
int Domain::nextNegTag(int indB, int ind) const
{  
  for (unsigned int i = ind+1; i<bucket[indB].size(); i++)
  {
    if (bucket[indB][i].getTag()<0)
      return i;
  }
  return bucket[indB].size();
}

/**
 *\param i index of the bucket.
 *\param j index of the bucket.
 *  
 * Pack vertices: all the vertices with tag -1 are deleted and other 
 * vertices are stored contiguously.
 * 
 */
void Domain::packVerticesPop(int i, int j)
{
  int indB = i+nx*j;
  int nbVertices = bucket[indB].size()-nbDel[indB];
  vector<coorTag> newCell(nbVertices);

  int index = 0;
  for (unsigned int k = 0; k<bucket[indB].size(); ++k)
  {
    if (bucket[indB][k].getTag()>0)
    {
      newCell[index] = bucket[indB][k];

      ++index;
    }
  }
  bucket[indB].swap(newCell);

  nbDel[indB] = 0;
}
