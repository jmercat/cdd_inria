/**
 * 
 * \file Simulation.cpp
 * \brief Main : settings and main loop for particle movement.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * 
 * 
 */
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#if defined(unix)        || defined(__unix)      || defined(__unix__) \
 || defined(linux)       || defined(__linux)     || defined(__linux__) \
 || defined(sun)         || defined(__sun) \
 || defined(BSD)         || defined(__OpenBSD__) || defined(__NetBSD__) \
 || defined(__FreeBSD__) || defined (__DragonFly__) \
 || defined(sgi)         || defined(__sgi) \
 || defined(__MACOSX__)  || defined(__APPLE__) \
 || defined(__CYGWIN__)
#include <sys/time.h>
#elif defined(_MSC_VER) || defined(WIN32)  || defined(_WIN32) || defined(__WIN32__) \
   || defined(WIN64)    || defined(_WIN64) || defined(__WIN64__)
#include "sys/time.h"
#endif

#include <ctime>

#include <omp.h>


#include "Domain.h"
#include "plot.h"
#include "interaction.h"
#include "debug.h"
#ifndef pi
#define pi 3.14159265359
#endif
using namespace std;
/**
 *
 * @param tm1 output time
 *
 * Start time count in ms
 *
 */
void start(struct timeval* tm1)
{
  gettimeofday(tm1, NULL);
}

/**
 *
 * @param tm1 input time
 *
 * Write time count since \a tm1 with ms precision.
 *
 */
void stop(struct timeval* tm1)
{
  struct timeval tm2;
  gettimeofday(&tm2, NULL);

  unsigned long long t = 1000 * (tm2.tv_sec - (*tm1).tv_sec) + (tm2.tv_usec - (*tm1).tv_usec) / 1000;
  printf("%llu ms\n", t);
}

/**
 *
 * @param sigma standard deviation
 * @param mu mean
 * @param x output value
 * @param y output value
 *
 * 2-D Normal distribution with standard deviation \a sigma and mean \a mu
 * outputs a random position \a x \a y.
 *
 */
void randg(double sigma, double mu, double& x, double& y)
{
  double r,theta;

  x = ((double) rand())/RAND_MAX;
  y = ((double) rand())/RAND_MAX;
  
  r = sqrt(-2*log(x));
  theta = 2*pi*y;
  x = sigma*r*cos(theta)+mu;
  y = sigma*r*sin(theta)+mu;

}


/**
 *
 * @param x output random position
 * @param y output random position
 *
 * Outputs a uniform random position \a x \a y.
 *
 */
void rande(double& x, double& y)
{
  x = ((double) rand())/RAND_MAX;
  y = ((double) rand())/RAND_MAX;
}

/**
 *
 * @param d domain
 *
 * Print for debug purpose
 *
 */
void printDomain(Domain d)
{
  Domain_for(d,i,j,k)
    cout << d(i,j,k)(0) << "   " << d(i,j,k)(1) << endl; 
}

/**
 *
 * @param d domain
 * @param posi first index of the cell
 * @param posj second index of the cell
 *
 * Print for debug purpose
 *
 */
void printCell(Domain d, int posi, int posj)
{
  Cell_for(d,posi,posj,k)
    cout << d(posi,posj,k)(0) << "   " << d(posi,posj,k)(1) << "    " << d(posi,posj,k).getTag() << endl; 
}

/**
 *
 * @param d domain
 * @param posi first index of the cell
 * @param posj second index of the cell
 *
 * Print for debug purpose (include positions with tag 0)
 *
 */
void printCellAll(Domain d, int posi, int posj)
{
  for(int k = 0; k<d.getNp(posi,posj); ++k)
    cout << d(posi,posj,k)(0) << "   " << d(posi,posj,k)(1) << "    " << d(posi,posj,k).getTag() << endl; 
}


/**
 *
 * @param d domain
 * @param lmoy mean distance between particles
 * @param name name of the file to output
 *
 * Draw domain in a post script file.
 *
 */
void drawDomain(Domain d, double lmoy, string name)
{
  postscript ps(name);
  //~ double r;
  Bucket_for(d,i,j)
  {      
    //~ r = (double) (rand())/RAND_MAX;
    Cell_for(d,i,j,k)
    {
      //~ ps.setHueColor(r);
      ps.drawPoint( d(i,j,k), lmoy/3);
    }
  }
  //~ #warning if nx and ny are different, this won't work
  //~ Bucket_for(d,i,j)
  //~ {
    //~ double l = 1./d.getNx(); 
    //~ coor pt(i*l, j*l);
    //~ ps.drawSquare(pt,l);
  //~ }
}


/**
 *
 * @param fileName name of the file to output
 * @param vec vector of data to output
 *
 * Outputs the values of the vector \a vec in a file.
 *
 */
void outputVect(string fileName, vector<double>  vec)
{
  ofstream fileOut;
  fileOut.open(const_cast < char* > (fileName.c_str()));
  
  for (unsigned int i = 0; i< vec.size(); i++)
  {
    fileOut << i << "    " << vec[i] << endl;
  }
  fileOut.close();
}


int main ()
{
  struct timeval tm;
  struct timeval t0;
  start(&t0);
  gettimeofday(&tm, NULL);
  srand(tm.tv_usec); // initialisation de rand 
  ofstream fileOut, xyout;
  xyout.open(const_cast < char* > ("xyout.txt"));
  
  ifstream inSettings;
  string nameSetting;
  double valSetting;

  //**** get the settings from file and set the other settings accordingly ****
  inSettings.open("settings.txt");
  if(!inSettings.is_open())
  {
    cout << "Couldn't read input file 'settings.txt'" << endl;
    return -1;
  }
  inSettings >> nameSetting >> valSetting;
  int nbParticles = valSetting;
  double sigma = 1./sqrt(nbParticles*cos(pi/6));
  //~ double mu = 0.5;
  //~ double lambda = 1;//0.995; // cooling speed 0: cools infinitly fast, 1: does not cool down
  inSettings >> nameSetting >> valSetting;
  double eps = valSetting/sqrt(nbParticles);//0.000000000000000001/nbParticles;
  inSettings >> nameSetting >> valSetting;
  double distInteract = valSetting/sqrt(nbParticles);
  inSettings >> nameSetting >> valSetting;
  double sigmaBro = sigma*valSetting;//sigma*0.002;

  double aniConst, freqSin, aniSin;
  inSettings >> nameSetting >> aniConst;
  inSettings >> nameSetting >> freqSin;
  inSettings >> nameSetting >> aniSin;
  Interaction interactionDomain(sigma,eps,distInteract,aniConst,freqSin,aniSin);  //create an interaction domain
  //~ interactionDomain.inputNode("domain024.node", eps);
  cout <<"Bucket size: " << interactionDomain.getNx() <<endl;


  // Set the positions of the particles in the domain
  //~ interactionDomain.inputNode("output/domain0.node", eps);
  //~ interactionDomain.inputNode("1h.node", eps);
  interactionDomain.inputRandNode(nbParticles);
  
  interactionDomain.printMembers();
  //~ cout << interactionDomain.getPotential() << " " << interactionDomain.getPotentialBF() << endl;
  string name = "output/domain";
  struct timeval t1;
  char buffer[128];
  sprintf(buffer,"0");
  drawDomain(interactionDomain,sigma, name + buffer);
  interactionDomain.outputNode(name+buffer);
  
  
  interactionDomain.setSigmaBro(sigmaBro);
  inSettings >> nameSetting >> valSetting;
  int tTot = valSetting;
  vector <double> potentialEvo(tTot,0);
  vector <double> nbModif;
  //~ int nbModifTemp=0;
  start(&t1);
  inSettings >> nameSetting >> valSetting;
  int saveEvery=valSetting;

  double aniConstPreorder, aniSinPreorder;
  int tPreorder;
  inSettings >> nameSetting >> aniConstPreorder;
  inSettings >> nameSetting >> aniSinPreorder;
  inSettings >> nameSetting >> tPreorder;
  
  interactionDomain.setAniConst(aniConstPreorder);
  interactionDomain.setAniSin(aniSinPreorder);
  // pre-ordering time loop
  for (int i = 1; i <= tPreorder; ++i)
  {
    interactionDomain.moveVertices();
    potentialEvo[i-1] = interactionDomain.getPotential()+1;

    sprintf(buffer,"%03d",i/saveEvery);

    if (i%saveEvery==0)
    {
      //~ drawDomain(interactionDomain,sigma, name + buffer);
      interactionDomain.outputNode(name + buffer);
      cout << (100*i)/tTot << "% Time per " << saveEvery <<" iterations ";
      stop(&t1);
      start(&t1);
      interactionDomain.printxyDist();
      interactionDomain.resetxyDist();
      
      
    }
    //~ interactionDomain.resetxyDist();
  }
  sprintf(buffer,"%03dEndPreorder",tPreorder/saveEvery);
  interactionDomain.outputNode(name + buffer);
  
  interactionDomain.setAniConst(aniConst);
  interactionDomain.setAniSin(aniSin);
  // time loop
  for (int i = tPreorder>0?tPreorder:1; i <= tTot; ++i)
  {
    interactionDomain.moveVertices();
    potentialEvo[i-1] = interactionDomain.getPotential()+1;

    sprintf(buffer,"%03d",i/saveEvery);

    if (i%saveEvery==0)
    {
      drawDomain(interactionDomain,sigma, name + buffer);
      interactionDomain.outputNode(name + buffer);
      cout << (100*i)/tTot << "% Time per " << saveEvery <<" iterations ";
      stop(&t1);
      start(&t1);
      interactionDomain.printxyDist();
      interactionDomain.resetxyDist();
      interactionDomain.printSaturate();
      interactionDomain.resetSaturate();
    }
    //~ interactionDomain.resetxyDist();
  }
  xyout.close();
  //exchange positions to verify periodic boundary conditions
  interactionDomain.checkPosition();
  
  sprintf(buffer,"check");
  drawDomain(interactionDomain, sigma, name + buffer);
  interactionDomain.outputNode(name + buffer);

  outputVect("output/potential.dat", potentialEvo);
  
  cout << "_______________________" << endl;
  cout << "Total time ";
  stop(&t0);
  
  return 0;
}
