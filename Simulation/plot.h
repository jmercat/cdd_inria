#ifndef PLOT_H
#define PLOT_H

#include <string>
#include <iostream>
#include <fstream>
#include "Domain.h"


// structure allowing postscript writing
class postscript
{
  public:
    postscript(std::string name);
    ~postscript();
    
    void drawPoint( coor c, double radius);
    void drawLine(coor pt1, coor pt2);
    void drawSquare(const coor& pt, double l);
    void setLineWidth(double width);
    void setHueColor(double h);
    void setHsbColor(double h, double s = 1, double b = 1);
    void setGrayColor(double g);
  private:
    std::ofstream file;
    std::string filename; // name of output file
    double color[3]; // current color
    double width; // current line width
    double box_width; // size of the square box in wich the mesh is drawn
    double point0[2]; // coordinates of the bottom left corner of the box
    static constexpr double margin = 30; // size of the margin
};



#endif
