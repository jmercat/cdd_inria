#ifndef DEBUG_H
#define DEBUG_H

#ifdef DEBUG
#define Debug(p) p
#else
#define Debug(p)
#endif


#ifdef DEBUG
#define IntTest asm("int3")
#define Trap(t) if(t) asm("int3")
#define TrapNot(t) if(!t) asm("int3")
#define IntTestElse else asm("int3")
#else
#define IntTest
#define Trap(t)
#define TrapNot(t)
#define IntTestElse
#endif

#endif
