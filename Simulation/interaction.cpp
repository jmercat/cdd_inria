/**
 * \file interaction.cpp
 * \brief Class adding interactions between the nodes of a Domain.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * 
 * 
 */

#include <math.h>
#include <cmath>
#include "interaction.h"
#include "debug.h"
#include <iostream>
#include <omp.h>

using namespace std;

/**
 *
 * @param sigma_i sigma constant of Lennard-Jones potential 4eps[(sigma/r)^12-(sigma/r)^6]
 * @param eps_i epsilon constant of Lennard-Jones potential
 * @param distInter_i distance of interaction.
 * @param aniconst Constant movement anisotropy
 * @param freqsin Frequence of sinusoïdal surface
 * @param anisin Sinusoïdal anisotropy
 *
 */
Interaction::Interaction(double sigma_i, double eps_i, double distInter_i,double aniconst, double freqsin, double anisin) :
Domain(((int) (1/(distInter_i))/2*2),((int) (1/(distInter_i)))/2*2), force(NULL), sigmaBro(sigma_i/50), sigma(sigma_i/pow(2,1./order)),
eps(eps_i), distInter(distInter_i), deltaDistInter(distInter_i/10), sinus(602,0), xDist(0), yDist(0),
freqSin(freqsin), aniConst(aniconst), aniSin(anisin), saturate(0), normal(0)
{
  safeAllocMotion();
}

/**
 *
 * @param nx domain number of subdomain in x direction
 * @param ny domain number of subdomain in y direction
 * @param sigma_i sigma constant of Lennard-Jones potential 4eps[(sigma/r)^12-(sigma/r)^6]
 * @param eps_i epsilon constant of Lennard-Jones potential
 * @param distInter_i distance of interaction.
 * @param aniconst Constant movement anisotropy
 * @param freqsin Frequence of sinusoïdal surface
 * @param anisin Sinusoïdal anisotropy
 *
 */
Interaction::Interaction(int nx, int ny, double sigma_i, double eps_i, double distInter_i,double aniconst, double freqsin, double anisin) :
Domain(nx,ny), force(NULL),sigmaBro(sigma_i/50), sigma(sigma_i/pow(2,1./order)),
eps(eps_i), distInter(distInter_i), deltaDistInter(distInter_i/10), sinus(602,0), xDist(0), yDist(0),
freqSin(freqsin), aniConst(aniconst), aniSin(anisin), saturate(0), normal(0)
{
  safeAllocMotion();
}


/**
 *
 * @param dom domain to copy
 *
 */
Interaction::Interaction(const Interaction& dom) :
Domain(dom), sigmaBro(dom.sigmaBro), sigma(dom.sigma), eps(dom.eps), distInter(dom.distInter),
deltaDistInter(dom.deltaDistInter), sinus(dom.sinus), xDist(0), yDist(0), freqSin(dom.freqSin),
aniConst(dom.aniConst), aniSin(dom.aniSin), saturate(0), normal(0)
{
  safeAllocMotion();
  for (int j = 0; j< ny; j++)
    for (int i = 0; i< nx; i++)
    {
      force[i+j*nx] = dom.force[i+j*nx];
    }
}

Interaction::~Interaction()
{
  delete[] force;
  force = NULL;
}

void Interaction::safeAllocMotion()
{
  if (nx && ny)
  {
    try 
    {
      force = new vector<coor>[nx*ny];
    }catch (std::bad_alloc&)
    {
      std::cerr << "Domain : force allocation failed" <<std::endl;
      delete[] bucket;
      exit(EXIT_FAILURE);
    }
  }
}

/**
 *
 * @param vertex vertex to add to the domain
 *
 */
void Interaction::addVertex(const coorTag& vertex)
{
  coor zero(0.,0.);
  double x = vertex(0);
  double y = vertex(1);
  int posi = max(min(int(x*nx),nx-1),0);
  int posj = max(min(int(y*ny),ny-1),0); 
  int indB = posi+nx*posj;
  bucket[indB].push_back(vertex);
  force[indB].push_back(zero);
  
  bucket[indB].back().setTag(1);
  ++npAll;
}

/** Not used
 *
 * @param vertex vertex to add to the domain
 * @param cForce force applied on the particle
 *
 * I really don't remember why I made this function but it is never called anyway...
 *
 */
void Interaction::addVertex(const coorTag& vertex, const coor& cForce)
{
  double x = vertex(0);
  double y = vertex(1);
  int posi = max(min(int(x*nx),nx-1),0);
  int posj = max(min(int(y*ny),ny-1),0); 
  int indB = posi+nx*posj;
  bucket[indB].push_back(vertex);
  force[indB].push_back(cForce);
  
  bucket[indB].back().setTag(1);
  ++npAll;
}


/**
 *
 * @param vertex vertex to add to the domain
 *
 * The vertex is added to an empty space instead of at the end.
 *
 */
void Interaction::addPackedVertex(const coorTag& vertex)
{
  double x = vertex(0);
  double y = vertex(1);
  int posi = max(min(int(x*nx),nx-1),0);
  int posj = max(min(int(y*ny),ny-1),0); 
  int indB = posi + nx*posj;
  if (nbDel[indB] > 0)
  {
    for (int k = bucket[indB].size()-1; k>=0; --k)//more chance of finding it close to the end
    {
      if (!bucket[indB][k].isTag())
      {
        bucket[indB][k] = vertex;
        bucket[indB][k].setTag(1);
        --nbDel[indB];
      }
    }
  }else
  {
    bucket[posi+nx*posj].push_back(vertex);
  }
  ++npAll;
}

/**
 *
 * @param nbParticles number of particles to input
 *
 * Input particles at uniform random positions.
 *
 */
void Interaction::inputRandNode(int nbParticles)
{
  for (int i = 0; i <nbParticles; ++i)
  {
    coorTag v;
    v.rande(generator);
    addVertex(v);
  }
}

/**
 *
 * @param name path and name of the file to read
 * @param eps_i epsilon constant of Lennard-Jones potential
 *
 * Input nodes from a .node file in the interaction domain.
 *
 */
void Interaction::inputNode(string name, double eps_i)
{
  ifstream file(name.c_str());
  
  if (file.is_open())
  {
    int no;
    float x,y;
    double s;
    int indB;
    int nbParticles;
    coorTag vertex;  
    coor zero(0.,0.);
    file >> nbParticles >> x >> y >> s;
    
    double distInteract = 3./sqrt(nbParticles);
       
    Interaction copy(1./sqrt(nbParticles*cos(pi/6)),eps_i,distInteract);
    
    cout << copy.sigma << " " << copy.eps << " " << copy.nx << " " << copy.ny << " " << distInter << " " << copy.nbDel[0] << endl;
    //~ cout << npAll << " " << nx << " " << ny << endl;
    // read the file
    for(int i = 0; i <nbParticles; i++)
    {
      //insert point in domain
      file >> no >> x >> y >> s;
      int posi = max(min(int(x*nx),nx-1),0);
      int posj = max(min(int(y*ny),ny-1),0); 
      vertex.set(x,y);
      vertex.setTag(1);
      indB = posi+copy.nx*posj;
      copy.bucket[indB].push_back(vertex);  
      copy.force[indB].push_back(zero);
      ++copy.npAll;
    }
    file.close();
    
    *this = copy;
    cout << npAll << " " << sigma << " " << eps << " " << nx << " "  << ny << " " << distInter << endl;
  }else
    cout << "Error : cannot read file " << name.c_str() << endl;
}

/**
 *
 * @param posi bucket i index
 * @param posj bucket j index
 * @param ind index of the vertex in the i j bucket
 *
 * Delete a vertex from the domain.
 *
 */
void Interaction::delVertex(int posi, int posj, int ind)
{
  int indB = posi + nx*posj;

  bucket[indB][ind].setTag(-1);
  ++nbDel[indB];
  --npAll;
  if (((double) (nbDel[indB]))/bucket[indB].size()>pDelMax)
  {
    packVerticesPop(posi, posj);
  }
}


/**
 *
 * @param i bucket index
 * @param j bucket index
 *
 * Pack the i j bucket. Meaning remove vertices with tag 0 and store the other verticies contiguously.
 *
 */
void Interaction::packVerticesPop(int i, int j)
{
  int indB = i+nx*j;
  int nbVertices = bucket[indB].size()-nbDel[indB];
  vector<coorTag> newCell(nbVertices);
  vector<coor> newCellForce(nbVertices);
  int index = 0;

  for (unsigned int k = 0; k<bucket[indB].size(); ++k)
  {
    if (bucket[indB][k].isTag())
    {
      newCell[index] = bucket[indB][k];
      newCellForce[index] = force[indB][k];
      ++index;
    }
  }
  bucket[indB].swap(newCell);
  force[indB].swap(newCellForce);
  nbDel[indB] = 0;
}

/**
 *
 * @param epsi new value of epsilon in Lennard-Jones potential
 *
 */
void Interaction::setEps(double epsi)
{
  eps = epsi;
}

/**
 *
 * @param newAniConst new value of constant anisotropy
 *
 */
void Interaction::setAniConst(double newAniConst)
{
  aniConst = newAniConst;
}

/**
 *
 * @param newAniSin new value of sinusoidal anisotropy
 *
 */
void Interaction::setAniSin(double newAniSin)
{
  aniSin = newAniSin;
}


/**
 *
 * @param sigmaBroi new value of sigmaBro in the model (thermal brownian movement)
 *
 */
void Interaction::setSigmaBro( double sigmaBroi)
{
  sigmaBro = sigmaBroi;
}

Interaction& Interaction::operator=(Interaction d) 
{
  swap(nx,d.nx);
  swap(ny,d.ny);
  swap(bucket,d.bucket);
  swap(force,d.force);
  swap(nbDel,d.nbDel);
  swap(sigma,d.sigma);
  swap(eps,d.eps);
  swap(distInter,d.distInter);
  swap(deltaDistInter,d.deltaDistInter);
  swap(npAll,d.npAll);
  sinus.swap(d.sinus);
  
  return *this;
}

/**
 *
 * @return potential of the particles
 *
 * Warning: This is not exactly the potential from wich the forces derive,
 * because the smoothing function at the border of the interaction distance
 * is the same in forces and potential.
 *
 */
double Interaction::getPotential()
{
  unsigned int sinSize = 10*sqrt(npAll);
  if (sinus.size()!=sinSize)
  {
    sinus.resize(sinSize);
    double x=0, dx;
    dx = 1./(sinus.size()+1);
    for (unsigned int i = 0; i < sinus.size(); i++)
    {
      x += dx;
      sinus[i] = sin(2*pi*x);
    }
  }
 
  double potential = 0;
  // loop over the whole domain 
  #pragma omp parallel for
  for (int j = 0; j< ny; j++)
    for (int i = 0; i< nx; i++)
      potential += getCellPotential(i,j);
      
  
  #pragma omp parallel for
  for (int j = 0; j< ny; j++) 
  {
    for (int i = 0; i< nx; i++)
    {  
      if((i+j)%2 == 0)
      {
        potential += getInterCellPotential(i,j,i-1,j);
        potential += getInterCellPotential(i,j,i,j-1);
        potential += getInterCellPotential(i,j,i,j+1);
        potential += getInterCellPotential(i,j,i+1,j);
      }
      if(i%2 == 0)
      {
        potential += getInterCellPotential(i,j,i-1,j-1);
        potential += getInterCellPotential(i,j,i-1,j+1);
        potential += getInterCellPotential(i,j,i+1,j-1);
        potential += getInterCellPotential(i,j,i+1,j+1);
      }
    }
  }
  return potential;
}

/**
 *
 * Compute the forces of particles pushing against each other (stored in interaction::force)
 *
 */
void Interaction::computeForce()
{
  
  // loop over the whole domain
  #pragma omp parallel for
  for (int j = 0; j< ny; j++)
    for (int i = 0; i< nx; i++)
      computeCellForce(i,j);
  #pragma omp parallel for
  for (int j = 0; j< ny; j++) 
  {
    for (int i = 0; i< nx; i++)
    {  
      if((i+j)%2 == 0)
      {
        computeInterCellForce(i,j,i-1,j);
        computeInterCellForce(i,j,i,j-1);
        computeInterCellForce(i,j,i,j+1);
        computeInterCellForce(i,j,i+1,j);
      }
      if(i%2 == 0)
      {
        computeInterCellForce(i,j,i-1,j-1);
        computeInterCellForce(i,j,i-1,j+1);
        computeInterCellForce(i,j,i+1,j-1);
        computeInterCellForce(i,j,i+1,j+1);
      }
    }
  }
}

/**
 *
 * @param posi cell i index (cell=bucket)
 * @param posj cell j index
 * @return potential of the cell
 *
 */
double Interaction::getCellPotential(int posi, int posj )
{
  int indB = posi + nx*posj;
  vector <coorTag> *b; 
  b = &(bucket[indB]);
  coorTag *bk, *bl;
  double potential = 0;
  for(unsigned int k = 0; k< b->size(); ++k)
  { 
    bk = &((*b)[k]);
    if(bk->isTag())
    {
      for(unsigned int l = k+1; l <bucket[indB].size(); ++l)
      {
        bl = &((*b)[l]);
        double dist2 = getDist2(*bk,*bl,0,0,0);
        double dist = sqrt(dist2);
        if(bl->isTag() && dist < distInter)
        {
          potential += getLJP2(dist2);
          //~ cout<< "potential cell" << potential << " " << dist/sigma << endl;
          //~ Trap(isnan(potential));
        }else if(bl->isTag() && dist < distInter + deltaDistInter)
        {
          potential += smoothSupport(dist)*getLJP(dist);
          //~ cout<< "potential2 cell" << potential << " " << dist/sigma << endl;
          //~ Trap(isnan(potential));
        }
      }
    }
  }
  return potential;
}

/**
 *
 * @param posi1 index i of cell 1
 * @param posj1 index j of cell 1
 * @param posi2 index i of cell 2
 * @param posj2 index j of cell 2
 * @return potential between cell 1 and cell 2
 *
 */
double Interaction::getInterCellPotential(int posi1, int posj1, int posi2, int posj2)
{
  double potential = 0;
  vector <coorTag> *b1, *b2;
  coorTag *bk, *bl;
  int i2 = posi2, j2 = posj2;
  int i1 = posi1, j1 = posj1;
  double xt = 0, yt = 0;
  
  // Periodic boundary conditions
  if (posi2>=nx)
  {
    i2 = posi2-nx;
    xt = -1;
  }else if(posi2<0)
  {
    i2 = nx+posi2;
    xt = +1;
  }
  
  if (posj2>=ny)
  {
    j2 = posj2-ny;
    yt = -1;
  }else if(posj2<0)
  {
    j2 = posj2+ny;
    yt = +1;
  } 
  
  int indB1 = i1 + nx*j1;
  b1 = &(bucket[indB1]);
  int indB2 = i2 + nx*j2;
  b2 = &(bucket[indB2]);
  for(unsigned int k = 0; k< b1->size(); ++k)
  { 
    bk = &((*b1)[k]);
    if(bk->isTag())
    {
      for(unsigned int l = 0; l <b2->size(); ++l)
      {
        bl = &((*b2)[l]);
        if(bl->isTag())
        {
          double dist2 = getDist2(*bk,*bl,xt,yt,0);
          double dist = sqrt(dist2);
          Debug(if (dist2<1E-12)
              std::cout << " Distance petite " << dist2 << " " << indB1 << " " << indB2 << std::endl;)
          
          if (dist < distInter)
          {

            potential += getLJP2(dist2);

          }else if(dist < distInter + deltaDistInter)
          {
            potential += smoothSupport(dist)*getLJP(dist);
          }
        }
      }
    }
  }
                              
  return potential;
}


/**
 *
 * @param posi cell i index (cell = bucket)
 * @param posj cell j index
 *
 * Compute the forces of particles within a cell.
 *
 */
void Interaction::computeCellForce(int posi, int posj)
{
  int indB = posi + nx*posj;
  double distInter2 = distInter*distInter;  
  double distInterPDelta2=distInter+deltaDistInter;
  distInterPDelta2*=distInterPDelta2;
  vector <coorTag> *b;
  b = &(bucket[indB]);
  coorTag *bk, *bl; 
  vector <coor> *f;
  f = &(force[indB]);
  coor *fl, *fk;
  int sizeb =  b->size();
  for(int k = 0; k< sizeb; ++k)
  { 
    (*f)[k].set(0.,0.);
  }
  for(int k = 0; k< sizeb; ++k)
  { 
    bk = &((*b)[k]);
    fk = &((*f)[k]);
    if(bk->isTag())
    {
      for(int l = k+1; l < sizeb; ++l)
      {
        bl = &((*b)[l]);
        fl = &((*f)[l]);
        double dist2 = getDist2(*bk,*bl,0,0,0);
        double forceTemp, forcex, forcey;
        if(bl->isTag() && dist2 < distInter2)
        {
          forceTemp = getLJF2(dist2);
          forcex = ((*bk)(0)-(*bl)(0))*forceTemp;
          forcey = ((*bk)(1)-(*bl)(1))*forceTemp;

          fk->pe(forcex,forcey);
          fl->me(forcex,forcey);
          
        }else if(bl->isTag() && dist2 < distInterPDelta2)
        {
          double dist = sqrt(dist2);
          forceTemp = smoothSupport(dist)*getLJF(dist)/dist; // warning attention, réutiliser smoothSupport fait que la force ne correspond pas au potentiel (il faudrait prendre le gradient...)
          forcex = ((*bk)(0)-(*bl)(0))*forceTemp;
          forcey = ((*bk)(1)-(*bl)(1))*forceTemp;
          
          fk->pe(forcex,forcey);
          fl->me(forcex,forcey);
        }
      }
    }
  }
}

/**
 *
 * @param posi1 cell 1 i index
 * @param posj1 cell 1 j index
 * @param posi2 cell 2 i index
 * @param posj2 cell 2 j index
 *
 * Compute the forces between the particles of the cells 1 and 2.
 *
 */
void Interaction::computeInterCellForce(int posi1, int posj1, int posi2, int posj2)
{

  double forceTemp, forcex, forcey;
  double distInter2=distInter*distInter;
  double distInterPDelta2=distInter+deltaDistInter;
  distInterPDelta2 *= distInterPDelta2;
  int i2 = posi2, j2 = posj2;
  int i1 = posi1, j1 = posj1;
  int xt = 0, yt = 0;
  
  
  // Periodic boundary conditions
  if (posi2>=nx)
  {
    i2 = posi2-nx;
    xt = -1;
  }else if(posi2<0)
  {
    i2 = nx+posi2;
    xt = +1;
  }
  
  if (posj2>=ny)
  {
    j2 = posj2-ny;
    yt = -1;
  }else if(posj2<0)
  {
    j2 = posj2+ny;
    yt = +1;
  } 
  
  int indB1 = i1 + nx*j1;
  int indB2 = i2 + nx*j2;
  vector <coorTag> *b1, *b2;
  b1 = &(bucket[indB1]);
  b2 = &(bucket[indB2]);
  vector <coor> *f1, *f2;
  f1 = &(force[indB1]);
  f2 = &(force[indB2]);
  coor *f2l, *f1k;
  coorTag *b1k, *b2l;
  
  
  for(unsigned int k = 0; k< b1->size(); ++k)
  { 
    b1k = &((*b1)[k]);
    f1k = &((*f1)[k]);
    if(b1k->isTag())
    {
      for(unsigned int l = 0; l <b2->size(); ++l)
      {
        b2l = &((*b2)[l]);
        f2l = &((*f2)[l]);
        double dist2 = getDist2((*b1k),(*b2l),xt,yt,0);
        if(dist2 < distInter2 && b2l->isTag())
        {
          forceTemp = getLJF2(dist2);
          forcex = (xt+(*b1k)(0)-(*b2l)(0))*forceTemp;
          forcey = (yt+(*b1k)(1)-(*b2l)(1))*forceTemp;
          f1k->pe(forcex,forcey);
          f2l->me(forcex,forcey);
        }else if(dist2 < distInterPDelta2 && b2l->isTag() )
        {
          double dist = sqrt(dist2);
          forceTemp = smoothSupport(dist)*getLJF(dist)/dist; // warning smoothSupport is the same as in potential instead of its gradient
          forcex = (xt+(*b1k)(0)-(*b2l)(0))*forceTemp;
          forcey = (yt+(*b1k)(1)-(*b2l)(1))*forceTemp;
          f1k->pe(forcex,forcey);
          f2l->me(forcex,forcey);
        }
      }
    }
  }
}


/**
 *
 * @param a input value
 * @return sign of \a a
 */
int sign(double a)
{
    if (a>0)
        return 1;
    if (a<0)
        return -1;
    if (a==0)
        return 0;
    return -1000; // will never happen
}



/**
 *
 * @param i1 bucket 1 i index
 * @param j1 bucket 1 j index
 * @param k1 particle index in bucket 1
 * @param i2 bucket 2 i index
 * @param j2 bucket 2 j index
 *
 * Move the \a k1^th particle from bucket 1 to bucket 2.
 *
 */
void Interaction::exchangeBucket(int i1, int j1, int k1, int i2, int j2)
{
  int indB1 = i1 +j1*nx;
  int indB2 = i2 +j2*nx;
  bucket[indB2].push_back(bucket[indB1][k1]);
  force[indB2].push_back(force[indB1][k1]);
  bucket[indB2].back().setTag(1);
  force[indB2].back().setTag(1);
  ++npAll;
  //~ addPackedVertex(bucket[indB1][k1]); // do not work properly
  delVertex(i1, j1, k1);
}

void Interaction::resetSaturate()
{
  saturate = 0;
  normal = 0;
}

void Interaction::printSaturate()
{
  cout<<"Saturation "<< double(saturate)/(normal+saturate)*1000<<"‰"<<endl;
}

/**
 *
 * Exchange quadrants of cells to check periodic boundary conditions.
 *
 */
void Interaction::checkPosition()
{
  vector <coorTag> *b;
  int indB;
  coorTag *bk;
  for (int j = 0; j<ny; ++j)
    for(int i = 0; i<nx; ++i) 
    {
      indB = i+nx*j;
      b = &(bucket[indB]);
      for(unsigned int k = 0; k<b->size(); ++k)
      {
        bk = &((*b)[k]);
        if ((*bk)(0) < 0.5 && (*bk)(1) < 0.5)
          bk->pe(0.5,0.5);
        else if ((*bk)(0) >= 0.5 && (*bk)(1) >= 0.5) 
          bk->me(0.5,0.5);
        else if ((*bk)(0) < 0.5 && (*bk)(1) >= 0.5) 
          bk->pe(0.5,-0.5);
        else if ((*bk)(0) >= 0.5 && (*bk)(1) < 0.5) 
          bk->pe(-0.5,0.5);
      }
    }
}


/**
 *
 * Print settings for information or debug purposes.
 *
 */
void Interaction::printMembers()
{
  cout << "sigma : " << sigma << endl; 
  cout << "eps : " << eps << endl; 
  cout << "distInter : " << distInter << endl; 
  cout << "deltaDistInter : " << deltaDistInter << endl; 
  cout << "order : " << order << endl; 
  cout << "nx : " << nx << endl; 
  cout << "ny : " << ny << endl; 
  cout << "npAll : " << npAll << endl; 
  cout << "nbDell not printed" << endl; 
}

/**
 *
 * Move the vertices using the forces applied.
 *
 */
void Interaction::moveVertices()
{
  unsigned int sinSize = 10*sqrt(npAll);
  // can gain time if stored values are used instead of recomputing (not used...)
  if (sinus.size()!=sinSize || sinus[100] == 0)
  {
    double x=0, dx;
    dx = 1./(sinus.size()+1);
    for (unsigned int i = 0; i < sinus.size(); i++)
    {
      //~ cout << i << endl;
      x += dx;
      sinus[i] = sin(2*pi*x);
    }
  }
  computeForce();
  vector <coorTag> *b;
  vector <coor> *f;
  coor *fk;
  coorTag *bk;
  int indB, posi, posj;
  //loops on all the buckets
  for (int j = 0; j<ny; ++j)
  {
    for(int i = 0; i<nx; ++i)
    {  
      indB = i +j*nx;
      b = &(bucket[indB]);
      f = &(force[indB]);
      //loop on the particles of the bucket
      for(unsigned int k = 0; k<b->size(); ++k)
      {
        bk = &((*b)[k]);
        fk = &((*f)[k]);
        if(bk->getTag()>0)
        {
          double f0 = (*fk)(0);
          double f1 = (*fk)(1);
          coor crand;
          crand.randg(generator,sigmaBro); // warning parameter chosen arbitrarily 
          
          
          double x1 = (*bk)(0);
          int sinusSize = sinus.size();
          double dx = 1./(sinusSize+1);
          int n1 =  ((int) ((x1*freqSin+0.25)/dx))%sinusSize;

          double sinValue = (1+sin(2*pi*x1*freqSin+pi/2))/2.;
          //~ double sinValue = (1+sinus[n1])/2.; //does not seem to be much faster
         
          
          double v;
          if (fabs(aniConst*aniSin - 1)>1.e-6)
            v = aniConst*(1.-aniSin*sinValue);
          else
            v = 1;
          
          double n = sqrt((f0*f0+f1*f1));
          double saturationLevel = 1./6;

          double aniConst2 = (aniConst +(1.-aniConst)*(1-fabs(f0)/(saturationLevel*sigma)));
          double aniSin2 = aniSin*(fabs(f0)/(saturationLevel*sigma));
          v = aniConst2*(1.-aniSin2*sinValue);
          double c = (f0*f0+f1*f1);
          if (std::isnan(n))
          {
            (*bk)(0)+=crand(0);
            (*bk)(1)+=crand(1);
            saturate++;
            continue;
          }else if (n>saturationLevel*sigma)
          {
            c = saturationLevel*sigma;
            c *= 2*c;
            if (v>0)
            {
              //~ f0 = v*f0/n*saturationLevel*sigma;
              //~ f1 = f1/n*saturationLevel*sigma;
              f0 = v*sign(f0)*saturationLevel*sigma;
              f1 = sign(f1)*saturationLevel*sigma;
              
            }else if (v<0)
            {
              //~ f0 = 0;
              //~ f1 = sign(f1)*saturationLevel*sigma;
              f0 = 0;
              f1 = sign(f1)*saturationLevel*sigma;
            }
            
            saturate++;
            bk->pe(crand(0),crand(1));
            //~ //f1/n*saturationLevel*sigma;
            
            
            //~ (*bk)(0)+=v*crand(0);
            //~ (*bk)(1)+=crand(1);
            //~ saturate++;
            //~ continue;
          }else
          {
            f0 = v*f0;
            
            normal++;
          }
          c = sqrt(c/(f0*f0+f1*f1));
          //~ double sinValue2 = (1+sin(2*pi*(x1+f0)*freqSin+pi/2))/2.;
          //~ cout << "avant " << v << endl;
          
          //~ cout << "apres " << v << endl;
          //~ double c = 1.;
          //~ if (aniSin>1.e-6 || fabs(aniConst-1)>1.e-6)
          bk->pe(c*f0,c*f1);
          //~ else
            //~ bk->pe(f0,f1);

          bk->mod1();
          double xDistTemp, yDistTemp;
          xDistTemp = c*f0+crand(0);
          //~ yDistTemp = f1*(2-v)+crand(1);
          yDistTemp = c*f1+crand(1);
          xDist += xDistTemp*xDistTemp;
          yDist += yDistTemp*yDistTemp;
          posi = max(min(int((*bk)(0)*nx),nx-1),0);          
          posj = max(min(int((*bk)(1)*ny),ny-1),0);

          if (!(posi == i && posj == j))
            exchangeBucket(i,j,k, posi,posj);
        }
      }
    }
  }
}

/**
 *
 * print ratio of distance in x and y direction to check anisotropy.
 *
 */
void Interaction::printxyDist()
{
  cout << "Distance ratio (x/y) " << sqrt(xDist/yDist) << endl;
}

/**
 *
 * @return total x distance of particles movements
 *
 */
double Interaction::getxDist()
{
  return sqrt(xDist);
}

/**
 *
 * @return total y distance of particles movements
 *
 */
double Interaction::getyDist()
{
  return sqrt(yDist);
}

/**
 *
 * Set particles movement distances to 0.
 *
 */
void Interaction::resetxyDist()
{
  xDist = 0;
  yDist = 0;
}


/**
 *
 * @param r distance from center
 * @return value of smoothed characteristic function around the maximum interaction distance
 *
 * Make a smooth transition of the characteristic function for the interaction distance.
 *
 */
double Interaction::smoothSupport(const double& r)
{
  double a, b, dr;
  dr = (r-distInter)/deltaDistInter;
  b = dr*dr;
  a = 2*b*dr;
  b = -3*b;

  Debug
  (
    if(a+b+1<0 || a+b+1>1 )
    {
      std::cout << "Error : smoothSupport wrong value " << a+b+1 << std::endl;
    }
  )
  return a+b+1;
}


/**
 *
 * @param x value to multiply
 * @param d power
 * @return x^d
 *
 * Compute power function for integer values.
 *
 */
double effPow(double x, int d) // twice as fast as pow for d = 6 but could be better using addition-chain exponentiation
{
  if (d == 1)
    return x;
  else if (d%2 == 0)
  {
    double r = effPow(x,d/2);
    return r*r;
  }
  else
    return effPow(x,d-1)*x;
}

/**
 *
 * @param x value to multiply 6 times
 * @return x^6
 *
 * Compute power 6 function efficiently.
 *
 */
double effPow6(double x) // addition-chain exponentiation for d = 6 = 4+2
{
  double x2;
  x2 = x*x;
  return x2*x2*x2;
}

/**
 *
 * @param r distance between particles
 * @return Lennard-Jones potential between these particles
 *
 */
double Interaction::getLJP(const double& r)
{
    double sr = sigma/r;
    if (order == 6)
      sr = effPow6(sr);
    else if (order == (int) (order))
      sr = effPow(sr, (int) (order));
    else
      sr = pow(sr, order);
    return eps*(sr*sr-sr);
}

/**
 *
 * @param r squared distance between particles
 * @return Lennard-Jones potential between these particles
 *
 */
double Interaction::getLJP2(const double& r2)
{

    double s = sigma*sigma;
    double sr = s/r2;
    if (order == 6)
      sr = sr*sr*sr;
    else if (order/2 == (int) (order/2))
      sr = effPow(sr, (int) (order/2));
    else
      sr = pow(sr, order/2);
    return eps*(sr*sr-sr);
}


/**
 *
 * @param r distance between particles
 * @return norm of Lennard-Jones force between these particles divided by the distance
 *
 */
double Interaction::getLJF(const double& r)
{
    double sr = sigma/r;
    if (order == 6)
      sr = effPow6(sr);
    else if (order == (int) (order))
      sr = effPow(sr, (int) (order));
    else
      sr = pow(sr, order);
    
    double srr = order*sr/r;  
    return eps*(2*sr*srr);
}

/**
*
* @param r squared distance between particles
* @return norm of Lennard-Jones force between these particles divided by the distance
*
*/
double Interaction::getLJF2(const double& r2)
{
    double s = sigma*sigma;
    double sr = s/r2;
    
    if (order == 6)
      sr = sr*sr*sr;
    else if (order/2 == (int) (order/2))
      sr = effPow(sr, (int) (order/2));
    else
      sr = pow(sr, order/2);
    double srr = order*sr/sqrt(r2);  
    return eps*(2*sr*srr);
}

/** Not used
*
* @param r distance between particles
* @return norm of Lennard-Jones force between these particles divided by the distance
*
*/
coor Interaction::getLJF(const coor& p1, const coor& p2)
{
  double r = getDist(p1,p2);
  double sr = sigma/r;
  if (order == (int) (order))
    sr = effPow(sr, (int) (order));
  else
    sr = pow(sr, order);
  double srr = order*sr/r;  
  double normForce = eps*(2*sr*srr-srr);
  coor forceTemp(p1);
  forceTemp -= p2;
  forceTemp *= normForce/r;
  
  return forceTemp;
}
