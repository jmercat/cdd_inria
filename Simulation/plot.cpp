#include "plot.h"
#include "math.h"

using namespace std;

// Open postscript, set values and define basic postscript functions
postscript::postscript(string name)
{
    // Initializing variables
    
    box_width =  595-2*margin;
    double hmargin = (842 - box_width)/2;
    double xmin = margin;
    double ymin = hmargin;
    double xmax = 595-hmargin;
    double ymax = hmargin + box_width;
    point0[0] = xmin;
    point0[1] = ymin;
    
    
    color[0]=0;
    color[1]=0;
    color[2]=0;
    
    width = 1;
    filename = name;
    if (filename.find(".ps")==string::npos)
      filename += ".ps";
    
    // Opening postscript file
    if( !file.is_open()) 
    {
      file.open(const_cast <char*> (filename.c_str()),ios::out | ios::trunc);
      if (!file.is_open())
        cout << "  ** UNABLE TO OPEN " << filename << endl;
    }
    
    // Initializing postscript header
    file<< "%%!PS-Adobe-3.0 EPSF-3.0"<<endl;
    file<< "  %%%%Creator: MMG2D postscript writer"<<endl;
    file<< "  %%%%Title: " << filename << endl;
    file<< "  %%%%DocumentMedia: A4 595 842 0 () ()"<<endl;
    file<< "  %%%%Orientation: Portrait"<<endl;
    file<< "  %%%%Pages: 1"<<endl;
    file<< "  %%%%BoundingBox: "<<xmin<<" "<<ymin<<" "<<xmax<<" "<<ymax<<endl;
    file<< "  %%%%LanguageLevel: 3"<<endl;
    file<< "  %%%%EndComments"<<endl;
    file<< "  %%%%BeginProlog"<<endl;
    file<< "    /line{newpath moveto lineto stroke} bind def"<<endl; // draw line
    file<< "    /ms{moveto show} bind def"<<endl;
    file<< "    /triangle{newpath moveto lineto lineto closepath stroke} bind def"<<endl;
    file<< "    /circle{moveto 0 360  arc stroke} bind def"<<endl;
    file<< "    /point{ newpath moveto 0 360 arc closepath fill stroke} bind def"<<endl;
    file<< "    /filltriangle{newpath moveto lineto lineto closepath fill stroke} bind def"<<endl;
    file<< "    /outputtext { \n      /data exch def \n      /rot exch def \n      /xfont exch def \n      /y1 exch def \n      /x1 exch def \n      /Helvetica findfont \n      xfont scalefont \n      setfont \n      x1 y1 moveto \n      rot rotate \n      data show \n      rot neg rotate \n    } def"<<endl;
    file<< "  %%%%EndProlog\n"<<endl;
    
}

// write final command and close postscript
postscript::~postscript()
{
    file << "showpage" << endl;
    file << "%%%%EOF" << endl;
    file.close();
}

// draw a filled circle of radius r pts in position of the given point of mesh using current color (in ps structure)
void postscript::drawPoint(coor c, double r)
{
    double x = c(0)*box_width+point0[0];
    double y = c(1)*box_width+point0[1];
    file << x << " " << y << " " << r*box_width << " " << x << " " << y  << " point" << endl;  
}


// draw a line in postscript file openned in ps structure
// between 2 points of the mesh using current color and width (in ps structure)
void postscript::drawLine(coor pt1, coor pt2)
{
    double x1 = pt1(0)*box_width+point0[0];
    double y1 = pt1(1)*box_width+point0[1];
    double x2 = pt2(0)*box_width+point0[0];
    double y2 = pt2(1)*box_width+point0[1];
    file << x1 << " " << y1 << " " << x2 << " " << y2 << " line" << endl;  
}

// set linewidth for next drawing instruction
void postscript::setLineWidth(double width)
{
    if( width != this->width)
    {
        this->width = width;
        file << width  << " setlinewidth"<<endl;
    }
}

// set hue color for next drawing instruction (h in [0,1], 0 red 1/3 green 2/3 blue)
// hue is periodic so h can be > 1
void postscript::setHueColor(double h)
{
    h = fmod(h,1.);
    if (color[0] != h || color[1] != 1. ||  color[2] != 1)
    {
        color[0] = h;
        color[1] = 1.;
        color[2] = 1;
        
        file << h << " " << color[1] << " " << color[2] << " sethsbcolor" << endl;
    }
} 

// set hue, saturation, brightness color for next drawing instruction (h s and b in [0,1], 0 red 1/3 green 2/3 blue)
// hue is cinlindrical so h can be > 1
void postscript::setHsbColor(double h, double s, double b)
{
    if (color[0] != h || 
        color[1] != s ||
        color[2] != b)
    {
        h = fmod(h,1.);
        
        color[0] = h;
        color[1] = s;
        color[2] = b;
        
        file << h << " " << s << " " << b << " sethsbcolor"<<endl;
    }
}


// set gray color for next drawing instruction (g in [0,1], 0 black 1 white)
// hue is periodic so h can be > 1
void postscript::setGrayColor(double g)
{
    if (color[0] != g || color[1] != g ||  color[2] != g)
    {
        
        color[0] = g;
        color[1] = g;
        color[2] = g;
        
        file << g << " setgray" << endl;
    }
} 

void postscript::drawSquare(const coor& pt, double l)
{
    double x1 = pt(0)*box_width+point0[0];
    double y1 = pt(1)*box_width+point0[1];
    double l1 = l*box_width;
    file << x1 << " " << y1 << " " << l1 << " " << l1 << " rectstroke" << endl;
}
