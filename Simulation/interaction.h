#ifndef INTERACTION_H
#define INTERACTION_H

#include "Domain.h"
#ifndef pi
#define pi 3.14159265359
#endif

// la classe interaction est un domaine muni d'une loi d'interaction : héritage
class Interaction : public Domain
{
  public :
    Interaction(double sigma_i = 0, double eps_i = 0, double distInter_i = 1,double aniconst = 1. , double freqsin = 0., double anisin = 0.);
    Interaction(int nx, int ny, double sigma_i = 0, double eps_i = 0, double distInter_i = 1,double aniconst = 1. , double freqsin = 0., double anisin = 0.);
    Interaction(const Interaction& d);
    ~Interaction();
    
    Interaction& operator=(Interaction d);
    
    void inputRandNode(int n);
    void inputNode( std::string name,double eps_i); //BUGBUG3: there is still a problem if the size are not consistent
    void addVertex(const coorTag& c, const coor& force);
    void addVertex(const coorTag& c);
    void addPackedVertex(const coorTag& vertex);
    void delVertex(int i, int j, int k);
    void setEps(double epsi);
    void setSigmaBro(double sigmaBroi);
    void setAniConst(double aniConst);
    void setAniSin(double aniSin);
    void checkPosition();
  
    double getPotential();
    double getPotentialBF();
    
    double getLJF(const double& r);

    
    
    void moveVertices();
    void printMembers();
  
    void printxyDist();
    double getxDist();
    double getyDist();
    void resetxyDist();
    void resetSaturate();
    void printSaturate();
  
  private :  
    double getCellPotential(int posi, int posj);
    double getInterCellPotential(int posi1, int posj1, int posi2, int posj2);
    double getLJP(const double& r);
    double getLJP2(const double& r2);
    double getLJF2(const double& r2);
    coor getLJF(const coor& p1, const coor& p2);
    double smoothSupport(const double& r);
    void safeAllocMotion(); // there might be a better way to shadow the Domain safeAlloc so the write function is automatically called...
    void packVerticesPop(int posi, int posj);
    void exchangeBucket(int i1, int j1, int k1, int i2, int j2);
    
    void computeForce();
    void computeInterCellForce(int posi1, int posj1, int posi2, int posj2);
    void computeCellForce(int posi, int posj);
    
    std::vector < coor >* force;
    double sigmaBro;
    double sigma;
    double eps;
    double distInter;
    double deltaDistInter;
    std::vector<double> sinus;
    static const int order = 6;
    double xDist, yDist;
    double freqSin;
    double aniConst;
    double aniSin;
    long int saturate, normal;
    
    
};

double effPow(double x, int d);

#endif
