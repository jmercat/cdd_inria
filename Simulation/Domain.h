#ifndef DOMAIN_H
#define DOMAIN_H 

#include <math.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <cstring>
#include <fstream>
#include <random>
#include <time.h>


#if defined(unix)        || defined(__unix)      || defined(__unix__) \
 || defined(linux)       || defined(__linux)     || defined(__linux__) \
 || defined(sun)         || defined(__sun) \
 || defined(BSD)         || defined(__OpenBSD__) || defined(__NetBSD__) \
 || defined(__FreeBSD__) || defined (__DragonFly__) \
 || defined(sgi)         || defined(__sgi) \
 || defined(__MACOSX__)  || defined(__APPLE__) \
 || defined(__CYGWIN__)
#include <sys/time.h>
#elif defined(_MSC_VER) || defined(WIN32)  || defined(_WIN32) || defined(__WIN32__) \
   || defined(WIN64)    || defined(_WIN64) || defined(__WIN64__)
#include "sys/time.h"
#endif



#ifndef pi
#define pi 3.14159265359
#endif


/**
 * Loop over the buckets
 */
#define Bucket_for(dom,i,j)\
 for (int j = 0; j<dom.getNy(); ++j)\
 for(int i = 0; i<dom.getNx(); ++i)
/**
 * Loop over the cells with tag 1
 */
#define Cell_for(dom,i,j,k)\
  for(int k = 0; k<dom.getNp(i,j); ++k)\
  if(dom(i,j,k).isTag())
/**
 * Loop over the whole domain (every cell with tag 1 of every bucket)
 */
#define Domain_for(dom,i,j,k)\
 Bucket_for(dom,i,j)\
 Cell_for(dom,i,j,k)

class coor
{
  public:
    coor();
    coor(double[2]);
    coor(double x, double y);
    coor(const coor&);
    coor& operator=(const coor&);
    coor& operator+=(const coor&);
    coor& operator-=(const coor&);
    coor& operator*=(const double& r);
    double& operator()(int i);
    double operator()(int i) const;
    void pe(const double& x, const double& y);
    void me(const double& x, const double& y);
    void set(const double& x, const double& y);
    void mod1();
    void randg( std::default_random_engine& generator, double sigma, double mu=0);
    void rande( std::default_random_engine& generator);
    friend double getDist(const coor& c1, const coor& c2,double x0=0, double y0=0, double z=0);
    friend double getDist2(const coor& c1, const coor& c2,double x0=0, double y0=0, double z=0);
    void setTag(const char t);

  protected:
    double c[2];    

};

class coorTag : public coor
{
    public:
    coorTag();
    coorTag(double[2]);
    coorTag(double x, double y);
    coorTag(const coorTag&);
    ~coorTag();
    coorTag& operator=(const coorTag&);
    int getTag() const;
    void setTag(const char t);
    bool isTag() const;

  protected:
    char tag;
};

inline bool coorTag::isTag() const
{
  return tag>0;
}

inline double& coor::operator()(int i)
{
  return c[i];
}

inline double coor::operator()(int i) const
{
  return c[i];
}

inline void coor::set(const double& x, const double& y)
{
  c[0] = x;
  c[1] = y;
}
/**
 * 
 * \param x value to be added to x coordinate. 
 * \param y value to be added to y coordinate. 
 * 
 * Performs plus equal operation without definition of a coor to add.
 * 
 */
inline void coor::pe(const double& x, const double& y)
{
  c[0]+=x;
  c[1]+=y;
}

/**
 * 
 * \param x value to be substracted to x coordinate. 
 * \param y value to be substracted to y coordinate. 
 * 
 * Performs minus equal operation without definition of a coor to add.
 * 
 */
inline void coor::me(const double& x, const double& y)
{
  c[0]-=x;
  c[1]-=y;
}

/**
 *
 * Performs modulo 1 on each coordinate, usefull for periodic conditions.  
 * 
 */
inline void coor::mod1()
{
  c[0] -= floor(c[0]);
  c[1] -= floor(c[1]);
}

inline int coorTag::getTag() const
{
  return tag;
}


/**
 * \param c1 first vertex.
 * \param c2 second vertex.
 * \param x0 offset on x coordinate.
 * \param y0 offset on y coordinate.
 * \param z artificial height difference between vertices.
 * 
 * Returns the distance between two vertices allowing offsets for 
 * periodic conditions and height for curved surfaces.
 * 
 */
inline double getDist(const coor& c1, const coor& c2, double x0, double y0, double z)
{
  double x,y;
  x = (x0+c1.c[0]-c2.c[0]);
  y = (y0+c1.c[1]-c2.c[1]);
  //~ x = (x0+c1.c[0]-c2.c[0])*0.9;
  //~ y = (y0+c1.c[1]-c2.c[1])*1.1;
  return sqrt(x*x+y*y+z*z);
}

/**
 * \param c1 first vertex.
 * \param c2 second vertex.
 * \param x0 offset on x coordinate.
 * \param y0 offset on y coordinate.
 * \param z artificial height difference between vertices.
 * 
 * Returns the squared distance between two vertices, allowing offsets
 * for periodic conditions and height for curved surfaces.
 * 
 */
inline double getDist2(const coor& c1, const coor& c2, double x0, double y0, double z)
{
  double x,y;
  x = (x0+c1.c[0]-c2.c[0]);
  y = (y0+c1.c[1]-c2.c[1]);
  //~ x = (x0+c1.c[0]-c2.c[0])*0.9*0.9;
  //~ y = (y0+c1.c[1]-c2.c[1])*1.1*1.1;
  return x*x+y*y+z*z;
}

class Domain
{
  public:
    Domain();
    Domain(int nx, int ny);
    Domain(const Domain& d);
    ~Domain();

    void outputNode( std::string name);
    void inputNode( std::string name);

    Domain& operator=(const Domain& d);
    std::vector<coorTag>& operator()(int i);
    std::vector<coorTag>& operator()(int i, int j);
    coorTag& operator()(int i, int j, int k);
    
    void addVertex(const coorTag& c);
    void addPackedVertex(const coorTag& vertex);
    void delVertex(int i, int j, int k);

    int getNx() const;
    int getNy() const;
    int getNp(int i) const;
    int getNp(int i, int j) const;
    int getNpAll() const;
  protected:
    int nx, ny;
    int npAll;
    std::vector < coorTag >* bucket;
    int* nbDel;
    static constexpr double pDelMax = 0.3;
    bool isBucket(int posi, int posj);    
    //~ void packVertices(int i, int j);
    void packVerticesPop(int i, int j);    
    void safeAlloc();
    int nextNegTag(int indB, int ind) const;
    struct timeval tm;
    std::default_random_engine generator;

    
};

#endif
