#Crysa

## Synopsis

Crysa, is an open source multi-platform software that automatize
2D CRYtal Structural Analysis from an image of a large number of objects
placed in an hexagonal array. It has been designed for atomic force microscope (AFM) images of block copolymer and can be applied to any such image of objects in a 2D hexagonal array.


## Motivation

This software meets the need of physicochemists for automatic analysis in the observations of block-copolymer 2D ordering. It has also been a useful tool for the comparisons between observations and simulations.

The image processing part uses the CImg library, the analysis part uses mmg Tools and the graphic interface uses Qt and QCustomPlot.It has been developed under Linux and adapted to compile with Microsoft Visual Studio.

## Compiling and/or Installation

This is a source version with no installation support.

### Linux 

Both QtCreator with .pro file and CMake should compile the code and allow its execution.

The CMakeLists.txt is in folder Troisieme programme. You should compile with the following commands:
$mkdir Troisieme\ programme/build
$cd Troisieme\ programme/build
$cmake ..
$make

The .pro file is in folder Troisieme\ programme/interface

### Windows

Both QtCreator with .pro file and Visual Studio with .rc file should compile the code and allow its execution.

If you wish to install Crysa without compiling, you may use the assistant in Troisieme\ programme/Installer_Crystal_analysis.exe. This should walk you through installation.



Since this software was not made for a long term maintenance, most dependencies are contained and hard included. Thus the compilation is quiet long.


## API Reference

This project uses Qt, libqcustomplot, mmgtools, png, zlib and CImg. Except for Qt, all dependencies are included and compiled with the version it was made for.

## Tests

Sadly there was a poor testing policy (none at all!) during the writting of this code. Thus there is no other test than running the whole program and trying it with different input images. 

## Contributors

This program is the result of a one-year research-engineering project financed by Idex(excellence initiative of Bordeaux university) in the CPU community. It has been hosted by Inria, and received the help of researchers from the laboratories in Bordeaux: LCPO, Inria, ISM and IMB.
It was entirely written by Jean Mercat under the supervision of Cécile Dobrzynski and with the scientific concelling of Karim Aissou, Luca Muccioli and Gérard Vignoles.

## License

This program is licensed under the LGPL License.
