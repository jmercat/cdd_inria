Main Page                         {#mainpage}
============
    
This is the documentation of the 2D crystal analysis program made in 
INRIA and financed by the University of Bordeaux through the CPU program.

##Purpose:

    The program performs an analysis of a hexagonal crystal structure. 
    It can make graphical outputs with postscript and other outputs as 
    correlation functions for plots. It is intended for easing and 
    speeding the process of 2D crystal analysis.
    
##Inputs:
    
    The inputs may be compressed images, AFM images in Nanoscope format
    or custom format .node text file.
    
    It reads coordinates of particles in .node file organized as
    follow:
        first line: <int number of particles> 2 0 0
        other lines: <int index> <double x coordinate> <double y coordinate> <double surface>
    
    The .node files can be generated from a picture or an AFM image with 
    the program.
  
##How to use it:

    Launch the graphical interface and drop the file(s) to be computed 
    in it or search for a file and click on the compute button. 
    Settings are made in the two first tabs of the interface and results
    can be seen in the other tabs. They are also saved in a folder 
    placed next to the input file. 
      
      
Documentation about the functions are in .cpp files and not header files.
