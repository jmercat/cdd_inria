
#include <vector>
#include <math.h>
#include <iostream>
#include <fstream>
#define pi 3.1415926

using namespace std;

double* gradVar(vector<double>& y,double a, double b, double c)
{
  double* res = new double[3];
  double sinTemp,sinTemp2;
  double cosTemp;
  double a2 = 2*a;
  double a22 = 2*a*a;
  double a22b = a22*b;
  double a2b = a2*b;
  for(int i = 0; i<y.size(); i++)
  {
    sinTemp = sin(b*i+c);
    cosTemp = cos(b*i+c);
    sinTemp2 = sinTemp*sinTemp;
    res[0] += a2*sinTemp2-2*sinTemp*y[i];
    res[1] += -a22b*sinTemp*cosTemp+a2b*cosTemp*y[i];
    res[2] += a22*sinTemp*cosTemp- a2*cosTemp*y[i];
  }
  
  return res;
}

double* sinFit(vector<double> y)
{
  double* grad = new double[3];
  double* res = new double [4];
  double a,b,c;
  double eps = 1.E-2;
  double epsp;
  double ym=0;
  double yp,ypp,yppp;
  int n0 = 0;
  int count;
  double aTemp;
  double err,errp,errTemp,errMin;
  int inter=0,inter0=0;
  a=1;
  b=1;
  c=0;
  
  for(int i=0; i<y.size(); i++)
  {
    ym += y[i];
   
  }
  ym /= y.size();
  for(int i=0; i<y.size(); i++)
  {
    y[i] -= ym; 
    if (yp*y[i]< 0 && yp*ypp>0 && ypp*yppp>0)
    {
      n0++;
      if(!inter0)
      {
        //~ c = -i;
        inter0 = i;
      }
      if (inter0)
      {
        inter += i-inter0;
        inter0=i;
      }
    }
    yppp = ypp;
    ypp = yp;
    yp = y[i];
    //~ a = max(a,y[i]);
  }
  inter /= n0;
  b = (pi/inter +double((n0-1)*pi)/(y.size()))/2; // mean of two ways of computing the value
  //~ b = double((n0-1)*pi)/(y.size());
  
  count =0;
  n0 = 0;
  //~ c = 0;
  for(int i=0; i<y.size(); i++)
  {
    //~ if (yp*y[i]< 0 && yp*ypp>0 && ypp*yppp>0)
    //~ {
      //~ c -= (i-n0*inter);
      //~ n0++;
    //~ }
    if ((i+1)%(2*inter)==0)
    {
      count++;
      a+= aTemp;
      aTemp = y[i];
    }
    aTemp = max(aTemp,y[i]);
    
    //~ yppp = ypp;
    //~ ypp = yp;
    //~ yp = y[i];
  }
  
  a /= count;
  
  c = 0;
  err = 0;
  for(int i=0; i<y.size(); i++)
  {
    errTemp = y[i] - a*sin(b*i+c);
    err += errTemp*errTemp;
  }
  errMin = err;
  for ( int j = 0; j< 2*inter/eps; j++)
  {
    c += eps;
    err = 0;
    for(int i=0; i<y.size(); i++)
    {
      errTemp = y[i] - a*sin(b*i+c);
      err += errTemp*errTemp;
    }
    if ( err < errMin)
    {
      errMin = err;
      n0 = j;
    }
  }
  c = n0*eps;
  
  err = 0;
  for(int i=0; i<y.size(); i++)
  {
    errTemp = y[i] - a*sin(b*i+c);
    err += errTemp*errTemp;
  }
  
  cout << err/(a*a*y.size()) << endl;
  a = (a/ym<0.001 || err/(a*a*y.size())>1.) ? 0:a; 
  
  res[0] = a;
  res[1] = b;
  res[2] = c;
  res[3] = ym;
  return res;
}

int main (int argc, char **argv)
{
  double* res;
  double ym;
  vector<double> y;
  double ytemp;
  double trash;
  if (argc > 1)
  {
    ifstream fichier(argv[1]);
    while (true)
    {
      fichier >> trash >> ytemp;
      y.push_back(ytemp);
      if( fichier.eof() ) break;
      //~ cerr << ytemp << endl;
    }
    
    res = sinFit(y);
    cout<< "Sin fit : f(x) = " << res[0] << "*sin(" << res[1] << "*x+" << res[2] << ")+"<< res[3] << endl;
    delete[] res;
  }
  return 0;
}
