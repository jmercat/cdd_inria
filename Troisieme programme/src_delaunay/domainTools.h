#ifndef DOMAINTOOLS_H
#define DOMAINTOOLS_H

#include <vector>
#include "mmg/mmg2d/libmmg2d.h"



bool isBorder(MMG5_pMesh mesh, int npt, double h, double ratioXoY);
double meanSpacing(MMG5_pMesh mesh, double ratioXoY);
double getArea(MMG5_pMesh mesh, const std::vector < std::vector <int> > adj, const std::vector <int>& listPt);
double getArea2(MMG5_pMesh mesh, const std::vector <int>& listPt);
void nearestNeighborDist(MMG5_pMesh mesh, const std::vector <std::vector <int> >& adj, std::vector <double>& NND);
void neighborDist(MMG5_pMesh mesh, const std::vector <std::vector <int> >& adj, std::vector <double>& ND);
void getPointType(MMG5_pMesh mesh, const std::vector<std::vector<int> >& adj, std::vector<int>& pointType , const double& lMean, int& ndefect, int& nPointCenter, double ratioXoY);

#endif
