#ifndef EDGE_H
#define EDGE_H

#include <vector>
#include <math.h>
#include "mmg/mmg2d/libmmg2d.h"

typedef struct edge
{
  double x,y;
  double r;
  double theta;
} edge;

void getEdgesDelaunay(MMG5_pMesh mesh, const std::vector< std::vector<int> >& adj, std::vector <edge>& listedge);
void getEdgesVoronoi(MMG5_pMesh mesh,const std::vector< std::vector<int> >& adj, std::vector <edge>& listedge);
void edgeToVec(std::vector<double>& vec,const std::vector< std::vector<double> >& edge_data);


#endif
