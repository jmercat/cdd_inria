/**
 * \file Edge.cpp
 * \brief Functions related to Edges.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */


#include "Edge.h"


using namespace std;


/**
 * \param mesh mmg mesh structure
 * \param adj adjacent table of the nodes
 * \param listedge list of edges
 * 
 * Fill the list of edges with edges of the Delaunay triangulation of
 * the nodes.
 * 
 */
void getEdgesDelaunay(MMG5_pMesh mesh, const vector< vector<int> >& adj, vector <edge>& listedge)
{
  edge a0;
  double x0, y0, x1, y1;
  unsigned int adjis;
  for (unsigned int i = 1; i < adj.size(); i++)
  {
    adjis = adj[i].size();
    for (unsigned int ai = 0; ai < adjis; ai++)
    {
      if (i<(unsigned int) (adj[i][ai]))
      {
      x0 = mesh->point[i].c[0];
      y0 = mesh->point[i].c[1];
      x1 = mesh->point[adj[i][ai]].c[0];
      y1 = mesh->point[adj[i][ai]].c[1]; 
      
      
      a0.theta = atan2(y1-y0,x1-x0);
      a0.x = (x0+x1)/2;
      a0.y = (y0+y1)/2;
      x0 = x1-x0;
      y0 = y1-y0;
      a0.r = sqrt(x0*x0+y0*y0);
    
      listedge.push_back(a0);
      }
    }
  }
}

/**
 * \param mesh mmg mesh structure
 * \param adj adjacent table of the nodes
 * \param listedge list of edges
 * 
 * Fill the list of edges with edges of the Voronoï diagram of
 * the nodes.
 * 
 */
void getEdgesVoronoi(MMG5_pMesh mesh,const vector< vector<int> >& adj, vector <edge>& listedge)
{
  edge a0;
  double x0, y0, x1, y1, x2, y2, x3, y3;
  unsigned int adjis;
  for (unsigned int i = 1; i < adj.size(); i++)
  {
    adjis = adj[i].size();
    for (unsigned int ai = 0; ai < adjis; ai++)
    {
      if (i<(unsigned int) (adj[i][ai]))
      {
      x0 = mesh->point[i].c[0];
      y0 = mesh->point[i].c[1];
      x1 = mesh->point[adj[i][ai]].c[0];
      y1 = mesh->point[adj[i][ai]].c[1]; 
      x2 = mesh->point[adj[i][(ai-1+adjis)%adjis]].c[0];
      y2 = mesh->point[adj[i][(ai-1+adjis)%adjis]].c[1];
      x3 = mesh->point[adj[i][(ai+1)%adjis]].c[0];
      y3 = mesh->point[adj[i][(ai+1)%adjis]].c[1];
      
      
      a0.theta = atan2(y3-y2,x3-x2);
      a0.x = (2*x0+2*x1+x2+x3)/6;
      a0.y = (2*y0+2*y1+y2+y3)/6;

      x0 = x3-x2;
      y0 = y3-y2;
      a0.r = sqrt(x0*x0+y0*y0)/3;
    
      listedge.push_back(a0);
      }
    }
  }
}

/**
 * \param vec vector to be filled with data from \a edgeData
 * \param edgeData values known on the positions of the edge
 * 
 * Compute the mean value of data known on edges around each node to store
 * it as data known on nodes.
 * 
 */
void edgeToVec(vector<double>& vec,const vector< vector<double> >& edgeData)
{ 
  int s;
  int n = edgeData.size();
  vec.resize(n,0);
  for(int i = 0; i< n; i++)
  {
    s = edgeData[i].size();
    for(int j = 0; j < s; j++)
    {
      vec[i] += edgeData[i][j];
    }
    vec[i] /= s;
  }
}
