/**
 * \file adj.cpp
 * \brief Functions for building and ordering the adj structure that
 * links every node to its adjacent neighbors going counter-clockwise.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * \a adj[i] is a vector of variable size containing indexes of the
 * adjacent nodes of the node indexed i. \a adj[i][0] is the index of a
 * random adjacent node. \a adj[i][1] is adjacent to nodes of index i and
 * \a adj[i][0]. It is the first adjacent node we encounter going counter-
 * clockwise and starting from adj[i][0].
 *        
 *        adj[i][0]____ adj[i][5]
 *                /    \
 *      adj[i][1]/  _i  \ adj[i][4]
 *               \      /
 *      adj[i][2] \ ___/adj[i][3]
 *   
 */
 
#include <stdio.h>

#include "adj.h"
#include "Dtree.h"
#include "vectorTools.h"


using namespace std;


/**
 * \param mesh mmg structure used here for linking vertex index to its coordinates.
 * \param adj in/out adjacency structure to be reordered. 
 * 
 * order vertex index in adj so that adj[i][j] and adj[i][j+1] are 
 * adjacent and in counter-clockwise rotation around i.
 * Terribly poor efficiency, could be improved.
 *  
 */
// Terribly poor efficiency, order points in adj so that adj[i][j] and adj[i][j+1] are adjacent and in counter-clockwise rotation around i
void orderAdj(MMG5_pMesh mesh, vector< vector<int> >& adj)
{
  for (unsigned int i=0; i<adj.size(); i++)
  {        
    for (unsigned int j = 1; j<adj[i].size(); j++)
    {
      for (unsigned int k = j; k<adj[i].size(); k++)
      {
        if(isInVect(adj[adj[i][j-1]],adj[i][k]))
        {
          int temp = adj[i][j];
          adj[i][j] = adj[i][k];
          adj[i][k] = temp;
          break;
        }
      }
    }
    
    if (adj[i].size()>1)
    {
      // Direct rotation
      double x, y, x1, y1, x2, y2;
      x = mesh->point[i].c[0];
      y = mesh->point[i].c[1];
      x1 = mesh->point[adj[i][0]].c[0]-x;
      y1 = mesh->point[adj[i][0]].c[1]-y;
      x2 = mesh->point[adj[i][1]].c[0]-x;
      y2 = mesh->point[adj[i][1]].c[1]-y;
      if(x1*y2-y1*x2<0)
        reverseVect(adj[i]);
    }
  }
  
}

/**
 * \param mesh mmg structure used here for linking vertex index to its coordinates.
 * \param adj out, adjacence structure to be formed. 
 * 
 * Forms the adjacence structure linking one node to all other nodes 
 * connected to it with the triangulation.
 * For i in [1,nbVertex], \a adj[i].size() is the number of neighbors and
 * adj[i][j] is one of its neighbors
 * Very poor efficiency, could be improved.
 *  
 */
void getAdj(MMG5_pMesh mesh, vector< vector<int> >& adj)
{
  int npt;
  
  if (adj.size() != (unsigned int) (mesh->np+1))
  {
    cout << "Vector adj not initialized properly " << adj.size() << "    " << mesh->np+1 << endl;
    return;
  }
  
  for(int i = 1; i<= mesh->nt; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      npt = mesh->tria[i].v[j];
      insert(adj[npt],mesh->tria[i].v[(j+1)%3]);
      insert(adj[npt],mesh->tria[i].v[(j+2)%3]);
    }
  }
  orderAdj(mesh, adj);
}
