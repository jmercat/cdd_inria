#ifndef MMG2D_PLOTTOPS_H
#define MMG2D_PLOTTOPS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "mmg/mmg2d/libmmg2d.h"

// structure allowing postscript writing
typedef struct MMG2D_postscript
{
    FILE* file;
    char filename[512]; // name of output file
    double color[3]; // current color
    double width; // current line width
    double box_width; // size of the square box in wich the mesh is drawn
    double point0[2]; // coordinates of the bottom left corner of the box
} MMG2D_postscript;

void getRGB( double fH, double fS, double fV, double* fR, double* fG, double* fB);
void MMG2D_openPostscript(MMG2D_postscript* ps, char * name);
void MMG2D_closePostscript(MMG2D_postscript* ps);
void MMG2D_sethsbColor(MMG2D_postscript* ps, double hsb[]);
void MMG2D_sethueColor(MMG2D_postscript* ps, double h);
void MMG2D_setgrayColor(MMG2D_postscript* ps, double g);
void MMG2D_setdarknessColor(MMG2D_postscript* ps, double b);
void MMG2D_setLineWidth(MMG2D_postscript* ps, double width);
void MMG2D_setLineWidthRelative(MMG2D_postscript* ps, double width);
void MMG2D_drawTriangle(MMG5_pMesh mesh,  MMG2D_postscript* ps, int num_tria);
void MMG2D_drawText(double x, double y, double size, int r, char* text, MMG2D_postscript* ps);
void MMG2D_drawCircle(MMG5_pMesh mesh, MMG2D_postscript* ps, int num_pt, double r);
void MMG2D_drawPoint(MMG5_pMesh mesh, MMG2D_postscript* ps, int num_pt, double r);
void MMG2D_drawLine(MMG5_pMesh mesh, MMG2D_postscript* ps, int num_pt1, int num_pt2);
void MMG2D_drawScale(double x, double y, double width, double height, double scale[], char* title, MMG2D_postscript* ps);
void MMG2D_plotMesh(MMG5_pMesh mesh, char* name);
void MMG2D_plotMeshColor(MMG5_pMesh mesh, MMG5_pSol sol, char* name);

#endif
