#ifndef ADJ_H
#define ADJ_H

#include <vector>
#include "mmg/mmg2d/libmmg2d.h"


void orderAdj(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj);
void getAdj(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj);

#endif
