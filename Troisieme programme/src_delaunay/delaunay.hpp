#ifndef H_DELAUNAY
#define H_DELAUNAY

#include <vector>
#include "DataSol.h"
#include "mmg/mmg2d/libmmg2d.h"
#include "../src_exchange/exchange.h"

void triangleToPt(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj, std::vector<double>& triangleData, std::vector <double>& pointData);
void analyseMesh(MMG5_pMesh mmgMesh, MMG5_pSol mmgSol, DataSol& sol, std::vector <double> cylindersArea, int argc, char **argv, double& lMean, int& nDefects,delaunayExchange& dataOut);
int mainDelaunay(int argc, char **argv, allExchange &dataOut);

#endif
