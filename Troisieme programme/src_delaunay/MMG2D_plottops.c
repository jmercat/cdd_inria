/**
 * \file MMG2D_plottops.c
 * \brief PostScript file plotting functions.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */
 

#include "MMG2D_plottops.h"


/**
 * \param fH hue input
 * \param fS saturation input
 * \param fV value input
 * \param fR red output
 * \param fG green output
 * \param fB blue output
 * 
 * Convert hue, saturation, value (HSV) input into RGB.
 * 
 */
// Code from a forum post to translate HSV into RGB (used by MMG2D functions)
void getRGB( double fH, double fS, double fV, double* fR, double* fG, double* fB)
{
  double fC = fV * fS; // Chroma
  double fHPrime = fmod(fH*6, 6);
  double fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
  double fM = fV - fC;
  
  if(0 <= fHPrime && fHPrime < 1) {
    *fR = fC;
    *fG = fX;
    *fB = 0;
  } else if(1 <= fHPrime && fHPrime < 2) {
    *fR = fX;
    *fG = fC;
    *fB = 0;
  } else if(2 <= fHPrime && fHPrime < 3) {
    *fR = 0;
    *fG = fC;
    *fB = fX;
  } else if(3 <= fHPrime && fHPrime < 4) {
    *fR = 0;
    *fG = fX;
    *fB = fC;
  } else if(4 <= fHPrime && fHPrime < 5) {
    *fR = fX;
    *fG = 0;
    *fB = fC;
  } else if(5 <= fHPrime && fHPrime < 6) {
    *fR = fC;
    *fG = 0;
    *fB = fX;
  } else {
    *fR = 0;
    *fG = 0;
    *fB = 0;
  }
  
  *fR += fM;
  *fG += fM;
  *fB += fM;
}


/**
 * \param ps PostScript structure to set
 * \param name name of the postscript file to output
 * 
 * Create an empty postscript file and set postscript structure for
 * later inputs in the file.
 * 
 */
// Open postscript, set values and define basic postscript functions
void MMG2D_openPostscript(MMG2D_postscript* ps, char * name)
{
    // Initializing variables
    double margin = 10;
    
    ps->box_width =  595-2*margin;
    double hmargin = (842 - ps->box_width)/2;
    double xmin = margin;
    double ymin = hmargin;
    double xmax = 595-hmargin;
    double ymax = hmargin + ps->box_width;
    ps-> point0[0] = xmin;
    ps-> point0[1] = ymin;
    
    
    ps->color[0]=0;
    ps->color[1]=0;
    ps->color[2]=0;
    
    ps->width = 1;
    
    char *ptr = NULL, filename_ext[512];
    strcpy(filename_ext,name);
    ptr = strstr(filename_ext,".ps");
    if ( !ptr )
        strcat(filename_ext,".ps");
    strcpy(ps->filename,filename_ext);
    // Opening postscript file
    if( !(ps->file = fopen(filename_ext,"wb")) ) 
    {
      fprintf(stderr,"  ** UNABLE TO OPEN %s.\n",filename_ext);
      return;
    }
    fprintf(stdout,"  %%%% %s OPENED\n",filename_ext);
    
    // Initializing postscript header
    fprintf(ps->file, "%%!PS-Adobe-3.0 EPSF-3.0\n");
    fprintf(ps->file, "  %%%%Creator: MMG2D postscript writer\n");
    fprintf(ps->file, "  %%%%Title: %s\n",ps->filename);
    fprintf(ps->file, "  %%%%DocumentMedia: A4 595 842 0 () ()\n");
    fprintf(ps->file, "  %%%%Orientation: Portrait\n");
    fprintf(ps->file, "  %%%%Pages: 1\n");
    fprintf(ps->file, "  %%%%BoundingBox:%.4g %.4g %.4g %.4g\n",xmin,ymin,xmax,ymax);
    fprintf(ps->file, "  %%%%LanguageLevel: 3\n");
    fprintf(ps->file, "  %%%%EndComments\n");
    fprintf(ps->file, "  %%%%BeginProlog\n");
    fprintf(ps->file, "    /line{newpath moveto lineto stroke} bind def\n"); // draw line
    fprintf(ps->file, "    /ms{moveto show} bind def\n");
    fprintf(ps->file, "    /triangle{newpath moveto lineto lineto closepath stroke} bind def\n");
    fprintf(ps->file, "    /circle{moveto 0 360  arc stroke} bind def\n");
    fprintf(ps->file, "    /point{ newpath moveto 0 360 arc closepath fill stroke} bind def\n");
    fprintf(ps->file, "    /filltriangle{newpath moveto lineto lineto closepath fill stroke} bind def\n");
    fprintf(ps->file, "    /outputtext { \n      /data exch def \n      /rot exch def \n      /xfont exch def \n      /y1 exch def \n      /x1 exch def \n      /Helvetica findfont \n      xfont scalefont \n      setfont \n      x1 y1 moveto \n      rot rotate \n      data show \n      rot neg rotate \n    } def\n");
    fprintf(ps->file, "  %%%%EndProlog\n\n");
}


/**
 * \param ps postscript structure
 * 
 * Close the postscript file.
 * 
 */
// write final command and close postscript
void MMG2D_closePostscript(MMG2D_postscript* ps)
{
    fprintf(ps->file, "showpage\n");
    fprintf(ps->file, "%%%%EOF\n");
    fclose(ps->file);
}


/**
 * \param ps postscript structure
 * \param hsb list of HSB (or HSV) values
 * 
 * Set the color using HSB values for later drawings.
 * h, s, and b are in [0,1]
 * hue values are as follow : 0 red 1/3 green 2/3 blue
 * hue is cinlindrical so h can be > 1
 * 
 */
void MMG2D_sethsbColor(MMG2D_postscript* ps, double hsb[])
{
    if (ps->color[0] != hsb[0] || 
        ps->color[1] != hsb[1] ||
        ps->color[2] != hsb[2])
    {
        hsb[0] = fmod(hsb[0],1.);
        
        ps->color[0] = hsb[0];
        ps->color[1] = hsb[1];
        ps->color[2] = hsb[2];
        
        fprintf(ps->file, "%.6g %.6g %.6g sethsbcolor\n", hsb[0], hsb[1], hsb[2]);
    }
}

/**
 * \param ps postscript structure
 * \param h hue value
 * 
 * Set the color using HSB values for later drawings with saturation 1
 * and value 0.8.
 * hue values are as follow : 0 red 1/3 green 2/3 blue
 * hue is cinlindrical so h can be > 1
 * 
 */
 void MMG2D_sethueColor(MMG2D_postscript* ps, double h)
{
    h = fmod(h,1.);
    if (ps->color[0] != h || ps->color[1] != 1. ||  ps->color[2] != 0.8)
    {
        
        ps->color[0] = h;
        ps->color[1] = 1.;
        ps->color[2] = 0.8;
        
        fprintf(ps->file, "%.6g %.6g %.6g sethsbcolor\n", h, ps->color[1], ps->color[2]);
    }
} 

/**
 * \param ps postscript structure
 * \param g gray value 0 black, 1 white
 * 
 * Set the color to a gray shade
 */
void MMG2D_setgrayColor(MMG2D_postscript* ps, double g)
{
    if (ps->color[0] != g || ps->color[1] != g ||  ps->color[2] != g)
    {
        
        ps->color[0] = g;
        ps->color[1] = g;
        ps->color[2] = g;
        
        fprintf(ps->file, "%.6g setgray\n", g);
    }
} 

/**
 * \param ps postscript structure
 * \param b darkness value for the current color
 * 
 * Set darkness for next drawing instruction (b in [0,1])
 * 
 */
void MMG2D_setdarknessColor(MMG2D_postscript* ps, double b)
{
    if ( ps->color[2] != b)
    {
        ps->color[2] = b;
        
        fprintf(ps->file, "%.6g %.6g %.6g sethsbcolor\n", ps->color[0], ps->color[1], b);
    }
} 

/**
 * \param ps postscript structure
 * \param width line width to be used for later drawings
 * 
 *  Set linewidth for later drawing instructions
 * 
 */
void MMG2D_setLineWidth(MMG2D_postscript* ps, double width)
{
    if( width != ps->width)
    {
        ps->width = width;
        fprintf(ps->file, "%.6g setlinewidth\n", width);
    }
}

/**
 * \param ps postscript structure
 * \param width line width to be used for later drawings
 *
 *  Set linewidth for later drawing instructions
 *
 */
void MMG2D_setLineWidthRelative(MMG2D_postscript* ps, double width)
{
    if( width*ps->box_width != ps->width)
    {
        ps->width = width*ps->box_width;
        fprintf(ps->file, "%.6g setlinewidth\n", width*ps->box_width);
    }
}

/**
 * \param mesh mmg mesh structure
 * \param ps postscript structure
 * \param num_tria index of the triangle to draw
 * 
 * Draw a triangle from the mesh in the postscript file.
 * 
 */
void MMG2D_drawTriangle(MMG5_pMesh mesh,  MMG2D_postscript* ps, int num_tria)
{
    MMG2D_sethsbColor(ps, ps->color);
    int num_pt1 = mesh->tria[num_tria].v[0];
    int num_pt2 = mesh->tria[num_tria].v[1];
    int num_pt3 = mesh->tria[num_tria].v[2];
    double x1 = mesh->point[num_pt1].c[0]*ps->box_width+ps->point0[0];
    double y1 = mesh->point[num_pt1].c[1]*ps->box_width+ps->point0[1];
    double x2 = mesh->point[num_pt2].c[0]*ps->box_width+ps->point0[0];
    double y2 = mesh->point[num_pt2].c[1]*ps->box_width+ps->point0[1];
    double x3 = mesh->point[num_pt3].c[0]*ps->box_width+ps->point0[0];
    double y3 = mesh->point[num_pt3].c[1]*ps->box_width+ps->point0[1];
    fprintf(ps->file, "%.6g %.6g %.6g %.6g %.6g %.6g triangle\n", x1, y1, x2, y2, x3, y3);  
}

/**
 * \param x horizontal position (in box width unit)
 * \param y vertical position (in box width unit)
 * \param size point size of the text
 * \param r angle of the text
 * \param text text to write
 * \param ps postscript structure
 * 
 * Draw text in the postscript file at given position with given size 
 * and rotation. Uses current color.
 * 
 */
 void MMG2D_drawText(double x, double y, double size, int r, char* text, MMG2D_postscript* ps)
{
    int w = ps->box_width;
    x = x*w + ps->point0[0];
    y = y*w + ps->point0[1];
    fprintf(ps->file, "%i %i %3.2f %i (%s) outputtext\n", (int) x, (int) y, size,  r, text);
}

/**
 * \param mesh mmg mesh structure
 * \param ps postscript structure
 * \param num_pt index of the node in the mesh
 * \param r radius of the circle (unit in pt)
 * 
 * Draw a circle around a node of the mesh structure.
 * 
 */
// draw circle of radius r pts in position of the given point of mesh using current color and width (in ps structure)
void MMG2D_drawCircle(MMG5_pMesh mesh, MMG2D_postscript* ps, int num_pt, double r)
{
    MMG2D_sethsbColor(ps, ps->color);
    double x = mesh->point[num_pt].c[0]*ps->box_width+ps->point0[0];
    double y = mesh->point[num_pt].c[1]*ps->box_width+ps->point0[1];
    fprintf(ps->file, "%.6g %.6g %.6g %.6g %.6g circle\n", x, y, r*ps->box_width, x+r*ps->box_width, y);
}


/**
 * \param mesh mmg mesh structure
 * \param ps postscript structure*
 * \param num_pt index of the node in the mesh
 * \param r radius of the circle
 * 
 * Draw a filled circle of radius \a r pt in position of the given point 
 * of mesh using current color in \a ps structure
 * 
 */
void MMG2D_drawPoint(MMG5_pMesh mesh, MMG2D_postscript* ps, int num_pt, double r)
{
    MMG2D_sethsbColor(ps, ps->color);
    double x = mesh->point[num_pt].c[0]*ps->box_width+ps->point0[0];
    double y = mesh->point[num_pt].c[1]*ps->box_width+ps->point0[1];
    fprintf(ps->file, "%.6g %.6g %.6g %.6g %.6g point\n", x, y, r*ps->box_width, x, y);  
}

/**
 * \param mesh mmg mesh structure
 * \param ps postscript structure*
 * \param num_pt1 index of a node in the mesh
 * \param num_pt1 index of a node in the mesh
 * 
 * Draw a line in postscript file openned in ps structure
 * between 2 points of the mesh using current color and width (in ps structure)
 * 
 */
void MMG2D_drawLine(MMG5_pMesh mesh, MMG2D_postscript* ps, int num_pt1, int num_pt2)
{
    MMG2D_sethsbColor(ps, ps->color);
    double x1 = mesh->point[num_pt1].c[0]*ps->box_width+ps->point0[0];
    double y1 = mesh->point[num_pt1].c[1]*ps->box_width+ps->point0[1];
    double x2 = mesh->point[num_pt2].c[0]*ps->box_width+ps->point0[0];
    double y2 = mesh->point[num_pt2].c[1]*ps->box_width+ps->point0[1];
    fprintf(ps->file, "%.6g %.6g %.6g %.6g line\n", x1, y1, x2, y2);  
}

/**
 * \param x horizontal position (in box width unit)
 * \param y vertical position (in box width unit)
 * \param width with of the rectangle
 * \param height height of the rectangle
 * \param scale values on the scale
 * \param title title of the scale
 * \param ps postscript structure
 * 
 * Draw a scale bar with values corresponding to the colors.
 * 
 */
void MMG2D_drawScale(double x, double y, double width, double height, double scale[], char* title, MMG2D_postscript* ps)
{
    int horizontal = width>height;
    int w = ps->box_width;
    fprintf(ps->file, "gsave\n");
    if (horizontal)
    {
        fprintf(ps->file, "/scaleh {%.6g mul} def\n", w*width/6);
        fprintf(ps->file, "/scalew {%.6g mul} def\n", w*height);
    }else
    {
        fprintf(ps->file, "/scaleh {%.6g mul} def\n", w*height/6);
        fprintf(ps->file, "/scalew {%.6g mul} def\n", w*width);
    }
    
    int rot = 0;
    x = x*w + ps->point0[0];
    y = y*w + ps->point0[1];
    if (horizontal)
    {
        double xtemp = x;
        x = y;
        y = -xtemp;
        rot = 90;
    }
    
    fprintf(ps->file, "/sx %i def\n/sy %i def\n/sr %i def\n", (int) x, (int) y, rot);
    
    if (horizontal)
       fprintf(ps->file, "6 scaleh 0 translate\n 90 rotate\n");
     
    fprintf(ps->file, "<<\n /ShadingType 4\n /ColorSpace [/DeviceRGB]\n /DataSource [ 0 0 scalew sx add 0 scaleh sy add 1 0 0 0 1 scalew sx add 0 scaleh sy add 1 0 0 0 1 scalew sx add 1 scaleh sy add 1 0 1 2 0 scalew sx add 1 scaleh sy add 1 0 1 1 1 scalew sx add 2 scaleh sy add 0 0 1 1 0 scalew sx add 2 scaleh sy add 0 0 1 1 1 scalew sx add 3 scaleh sy add 0 1 1 1 0 scalew sx add 3 scaleh sy add 0 1 1 1 1 scalew sx add 4 scaleh sy add 0 1 0 1 0 scalew sx add 4 scaleh sy add 0 1 0 1 1 scalew sx add 5 scaleh sy add 1 1 0 1 0 scalew sx add 5 scaleh sy add 1 1 0 1 1 scalew sx add 6 scaleh sy add 1 0 0 1 0 scalew sx add 6 scaleh sy add 1 0 0 ]\n"); 
    fprintf(ps->file, ">>\nshfill\n");
    
    fprintf(ps->file, "0.1 setgray\n");
    if (!horizontal)
    {
        for (int i = 0; i < 7; i++)
            fprintf(ps->file, "1.2 scalew sx add %i 0.1 neg add scaleh sy add %i 0 (%1.2g) outputtext\n", i, (int) (height*w/20), scale[6-i]);
        fprintf(ps->file, "0 scalew sx add 6.3 scaleh sy add %i 0 (%s) outputtext\n", (int) (height*w/12), title);
    }else
    {
        for (int i = 0; i < 7; i++)
            fprintf(ps->file, "-0.6 scalew sx add %i scaleh sy add %i -135 (%1.2g) outputtext\n", i, (int) (width*w/23), scale[6-i]);
        
        fprintf(ps->file, "1.2 scalew sx add 5 scaleh sy add %i -90 (%s) outputtext\n", (int) (width*w/12), title);
    }
    
    fprintf(ps->file, "grestore\n");
    
    
}


/**
 * \param mesh mmg mesh structure
 * \param name name of the file to create
 * 
 * Plot the mesh in postscript. Only black and white wire. Vertices
 * coordinates must be in [0,1].
 * 
 */
void MMG2D_plotMesh(MMG5_pMesh mesh, char* name)
{
    //Declare postscript
    MMG2D_postscript ps;
    //Open postscript file
    MMG2D_openPostscript(&ps,name);
    double color[3];
    color[0] = 0;
    color[1] = 0;
    color[2] = 0;
    MMG2D_sethsbColor(&ps, color);
    MMG2D_setLineWidth(&ps, 0.000001);
    for (int i = 1; i<= mesh->nt; i++)
    {
        MMG2D_drawTriangle(mesh, &ps, i);
    }
    MMG2D_closePostscript(&ps);
}


// Fonctionne mais redéfinit deux fois chaque triangle (une pour la couleur l'autre pour les arretes)
// et n'utilise pas le edge flag de la structure Free-Form Gouraud-Shaded Triangle Meshes
// on pourrait donc diviser la taille due au shading (la coloration) par 6 en utilisant mieux la structure
// Je ne connais pas le moyen de réutiliser la structure du shading pour tracer les triangles
// on les redéfinit donc une deuxième fois... (en somme on peut probablement diviser la taille du fichier par 20)

/**
 * \param mesh mmg mesh structure
 * \param sol mmg sol structure
 * \param name name of the file to create
 * 
 * Plot mesh with colors depending on sol values. Values should be in [0,1].
 * 
 */
void MMG2D_plotMeshColor(MMG5_pMesh mesh, MMG5_pSol sol, char* name)
{
    FILE* datafile; 
    // Opening data file
    if( !(datafile = fopen("data","w")) ) 
    {
      fprintf(stderr,"  ** UNABLE TO OPEN %s.\n","data");
      return;
    }
    //Declare postscript
    MMG2D_postscript ps;
    //Open postscript file
    MMG2D_openPostscript(&ps,name);
    double color[3];
    
    // define shading in post-script file
    fprintf(ps.file, "/DeviceRGB setcolorspace\ngsave\n<<\n /ShadingType 4\n");
    fprintf(ps.file, "/ColorSpace [/DeviceRGB]\n /BitsPerCoordinate 12\n /BitsPerComponent 8\n /BitsPerFlag 8\n /Decode [10 585 133.5 708.5 0 1 0 1 0 1]\n /DataSource \n <");  
   
    int num_pt; // numéro global du point du triangle
    int flag = 0; // flag de triangle (toujours 0 : on redéfinit les 3 points de chaque triangle)
    double x,y; // positions
    double r, g, b; // couleurs
    int bufferP[2]; // buffer de position
    int bufferC[3]; // buffer de couleur

    // write triangles and color in shading data (there is a mysterious problem
    // I couldn't get it to work with BitsPerFlag set to 2
    // (if set to 4 it needs two hex character except for the first flag...)
    for (int i = 1; i<= mesh->nt; i++)
    {
        for (int j = 0; j<3; j++)
        {
            num_pt = mesh->tria[i].v[j];
            getRGB(sol->m[num_pt],1.,1.,&r,&g,&b);
            x = mesh->point[num_pt].c[0];
            y = mesh->point[num_pt].c[1];
            bufferC[0] = flag; // flag = 0 on 8 bits : 2 hex character
            fprintf(ps.file, "%02x", bufferC[0]);
            bufferP[0] = x*4095; // coordinates on 12 bits : 0 to 4095 on 3 hex character
            bufferP[1] = y*4095;
            fprintf(ps.file, "%03x%03x", bufferP[0], bufferP[1]);
            bufferC[0] = r*255; // color on 8 bits : 0 to 255 on 2 hex character
            bufferC[1] = g*255;
            bufferC[2] = b*255;
            fprintf(ps.file, "%02x%02x%02x",bufferC[0], bufferC[1], bufferC[2]);
        }
    }
    // close shading dictionary and print
    fprintf(ps.file,">\n>>\nshfill\ngrestore\n");
    
    color[0] = 0;
    color[1] = 0;
    color[2] = 0;
    MMG2D_sethsbColor(&ps, color);    // set color to 0
    MMG2D_setLineWidth(&ps, 0.0000001); 
   
    for (int i = 1; i<= mesh->nt; i++) // draw each triangle
    {
        MMG2D_drawTriangle(mesh, &ps, i); 
    }
    double scale[7];
    scale[6] = sol->m[0];
    for (int i = 0; i<6; i++)
        scale[i] = scale[6]*((float) i)/6;
    
    char nameoutTemp[512];
    strcpy(nameoutTemp, sol->nameout);    
    MMG2D_drawScale(0.1, -0.11, 0.8, 0.05, scale, nameoutTemp, &ps);

    
    fprintf(ps.file, "showpage\n");
    fprintf(ps.file, "%%%%EOF\n");
    

    // closing
    fclose(ps.file);
    fclose(datafile);
}
