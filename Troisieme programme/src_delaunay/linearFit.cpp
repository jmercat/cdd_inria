/**
 * \file linearFit.cpp
 * \brief Linear ang linearised fit using least square.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */
 
#include <math.h>
#include <iostream>
#include <stdio.h>

#include "linearFit.h"

#define pi 3.1415926


using namespace std;

/**
 * \param x vector of x coordinates
 * \param y vector of y coordinates
 * \param isPow switch to 1 for linearized power law fit
 * \param isExp switch to 1 for linearized exponential law fit
 * 
 * Computes a least square fit of the data, adds a linearization using 
 * log on y or both x and y. 
 * 
 */
// linearFit with least square method using direct calculation and inversion of 2x2 matrix
double* linearFit(vector<double> x, vector<double> y, const bool isPow, const bool isExp)
{
  if ( x.size() != y.size())
  {
    cout << "linearFit : size incompatible" << endl;
    return NULL;
  }
    
  double mat[3], invMat[3], eqTo[2];

  //~ memset( mat, 0, sizeof(mat));
  //~ memset( mat, 0, sizeof(invMat));
  //~ memset( mat, 0, sizeof(eqTo));
  //~ 
  eqTo[0] = 0;
  eqTo[1] = 0;
  mat[0] = 0;
  mat[1] = 0;
  //~ mat[2] = x.size();
  mat[2] = 0;
  double fX, fY;
  for (int i = 0; i<x.size(); i++)
  {
    if ( (isPow && x[i] <= 0 || y[i] <= 0) || (isExp && y[i] <= 0))
    {
      //~ mat[2]-=exp(-x[i]);
    }else
    {
       if( isPow) // no need to check it every step but simpler to write
      {
        fX = log(x[i]);
        fY = log(y[i]);
        //~ mat[0] += fX*fX*(x[i]+1);
        //~ mat[1] += fX*(x[i]+1);
        //~ mat[2] += 1.*(x[i]+1);
        //~ eqTo[0] += fX*fY*(x[i]+1);
        //~ eqTo[1] += fY*(x[i]+1);
        mat[0] += fX*fX;
        mat[1] += fX;
        mat[2] += 1;
        eqTo[0] += fX*fY;
        eqTo[1] += fY;
      }else if (isExp)
      {
        fX = x[i];
        fY = log(y[i]); 
        mat[0] += exp(-x[i])*fX*fX;
        mat[1] += exp(-x[i])*fX;
        mat[2] += exp(-x[i]);
        eqTo[0] += exp(-x[i])*fX*fY;
        eqTo[1] += exp(-x[i])*fY;
        //~ mat[0] += fX*fX;
        //~ mat[1] += fX;
        //~ mat[2] += 1;
        //~ eqTo[0] += fX*fY;
        //~ eqTo[1] += fY;
      }else
      {
        fX = x[i];
        fY = y[i];
        mat[0] += fX*fX;
        mat[1] += fX;
        mat[2] += 1;
        eqTo[0] += fX*fY;
        eqTo[1] += fY;
      }
    }
  }
  
  double det;
  det = mat[0]*mat[2]-mat[1]*mat[1];
  
  if (det == 0)
  {
    cout << "linearFit : null determinant, cannot fit" << endl;
    return NULL;
  }
  
  invMat[0] = mat[2]/det;
  invMat[1] = -mat[1]/det;
  invMat[2] = mat[0]/det;

  double* res = new double[2];
  
  res[0] = eqTo[0]*invMat[0]+eqTo[1]*invMat[1];
  res[1] = eqTo[0]*invMat[1]+eqTo[1]*invMat[2];

  if (isPow || isExp)
  {
    res[1] = exp(res[1]);
    double resTemp = res[1];
    res[1] = res[0];
    res[0] = resTemp;
  }
  //~ if (isPow)
    //~ cout << res[1] << "*x^" << res[0] << endl;
  //~ if (isExp)
    //~ cout << res[1] << "*exp(" << res[0] << "*x)" << endl;
  //~ if (!isPow && !isExp)
    //~ cout << res[0] << "*x + " << res[1] << endl;
  return res;
}
