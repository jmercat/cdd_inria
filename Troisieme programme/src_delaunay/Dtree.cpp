/**
 * \file Dtree.cpp
 * \brief Functions associated with Dynamic tree structure Dtree.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * Dtree is a dynamic tree, meaning that every node contains a dynamic list of
 * the sub-Dtree called branch.
 * Every node of the tree contains a list of branch
 * that are also Dtrees, a node index, and a value. In the explanation below,
 * do not mix the node index wich is the index the user assigned to the node
 * and the branch index wich is the index of a branch in the list of branches.
 * 
 */
 
#include "Dtree.h"

using namespace std;

/**
 * Default constructor
*/
Dtree::Dtree() : node(-1), value(0)
{
  branch = new vector < Dtree* >;
}
/**
 * \param n index of the node
 * \param v value of the node 
*/
Dtree::Dtree(int n, double v) : node(n), value(v)
{
  branch = new vector < Dtree* >;
}

/**
 * \param br branch to be added to the tree 
 * 
*/
Dtree::Dtree(const Dtree& br)
{
  node = br.node;
  value = br.value;
  branch = new vector < Dtree* >;
  for(unsigned int i = 0; i<br.branch->size(); i++)
  {
    branch->push_back(new Dtree(*((*br.branch)[i])));
  }
}

Dtree::~Dtree()
{
  if ( branch->size() == 0)
  {
    delete branch;
  }else
  {
    for (unsigned int i = 0; i < branch->size(); i++)
    {
      delete (*branch)[i];
    }
    delete branch;
  }
}

/**
 * \param ind index of the branch to be accessed 
 * \return branch of index \a ind
 * Access the branch of index \a ind in the tree.
*/

Dtree& Dtree::getBranch(const int ind)
{
  //~ if(ind < branch->size())
  //~ {
    return *((*branch)[ind]);
  //~ }else
    //~ cout << "Indice inexistant" << endl;
  //~ return *this;
}


/**
 * \param v vector of the index leading to the branch to be accessed 
 * \return branch accessed following index in \a v
 * 
 * Access the branch located at the end of the path in the tree. The
 * path to the branch is described by the index of the branch to access
 * at each step.
*/
Dtree& Dtree::getBranchRec(vector <int>& v)
{
  if( v.size() == 0)
    return *this;
  else
  {
    int i = v.back();
    v.pop_back();
    return ((*branch)[i])->getBranchRec(v);
  }
}

/**
 * \param v vector of the index leading to the branch to be accessed in reverse order
 * \return branch accessed following index in \a v
 * 
 * Access the branch located at the end of the path in the tree. The
 * path to the branch is described by the index of the branch to be accessed
 * at each step.
*/
Dtree& Dtree::getBranch(vector <int>  v)
{
  reverseVect(v);
  return getBranchRec(v);
}

/**
 * 
 * Add a new branch to the tree 
 * 
*/
void Dtree::addBranch()
{
  branch->push_back(new Dtree);
}

/**
 * \param n index of the node to be added
 * \param v value of the node to be added
 * 
 * Add a new branch to the tree with node index and value
 * 
*/
void Dtree::addBranch(int n, double v)
{
  branch->push_back(new Dtree(n,v));
}


/**
 * \param br branch to be added to the tree 
 * 
 * Add the given branch to the tree
*/
void Dtree::addBranch(const Dtree& br)
{
  branch->push_back(new Dtree(br));
}

/**
 * \param br branch to be added to the tree
 * \param v vector of the index of branches that leads to a node in the tree
 * 
 * Add the given branch to the tree in the node described by v.
 * 
*/
void Dtree::addBranch(const Dtree& br, const vector <int>& v)
{
  this->getBranch(v).addBranch(br);
}

/**
 * \param n index of the new node
 * \param v value of the new node
 * \param vec vector describing the path leading to a node of the tree.
 * 
 * Add the node with given value to the tree in position described by v.
 * 
*/
void Dtree::addBranch(int n, double v, const vector <int>& vec)
{
  this->getBranch(vec).addBranch(n,v);
}

/**
 * \param v vector of the index leading to the node to be accessed 
 * \return node of the branch accessed following index in \a v
 * Access the node located at the end of the path in the tree. The
 * path to the branch is described by the index of the branch to access
 * at each step.
*/
int Dtree::getNodeRec(vector <int> v)
{
  if( v.size() == 0)
    return node;
  else
  {
    int i = v.back();
    v.pop_back();
    return ((*branch)[i])->getNodeRec(v);
  }
}

/**
 * \param n index of a branch
 * 
 * \return node of the branch of index \a n
 * 
*/
int Dtree::getNode(int n)
{
  return this->getBranch(n).node;
}

/**
 * \param v vector of the index leading to the node to be accessed 
 * \return node accessed following index of \a v
 * 
 * Access the node located at the end of the path in the tree. The
 * path to the branch is described by the index of the branch to access
 * at each step.
*/
int Dtree::getNode(vector <int>  v)
{
  reverseVect(v);
  return getNodeRec(v);
}

/**
 * \return size of the list of branch 
 * 
 * If returned value is 0, the node is a leaf. If not, it has that number
 * of children (branches).
*/
int Dtree::getSize()
{
  return branch->size();
}

/**
 * 
 * Print nodes values and index in shell
 * 
*/
void Dtree::printDtreeRec()
{
  if (branch->size() == 0)
    cout << "x ";
  for (unsigned int i = 0; i < branch->size(); i++)
  {
    cout << ((*branch)[i])->node << "|" << ((*branch)[i])->value << " ";
  }
  cout << endl;
  for (unsigned int i = 0; i < branch->size(); i++)
  {
     ((*branch)[i])->printDtreeRec();
  }
  
}

/**
 * 
 * Print nodes values and index in shell
 * 
*/
void Dtree::print()
{
  cout << node<< "|" << value << endl;
  printDtreeRec();
  cout << endl ;
}

/**
 * 
 * \param vec vector to be reversed
 * 
 * Reverse the values of the given vector so that the first value becomes the last.
 * 
*/
void reverseVect( vector<int>& vec)
{
  int temp, n = vec.size();
  for (int i = 0; i < n/2; i++)
  {
    temp = vec[i];
    vec[i] = vec[n-i-1];
    vec[n-i-1] = temp;
  }
}
