/**
 * \file radialDistrib.cpp
 * \brief Computation of pair correlation and orientationnal correlation functions.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <time.h> 
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "debug.h"
#include "radialDistrib.h"
#include "linearFit.h"
#include "Edge.h"
#define pi 3.1415926

using namespace std;


/**
 * \param mesh mmg mesh structure
 * \param pt index of the central node
 * \param r radius of the ring
 * \param dl with of the ring
 * 
 * Compute the area of the intersection of the domain ([0,1]x[0,1]) and a
 * circular ring of radius \a r and width \a dl centered on the node of
 * index \a pt.
 * 
 */
// area of the ring that is in the domain
double area(MMG5_pMesh mesh,int pt, double r, double dl)
{
  //~ double pi = atan(1.)*4;
  double x, y;
  x = mesh->point[pt].c[0];
  y = mesh->point[pt].c[1];
  
  double xx=0, yx=0;
  double dbx = 1 , dby = 1;
  
  if (x<r)
  {
    dbx = x;
    xx = 0;
    if (y > 0.5)
      yx = y - sqrt(r*r-dbx*dbx);
    else
      yx = y + sqrt(r*r-dbx*dbx);
  }else if (1-x<r)
  {
    dbx = 1-x;
    xx = 1;
    if (y > 0.5)
      yx = y - sqrt(r*r-dbx*dbx);
    else
      yx = y + sqrt(r*r-dbx*dbx);
  }
  
  double xy=0, yy=0;
  
  if (y<r)
  {
    dby = y;
    yy = 0;
    if (x > 0.5)
      xy = x - sqrt(r*r-dby*dby);
    else
      xy = x + sqrt(r*r-dby*dby);
  }else if (1-y<r)
  {
    dby = 1-y;
    yy = 1;
    if (x > 0.5)
      xy = x - sqrt(r*r-dby*dby);
    else
      xy = x + sqrt(r*r-dby*dby);
  }
  
  double x1, y1, x2, y2;
  double angle;
  if ((y<r || 1-y<r) && (x<r || 1-x<r)) // angles
  {
    x1 = xx;
    y1 = yx;
    x2 = xy;
    y2 = yy;
    if (x<r)
      angle = fabs(atan2(y1-y,x1-x)-atan2(y2-y,x2-x));
    else
      angle = 2*pi-fabs(atan2(y1-y,x1-x)-atan2(y2-y,x2-x));
  }else if (1-x<r || x<r) // left or right
  {
    x1 = xx;
    y1 = yx;
    x2 = xx;
    y2 = 2*y-yx;
    if (x<r)
      angle = fabs(atan2(y1-y,x1-x)-atan2(y2-y,x2-x));
    else
      angle = 2*pi-fabs(atan2(y1-y,x1-x)-atan2(y2-y,x2-x));
  }else if (1-y<r || y<r) // top or bottom
  {
    x1 = xy;
    y1 = yy;
    x2 = 2*x-xy;
    y2 = yy;
    
    angle = fabs(atan2(y1-y,x1-x)-atan2(y2-y,x2-x));
    if (angle<pi)
      angle = 2*pi - angle;
  }else
  {
    angle = 2*pi;
  }
  //~ if (angle*100./(2*pi) < 25 || angle*100./(2*pi) > 100)
    //~ cout << "Coord " << x << " " << y << " rayon " << r << " pourcentage " << angle*100./(2*pi)  << endl; 
  return angle*r*dl;
}


/**
 * \param mesh mmg mesh structure
 * \param i index of a node of the mesh
 * \param lmax distance
 * 
 * Check if the node of index \a i is further than \a lmax from the
 * domain boundaries.
 * 
 */
bool isCenter(MMG5_pMesh mesh, int i,double lmax)
{
  double x, y;
  x = mesh->point[i].c[0];
  y = mesh->point[i].c[1];
  return (x>lmax && x<1-lmax && y<1-lmax && y > lmax); 
}

/**
 * \param mesh mmg mesh structure
 * \param g pair correlation function values
 * \param g6 orientational correlation function values
 * \param adj adjacence table of the nodes
 * \param dim number of point values to use for the functions
 * \param r_max maximum distance at wich the functions are computed
 * 
 * Compute values of pair correlation and orientational correlation
 * functions.
 * 
 */
 void radialDistribution(MMG5_pMesh mesh, vector <double>& g, vector <double>& g6, const vector< vector <int> >& adj, int dim, double r_max)
{
  g.resize(dim,0);
  g6.resize(dim,0);
  double dl = r_max/(dim), r;
  int k;
  double x0, y0, x, y, x1, y1;
  MMG5_pPoint ppt = mesh->point;
  MMG5_Point pti,ptj;
  
  
  for (int i = 1; i <= mesh->np; i++)
  {
    pti = ppt[i];
    x0 = pti.c[0];
    y0 = pti.c[1];
    for (int j = i+1; j <= mesh->np; j++)
    {
      ptj = ppt[j];
      x = ptj.c[0]-x0;
      y = ptj.c[1]-y0;
      r = sqrt(x*x+y*y);
      if (r < r_max)
      {
        k = r/dl;
        g[k] += 1./area(mesh,i,r,dl);
      }
      
    }
  }
  
  vector <edge> listEdges;
  edge a0;
  unsigned int adjiai;
  for (unsigned int i = 1; i < adj.size(); i++)
  {
    for (unsigned int ai = 0; ai < adj[i].size(); ai++)
    {
      adjiai = adj[i][ai];
      if (i<adjiai)
      {
        x0 = mesh->point[i].c[0];
        y0 = mesh->point[i].c[1];
        x1 = mesh->point[adjiai].c[0];
        y1 = mesh->point[adjiai].c[1]; 
        a0.theta = atan2(y1-y0,x1-x0);
        a0.x = (x0+x1)/2;
        a0.y = (y0+y1)/2;
        x0 = x1-x0;
        y0 = y1-y0;
        a0.r = sqrt(x0*x0+y0*y0);
      
        listEdges.push_back(a0);
      }
    }
  }


  vector<int> countatemp(dim,0);
  vector<int> counta(dim,0);
  vector<double> g6temp(dim,0);
  //~ double h = 1./sqrt(mesh->np*cos(pi/6));
  unsigned int nEdges = listEdges.size();
  unsigned int nEdgesMax = 1000;
  bool isBig = nEdges>nEdgesMax;
  int nEdgesFactor = nEdges/nEdgesMax;


  if (isBig)
  {
    for (unsigned int ii = 0; ii<nEdgesMax; ii++)
    {
      int i = ii*nEdgesFactor;
      x0 = listEdges[i].x;
      y0 = listEdges[i].y;
      for (unsigned int jj = 0; jj<nEdgesMax; jj++)
      {
        int j = jj*nEdgesFactor;
        x = listEdges[j].x-x0;
        y = listEdges[j].y-y0;
        r = sqrt(x*x+y*y);
        if (r < r_max)// && fabs(listEdges[j].r-h)<0.5*h)
        {
          k = r/dl;
          g6temp[k] += cos(6*(listEdges[j].theta-listEdges[i].theta));
          countatemp[k]++;
        }
      }
    }
  }else
  { 
    for (unsigned int i = 0; i<nEdges; i++)
    {
      x0 = listEdges[i].x;
      y0 = listEdges[i].y;
      for (unsigned int j = 0; j<nEdges; j++)
      {
        x = listEdges[j].x-x0;
        y = listEdges[j].y-y0;
        r = sqrt(x*x+y*y);
        if (r < r_max)// && fabs(listEdges[j].r-h)<0.5*h)
        {
          k = r/dl;
          g6temp[k] += cos(6*(listEdges[j].theta-listEdges[i].theta));
          countatemp[k]++;
        }
      }
    }
  }
  
  
  for (unsigned int l = 0; l < g.size(); l++)
  {
    if (countatemp[l] > 0)
    {
      g6temp[l] /= countatemp[l];
      g6[l] += g6temp[l];
      counta[l]++;
    }
    countatemp[l] = 0;
    g6temp[l] = 0;
  }

  for (unsigned int i = 0; i < g.size(); i++)
  {
    g[i] /= (mesh->np*mesh->np)/2;
    if (counta[i]>0)
    {
      if (g6[i]>0)
        g6[i] /= counta[i];
      else
        g6[i] = 1.e-15;
    }
    else
       g6[i] = 1;
  }
}


/**
 * \param g pair correlation function values
 * \param g6 orientational correlation function values
 * \param r_max maximum distance at wich the functions are computed
 * \param lMean mean distance between the nodes
 * \return parameter of distance of correlation
 *
 * Output radial distributions in a file. Perform linear fit of linearized
 * exponential and power functions to set first values for non-linear fit
 * using levmarq algorithm.
 * 
 */
double* radialDistributionOut(vector <double> g, vector <double> g6, double r_max, double lMean,double dl)
{
  ofstream gr6;
  gr6.open("orientationalDistrib.dat");
  //~ gr << "r    g(r)" << endl; 
  //~ double dl = r_max/(g6.size()+0.5);
  double x = dl/2;
  double *distribParam = new double[5];
  int n1=0;
  int k;
  

  while(g6.size()>n1 && g6[n1] == 1)
    n1++;
  //~ vector <double> xg6(g6.size(),0);
  //~ xg6[0] = x;
  //~ for (unsigned int i = 1; i< g6.size(); i++)
  //~ {
    //~ xg6[i] = xg6[i-1]+dl;
  //~ }
  
  // not working, functions from linearFit.h are not recognized
  //~ double* expFit = weightedLinearFit(xg6, g6, 0, 1); 
  //~ double* powFit = linearFit(xg6, g6, 1, 0); 
  double* expFit; 
  double* powFit; 
  double *fData = new double[2];
  fData[0] = dl;
  fData[1] = dl*n1;
  
  LMstat lmstat;
  levmarq_init(&lmstat);

  vector<double> vecx(g6.size());
  for (unsigned int i = 0; i < g6.size(); i++)
    vecx[i]= (i)*dl;

//  powFit[0]=1.;
//  powFit[1]=-0.36;
  powFit = linearFit(vecx,g6,1,0);
  if (powFit)
  {
    std::cout << "powFit" << powFit[0] << " " << powFit[1] << std::endl;
    levmarq(2, powFit, (int) g6.size()-n1, &(g6[n1]), NULL,
          axpowb,   // a*exp(b*i)
          gradaxpowb, // gradient in a and b directions
          fData, &lmstat);
  }
//  expFit[0]=0.5;
//  expFit[1]=-0.15/lMean;
  expFit = linearFit(vecx,g6,0,1);
  if (expFit)
  {
    std::cout << "expFit" << expFit[0] << " " << expFit[1] << std::endl;
    levmarq(2, expFit, (int) g6.size()-n1, &(g6[n1]), NULL,
          aexpbi,   // a*exp(b*i)
          gradaexpbi, // gradient in a and b directions
          fData, &lmstat);

    cout << "g6 exp fit : " << expFit[0] << "   " << expFit[1] << endl;
    cout << "g6 pow fit : " << powFit[0] << "   " << powFit[1] << endl;
    cout << "parameter of distribution : " << lMean/expFit[1] << endl;
  }
  for (unsigned int i = 0; i< (g6.size()<g.size()? g6.size():g.size()); i++)
  {
    k=x/dl;
    gr6 << x/lMean << "    " << g6[k] << "    " << (g[k])  << "    " << expFit[0]*exp(expFit[1]*x) << "    " << powFit[0]*pow(x,powFit[1]) << endl;
    //~ gr6 << x/lMean << "    " << g6[i] << "    " << g[i]  << "    " << expFit[0]*exp(expFit[1]*x) << "    " << powFit[0]*pow(x,powFit[1]) << endl;
    x += dl;
  }

  if (expFit)
  {
    distribParam[0] = -lMean/expFit[1];
    distribParam[1] = expFit[0];
    distribParam[2] = expFit[1]*lMean;
  }else
  {
    distribParam[0] = 0;
    distribParam[1] = 0;
    distribParam[2] = 0;
  }

  if (powFit)
  {
    distribParam[3] = powFit[0]*pow(lMean,powFit[1]);
    distribParam[4] = powFit[1];
  }else
  {
    distribParam[3] = 0;
    distribParam[4] = 0;
  }
  
  delete[] expFit;
  delete[] powFit;
  delete[] fData;
  gr6.close();
  return distribParam;
}
