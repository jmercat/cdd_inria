#ifndef DATASOL_H
#define DATASOL_H

#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include "mmg/mmg2d/libmmg2d.h"
#include "debug.h"

/**
 * \brief Class for solution data storage.
 * 
 * This class was made to avoid using the MMG5_Sol structure. It stores
 * values, often result of computation on the mesh, at the nodes. It contains
 * the information needed to output the data using
 * the postscript output functions. 
 * Additional functions allow transfer of location of the solution from 
 * triangle or edge. Gradients can be computed but are then stored in another format.
 * 
 * 
 */
class DataSol
{
  public:      
    
    DataSol(int n, char* name = const_cast<char*> ("solOut")); // constructeur par taille
    DataSol(char* name = const_cast<char*> ("solOut")); // constructeur par défaut
    DataSol(DataSol& sol_c); // constructeur par copie
    ~DataSol();
    
    double& operator ()(int ind);
    DataSol& operator =(DataSol from);
    
    void setName(char* name);
    std::string getName();
    
    void setSol( MMG5_pSol mmgSol, MMG5_pMesh mmgMesh) const;
    void getGrad(const std::vector< std::vector<int> >& adj, std::vector< std::vector<double> >& gradSol, double& gradSolMean, bool isPeriodic);
    void setFromTriangle(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj, std::vector<double>& triData);
    void setFromEdge(MMG5_pMesh mesh, const std::vector< std::vector<double> >& edge_data);
    void setFromVector(const std::vector <double>& vec);
    void setFromVector(const std::vector<std::vector <double> >& vec, int n);
    void setFromVector(const std::vector<std::vector <double> >& vec, int n1, int n2);
    void normalize();
    void normalizeStd();

  private:
  
    int size;
    double* m;
    char* nameout;
    
    double getMedian();
};
    
//~ double getMedianRec(double* values, int subSize);
/**
 * \param ind index of the data to access
 * 
 * Access an element of the stored data.
 * 
 */
inline double& DataSol::operator ()(int ind)
{
  Debug
  (
    if(ind<0 || ind>size || !m)
    {
      std::cout << "DataSol: operator () access to unallocated element\
       of index " << ind << " in data of size " <<  size;
      IntTest;
    }
  )
  return m[ind];
}


#endif
