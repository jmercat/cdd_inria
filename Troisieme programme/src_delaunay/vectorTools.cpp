/**
 * \file vectorTools.cpp
 * \brief Functions related to vectors.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */

#include <math.h>
#include "vectorTools.h"
#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif
using namespace std;

/**
 * \param vec vector to normalize
 * \param minVec minimum value in the vector (output)
 * \param maxVec maximum value in the vector (output)
 * 
 * Normalize the vector so that the values are in [0,1] (affine transformation)
 * 
 */
void normalizeVec(vector <double>& vec,double& minVec, double& maxVec)
{
  minVec = vec[0];
  maxVec = vec[0];
  for (unsigned int i = 1; i < vec.size(); i++)
  {
    minVec = min(minVec,vec[i]);
    maxVec = max(maxVec,vec[i]);
  }
  for (unsigned int i = 0; i < vec.size(); i++)
  {
    vec[i] = (vec[i]-minVec)/(maxVec-minVec);
  }
}

/**
 * \param vec vector to normalize
 * \param pointType values defining type of each node (0 normal,  1 boundary of a grain,  2 less than 6 neighbors,3 more than 6 neighbors,  4 border)
 * \param minVec minimum value in the vector (output)
 * \param maxVec maximum value in the vector (output)
 * 
 * Normalize the vector so that the values of normal are in [0,1] (affine transformation)
 * Only take into account the nodes that are far enough from boundaries (avoid
 * very different values)
 */
void normalizeVec(vector <double>& vec, const vector<int>& pointType, double& minVec, double& maxVec)
{
  minVec = vec[0];
  maxVec = vec[0];
  for (unsigned int i = 1; i < vec.size(); i++)
  {
    if (pointType[i] != 4)
    {
      minVec = min(minVec,vec[i]);
      maxVec = max(maxVec,vec[i]);
    }
  }
  for (unsigned int i = 1; i < vec.size(); i++)
  {
    vec[i] = (vec[i]-minVec)/(maxVec-minVec);
  }
}

/**
 * \param vec vector to normalize
 * 
 * Normalize a vector so that values between mean +/- std are in [0,1]
 * 
 */
void normalizeVecStd(vector <double>& vec)
{
  double mean = -1;
  int n = vec.size();
  double std = standardDev(vec,mean);
  for (int i = 0; i < n; i++)
  {
    vec[i] = (vec[i]-(mean-2*std))/(4*std);
  }
}

/**
 * \param vec vector to normalize
 * 
 * Normalize a vector so that values between median +/- std are in [0,1]
 * 
 */
void normalizeVecStdMed(vector <double>& vec)
{
  int n = vec.size();
  double med = getMedian(vec);
  double std = standardDev(vec,med);
  for (int i = 0; i < n; i++)
  {
    vec[i] = (vec[i]-(med-2*std))/(4*std);
  }
}

/**
 * \param vec vector to smooth
 * \param dim length of the shorter vector
 * \param k number of values to use for smoothing
 * 
 * Perform smoothing of given vector \a vec using average over k values.
 * 
 */
void smoothVec(vector <double>& vec, int dim, int k)
{
  int Nvec = vec.size();
  
  int n = Nvec/dim+1;
  
  dim = Nvec/n;
  
  //~ cout  << "##############" << n << " " << Nvec << " " << dim << endl;
  vector <double> vec_short(dim+1,0);
  
  if (k%2 == 0)
  {
    for (int i = 0; i<Nvec; i+=n)
    {
      for (int j = -(k-1)/2; j<=k/2; j++)
      {
        vec_short[i/n] += vec[min(max(i+j,0),Nvec-1)]/k;
      }
    }
  }else
  {
    for (int i = 0; i<Nvec; i+=n)
    {
      for (int j = -k/2; j<=k/2; j++)
      {
        vec_short[i/n] += vec[min(max(i+j,0),Nvec-1)]/k;
      }
    }
  }
  vec.resize(dim+1);
  vec = vec_short;
}

/**
 * \param vec vector to smooth
 * \param n length over wich to smooth
 * 
 * Perform smoothing of given vector \a vec using average over \a n values.
 * 
 */
void smoothVec2(vector <double>& vec, int n)
{
  vector <double> vecCopy(vec);
  for (int i = n/2; i<(int) (vec.size())-(n+1)/2; i++)
  {
    vec[i]=0;
    for(int j = -n/2; j<(n+1)/2; j++)
    {
      vec[i] += vecCopy[i+j];
    }
    vec[i]/=n;
  }
}


/**
 * \param vec input vector for std calculation
 * \param mean mean value of the vector (input or output)
 * \return standard deviation of the values in vec
 * 
 * Compute standard deviation around \a mean (or compute mean value if given -1).
 * 
 */
double standardDev(const vector <double>& vec, double& mean)
{
  double s = 0, dv;
  int n = vec.size();
  if( mean == -1)
  {
    mean = 0;
    for (int i = 0; i < n; i++)
    {
      mean += vec[i]/n;
    }
  }
  for (int i = 0; i < n; i++)
  {
    dv = vec[i] - mean;
    if (dv < mean)
    {
      s += dv*dv;
    }else
    {
      n--;
    }
  }
  s /= n-1.5;
  return sqrt(s);
}

/**
 * \param vec vector in wich search is performed
 * \param elem element to be found in \a vec
 * \return wether \a elem was found in \a vec
 * 
 */
bool isInVect(vector<int> vec, int elem)
{
  for (unsigned int i = 0; i< vec.size(); i++)
  {
    if(elem == vec[i])
      return true;
  }
  return false;
}

/**
 * \param v vector in wich insertion is done
 * \param n value to insert
 * \return wether \a n was found in \a v
 * 
 * Insert \a n in the vector \a v if there is no existing value equal to
 * n in \a v.
 * 
 */
bool insert(vector<int>& v, int n)
{
  bool contain = false;
  for(unsigned int i = 0; i<v.size();i++)
  {
    contain = (v[i]==n);
    if (contain)
      break;
  }
  if(!contain && n>-1)
  {
    v.push_back(n);
    return true;
  }
  return false;
}

/**
 * \param vec vector of vector in wich search is performed
 * \param elem element to be found in \a vec
 * \return wether \a elem was found in \a vec
 * 
 */
bool isInVect(vector < vector<int> > vec, int elem)
{
  for (unsigned int i = 0; i<vec.size(); i++)
  {
    for (unsigned int j = 0; j< vec[i].size(); j++)
    {
      if(elem == vec[i][j])
        return true;
    }
  }
  return false;
}

/**
 * \param vec vector
 * \return mean value of the vector
 * 
 */
double getMean(vector<double> vec)
{
  double mean = 0;
  for (unsigned int i = 0; i < vec.size(); i++)
  {
    mean += vec[i];
  }
  mean /= vec.size();
  return mean;
}


/**
 * \param values array of values
 * \param subSize subSize of the array (number of element to use)
 * \return median value of the array
 * 
 */
double getMedianRec(double* values, int subSize)
{
  if (subSize <= 3)
  {
    double x0 = values[0%subSize];
    double x1 = values[1%subSize];
    double x2 = values[2%subSize];
    double x;
    x = max(x0, x1);
    x1 = x0+x1-x;
    x0 = x;
    if (x0> x2)
      return max(x1,x2);
    else
      return x;
    }else
    {
    int r = subSize%3;
    double x0 = getMedianRec(&values[0],subSize/3);
    double x1 = getMedianRec(&values[subSize/3],(subSize/3));
    double x2 = getMedianRec(&values[2*(subSize/3)],(subSize/3)+r);
    double x;
    x = max(x0, x1);
    x1 = x0+x1-x;
    x0 = x;
    if (x0> x2)
      return max(x1,x2);
    else
      return x;
  }
}

/**
 * \param vec vector wich median value is to be found
 * \return median value of the array
 * 
 */
double getMedian(vector<double> vec)
{
  return getMedianRec(&vec[0],vec.size());
}
