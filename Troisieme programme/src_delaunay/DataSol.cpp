/**
 * \file DataSol.cpp
 * \brief Data format to store solution. 
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * Data format to store solutions known on different elements of the mesh
 * (nodes, triangles or vertices). Functions making passage from one representation
 * to another easier.
 * 
 */

#include "vectorTools.h"
#include "DataSol.h"
#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif
using namespace std;

/**
 *
 * \param n number of double to store.
 * \param name name of the data (optional, default is "solOut").
 *
 *  Constructor with size and optional name.
 *
 */
DataSol::DataSol(int n, char* name)
{
  try
  {
    m = new double[n];
    size = n;
  }catch (std::bad_alloc&)
  {
    m = NULL;
    size = 0;
  }
  try
  {
    nameout = new char[512];
  }catch (std::bad_alloc&){}   
  memcpy(nameout, name, 512*sizeof(char));
}

/**
 *
 * \param name name of the data (optional, default is "solOut").
 *
 *  Default constructor, optional name.
 *
 */
DataSol::DataSol(char* name)
{
  m = NULL;
  size = 0;
  try
  {
    nameout = new char[512];
  }catch (std::bad_alloc&){}
  
  memcpy(nameout, name, 512*sizeof(char));
}

/**
 *
 * \param solFrom data to copy.
 *
 *  Copy constructor.
 *
 */
DataSol::DataSol(DataSol& solFrom) // constructeur par copie
{
  try
  {
    m = new double[solFrom.size];
    size = solFrom.size;
  }catch (std::bad_alloc&)
  {
    m = NULL;
    size = 0;
    cout << "DataSol copy: data allocation failed" << endl;
  }
  try
  {
    nameout = new char[512];
  }catch (std::bad_alloc&)
  {
      cout << "DataSol copy: name allocation failed" << endl;
  }
  memcpy(m,solFrom.m,size*sizeof(double));
  memcpy(nameout, solFrom.nameout, 512*sizeof(char));
}

DataSol::~DataSol()
{
  if (m!=NULL)
    delete[] m;
  delete[] nameout;
}

DataSol& DataSol::operator=(DataSol from)
{
  swap(from.m, this->m);
  swap(from.nameout, this->nameout);
  size = from.size;
  
  return *this;
}

void DataSol::setName(char* name)
{
  memcpy(nameout, name, 512*sizeof(char));
}

string DataSol::getName()
{
  string nameoutstr=nameout;
  return nameoutstr;
}

/**
 *
 * \param mmgSol MMG formated solution.
 * \param mmgMesh MMG formated mesh.
 *
 * Set Data from mmgSol to DataSol. Warning: the mmgSol object should have the right size
 *
 */
void DataSol::setSol(MMG5_pSol mmgSol, MMG5_pMesh mmgMesh) const
{
    memcpy(mmgSol->m, m, size*sizeof(double));
    MMG2D_Set_outputSolName(mmgMesh, mmgSol, nameout);
}

/**
 *
 * \param mesh MMG formated mesh.
 * \param adj adjacence table of the nodes.
 * \param gradSol output gradient of the solution in directions of every adjacent nodes.
 * \param gradSolMean mean value of precedent.
 * \param isPeriodic tell if the value on wich the gradient is performed is periodic (0=1)
 *
 * Performs gradient computation at every node in directions of each neighbor.
 *
 */
void DataSol::getGrad(const vector< vector<int> >& adj, vector< vector<double> >& gradSol, double& gradSolMean, bool isPeriodic)
{
  gradSolMean = 0;
  gradSol.resize(adj.size());
  int s, nb_mean = 0;
  double smin = HUGE_VALF, smax = 0;
  for (unsigned int i = 0; i< gradSol.size(); i++)
  {
    s = adj[i].size();
    gradSol[i].resize(s);
    for(int j=0; j<s;j++)
    {

      gradSol[i][j] = fabs(m[adj[i][(j+1)%s]] - m[adj[i][(s+j-1)%s]]);
      if (isPeriodic)
        gradSol[i][j] = min(gradSol[i][j],1-gradSol[i][j]);
      if(s==6)
      {    
        smin = min(smin, gradSol[i][j]);
        smax = max(smax, gradSol[i][j]);
        gradSolMean += gradSol[i][j];
        nb_mean++;
      }
    }
  }
  gradSolMean /= nb_mean;
}


/**
 *
 * \param mesh MMG formated mesh.
 * \param adj adjacence table of the nodes.
 * \param triData data at triangles.
 *
 * Set Data at points from data at triangles.
 * Warning: size of the DataSol object is only tested in debug mode
 *
 */
void DataSol::setFromTriangle(MMG5_pMesh mesh, vector< vector<int> >& adj, vector<double>& triData)
{ 
  int npt;
  Debug(
  if (triData.size() != (unsigned int) (mesh->nt+1))
  {
    cout << "Input vector in setFromTriangle not initialized properly" << endl;
    return;
  }
    if (m == NULL)
      cout << "Sol not allocated" << endl;
  )
  
  memset(m, 0, size*sizeof(double));
  
  for(int i = 1; i<= mesh->nt; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      npt = mesh->tria[i].v[j];
      m[npt] += triData[i]/adj[npt].size();
    }
  }
  for(int i = 1; i< size; i++)
  {
    if (m[i]>m[0])
      m[0] = m[i];
  }
}

/**
 *
 * \param mesh MMG formated mesh.
 * \param edgeData data at edges of the mesh.
 *
 * Set data at node from data at edges.
 *
 */
void DataSol::setFromEdge(MMG5_pMesh mesh, const vector< vector<double> >& edgeData)
{ 
  int s;
  Debug(
  if (edgeData.size() != (unsigned int) (mesh->np+1))
  {
    cout << "Input vector in setFromEdge not initialized properly" << endl;
    return;
  }
  )
  
  for(int i = 1; i<= mesh->np; i++)
  {
    m[i] = 0;
    s = edgeData[i].size();
    for(int j = 0; j < s; j++)
    {
        m[i] += edgeData[i][j];
    }
    m[i] /= s;
    m[i] *= 2;
  }
  m[0] = 0;
  for(int i = 1; i< size; i++)
  {
    if (m[i]>m[0])
      m[0] = m[i];
  }
}

/**
 *
 * \param vec vector of data at nodes.
 *
 * Set DataSol data at nodes from vector of data at nodes.
 *
 */
void DataSol::setFromVector(const vector <double>& vec)
{
  Debug(
    if (vec.size() != (unsigned int) (size))
      cout << "vectoSol : incompatible size" << endl;
  )
  memcpy(m, &vec[0], sizeof(double)*vec.size());
}

/**
 *
 * \param vec vector of vector of data at nodes.
 * \param n direction to copy in DataSol object.
 *
 * Set DataSol data at nodes from vector of vector of data at nodes, choosing the
 * direction \a n.
 *
 */
void DataSol::setFromVector(const vector<vector <double> >& vec, int n)
{
  Debug(if(n>vec[0].size()) cout<< "DataSol::setFromVector : incompatible size" << endl;)
  for (unsigned int i =0; i<vec.size(); i++)
  {
    m[i]=vec[i][n];
  }
}


/**
 *
 * \param vec vector of vector of data at nodes.
 * \param n1 first direction to sum.
 * \param n2 second direction to sum.
 *
 * Set DataSol data at nodes from vector of vector of data at nodes, sum of the
 * directions \a n1 and \a n2.
 *
 */
void DataSol::setFromVector(const vector<vector <double> >& vec, int n1, int n2)
{
  Debug(if(n1>vec[0].size()||n2>vec[0].size()) cout<< "DataSol::setFromVector : incompatible size" << endl;)
  for (unsigned int i =0; i<vec.size(); i++)
  {
    m[i]=vec[i][n1]+vec[i][n2];
  }
}

/**
 *
 * Normalize the data with affine function so it fits in [0,1] (using min and max values)
 *
 */
void DataSol::normalize()
{
  double minVal = 1.e16, maxVal = 0;
  for(int i = 1; i< size; i++)
  {
    minVal = min(minVal,m[i]);
    maxVal = max(maxVal,m[i]);
  }
  for(int i = 1; i< size; i++)
  {
    m[i]= (m[i]-minVal)/(maxVal-minVal);
  }
}

//~ double getMedianRec(double* values, int subSize)
//~ {
  //~ if (subSize <= 3)
  //~ {
    //~ double x0 = values[0%subSize];
    //~ double x1 = values[1%subSize];
    //~ double x2 = values[2%subSize];
    //~ double x;
    //~ x = max(x0, x1);
    //~ x1 = x0+x1-x;
    //~ x0 = x;
    //~ if (x0> x2)
      //~ return max(x1,x2);
    //~ else
      //~ return x;
  //~ }else
  //~ {
    //~ int r = subSize%3;
    //~ double x0 = getMedianRec(&values[0],subSize/3);
    //~ double x1 = getMedianRec(&values[subSize/3],(subSize/3));
    //~ double x2 = getMedianRec(&values[2*(subSize/3)],(subSize/3)+r);
    //~ double x;
    //~ x = max(x0, x1);
    //~ x1 = x0+x1-x;
    //~ x0 = x;
    //~ if (x0> x2)
      //~ return max(x1,x2);
    //~ else
      //~ return x;
  //~ }
//~ }

/**
 *
 * \return Median value of data.
 */
double DataSol::getMedian()
{
  return getMedianRec(m,size);
}

/**
 *
 * Normalize the data with affine function so it almost fits in [0,1]
 * using +/-3*standard deviation around median value.
 *
 */
void DataSol::normalizeStd()
{
  double minVal = 1.e16, maxVal = 0, medVal = 0, std = 0;
  double dist;
  for(int i = 1; i< size; i++)
  {
    minVal = min(minVal,m[i]);
    maxVal = max(maxVal,m[i]);
    medVal += m[i];
  }
  medVal = getMedian();
  int nbRepVal = 0;
  for(int i = 1; i< size; i++)
  {
    dist = m[i]-medVal;
    if (dist/medVal<2)
    {
      nbRepVal++;
      std += dist*dist;
    }
  }
  std /= nbRepVal-1;
  std = sqrt(std);
  cout << "Std normalization " << medVal << "    "<<std<<endl;
  //~ medVal = -4.69269e-6;
  //~ std = 0.00027203;
  for(int i = 0; i< size; i++)
  {
    m[i]= (m[i]-medVal+3*std)/(6*std);
    m[i]= max(0.,m[i]);
    m[i]= min(1.,m[i]);
  }
}
