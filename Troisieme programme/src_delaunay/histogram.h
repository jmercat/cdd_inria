#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
extern "C"
{
  #include "MMG2D_plottops.h"
}
#include "DataSol.h"
#include "../src_exchange/exchange.h" //contains definition of the histogram structure

void getHistogramSol(MMG5_pMesh mesh, DataSol& sol, histogram& histSol);
void getHistogramVec(std::vector <double> data, histogram& histData);
void plotHistogram(double x, double y, double h, double w, histogram& hist, MMG2D_postscript* ps);
void printHistogram(const histogram& hist);
int maxvec(const std::vector <int>& vec);
void unnormalizeHistogramLevels(histogram& hist, double minLevel, double maxLevel, double factor = 1);
void unnormalizeHistogramLevelsStd(histogram& hist, double meanLevel, double stdLevel, double factor = 1);
void saveHist(std::vector<histogram>& vecHist, char* name);


#endif
