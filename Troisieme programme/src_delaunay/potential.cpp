/**
 * \file potential.cpp
 * \brief Compute potential between nodes using Lennard-Jones function.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */
 
#include "potential.h"

#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif

#define pi 3.1415926

using namespace std;

/**
 * \param mesh mmg mesh structure
 * \param adj adjacence table 
 * 
 * Move the nodes to its neighbors' center of gravity. It has the effect
 * of relaxing potential and straightening the organization.
 * 
 */
void relaxPosition(MMG5_pMesh mesh, vector< vector<int> >& adj)
{
  double** positions = 0;
  int np = mesh->np;
  positions = new double*[np+1];
  if (positions)
    positions[0] = new double[(np+1)*2];
  else
    cout<<"relaxPosition: allocation failed"<<endl;
  if (positions[0])
  {
    for (int i = 1; i<=np; i++)
    {
      positions[i] = positions[i-1]+2; 
    } 
  }
  else
    cout<<"relaxPosition: allocation failed"<<endl;
  for (int i =1; i<=np; i++)
  {
    memcpy(positions[i], mesh->point[i].c, 2*sizeof(double));
  }
  
  double meanPosition[2];
  double meanDist = 0,x,y;
  int nAdj;
  for (int i= 1; i <=np; i++)
  {
    nAdj = adj[i].size();
    memset(meanPosition, 0, 2*sizeof(double));
    for(int j = 0; j<nAdj; j++)
    {
      meanPosition[0] += positions[adj[i][j]][0];
      meanPosition[1] += positions[adj[i][j]][1];
    }
    meanPosition[0] /= nAdj;
    meanPosition[1] /= nAdj;
    x = mesh->point[i].c[0] - meanPosition[0];
    y = mesh->point[i].c[1] - meanPosition[1];
    
    mesh->point[i].c[0] = meanPosition[0];
    mesh->point[i].c[1] = meanPosition[1];
    meanDist += sqrt(x*x+y*y);
  }
  meanDist /= np;
  cout << "Mean distance for position relaxation: " << meanDist << endl;
  delete[] positions[0];
  delete[] positions;
}


/**
 * \param mesh mmg mesh structure
 * \param adj adjacence table 
 * \param potentialOut potential value of each node
 * 
 * Compute the potential for each node using only its first neighbors.
 * 
 */
void getPotential(MMG5_pMesh mesh, vector< vector<int> >& adj, DataSol& potentialOut)
{
  int np = mesh->np;
  int nAdj;
  DataSol potential(np+1);
  potential.setName(const_cast <char*> ("Potential"));
  double r;
  double x,y,x1,y1;
  double maxVal=0;
  for (int i= 1; i <=np; i++)
  {
    nAdj = adj[i].size();
    potential(i) = 0;
    x = mesh->point[i].c[0];
    y = mesh->point[i].c[1];
    for(int j = 0; j<nAdj; j++)
    {
      x1 = mesh->point[adj[i][j]].c[0]-x;
      y1 = mesh->point[adj[i][j]].c[1]-y;
      r = x1*x1+y1*y1;
      potential(i) += sqrt(lennardJonesP2(1.,1.,r));
    }
    maxVal = max(maxVal, potential(i));
  }
  potential(0) = maxVal;
  potentialOut = potential;
}

/**
 * \param s mean space between the nodes (with a factor of pow(2,1./6))
 * \param eps multiplicative constant
 * \param r2 squared distance between nodes
 * 
 * Output the Lennard-Jones potential between two nodes.
 * 
 */
double lennardJonesP2(double s,double eps, double r2)
{
  //~ s= s/pow(2,1./6);
  s = s*s;
  double sr = s/r2;
  sr = sr*sr*sr;
  return 4*eps*(sr*sr-sr);
}


/**
 * \param mesh mmg mesh structure
 * \param adj adjacence table 
 * \param potentialOut potential value of each node
 * 
 * Compute the deformation for each node by comparing the first neighbors
 * position with a perfect hexagon.
 * 
 */
void getDeformationEnergy(MMG5_pMesh mesh, vector< vector<int> >& adj,vector<double>& angle, double lMean, vector< vector<double> >& deformationOut)
{
  deformationOut.resize(adj.size());
  for (unsigned int i= 1; i <adj.size(); i++)
  {
    deformationOut[i].resize(3);
    int nAdj = adj[i].size();
    if (nAdj == 6)
    {
      double x1,y1,x,y,dx1,dy1;
      
      x = mesh->point[i].c[0];
      y = mesh->point[i].c[1];
      
      //~ double theta0 = atan2(mesh->point[adj[i][0]].c[1]-y,mesh->point[adj[i][0]].c[0]-x);
      double theta,theta0;
      //~ theta = theta0 - fmod(theta0+pi/6,pi/3);
      //~ theta += angle[i]-pi/6;
      theta0 = angle[i]*pi/3-pi/6;
      theta = theta0;
      int minjj = 0;
      double dx,dy;
      double mindist =  1;
      double dist;
      for (int jj=0;jj<6;jj++)
      { 
        dx = cos(theta);
        dy = sin(theta);
        x1 = x+lMean*dx;
        y1 = y+lMean*dy;
        dx1 = mesh->point[adj[i][jj]].c[0]-x1;
        dy1 = mesh->point[adj[i][jj]].c[1]-y1;
        dist = dx1*dx1+dy1*dy1;
        if(dist<mindist)
        {
          mindist =  dist;
          minjj = jj;
        }
      }
      
      theta = theta0;
      deformationOut[i][0] = 0;
      deformationOut[i][1] = 0;
      deformationOut[i][2] = 0;
      for(int j = 0; j<6; j++)
      {        
        dx = cos(theta);
        dy = sin(theta);
        x1 = x+lMean*dx;
        y1 = y+lMean*dy;
        dx1 = mesh->point[adj[i][(j+minjj)%6]].c[0]-x1;
        dy1 = mesh->point[adj[i][(j+minjj)%6]].c[1]-y1;
        
        deformationOut[i][0] += dx*dx1/12;
        deformationOut[i][1] += dy*dy1/12;
        deformationOut[i][2] += (dx*dy1+dy*dx1)/24;
        
        theta += pi/3;
        //~ cout<< dx1/lMean<< "    " << dy1/lMean << "    " << atan2(mesh->point[adj[i][j]].c[1]-y,mesh->point[adj[i][j]].c[0]-x)<< endl;
        
      }
      //~ cout << "############################################" << endl;
    }
    else
    {
      deformationOut[i][0] = 0;
      deformationOut[i][1] = 0;
      deformationOut[i][2] = 0;
    }
  }
}
