#include "levmarq.h"
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

double aexpbic(double* par,int i,void* a)
{
  return par[0]*exp(par[1]*i);
}

void gradaexpbic(double* g,double* par,int i,void* a)
{
  g[0] = exp(par[1]*i);
  g[1] = i*par[0]*exp(par[1]*i);
}

int main(int argc, char **argv)
{   
  
  vector<double> yv;
  double ytemp,trash;
  ifstream fichier(argv[1]);
  while (true)
  {
    fichier >> trash >> ytemp >> trash >> trash >> trash;
    yv.push_back(ytemp);
    if( fichier.eof() ) break;
  }
  
  /* perform least-squares minimization using the Levenberg-Marquardt
   algorithm.  The arguments are as follows:

   npar    number of parameters
   par     array of parameters to be varied
   ny      number of measurements to be fit
   y       array of measurements
   dysq    array of error in measurements, squared
           (set dysq=NULL for unweighted least-squares)
   func    function to be fit
   grad    gradient of "func" with respect to the input parameters
   fdata   pointer to any additional data required by the function
   lmstat  pointer to the "status" structure, where minimization parameters
           are set and the final status is returned.

   Before calling levmarq, several of the parameters in lmstat must be set.
   For default values, call levmarq_init(lmstat).
  */
  int npar = 2;
  double par[2]={1,-0.1};
  int ny = yv.size();
  double* y = &(yv[0]);
  double* dysq = NULL;
  double* fdata = NULL;

  LMstat lmstat;
  
  levmarq_init(&lmstat);
  nIter = levmarq(npar, par, ny, y, dysq,
        aexpbic,
        gradaexpbic,
        fdata, &lmstat);
        
  cout << par[0]<<"*exp("<<par[1]<<"*x)"<< "  number of iterations: " << nIter << endl;
  
}
