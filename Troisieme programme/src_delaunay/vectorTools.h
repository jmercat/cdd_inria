#ifndef vectorTOOLS_H 
#define vectorTOOLS_H

#include <vector>

void normalizeVec(std::vector <double>& vec,double& minVec, double& maxVec);
void normalizeVec(std::vector <double>& vec, const std::vector<int>& pointType, double& minVec, double& maxVec);
void normalizeVecStd(std::vector <double>& vec);
void normalizeVecStdMed(std::vector <double>& vec);
void smoothVec(std::vector <double>& vec, int dim, int k);
void smoothVec2(std::vector <double>& vec, int n);
double standardDev(const std::vector <double>& vec, double& m);
bool isInVect(std::vector<int> vec, int elem);
bool isInVect(std::vector < std::vector<int> > vec, int elem);
bool insert(std::vector<int>& v, int n);
double getMedianRec(double* values, int subSize);
double getMedian(std::vector<double> vec);
double getMean(std::vector<double> vec);

#endif
