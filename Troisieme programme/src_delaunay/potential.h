#ifndef POTENTIAL_H
#define POTENTIAL_H

#include <vector>
#include "DataSol.h"
#include "mmg/mmg2d/libmmg2d.h"

void getPotential(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj, DataSol& potentialOut);
void relaxPosition(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj);
double lennardJonesP2(double s,double eps, double r2);
void getDeformationEnergy(MMG5_pMesh mesh, std::vector< std::vector<int> >& adj,std::vector<double>& angle, double lMean, std::vector< std::vector<double> >& deformationOut);



#endif
