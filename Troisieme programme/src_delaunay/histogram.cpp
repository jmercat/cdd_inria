/**
 * \file histogram.cpp
 * \brief functions related to histograms.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */



#include "histogram.h"
#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif
using namespace std;


/**
 * \param mesh mmg mesh structure
 * \param sol DataSol structure
 * \param histSol histogram of the \a sol to be computed
 *
 * Fill the \a histSol structure with the data from DataSol for an
 * histogram representation of it.
 * 
 */
void getHistogramSol(MMG5_pMesh mesh, DataSol& sol, histogram& histSol)
{
  int  nbLevels = histSol.levels.size();
  if (nbLevels != (int) (histSol.count.size()) -1)
  {
  cout << "Levels size and histogram size incompatible adjusting histogram to level" << endl;
  }
  histSol.count.resize(nbLevels-1,0);
  histSol.levelsMean.resize(nbLevels-1,0);
  histSol.countAll =  mesh->np;
  for (int i = 0; i<nbLevels; i++)
  {
  histSol.levels[i] = float(i)/(nbLevels-1);
  }
  for (int i = 1; i <= mesh->np; i++)
  {
  int j = 0;
  //~ cout << (sol->m[i] > histSol.levels[j]) << endl;
  while( j < nbLevels-1 && (sol(i) > histSol.levels[j])) // algo le plus basique pour trouver le level...
  {
    j++;
  }
  j = max(j,1);
  histSol.count[j-1]++;
  histSol.levelsMean[j-1] += sol(i);
  }
  for (int i = 0; i<nbLevels-1; i++)
  {
  histSol.levelsMean[i] /= histSol.count[i];
  }
}

/**
 * \param data vector of data 
 * \param histSol histogram of the \a data to be computed
 *
 * Fill the \a histSol structure with the data from \a data for an
 * histogram representation of it.
 * 
 */
void getHistogramVec(vector <double> data, histogram& histSol)
{
  int  nbLevels = histSol.levels.size();
  if (nbLevels != (int) (histSol.count.size())-1)
  {
    cout << "Levels size and histogram size incompatible adjusting histogram to level" << endl;
  }
  histSol.count.resize(nbLevels-1,0);
  histSol.levelsMean.resize(nbLevels-1,0);
  histSol.countAll =  data.size();
  for (int i = 0; i<nbLevels; i++)
  {
    histSol.levels[i] = float(i)/(nbLevels-1);
  }
  for (unsigned int i = 0; i < data.size(); i++)
  {
    int j = 0;
    while( j < nbLevels-1 && (data[i] > histSol.levels[j])) // algo le plus basique pour trouver le level...
    {
      j++;
    }
    j = max(j,1);
    histSol.count[j-1]++;
    histSol.levelsMean[j-1] += data[i];
  }
  for (int i = 0; i<nbLevels-1; i++)
  {
    histSol.levelsMean[i] /= histSol.count[i];
  }
}

/**
 * \param hist histogram with normal values
 * \param minLevel minimum value (not normalized) of the data in \a hist
 * \param maxLevel maximum value (not normalized) of the data in \a hist
 * 
 * Retrive real data from affine normalization.
 * 
 */
void unnormalizeHistogramLevels(histogram& hist, double minLevel, double maxLevel, double factor)
{
  for (unsigned int i = 0; i < hist.levelsMean.size(); i++)
    hist.levelsMean[i] = factor*(hist.levelsMean[i]*(maxLevel-minLevel)+minLevel);
}

/**
 * \param hist histogram with normal values
 * \param meanLevel mean value (not normalized) of the data in \a hist
 * \param stdLevel standard deviation (not normalized) of the data in \a hist
 * 
 * Retrive real data from normalization using standard deviation.
 * 
 */
void unnormalizeHistogramLevelsStd(histogram& hist, double meanLevel, double stdLevel, double factor)
{
  for (unsigned int i = 0; i < hist.levelsMean.size(); i++)
    hist.levelsMean[i] = factor*(hist.levelsMean[i]*(4*stdLevel)+(meanLevel-2*stdLevel));
  hist.mu = meanLevel;
  hist.sigma = stdLevel;
}

/**
 * \param hist histogram structure
 * 
 * print the histogram in the shell
 * 
 */
void printHistogram(const histogram& hist)
{
  cout << "Histogram " << hist.name << ", counts " << hist.countAll << " elements " << endl;
  for (unsigned int i = 0; i < hist.count.size(); i++)
  {
    printf("range(%5.3f:%5.3f) value(%5.3f) |%5d| ",hist.levels[i],hist.levels[i+1], hist.levelsMean[i], hist.count[i]);
    for (int j = 0; j< (100*hist.count[i])/(hist.countAll); j++)
    {
      cout << "*" ;
    }
    cout << endl;
  } 
}

/**
 * \param x horizontal position
 * \param y vertical position
 * \param h height
 * \param w width
 * \param hist histogram to be plotted
 * \param ps postScript structure 
 * 
 * plot the histogram in a post script file (using postscript structure)
 * 
 */
void plotHistogram(double x, double y, double h, double w, histogram& hist, MMG2D_postscript* ps)
{
  fprintf(ps->file,"/Helvetica findfont\n");
  fprintf(ps->file,"%i scalefont setfont\n", int(min(h*0.15, 2*w/hist.name.size())));
  fprintf(ps->file,"%.6g %.6g moveto\n", x+w*0.025, y+h*0.825);
  MMG2D_setgrayColor(ps, 0.1);
  fprintf(ps->file,"(%s) show\n", hist.name.c_str());
  
  double bw;
  int cmax = maxvec(hist.count);
  bw = (w*0.95)/hist.count.size();
      
  MMG2D_setgrayColor(ps, 0.1);

  for (unsigned int i = 0; i <hist.count.size(); i++)
  {
    fprintf(ps->file,"%.6g %.6g %i %i (%3.2g) outputtext\n",  x+(i+0.3)*bw+w*0.025, y+h*0.075, int(bw/1.5), -45, hist.levelsMean[i]);
  }
  
  for (unsigned int i = 0; i <hist.count.size(); i++)
  {
    MMG2D_setgrayColor(ps, 1-float(hist.count[i])/(2*cmax)-0.5);
    fprintf(ps->file,"%.6g %.6g %.6g %.6g rectfill\n", x+i*bw+w*0.025, y+h*0.1, bw, (hist.count[i]*h*0.7)/cmax);
    if ((100.*hist.count[i])/hist.countAll>=5)
    {
      MMG2D_setgrayColor(ps, 0.9);
      fprintf(ps->file,"%.6g %.6g %i %i (%i%%) outputtext\n",  x+(i+0.3)*bw+w*0.025, y+h*0.1+(hist.count[i]*h*0.7)/(2*cmax)+int(bw/1.5), int(bw/1.5), -90, int((100*hist.count[i])/hist.countAll));
    }
  }
}


/**
 * \param vec vector of values
 * \return maximum value of the vector vec
 */
int maxvec(const vector <int>& vec)
{
  int m=0;
  if (vec.size() > 0)
    m = vec[0];
  for (unsigned int i = 1; i< vec.size(); i++)
  {
    m = max(vec[i], m);
  }
  return m;
}

void saveHist(vector<histogram>& vecHist, char* name)
{
  setlocale(LC_ALL, "en_US.UTF-8");
  string name_out;
  char name_out_temp[128];

  strcpy(name_out_temp, name);
  name_out = name_out_temp;
  size_t found = name_out.find_last_of(".");
  if (name_out.size()-found<7)
    name_out =  name_out.substr(0,found);

  found = name_out.find_last_of("/");
  string nameTemp=name_out.substr(found);
  if (name_out.find_first_of(nameTemp)==name_out.find_last_of(nameTemp))
      name_out += nameTemp;
  name_out += "_histogram";

  name_out += ".txt";

  ofstream file;
  file.open(const_cast < char* > (name_out.c_str()));

  for (int i = 0; i<vecHist.size(); i++)
  {
    file << "Histogram    " << vecHist[i].name << endl;
    file << "mean_value    "<< "count    "<< endl;
    for (int j = 0; j<vecHist[i].levelsMean.size(); j++)
    {
      file << vecHist[i].levelsMean[j] << "    " << vecHist[i].count[j] << endl;
    }
    file <<endl;
  }

  file.close();

}
