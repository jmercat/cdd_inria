#ifndef INOUT_H
#define INOUT_H

#include <vector>
#include "histogram.h"
#include "mmg/mmg2d/libmmg2d.h"

bool isfexist(const char *fileName);
void input_node(MMG5_pMesh mmgMesh, std::vector <double>& areas, double& ratioXoY, double &size, char* nomfichier);
void output_node(MMG5_pMesh mesh, std::vector <double>& areas, double size, char* nom, int nofichier);
void plotMeshCustom(int opt, MMG5_pMesh mesh, DataSol& sol, char* name, std::vector< std::vector<int> > adj, std::vector< std::vector<bool> > connection, std::vector<int> pointType, std::vector < std::vector <int> > grains, double lMean, int ndefaut, std::vector <histogram> vecHist, double ratioXoY );
void plotpoint( MMG5_pMesh mesh, char* name, double lMean, const std::vector<std::vector <std::vector <int> > >& partition);


#endif
