/**
 * \file grains.cpp
 * \brief Partitionning of crystalline block copolymer organisation into
 * grains of similar properties (orientation, stress).
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * Functions allowing definitions of boundary between different grains
 * of the crystalline organisation. And building a structure to associate
 * nodes with the grains they are in.
 * 
 */

#include "grains.h"
#include "adj.h"
#include "Edge.h"
#include "domainTools.h"
#include "vectorTools.h"
#include "debug.h"

#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif
#define pi 3.1415926

using namespace std;

/**
 * \param vec vector into which \a elem is to be found.
 * \param elem element which index is to be found in \a vec.
 * 
 * Finds the index of an element in a vector.
 *
 */
int getIndex( vector <int> vec, int elem)
{
  for (unsigned int i = 0; i < vec.size(); i++)
  {
    if (vec[i] == elem)
      return i;
  }
  return -1;
  //~ cout << "Element non present " << elem << endl;
}

/**
 * \param chain tree of connected nodes unfolding recursively.
 * \param chain0 initial tree of connected nodes.
 * \param adj adjacence table of nodes.
 * \param pathway vector filled recursively with adjacent nodes of the 
 * chain as it is unfolded.
 * 
 * Finds all the neighbors of a chain of nodes and store them in pathway.
 *
 */
void chainNeighbor(Dtree& chain, Dtree& chain0, const vector < vector <int> >& adj,  vector<int>& pathway)
{
  for(unsigned int i = 0; i<adj[chain.getNode()].size(); i++) // pathway des voisins du noeud de l'arbre
  {
    if(!chain0.isInTree(adj[chain.getNode()][i]))
    {
      insert(pathway,adj[chain.getNode()][i]);
    }
  }
  for(int i=0; i<chain.getSize(); i++)
  {
    chainNeighbor(chain.getBranch(i),chain0, adj, pathway);
  }
}

/**
 * \param chain tree of connected nodes filled recursively.
 * \param pathway vector of index of branches defining a pathway through the tree.
 * \param connection connection between nodes of the boundary of a grain.
 * \param adj adjacence table of nodes.
 * 
 * 
 * Finds all the neighbors of a chain of nodes and store them in pathway.
 *
 */
void getchain(Dtree& chain, vector <int> pathway, const vector< vector <bool> >& connection, const vector < vector < int > >& adj)
{
  Dtree subchain(chain.getBranch(pathway));
  vector <int> pathwayTemp(pathway);
  if (subchain.getValue() < 25)
  {
    int n = subchain.getNode();
    pathway.push_back(-1);
    int j;
    j = 0;
    for (unsigned int i = 0; i < connection[n].size(); i++)
    {
      //~ cout << n << " "  << adj[n][i] << " " << (connection[n][i] && !appartient_path(chain, adj[n][i])) << endl;
      if (connection[n][i] && !chain.isInTree(adj[n][i]))
      {
        chain.addBranch(adj[n][i], subchain.getValue()+1,pathwayTemp);
        pathway.back()=subchain.getSize()+j; 
        getchain(chain,pathway, connection, adj);
        j++;
      }
    }
  }
}


/**
 * \param mesh pointer toward the mmg mesh structure.
 * \param node index of one node that is a defect.
 * \param adj adjacence table of nodes.
 * \param connection connection between nodes of the boundary of a grain.
 * 
 * Computes the value of the burger vector of a dislocation or any chain of defects.
 * Warning: in some cases, the burger vector is not unique.
 *
 */
vector <double> BurgerVector(MMG5_pMesh mesh, int node, const vector < vector <int> >& adj,  const vector< vector <bool> >& connection)
{
  
  Dtree chain(node,0);
  vector <int> pathwaychain;
  getchain(chain, pathwaychain, connection, adj);
  //~ chain.print();
  
  vector <int> neighbors;
  Dtree chain0(chain);
  chainNeighbor(chain, chain0, adj, neighbors);
  
  for (unsigned int j = 1; j<neighbors.size(); j++)
  {
    for (unsigned int k = j; k<neighbors.size(); k++)
    {
      if(isInVect(adj[neighbors[j-1]],neighbors[k]))
      {
        int temp = neighbors[j];
        neighbors[j] = neighbors[k];
        neighbors[k] = temp;
        break;
      }
    }
  }
  cout << endl;
  for (int j =neighbors.size()-1; j>=0 ; j--)
  {
    cout << neighbors[j] <<endl; 
  }   
  cout << endl;

  int nodePrec, nodePrec2;
  vector <int> type(1,1);    
  vector <int> pathway(1,neighbors[0]);
  pathway.push_back(neighbors.back());
  nodePrec2 = neighbors[0];
  nodePrec = neighbors.back(); 
  neighbors.pop_back();
  int ip, in;
  
  double x, y;
  x = mesh->point[nodePrec2].c[0];
  y = mesh->point[nodePrec2].c[1];
  x = mesh->point[nodePrec].c[0]-x;
  y = mesh->point[nodePrec].c[1]-y;

  //~ for (int i = 0; i<adj[nodePrec].size(); i++)
  //~ {
    //~ int no = adj[nodePrec][i];
    //~ int ind = getIndex(neighbors,no);
    //~ if(ind > -1)
    //~ {
      //~ type.push_back(1);
      //~ nodePrec2 = nodePrec;
      //~ nodePrec = no;
      //~ pathway.push_back(no);
      //~ neighbors.erase(neighbors.begin()+ind);
    //~ }
  //~ }
  //~ int sizePrec = neighbors.size();
  cout << nodePrec2 << endl;
  cout << type.back() << endl;
  cout << nodePrec << endl;
  while(neighbors.size()>0)
  {
    in = -1;
    ip = -1;
    for (unsigned int j = 0; j<adj[nodePrec].size(); j++) // get the local indix of nodePrec2 around nodePrec
    {
      if(adj[nodePrec][j] == nodePrec2)
      {
        ip = j;
      }else if(adj[nodePrec][j] == neighbors.back())
      {
        in = j;
        if (in != -1 && ip != -1)
          break;
      }
      if (in != -1 && ip != -1)
        break;
    }
    cout << "____" << ip << " " << in << endl;
    int diffind = (6+ip-in)%6;
    if (type.back()>0)
    {
      if (diffind%2 == 0)
        type.push_back( -(type.back()+diffind-1)%3-1);
      else
        type.push_back(  (type.back()+diffind-1)%3+1);
    }else
    {
      if (diffind%2 == 0)
        type.push_back( (-type.back()+diffind-1)%3+1);
      else
        type.push_back( (type.back()-diffind+1)%3-1);
    }
    cout << type.back() << endl;
    cout << neighbors.back() << endl;
    nodePrec2 = nodePrec;
    nodePrec = neighbors.back();
    pathway.push_back(neighbors.back());
    neighbors.pop_back();
  }
  
  vector <int> decompte(3,0);
  cout << endl;
  for (unsigned int i = 0; i < type.size(); i++)
  {
    if (type[i]>0)
      decompte[type[i]-1]++;
    else
      decompte[-1-type[i]]--;
    cout << type[i] << " ";
  }
  cout << endl ;
  
  vector <double> burger(2,0);

  //~ double l = sqrt(x*x+y*y); 

  burger[0] = -decompte[0]*x;
  burger[1] = -decompte[0]*y;

  burger[0] += decompte[1]*(x/2+y/2);
  burger[1] += -decompte[1]*(x/2-y/2);
   
  burger[0] += decompte[2]*(x/2-y/2);
  burger[1] += decompte[2]*(x/2+y/2);
  
  return burger;
}

/**
 * \param chain tree of the already connected nodes to the node elem0.
 * \param adj adjacence table of nodes.
 * \param gradSol gradient of solution, known on edges: invert of weight of connection.
 * \param path tree structure containing current explored path throught the mesh.
 * \param pathWay indicates the closest adjacent node of the graph.
 * \param distance current weight of the explored path.
 * 
 * Finds the least expensive connection, from the current exploration tree
 * \a path, to any neighboring node that is not in the tree. Stops 
 * if all the connections are too expensive.
 *
 */
// elem0 is the element we are trying to connect, path contains the explored connections, pathway indicates the closest adjacent node of the graph, pathwayTemp saves the exploration in process
void dijkstraNext(Dtree& chain, const vector< vector <int> >& adj, const vector< vector<double> >& gradSol, const int& elem0, int& elem, Dtree& path0, Dtree& path, double& distance, vector<int>& pathway, vector<int> pathwayTemp )
{
  for(unsigned int i = 0; i<adj[path.getNode()].size(); i++) // pathway des voisins du noeud de l'arbre
  {
    
    if (distance > path.getValue() + 1./(gradSol[path.getNode()][i]+0.0001)+10 &&
      !path0.isInTree(adj[path.getNode()][i]) &&
      !chain.isInTree(adj[path.getNode()][i]))
    {
      distance = path.getValue() + 1./(gradSol[path.getNode()][i]+0.0001)+10;
      elem = adj[path.getNode()][i];
      pathway = pathwayTemp;
    }   
  }

  pathwayTemp.push_back(-1);
  for(int i = 0; i<path.getSize(); i++) // pathway of the sub-trees
  {
    pathwayTemp.back() = i;
    dijkstraNext(chain, adj, gradSol, elem0, elem, path0, path.getBranch(i), distance, pathway, pathwayTemp);
  }
}


/**
 * \param connection connection between nodes of the boundary of a grain.
 * \param adj adjacence table of nodes.
 * \param pointType 0 normal, 1 frontiere, 2 less than 6 neighbor, 3 more than 6 neighbor, 4 border.
 * \param gradSol gradient of solution, known on edges: invert of weight of connection.
 * \param path tree structure containing current explored path throught the mesh.
 * \param pathWay indicates the closest adjacent node of the graph.
 * \param distance current weight of the explored path.
 * 
 * Finds the least expensive connection, from the current exploration tree
 * \a path, to another node that is either on a grain boundary,
 * on the domain boundary or a defect. Stops before finding it if the 
 * connection path gets too expensive.
 *
 */
void dijkstraLoop(const vector< vector <bool> >& connection, const vector< vector <int> >& adj, const vector< int >& pointType, const vector< vector<double> >& gradSol, Dtree& path, vector <int>& pathway, double& distance)
{
  double weightMax;
  weightMax = distance;
  distance = 0;
  int type = 0, elem;   
  if (pointType[path.getNode()] == 4)
    cout << "dijkstraLoop: error, wrong pointType value" << endl;
  else
  {
    Dtree chain(path.getNode(),0);
    vector<int> pathwayChain;
    getchain(chain, pathwayChain, connection, adj);
    while ( (type == 0) && distance<weightMax)
    {   

      vector <int> pathwayTemp;
      pathwayTemp.clear();
      distance = HUGE_VAL;
      
      elem = -1;
      dijkstraNext(chain, adj, gradSol, path.getNode(), elem, path, path, distance, pathway, pathwayTemp);
      if (elem == -1) // all neighbors are connected
        return;
      path.addBranch(elem, distance, pathway);
      type = pointType[elem];

    }
  }
  pathway.push_back(elem);
}

/**
 * \param connection define for each node to which of the adjacent node it is connected.
 * \param adj adjacence table of nodes.
 * \param pointType 0 normal, 1 frontiere, 2 less than 6 neighbor, 3 more than 6 neighbor, 4 border.
 * \param path tree structure containing current explored path throught the mesh.
 * \param pathway contains the connections to be added reference to the tree \a path.
 * 
 * Adds a new line of connection to the connection structure. Pathway refers to
 * the index in the tree structure that contain the nodes index of the mesh structure.
 * \a connection' structure is the same as \a adj but do not contain nodes' index of the
 * mesh structure but wherease they are connected to another node.
 *
 */
void addConnection(vector< vector <bool> >& connection, const vector< vector <int> >& adj, vector< int >& pointType, Dtree& path, vector <int> pathway)
{
  int ind;
  if (pathway.size() == 0)
  {
    Debug(cout << "pathway nul" << endl;)
  }else if (pathway.size() == 1)
  {
    Debug(if ((unsigned int) (path.getNode()) >= adj.size() || path.getNode()<0)
      cout << "path errone " << path.getNode() << endl;)
    connection[path.getNode()][getIndex(adj[path.getNode()],pathway[0])] = true;
    ind = getIndex(adj[pathway[0]], path.getNode());
    if (ind != -1)        
      connection[pathway[0]][ind] = true;

  }
  else
  {   
    int i = pathway.back();
    connection[path.getNode()][getIndex(adj[path.getNode()],path.getNode(i))] = true;
    ind = getIndex(adj[path.getNode(i)], path.getNode());
    if (ind != -1)
      connection[path.getNode(i)][ind] = true;
    pointType[path.getNode(i)] = 1;
    pathway.pop_back();
    addConnection(connection, adj, pointType, path.getBranch(i), pathway);
  }
}


/**
 * \param connection connection between nodes of the boundary of a grain.
 * \param adj adjacence table of nodes.
 * \param pointType 0 normal, 1 frontiere, 2 less than 6 neighbor, 3 more than 6 neighbor, 4 border.
 * \param gradSol gradient of solution, known on edges: invert of weight of connection.
 * \param elem index of the node to be connected using dijkstra.
 * \param weightMax maximum weight accepted for a connection.
 * 
 * Finds the least expensive connection from the node \a elem to another
 * node that is either on a grain boundary, on the domain boundary or a defect.
 * Changes connection and pointType accordingly. Nothing done if such a connection
 * is too expensive.
 *
 */
void dijkstra(vector< vector <bool> >& connection, const vector< vector <int> >& adj, vector< int >& pointType, const vector< vector<double> >& gradSol, int elem, double weightMax)
{
  double distance;
  distance = weightMax;
  Dtree path(elem,0);
  vector <int> pathway;
  //~ Debug(cout << "dijkstra pour " << elem << endl;)
  dijkstraLoop(connection, adj, pointType, gradSol, path, pathway, distance);
  //~ Debug(cout << "ok" << endl;)
  if(distance < weightMax)
  {
    reverseVect(pathway);
    addConnection(connection, adj, pointType, path, pathway);
  }
}

/**
 * \param adj adjacence table of nodes.
 * \param pointType 0: normal, 1: frontiere, 2: less than 6 neighbor, 3: more than 6 neighbor, 4: border.
 * \param listPt list of nodes not yet assigned.
 * \param grain list of nodes contained in the current grain.
 * 
 * Find all the nodes in a specific grain by filling a list of the nodes.
 * Nodes are explored by spreading from one node to the adjacents.
 *
 */
void spreadGrain(const vector< vector <int> >& adj, const vector< int >& pointType, vector <int>& listPt, vector <int>& grain)
{
  int ind;
  int n = grain.back();
  for (unsigned int i = 0; i<adj[n].size(); i++)
  {
    ind = getIndex(listPt, adj[n][i]);
    if ( ind > -1 && insert(grain, adj[n][i]))
    {
      listPt.erase(listPt.begin()+ind);
      spreadGrain(adj, pointType,listPt, grain);
    }
  }
}

/**
 * \param adj adjacence table of nodes.
 * \param pointType 0: normal, 1: frontiere, 2: less than 6 neighbor, 3: more than 6 neighbor, 4: border.
 * \param grains vector of grain that are vectors of nodes contained in each grain.
 * 
 * Assign each non-boundary nodes (pointType=0) to a grain. Produces the
 * vector \a grains. 
 *
 */
void getGrains(const vector< vector <int> >& adj, const vector< int >& pointType, vector< vector <int> >& grains)
{
  vector <int> listPt;
  
  for (unsigned int i = 0; i<pointType.size(); i++)
  {
    if (pointType[i] == 0)
      listPt.push_back(i);
  }
  
  while( listPt.size()>0)
  {
    vector <int> temp(1,listPt.back());
    grains.push_back(temp);
    listPt.pop_back();
    spreadGrain( adj, pointType, listPt, grains.back());
  }
}

/**
 * \param grains vector of grain that are vectors of nodes contained in each grain.
 * \param pointType 0: normal, 1: frontiere, 2: less than 6 neighbor, 3: more than 6 neighbor, 4: border.
 * \param grainsList vector matching each node to the grain it is in.
 * 
 * Assign each node that belongs to a grain to the index of that grain.
 * The node of index \a i belongs to the grain \a grainsList[i].
 *
 */
void getGrainsList(const vector< vector <int> >& grains, const vector< int >& pointType, vector< int >& grainsList)
{
  grainsList.resize(pointType.size()); // pointType taken as reference because points that do not belong to a grain are likely to have a type != 0, this could be included in grainsList...
  fill(grainsList.begin(),grainsList.end(),-1);
  for (unsigned int i = 0; i < grains.size(); i++)
  {
    for (unsigned int j = 0; j < grains[i].size(); j++)
    {
      grainsList[grains[i][j]] = i;
    }
  }
}




/**
 * \param adjNode list of adjacent nodes to a central node.
 * \param anglePt orientation of the hexagonal organisation interpolated on the nodes.
 * \param grainsList vector matching each node to the grain it is in.
 * 
 * Assign the central node (center of \a adjNode) to the grain it should
 * be in. Chooses it according to the orientation of the organisation of the adjacent nodes.
 *
 */
int whichGrain(const vector <int>& adjNode, const vector <double>& anglePt, vector <int> grainsList, int gr)
{
  vector <int> nbNeighborGrain;
  vector <double> anglesNeighborGrain;
  vector <int> vec;
  int ind;
  for (unsigned int i = 0; i < adjNode.size(); i++)
  {
    if (insert(vec, grainsList[adjNode[i]]))
    {
      nbNeighborGrain.push_back(1);
      anglesNeighborGrain.push_back(anglePt[adjNode[i]]);
    }
    else
    {
      ind = getIndex(vec, grainsList[adjNode[i]]);
      if (ind > -1)
      {
        nbNeighborGrain[ind]++;
        anglesNeighborGrain[ind] += anglePt[adjNode[i]];
      }
    }
  }
  double min = 100;
  int indmin = 0;
  if (vec.size()<=0)
    return -1;
  for (unsigned int i = 0; i < vec.size(); i++)
  {
    anglesNeighborGrain[i] /= nbNeighborGrain[i];
    if( vec[i] != -1 && min > fabs(anglesNeighborGrain[i]- anglePt[gr]))
    {
      min = fabs(anglesNeighborGrain[i]- anglePt[gr]);
      indmin = i;
    }
  }
  return vec[indmin];
}

void verif(vector <int>& grainsList, int gr2, int gr1)
{
  for(unsigned int i = 0; i<grainsList.size(); i++)
  {
    if(grainsList[i]==gr2)
    {
      //~ cout << "Big bad problem " << i << endl;
      grainsList[i] = gr1;
    }
  }
}


/**
 * \param grainsList vector matching each node to the grain it is in.
 * \param grains vector of grain that are vectors of nodes contained in each grain.
 * \param anglesGrains vector of the mean angle of the grains.
 * \param gr1 index of the first grain to merge.
 * \param gr2 index of the second grain to merge.
 *
 * merge grains \a gr1 and \a gr2 in \a grainsList, \a grains and \a anglesGrains vectors
 *
 */
void mergeGrains(vector <int>& grainsList, vector< vector <int> >& grains, vector <double>& anglesGrains, int gr1, int gr2)
{
  if (gr1 == gr2 || gr2 >= (int) (grains.size()) || gr1 < 0)
  {
    return;
  }
  if (gr1 > gr2)
    mergeGrains(grainsList,grains,anglesGrains,gr2,gr1);
  else
  {
    unsigned int gr2Size = grains[gr2].size();
    unsigned int gr1Size = grains[gr1].size();
    grains[gr1].resize(gr1Size+gr2Size);
    for(unsigned int i = 0; i<gr2Size; i++)
    {
      //~ cout << gr2 << " " << i  << endl;
      //~ cout << grains[gr2][i] << endl;
      grainsList[grains[gr2][i]] = gr1;
      grains[gr1][gr1Size+i] = grains[gr2][i];
    }
    grains.erase(grains.begin()+gr2);
    anglesGrains[gr1] = (anglesGrains[gr1]*gr1Size +  anglesGrains[gr2]*gr2Size)/(gr1Size + gr2Size); 
    anglesGrains.erase(anglesGrains.begin()+gr2);
    
    //bugbug2 should not be needed
    verif(grainsList,gr2,gr1);
  }  
}

// bugbug1 does not seem to work
/**
 *
 * \param adj adjacence table of nodes.
 * \param grainsList vector matching each node to the grain it is in.
 * \param i index of the element to check.
 * \param gr1 index of the first grain.
 * \param gr2 index of the second grain.
 *
 * Recursively puts element \a i in listBoundary if it is a boundary between
 * grains \a gr1 and \a gr2. Stops when all shared boundary nodes have been found.
 *
 * bugbug1 does not seem to work
 *
 */
void lengthGrainBoundary(const vector <vector <int> >& adj, vector <int>& grainsList, int i, int gr1, int gr2, vector <int>& listBoundary)
{
  bool b1 = 0, b2 = 0;
  int j = 0;
  int nAdj = adj[i].size();
  while (!b1 && j<nAdj)
  {
    b1 = (grainsList[adj[i][j]]==gr1);
    j++;
  }
  while (!b2 && j<nAdj)
  {
    b2 = (grainsList[adj[i][j]]==gr2);
    j++;
  } 
  
  if (b1 && b2)
    if (insert(listBoundary,i))
    {
      for(j=0; j< nAdj; j++)
      {
        if (grainsList[i] == -1)
          lengthGrainBoundary( adj, grainsList, j, gr1, gr2, listBoundary);
      }
    }
}

/**
 * \param grainsList vector matching each node to the grain it is in.
 * \param grains vector of grain that are vectors of nodes contained in each grain.
 * \param anglePt vector of the angles on each node.
 * \param adj adjacence table of nodes.
 *
 * merge grains \a gr1 and \a gr2 in \a grainsList, \a grains and \a anglesGrains vectors
 *
 * bugbug using a correction that should not be needed...
 *
 */
void mergeAllGrains(vector <int>& grainsList, vector< vector <int> >& grains,const vector <double>& anglePt,const vector <vector <int> >& adj)
{
  int gr1,gr2, nAdj,j;
  double threshold=0.30;
  double cosMean;
  double sinMean;

  // compute mean angle for each grain
  vector <double> anglesGrains(grains.size(),0);
  for (unsigned int i = 0; i <grains.size(); i++)
  {
    cosMean = 0;
    sinMean = 0;
    for (unsigned int k = 0; k<grains[i].size(); k++)
    {
      cosMean += cos(anglePt[grains[i][k]]);
      sinMean += sin(anglePt[grains[i][k]]);
    }
    anglesGrains[i]=atan2(sinMean,cosMean);
  }
  
  //test each node for being a boundary
  for (unsigned int i = 0; i < grainsList.size(); i++)
  {
    if (grainsList[i] == -1)
    {
      nAdj = adj[i].size();
      j = 0;
      gr1 = -1;
      gr2 = -1;
      while (gr1 ==-1 && j<nAdj)
      {
        gr1 = grainsList[adj[i][j]];
        j++;
      }
      while ((gr2 ==-1 || gr2==gr1) && j<nAdj)
      {
        gr2 = grainsList[adj[i][j]];
        j++;
      } 
      if (gr1!=gr2 && gr1 !=-1 && gr2!=-1)
      {
        //~ vector <int> listBoundary;
        //~ lengthGrainBoundary(adj, grainsList, i,gr1,gr2,listBoundary);
        double distAngles = fabs(anglesGrains[gr1]-anglesGrains[gr2]);
        distAngles = min(distAngles, 1-distAngles)*sqrt(min(grains[gr1].size(),grains[gr2].size()));
        
        if (distAngles<threshold)
        {
          mergeGrains(grainsList,grains, anglesGrains, gr1,gr2);

          //bugbug2 should not be needed but simple correction does not work for an unknown reason
          getGrainsList(grains,grainsList,grainsList);
        }
      }
    }
  }
}


/**
 * \param adj adjacence table of nodes.
 * \param pointType 0: normal, 1: frontiere, 2: less than 6 neighbor, 3: more than 6 neighbor, 4: border.
 * \param anglePt orientation of the hexagonal organisation interpolated on the nodes.
 * \param grains vector of grain that are vectors of nodes contained in each grain.
 * 
 * Assign boundary nodes to the adjacent grain that has the same orientation angle.
 *
 */
void convertBoundary(const vector< vector <int> >& adj, const vector< int >& pointType,  const vector <double>& anglePt, vector< vector <int> >& grains)
{
  int gr;
  vector <int> grainsList;
  vector <int> retry;
  getGrainsList(grains, pointType, grainsList);
  mergeAllGrains(grainsList, grains, anglePt, adj);
  getGrainsList(grains, pointType, grainsList);

//  cout << "convert Boundary " << grainsList.size()<< endl;
  for (unsigned int i = 0; i < grainsList.size(); i++)
  {
    if (grainsList[i] == -1)
    {
//      cout << "node " << i << "is boundary"<<endl;
      // bugbug whichGrain should NOT return number of grains that does not exist
      // problem in grainsList or in the function
      gr = whichGrain( adj[i], anglePt, grainsList, i);
      Debug(if (gr >= (int) (grains.size()))
        cout << "Output of whichGrain too big " << gr << ">=" << grains.size() << endl;)
      if (gr != -1 && gr < (int) (grains.size()))
      {
//        cout << gr << endl;
        grains[gr].push_back(i);
      }else
        retry.push_back(i);
    }
  } 
  if (retry.size() > 5)
     convertBoundary(adj, pointType, anglePt, grains);
   
}

/**
 * \param connection connection between nodes of the boundary of a grain.
 * \param adj adjacence table of nodes.
 * \param pointType 0 normal, 1 frontiere, 2 less than 6 neighbor, 3 more than 6 neighbor, 4 border.
 * \param gradSol gradient of solution, known on edges: invert of weight of connection.
 * \param gradSolMean mean value of precedent.
 * 
 * 
 * Function modifying connection and pointType to define the grains 
 * boundary. A boundary is a connection of defects throught least 
 * expensive way throught the mesh.  
 *
 */
void boundaryGrains(vector< vector <bool> >& connection, const vector< vector <int> >& adj, vector< int >& pointType, const vector< vector<double> >& gradSol, double gradSolMean)
{
  int nConnect;
  vector <double> gradAnglePt;
  edgeToVec(gradAnglePt, gradSol);
  //~ normalizeVecStdMed(gradAnglePt);
  for(unsigned int k = 1; k<=5; k++)
  {
    for (unsigned int i = 0; i< adj.size(); i++)
    {
      if (gradAnglePt[i] > 0.15)
        pointType[i] = 1;
      nConnect = 0;
      if (pointType[i] != 4 && pointType[i] != 0)
      {
        for (unsigned int j = 0; j<adj[i].size(); j++)
        {
          if(connection[i][j])
            nConnect++;
        }
        if(nConnect<2 && gradAnglePt[i] > 0.05)
        {
          dijkstra(connection, adj, pointType, gradSol, i,k*200);
        }
      }
    }
  }
}

/**
 * \param mesh mmg mesh structure (simple delaunay triangulation of the nodes).
 * \param adj adjacence table of nodes.
 * \param connection connection between defect nodes (!=6 neighbors) of different defects.
 * \param lMean mean distance between nodes.
 * \param ratioXoY square rooot of ratio of width over height of the image
 * 
 * 
 * Connects the defects (nodes with a number of neighbors !=6). A defect is connected to
 * an adjacent opposit defect only ( >6 with <6).
 *
 */
void getConnection(MMG5_pMesh mesh,const vector< vector<int> >& adj, vector< vector<bool> >& connection, const double& lMean, double ratioXoY )
{
  int nAdji, nAdjj;
  for (int i = 1; i<= mesh->np; i++)
  {
    if (!isBorder(mesh, i, lMean, ratioXoY))
    {
      nAdji = adj[i].size();
      if (nAdji != 6)
      {
        for (unsigned int j = 0; j< adj[i].size(); j++)
        {
          nAdjj = adj[adj[i][j]].size();
          if (nAdjj != 6)
          {
            if(nAdji>6 && nAdjj<6)
            {
              connection[i][j] = true;
              
            }else if(nAdji<6 && nAdjj>6)
            {
              connection[i][j] = true;
              
            }
          }
        }
      }
    }
  }
}

/**
 * \param adj adjacence table of nodes.
 * \param connection connection between defect nodes (!=6 neighbors) of different defects.
 * 
 * 
 * Initialisation of the size of connection vector. Set the values to false.
 *
 */
void initConnection(const vector< vector<int> >& adj, vector< vector<bool> >& connection)
{
  for (unsigned int i = 0; i<adj.size(); i++)
  { 
    for (unsigned int j = 0; j< adj[i].size(); j++)
    {
      connection[i].push_back(false);
    }
  }
}
