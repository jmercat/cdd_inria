/**
 * \file delaunay.cpp
 * \brief Analysis of crystalline block copolymer organisation.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>
#include <iostream>
#include <fstream>
#include <sstream>
  #include <sys/types.h>
   #include <sys/stat.h>
   #include <fcntl.h>  
   //#include <unistd.h>
#include <ctime>
#include <locale>

using namespace std;

#include "grains.h"
#include "histogram.h"
#include "Edge.h"
#include "adj.h"
#include "radialDistrib.h"
#include "inOut.h"
#include "anglesTools.h"
#include "domainTools.h"
#include "vectorTools.h"
#include "potential.h"
#include "delaunay.hpp"
#define pi 3.1415926

/**
 * \param mesh pointer on mmg mesh structure.
 * \param adj vector matching nodes with the adjacent nodes.
 * \param triangleData data known on triangle positions.
 * \param pointData output of data interpolated on nodes.
 * 
 * Interpolate data on nodes based on data on triangles.
 *
 */
void triangleToPt(MMG5_pMesh mesh, vector< vector<int> >& adj, vector<double>& triangleData, vector <double>& pointData)
{ 
  int npt;
  if (triangleData.size() != (unsigned int) (mesh->nt+1))
  {
    cout << "Input vector in setFromTriangle not initialized properly" << endl;
    return;
  }
  pointData.resize(mesh->np+1, 0);
  for(int i = 1; i<= mesh->nt; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      npt = mesh->tria[i].v[j];
      //~ cout << npt << endl;
      pointData[npt] += triangleData[i]/adj[npt].size();
    }
  }
}

/**
 * \param mmgMesh pointer on mmg mesh structure.
 * \param mmgSol pointer on mmg sol structure.
 * \param sol output data on either nodes, triangles or edges.
 * \param cylindersArea input of nodes' surface.
 * \param argc parameter transfered from main.
 * \param argv parameter transfered from main.
 * \param lMean output mean length seperating two nodes.
 * \param nDefects output number of defects (!=6 neighbors).
 * 
 * Function performing the analysis of nodes disposition: defects,
 * orientation, global organisation, grains partition.
 *
 */
void analyseMesh(MMG5_pMesh mmgMesh, MMG5_pSol mmgSol, DataSol& sol, vector <double> cylindersArea, int argc, char **argv, double& lMean, int& nDefects, double*& distribParam,allExchange& dataOut)
{
  
  vector< vector<int> > adj(mmgMesh->np+1,vector<int>(0));
  vector< vector<bool> > connection(mmgMesh->np+1,vector<bool>(0));
  vector<double> angle(mmgMesh->np+1,0);
  vector< vector<double> > gradAngle(mmgMesh->np+1,vector<double>(0)); 
  vector< vector<double> > gradSol(mmgMesh->np+1,vector<double>(0));
  vector<int> pointType(mmgMesh->np+1,0);
  double gradSolMean;
  int nPointCenter;
  //~ lMean = meanSpacing(mmgMesh);
  //~ lMean = sqrt(2/(sqrt(3.)*mmgMesh->np));
  double totalArea = lMean>1 ? 1./lMean:lMean;
  double ratioXoY = lMean;
  lMean = sqrt(2./(sqrt(3.)*mmgMesh->np))*totalArea;
  
  cout << " Mean spacing " << lMean << endl; 
  dataOut.delaunay.meanDistance = lMean;
  dataOut.delaunay.numberOfPoints = mmgMesh->np;
  dataOut.delaunay.numberOfTriangles = mmgMesh->nt;

  // get point adjacence list
  getAdj(mmgMesh, adj);

  // get distribution (one of the longest step)
  vector<double> g;
  vector<double> g6;
  double rMax = 14*lMean;
  radialDistribution(mmgMesh, g, g6, adj, sqrt(mmgMesh->np)*100*rMax, rMax);
  smoothVec2(g,5);
  smoothVec2(g6,9);
  distribParam = radialDistributionOut(g,g6,rMax,lMean,1./(sqrt(mmgMesh->np)*100));
  dataOut.delaunay.expFit[0] = distribParam[0];
  dataOut.delaunay.expFit[1] = distribParam[1];
  dataOut.delaunay.powFit[0] = distribParam[2];
  dataOut.delaunay.powFit[1] = distribParam[3];
  // get triangles angle list
  getAngle(mmgMesh, adj, angle);
  sol.setFromVector(angle);
  
   
  sol.getGrad(adj, gradSol, gradSolMean,1);
  cout << " gradSolMean " << gradSolMean << " " << 10./pow(gradSolMean,3) << endl;
   
  getPointType(mmgMesh, adj, pointType, lMean, nDefects,nPointCenter,ratioXoY);
  dataOut.delaunay.numberOfDefects = nDefects;
  dataOut.delaunay.numberOfPointsCenter = nPointCenter;
  dataOut.delaunay.defectDensity = ((double) (nDefects))/nPointCenter*100;
  initConnection(adj, connection);
  getConnection(mmgMesh, adj, connection, lMean, ratioXoY);
  
  char nom_out[512];
  string nom_out_temp;
  if (argc>1)
  {
    strcpy(nom_out, argv[1]);
    nom_out_temp = nom_out;
  }
  size_t found = nom_out_temp.find_last_of(".");
  if (nom_out_temp.size()-found<7)
    nom_out_temp =  nom_out_temp.substr(0,found);
  found = nom_out_temp.find_last_of("/");
  string nameTemp=nom_out_temp.substr(found);
  found = nom_out_temp.find(nameTemp);
  if (nom_out_temp.find(nameTemp,found+1)== string::npos)
    nom_out_temp += "/"+nameTemp;

  strcpy(nom_out, nom_out_temp.c_str());
  sprintf(nom_out, "%s_out.ps", nom_out);

  int opt = -1;
  if (argc > 3)
    opt = atoi(argv[3]);
  //opt *= 3*5*11;
  vector < vector <int> > grains;
//  vector<histogram> vecHist;
  //~ Debug(bool testetsets;
  //~ for (unsigned int i = 0; i< connection.size(); i++)
  //~ {
    //~ testetsets = false;
    //~ for (unsigned int j = 0; j< connection[i].size(); j++)
    //~ {
      //~ if (connection[i][j])
      //~ {
        //~ cout << i << "  " << adj[i][j] << " | " ;
        //~ testetsets = true;
      //~ }
    //~ }
    //~ if (testetsets)
      //~ cout << endl;
  //~ })

  
  //~ MMG2D_plotMesh(mmgMesh,const_cast < char* > ("mesh_out"));
   
   
  histogram histSol;
  histSol.name = "Orientation";
  histSol.levels.resize(10);
  getHistogramSol(mmgMesh, sol, histSol);
  for (unsigned int i = 0; i < histSol.levelsMean.size(); i++)
    histSol.levelsMean[i] *= 180./pi;
  //~ printHistogram(histSol);
  
  dataOut.delaunay.vecHist.push_back(histSol);
  
  
  double minArea, maxArea;
  histogram histArea;
  histArea.name = "Cylinders' area";
  histArea.levels.resize(15);
  normalizeVec(cylindersArea, pointType, minArea, maxArea);
  getHistogramVec(cylindersArea, histArea);
  if (dataOut.image.dimension>0)
  {
    double areaUnit=dataOut.image.dimension*dataOut.image.dimension;
    unnormalizeHistogramLevels(histArea, minArea, maxArea,areaUnit);
  }else
    unnormalizeHistogramLevels(histArea, minArea, maxArea);
  //~ printHistogram(histArea);
  
  dataOut.delaunay.vecHist.push_back(histArea);
  
  

  vector <double> nearestNeighbor;
  histogram histNearestNeighbor;
  histNearestNeighbor.name = "Nearest neighbor distance";
  histNearestNeighbor.levels.resize(15);
  nearestNeighborDist(mmgMesh, adj, nearestNeighbor);
  double stdNearestNeighbor = standardDev(nearestNeighbor, lMean);
  cout << " Moyenne des distance " << lMean << " ecart-type " << stdNearestNeighbor << endl;
  double meanNearestNeighbor = getMean(nearestNeighbor);
  normalizeVecStd(nearestNeighbor);
  getHistogramVec(nearestNeighbor, histNearestNeighbor);
  if (dataOut.image.dimension>0)
  {
    double lengthUnit = dataOut.image.dimension;
    unnormalizeHistogramLevelsStd(histNearestNeighbor, meanNearestNeighbor, stdNearestNeighbor,lengthUnit);
  }else
    unnormalizeHistogramLevelsStd(histNearestNeighbor, meanNearestNeighbor, stdNearestNeighbor);

  //~ printHistogram(histNearestNeighbor);
  
  dataOut.delaunay.vecHist.push_back(histNearestNeighbor);

//################################################$$$$
  
  MMG5_pPoint meshTemp;
  meshTemp = new MMG5_Point[mmgMesh->np+1];
  vector< vector<double> > deformationOut(mmgMesh->np+1,vector<double>(3,0));
  getDeformationEnergy(mmgMesh, adj,angle,lMean, deformationOut);
  
  memcpy(meshTemp, mmgMesh->point,(mmgMesh->np+1)*sizeof(MMG5_Point)); 
  relaxPosition(mmgMesh, adj); // better grain definition but looses information with distortion 
 
  //~ getPotential(mmgMesh, adj, sol);
  getAngle(mmgMesh, adj, angle);
  sol.setFromVector(angle);
 
  //~ sol.normalizeStd();
  sol.getGrad(adj, gradSol, gradSolMean,1);
  cout << "Grains boundary" << endl;
  boundaryGrains(connection, adj, pointType, gradSol, gradSolMean);

  getGrains(adj, pointType, grains);
  convertBoundary(adj, pointType, angle, grains);
  //~ plotMeshCustom(opt, mmgMesh, sol, nom_out, adj, connection, pointType, grains, lMean, nDefects, vecHist);
  memcpy(mmgMesh->point,meshTemp,(mmgMesh->np+1)*sizeof(MMG5_Point)); // recover real positions

  delete[] meshTemp;
  //~ cout << "Nombre de grains " << grains.size() << endl;
  
  //~ sol.setFromTriangle(mmgMesh, adj, angle);
  sol.setFromVector(angle);
  sol(0)*=180./pi;
  sol.setName(const_cast <char*> ("Orientation"));
  

  //~ char nameSol[512];
  //~ sprintf(nameSol,const_cast<char*> ("Orientation"));
  //~ MMG2D_Set_outputSolName(mmgMesh, mmgSol, nameSol);
  //~ double distPart =  3*lMean;

  
  //~ cout << "##############################Option recue " << opt << endl;
   
  //~ edgeToSol(mmgMesh, mmgSol, gradSol);  
  
  //~ double minDist, maxDist;
  vector <double> meanDist;
  neighborDist(mmgMesh, adj, meanDist);    

  //~ double aire1 = 0, aire2 = 0, aire3 = 0;
  double aire2 = 0;
  int nGrains=0;
  for (unsigned int i = 0; i < grains.size(); i++)
  {
    if (grains[i].size()> 30)
      nGrains++;
    aire2 += (double) (grains[i].size())/mmgMesh->np;
    //~ }
  }
  dataOut.delaunay.numberOfGrains = nGrains;
  //~ cout <<  " Surface du grain, calcul par point " << aire1 << " calcul par nombre de point " << aire2 << " calcul par triangles " << aire3 << endl;
    

  normalizeVecStd(meanDist);
  if (argc > 2)
  {
    if (strcmp(argv[2],const_cast < char* > ("l"))==0)
    {
      getPotential(mmgMesh, adj, sol);
      double sol0 = sol(0);
      sol.getGrad(adj, gradSol, gradSolMean,0);
      sol.setFromEdge(mmgMesh,gradSol);
      sol.normalizeStd();
      sol(0) = sol0;
      sol.setName(const_cast<char*> ("Potential"));
    }else if (strcmp(argv[2],const_cast < char* > ("d01"))==0)
    {
      sol.setFromVector(deformationOut,0,1);
      sol.normalizeStd();
      sol.setName(const_cast<char*> ("Expansion"));
    }else if (strcmp(argv[2],const_cast < char* > ("d2"))==0)
    {
      sol.setFromVector(deformationOut,2);
      sol.normalizeStd();
      sol.setName(const_cast<char*> ("Shear\ndeformations"));
    }else if (strcmp(argv[2],const_cast < char* > ("d0"))==0)
    {
      sol.setFromVector(deformationOut,0);
      sol.normalizeStd();
      sol.setName(const_cast<char*> ("x\ndeformations"));
    }else if (strcmp(argv[2],const_cast < char* > ("d1"))==0)
    {
      sol.setFromVector(deformationOut,1);
      sol.normalizeStd();
      sol.setName(const_cast<char*> ("y\ndeformations"));
    }
  }
  //~ sol.setFromVector(deformationOut,0,1);
  //~ sol.normalizeStd();
  //~ sol.setName(const_cast<char*> ("Volume\ndeformations"));
  //~ sol.setFromVector(deformationOut,2);
  //~ sol.normalizeStd();
  //~ sol.setName(const_cast<char*> ("Shear\ndeformations"));
  cout << "plotMesh" << endl;
  plotMeshCustom(opt, mmgMesh, sol, nom_out, adj, connection, pointType, grains, lMean, nDefects, dataOut.delaunay.vecHist,ratioXoY);
  cout << "plotMesh ok" << endl;
  sol.setSol(mmgSol, mmgMesh);
  MMG2D_plotMeshColor(mmgMesh,mmgSol,const_cast < char* > ("color_mesh_out"));
  cout << "parameter of distribution : " << distribParam << endl;
  if (argc>1)
  {
    strcpy(nom_out, argv[1]);
    nom_out_temp = nom_out;
  }
  found = nom_out_temp.find_last_of(".");
  if (nom_out_temp.size()-found<7)
    nom_out_temp =  nom_out_temp.substr(0,found);
  found = nom_out_temp.find_last_of("/");
  nameTemp=nom_out_temp.substr(found);
  found = nom_out_temp.find(nameTemp);
  if (nom_out_temp.find(nameTemp,found+1)== string::npos)
    nom_out_temp += "/"+nameTemp;

  strcpy(nom_out, nom_out_temp.c_str());

  saveHist(dataOut.delaunay.vecHist, nom_out);
}



int mainDelaunay(int argc, char **argv, allExchange& dataOut)
{
  setlocale(LC_ALL, "en_US.UTF-8");
  double time_init = time(0);
  MMG5_pMesh      mmgMesh;
  MMG5_pSol       mmgSol;
  int             ier;

  mmgMesh = NULL;
  mmgSol  = NULL;
  MMG2D_Init_mesh(MMG5_ARG_start, MMG5_ARG_ppMesh, &mmgMesh, MMG5_ARG_ppMet, &mmgSol, MMG5_ARG_end);
  
  // prevent insertion, moving the points and swaping edges
  MMG2D_Set_iparameter(mmgMesh, mmgSol, MMG2D_IPARAM_noinsert,1);
  MMG2D_Set_iparameter(mmgMesh, mmgSol, MMG2D_IPARAM_nomove,1);
  MMG2D_Set_iparameter(mmgMesh, mmgSol, MMG2D_IPARAM_noswap,1);
  
  // get the name of the file containing the points
  char nom[512],nomt[512];
  if(argc>1)
  {
    strcpy(nom,argv[1]);
  }else
  {
    while(!isfexist(nomt))
    {
      cout << "Name of file to process : " ;
      cin >> nomt;
      strcpy(nom,nomt);
      cout << endl;
    }       
  }
  
  vector <double> cylindersArea;
  double ratioXoY;
  double size;
  input_node(mmgMesh, cylindersArea, ratioXoY, size, nom);
 
  ier = MMG2D_mmg2dmesh(mmgMesh,mmgSol);
  
  if ( ier == MMG5_STRONGFAILURE ) 
  {
    fprintf(stdout,"BAD ENDING OF MMG3DLIB: UNABLE TO SAVE MESH\n");
    return(ier);
  } else if ( ier == MMG5_LOWFAILURE )
    fprintf(stdout,"BAD ENDING OF MMG3DLIB\n");
    
  
  DataSol sol(mmgMesh->np+1);
  
  
  double lMean = ratioXoY;
  int nDefects;
  double *distribParam;
  if (dataOut.image.dimension<=0)
      dataOut.image.dimension = size;
  analyseMesh(mmgMesh, mmgSol, sol, cylindersArea, argc, argv, lMean, nDefects, distribParam, dataOut);
  cout << "lMean " << lMean << endl;
  char* name = (char*)malloc(512*sizeof(char));
  vector< vector < vector < int> > > partition;
  
  //~ double distPart =  3*lMean;
  //~ getPartition(mmgMesh, distPart, partition);
  
  sol.setSol(mmgSol, mmgMesh);
  if ( MMG2D_saveMesh(mmgMesh,const_cast < char* > ("result.mesh")) != 1 )
    exit(EXIT_FAILURE);
  
  cout << "Temps de calcul " << time(0)-time_init << "s" << endl;
  ofstream log;
  if (isfexist(const_cast < char* > ("./log.log")))
  {
    ifstream log_in;
    log_in.open(const_cast < char* > ("./log.log"));
    
    string buff;
    string line;
    for (int i = 0; i < 6; i++)
    {
      std::getline(log_in, line);
      buff+=line+ "\n";
    }
    log_in.close();
    //~ log.open(const_cast < char* > ("log.log"),ofstream::app);
    log.open(const_cast < char* > ("log.log"));
    log << buff;
    log << mmgMesh->nt << endl;
    log << float(nDefects)/mmgMesh->np << endl;
    log << distribParam[1] << "    "<< distribParam[2] << endl;
    log << distribParam[3] <<"    "<< distribParam[4] << endl;
    log.close();
  }else
  {
     log.open(const_cast < char* > ("log.log"));
     log << mmgMesh->np << endl;
     log << "NA" << endl;
     log << "NA" << endl;
     log << "NA" << endl;
     log << "NA" << endl;
     log << "NA" << endl;        
     log << mmgMesh->nt << endl;
     log << float(nDefects)/mmgMesh->np << endl;
//     cout << "#A#A#A#A#A#A#A#A#2 " << nDefects << " " << float(nDefects)/mmgMesh->np << endl;
     log << distribParam[1] << "    "<<distribParam[2] << endl;
     log << distribParam[3] << "    "<<distribParam[4] << endl;
     log.close();
  }
  cout << "lMean " << lMean << endl;

  ofstream ndefect;
  ndefect.open("history.dat", ofstream::app);
  ndefect << float(nDefects)/mmgMesh->np*100. << "    " << distribParam[0] << "    " << mmgMesh->np << "    "   << distribParam[1] << "    " << distribParam[2]<< "    "   << distribParam[3] << "    " << distribParam[4] << endl;
  ndefect.close();
  MMG2D_Free_all(MMG5_ARG_start, MMG5_ARG_ppMesh, &mmgMesh, MMG5_ARG_ppMet, &mmgSol, MMG5_ARG_end);
  free(name);
  delete[] distribParam;
  return 0;
}
