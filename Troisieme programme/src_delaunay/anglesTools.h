#ifndef ANGLESTOOLS_H
#define ANGLESTOOLS_H

#include <vector>
#include "mmg/mmg2d/libmmg2d.h"

double angleTr(MMG5_pMesh mesh, int ntria);
double getAngleSegment(MMG5_pMesh mesh, int npt1, int npt2);
void getAngle(MMG5_pMesh mesh, std::vector < std::vector <int> > adj, std::vector<double>& angle);
void getGradAngle(MMG5_pMesh mesh, const std::vector< std::vector<int> >& adj, std::vector< std::vector<double> >& gradAngle, double& ga_mean);



#endif
