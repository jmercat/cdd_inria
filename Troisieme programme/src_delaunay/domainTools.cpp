/**
 * \file domainTools.cpp
 * \brief Functions in relation with the organisation in the domain.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */

#include <math.h>
#include <iostream>
#include "domainTools.h"
#include "vectorTools.h"
#define pi 3.1415926
#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif
using namespace std;

/**
 * \param mesh mesh structure
 * \param npt index of a node in the mesh
 * \param h distance given
 * \param ratioXoY square rooot of ratio of width over height of the image
 * \return boolean telling if node is close to the borders
 * 
 * Test if the node of index npt is at a distance inferior than h to a border.
*/
bool isBorder(MMG5_pMesh mesh, int npt, double h, double ratioXoY)
{
  // get borders of the image (with offset for rectangular images)
  double ratioXoY2 = ratioXoY*ratioXoY;
  double offsetx = ratioXoY>1. ? 0:((1.-(ratioXoY2))/2);
  double offsety = ratioXoY<1. ? 0:((1.-(1./ratioXoY2))/2);
  double ratiox = ratioXoY>1. ? 1:1./ratioXoY; // ratio because in case of rectangular images, the image processing produce errors on a dilated border
  double ratioy = ratioXoY<1. ? 1:ratioXoY;

  // get coordinates of the point to check
  double x = mesh->point[npt].c[0];
  double y = mesh->point[npt].c[1];

  // check every borders
  bool left = x>(h*ratiox+offsetx);
  bool bottom = y>(h*ratioy+offsety);
  bool right = x<(1-h*ratiox-offsetx);
  bool top = y<(1-h*ratioy-offsety);

  bool res = !(left && bottom && right && top);

  return res;
}

/**
 * \param mesh mmg mesh structure
 * \param ratioXoY square rooot of ratio of width over height of the image
 * \return mean spacing between the nodes of the mesh structure
 * 
 * Do not includes nodes that are too close or too far from each other
 * 
*/
double meanSpacing(MMG5_pMesh mesh, double ratioXoY)
{
  double lMean = 0,l1,l2,l3;
  double x1, y1, x2, y2, x3, y3;
  int npt1, npt2, npt3, nptall = 0;
  //~ double h = sqrt(1./(sqrt(3.)/2*mesh->np)); // normal edge length if perfectly hexagonal
  double h = 1./sqrt(mesh->np*cos(pi/6)); // normal edge length if perfectly hexagonal
  
  for(int i = 1; i<= mesh->nt; i++)
  {
    npt1 = mesh->tria[i].v[0];
    npt2 = mesh->tria[i].v[1];
    npt3 = mesh->tria[i].v[2];
    if (!isBorder(mesh, npt1, h, ratioXoY) &&
        !isBorder(mesh, npt2, h, ratioXoY) &&
        !isBorder(mesh, npt3, h, ratioXoY))
    {
      x1 = mesh->point[npt1].c[0];
      y1 = mesh->point[npt1].c[1];
      x2 = mesh->point[npt2].c[0];
      y2 = mesh->point[npt2].c[1];
      x3 = mesh->point[npt3].c[0];
      y3 = mesh->point[npt3].c[1];
      
      l1 = sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));
      l2 = sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3));
      l3 = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
      
      //~ if(l1> h/3 && l1 < 3*h)
      //~ {
        lMean += l1;
        nptall++;
      //~ }
      
      //~ if(l2> h/3 && l2 < 3*h)
      //~ {
        lMean += l2;
        nptall++;
      //~ }
      
      //~ if(l3> h/3 && l3 < 3*h)
      //~ {
        lMean += l3;
        nptall++;
      //~ }
    }
  }
  
  lMean /= nptall;
  return lMean;
}


/**
 * \param mesh mmg mesh structure
 * \param listPt list of nodes
 * \return area covered by the given list of nodes
 * 
 * Sum of the area of triangles formed by adjacent nodes in the list
 * (the division by 6 comes from each triangle being counted 3 times and
 * the normal division by 2 of vector product)
 * 
*/
double getArea2(MMG5_pMesh mesh, const vector <int>& listPt)
{
  double area = 0;
  double x, y, x1, y1, x2, y2;
  for (int i = 1; i<= mesh->nt; i++)
  {
      
    int isGrain = 0;
    
    for(int j = 0; j < 3; j++)
    {
      int npt = mesh->tria[i].v[j];
      //~ cout<< " uinres " << npt << endl;
      if(isInVect(listPt, npt))
        isGrain++;
    }
    if (isGrain>0)
    {
      x = mesh->point[mesh->tria[i].v[0]].c[0];
      y = mesh->point[mesh->tria[i].v[0]].c[1];
  
      x1 = mesh->point[mesh->tria[i].v[1]].c[0]-x;
      y1 = mesh->point[mesh->tria[i].v[1]].c[1]-y;
      x2 = mesh->point[mesh->tria[i].v[2]].c[0]-x;
      y2 = mesh->point[mesh->tria[i].v[2]].c[1]-y;
      
      area += isGrain*(fabs(x1*y2-y1*x2)/6);
    }
  }
  return area;
}

/**
 * \param mesh mmg mesh structure
 * \param adj adjacence table of the nodes
 * \param NND nearest neighbor distance of each node
 * 
 * Store each node's nearest neighbor distance in the vector NND.
 * 
*/
void nearestNeighborDist(MMG5_pMesh mesh, const vector <vector <int> >& adj, vector <double>& NND)
{
    NND.resize(adj.size());
    double x, y, l, x1, y1, lmin;
    for (unsigned int i = 1; i < adj.size(); i++)
    {
        x = mesh->point[i].c[0];
        y = mesh->point[i].c[1];
        lmin = 1;
        for (unsigned int j = 0; j < adj[i].size(); j++)
        {
            x1 = mesh->point[adj[i][j]].c[0]-x;
            y1 = mesh->point[adj[i][j]].c[1]-y;
            l = x1*x1+y1*y1;
            lmin = min(lmin,l);
        }
        NND[i] = sqrt(lmin);
    }
}

/**
 *
 * \param mesh mmg mesh structure
 * \param adj adjacence table of the nodes
 * \param ND mean neighbor distance  
 * 
 * Store the mean neighbor distance of each node in the vector ND
 * 
*/
void neighborDist(MMG5_pMesh mesh, const vector <vector <int> >& adj, vector <double>& ND)
{
    ND.resize(adj.size(),0);
    double x, y, l, x1, y1;
    for (unsigned int i = 1; i < adj.size(); i++)
    {
        x = mesh->point[i].c[0];
        y = mesh->point[i].c[1];
        for (unsigned int j = 0; j < adj[i].size(); j++)
        {
            x1 = mesh->point[adj[i][j]].c[0]-x;
            y1 = mesh->point[adj[i][j]].c[1]-y;
            l = x1*x1+y1*y1;
            ND[i] += sqrt(l)/adj[i].size();
        }
    }
}

/**
 * \param mesh mmg mesh structure
 * \param adj adjacence table of the nodes
 * \param pointType type of each node
 * \param lMean mean distance between neighbors over the whole domain
 * \param ndefect number of defects
 * \param ratioXoY square rooot of ratio of width over height of the image
 * 
 * Assign a type to each node depending on its situation :
 * 0 normal,  1 boundary of a grain,  2 less than 6 neighbors,
 * 3 more than 6 neighbors,  4 border
 * 
*/
void getPointType(MMG5_pMesh mesh, const vector< vector<int> >& adj, vector<int>& pointType , const double& lMean, int& ndefect, int &nPointCenter, double ratioXoY)
{
    int nvoisins = 6;
    ndefect = 0;
    nPointCenter = 0;
    for (int i = 1; i<= mesh->np; i++)
    {
        if (isBorder(mesh, i, 2*lMean,ratioXoY))
        {
            pointType[i] = 4;
        }else
        {
            nPointCenter++;
            nvoisins = adj[i].size();
            if (nvoisins == 6)
                pointType[i] = 0;
            else if (nvoisins < 6)
            {
                pointType[i] = 2;
                ndefect++;
            }
            else if (nvoisins > 6)
            {
                pointType[i] = 3;
                ndefect++;
            }
        }
    }
}
