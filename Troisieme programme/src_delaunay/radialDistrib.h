#ifndef RADIALDISTRIB_H
#define RADIALDISTRIB_H

#include <vector>
extern "C"
{
  #include "levmarq.h"
}
#include "mmg/mmg2d/libmmg2d.h"


double area(MMG5_pMesh mesh,int pt, double r, double dl);
void radialDistribution(MMG5_pMesh mesh, std::vector <double>& g, std::vector <double>& g6, const std::vector< std::vector <int> >& adj, int dim, double r_max);
double* radialDistributionOut(std::vector <double> g, std::vector <double> g6, double r_max, double lMean, double dl);


/**
*
* \param par parameters of the function
* \param i value at wich the function is computed
* \param a untyped offset and dilatation for the function.
*
* \return value of par[0]*exp(par[1]*i) with offset a[1] and dilatation a[0]
*
* This is the function x->p0*exp(p1*x) with x=i*dl+d0.
*
*/
inline double aexpbi(double* par,int i,void* a)
{
 double *at = (double*) a;
 double dl = at[0];
 double d0 = at[1];
 return par[0]*exp(par[1]*(i*dl+d0));
}


/**
*
* \param g output value for the gradient
* \param par parameters of the function
* \param i value at wich the function is computed
* \param a untyped offset and dilatation for the function.
*
* This is the gradient of the function aexpbi
*
*/
inline void gradaexpbi(double* g,double* par,int i,void* a)
{
  double *at = (double*) a;
  double dl = at[0];
  double d0 = at[1];
  g[0] = exp(par[1]*(i*dl+d0));
  g[1] = i*par[0]*exp(par[1]*(i*dl+d0));
}

/**
*
* \param par parameters of the function
* \param i value at wich the function is computed
* \param a untyped offset and dilatation for the function.
*
* \return value of par[0]*i^par[1] with offset a[1] and dilatation a[0]
*
* This is the function x->par[0]*x^par[1] with x=i*dl+d0.
*
*/
inline double axpowb(double* par,int i,void* a)
{
  double *at = (double*) a;
  double dl = at[0];
  double d0 = at[1];
  return par[0]*pow(i*dl+d0,par[1]);
}

/**
*
* \param g output value for the gradient
* \param par parameters of the function
* \param i value at wich the function is computed
* \param a untyped offset and dilatation for the function.
*
* \return value of par[0]*i^par[1] with offset a[1] and dilatation a[0]
*
* This is the gradient of the function axpowb
*
*/
inline void gradaxpowb(double* g,double* par,int i,void* a)
{
  double *at = (double*) a;
  double dl = at[0];
  double d0 = at[1];
  g[0] = pow(i*dl+d0,par[1]);
  g[1] = par[0]*log(i*dl+d0)*pow(i*dl+d0,par[1]);
}

#endif
