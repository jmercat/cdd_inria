#ifndef LINEARFIT_H
#define LINEARFIT_H

#include <stdlib.h>
#include <vector>
#include <cstring>
#include <ctime>

double* linearFit(std::vector<double> x, std::vector<double> y, const bool isPow = 0, const bool isExp = 0);

#endif
