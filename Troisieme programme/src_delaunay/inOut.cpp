/**
 * \file inOut.cpp
 * \brief Functions related to input and output of data.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <locale.h>     /* struct lconv, setlocale, localeconv */


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>


#include "inOut.h"
#include "DataSol.h"
#include "Edge.h"
#include "anglesTools.h"
#include "domainTools.h"
extern "C"
{
  #include "MMG2D_plottops.h"
}

using namespace std;

/**
 * \param fileName name of the file
 * \return wether the file exists or not
 */
bool isfexist(const char *fileName)
{
//    cout << "check file : " << fileName << endl;
    ifstream infile(fileName);
    return infile.good();
}


/**
 * \param mmgMesh mmg mesh structure to be filled from file
 * \param areas list of areas of nodes to be filled from file
 * \param nameFile name of the file to read
 * 
 * Read the file \a nameFile and store read values in \a mmgMesh and \a areas.
 * No error handeling: the file should exist and be correctly formatted.
 * 
 */
void input_node(MMG5_pMesh mmgMesh, vector <double>& areas, double& ratioXoY, double& size, char* nameFile)
{
  setlocale(LC_ALL, "en_US.UTF-8");

  std::cout << "name "<< nameFile << std::endl;
  ifstream fichier(nameFile,std::ifstream::in);
  if (fichier.fail())
    std::cout << "File " << nameFile <<" cannot be read" << std::endl;
  int no;
  float x,y;
  double s;
  int nb_pt;  
  int dim;

  fichier >> nb_pt >> dim >> size >> ratioXoY;
  std::cout << "Number of points to compute  " << nb_pt <<  std::endl;
  if (nb_pt < 1000000)
  {
      areas.resize(nb_pt);
      if ( MMG2D_Set_meshSize(mmgMesh,nb_pt,0,0) != 1 )  exit(EXIT_FAILURE);

      // read the file
      for(int i = 1; i <= nb_pt && fichier.good(); i++)
      {
        //insert point in triangulation
        fichier >> no >> x >> y >> s;
        if ( MMG2D_Set_vertex(mmgMesh,x  ,y  ,no  ,  i) != 1 )
        {
          cout << "Error at point insertion" << endl;
          exit(EXIT_FAILURE);
        }
        areas[i-1] = s;
      }
  }else
  {
    std::cout << "Too many points to compute, not reading" << endl;
  }
  fichier.close();
}

/**
 * \param mmgMesh mmg mesh structure
 * \param areas list of areas of nodes
 * \param name name of the file to output
 * \param noFile number of the file (to be inserted in its name)
 * 
 * Write the file \a name with nodes coordinates and area values.
 * 
 */
void output_node(MMG5_pMesh mesh, vector <double>& areas, double size, char* name, int noFile)
{
    setlocale(LC_ALL, "en_US.UTF-8");
    string name_out;
    char name_out_temp[512];

    strcpy(name_out_temp, name);
    name_out = name_out_temp;
    size_t found = name_out.find_last_of(".");
    if (name_out.size()-found<7)
      name_out =  name_out.substr(0,found);
    stringstream ss;
    ss<< noFile;
    found = name_out.find_last_of("/");
    string nameTemp=name_out.substr(found);
    if (name_out.find_first_of(nameTemp)==name_out.find_last_of(nameTemp))
        name_out += nameTemp;
    name_out += "_"+ss.str();

    name_out += "_.node";
    
    ofstream fichier;
    fichier.open(const_cast < char* > (name_out.c_str()));
    fichier << mesh->np <<"  " << 2 <<"  " << size << "  " << 0 << endl;
    double x, y;
    for (int i =0; i<mesh->np ; i++)
    {
        x = mesh->point[i+1].c[0];
        y = mesh->point[i+1].c[1];
        fichier << i << "  " << x<< "  " << y << "  " << areas[i] << endl;
    }
    fichier << "# end of file" << endl;
    fichier.close();
}

/**
 * \param opt option parameter
 * \param mesh mmg mesh structure
 * \param sol DataSol structure containing node values to be ploted (normalized)
 * \param name name of the output file
 * \param adj adjacence table of the nodes
 * \param connection connection table of the nodes (defining grain boundaries)
 * \param pointType type values of the nodes
 * \param grains vector of grains that are vectors of nodes
 * \param lMean mean distance between nodes
 * \param ndefect number of defects
 * \param vecHist list of histograms to be plotted
 * \param ratioXoY square rooot of ratio of width over height of the image
 * 
 * Plot the mesh, informations about it and histograms in a post script file.
 * \a opt defines what should be printed. It may be colors (defined by \a sol),
 * defect visualisation, grain boundaries or grain nodes.
 * 
 */

void plotMeshCustom(int opt, MMG5_pMesh mesh, DataSol& sol, char* name, vector< vector<int> > adj, vector< vector<bool> > connection, vector<int> pointType, vector < vector <int> > grains, double lMean, int ndefect, vector <histogram> vecHist, double ratioXoY)
{
    cout << "plotMeshCustom" << endl;
    setlocale(LC_NUMERIC, "en_US.UTF-8");
    //Declare postscript
    MMG2D_postscript ps;
    //Open postscript file
    MMG2D_openPostscript(&ps,name);
    double color[3];
    color[0] = 0;
    color[1] = 0;
    color[2] = 0;
    MMG2D_sethsbColor(&ps, color);
    MMG2D_setLineWidth(&ps, 1);
    vector <double> angle(mesh->np+1);
    getAngle(mesh, adj, angle);

    if (opt%7 == 0)
    {
         // define shading in post-script file
        fprintf(ps.file, "/DeviceRGB setcolorspace\ngsave\n<<\n /ShadingType 4\n");
        fprintf(ps.file, "/ColorSpace [/DeviceRGB]\n /BitsPerCoordinate 12\n /BitsPerComponent 8\n /BitsPerFlag 8\n /Decode [10 585 133.5 708.5 0 1 0 1 0 1]\n /DataSource \n <");  
       
        int num_pt; // global index of the triangle
        int flag = 0; // flag de triangle (allways 0 : the 3 nodes of the triangles are set every time)
        double x,y; // positions
        double r, g, b; // couleurs
        int bufferP[2]; // buffer de position
        int bufferC[3]; // buffer de couleur

       // cout << "avant triangles " << mesh->nt << endl;
        // write triangles and color in shading data (there is a mysterious problem
        // I couldn't get it to work with BitsPerFlag set to 2
        // (if set to 4 it needs two hex character except for the first flag...)
        // so BitsPerFlag is set to 8
        for (int i = 1; i<= mesh->nt; i++)
        {
            for (int j = 0; j<3; j++)
            {
                num_pt = mesh->tria[i].v[j];
                
                if(sol(num_pt)>1 || sol(num_pt)<0)
                  cout << "Problem. Sol value out of bound " << sol(num_pt) << endl;
                
                getRGB(sol(num_pt),1.,1.,&r,&g,&b);
                x = mesh->point[num_pt].c[0];
                y = mesh->point[num_pt].c[1];
                bufferC[0] = flag; // flag = 0 on 8 bits : 2 hex character
                fprintf(ps.file, "%02x", bufferC[0]);
                bufferP[0] = x*4095; // coordinates on 12 bits : 0 to 4095 on 3 hex character
                bufferP[1] = y*4095;
                fprintf(ps.file, "%03x%03x", bufferP[0], bufferP[1]);
                bufferC[0] = r*255; // color on 8 bits : 0 to 255 on 2 hex character
                bufferC[1] = g*255;
                bufferC[2] = b*255;
                fprintf(ps.file, "%02x%02x%02x",bufferC[0], bufferC[1], bufferC[2]);
            }
        }
    
    // close shading dictionary and print
    fprintf(ps.file,">\n>>\nshfill\ngrestore\n");
    
    double scale[7];
    scale[6] = sol(0)/2;
    for (int i = 0; i < 6; i++)
        scale[i] = (2*scale[6]*i)/6 - scale[6];
    if (sol.getName()=="Orientation")
      for (int i = 0; i < 7; i++)
         scale[i] += scale[6];
    MMG2D_drawScale(0.85, 1.001, 0.02 , 0.18, scale, const_cast <char*> (sol.getName().c_str()), &ps);
    
    
    }
    
    if (opt%5 == 0)
    {
        vector <edge> listEdges;
        
        //cout << "get edges " << adj.size() << endl;
        if (opt%11 == 0)
          getEdgesVoronoi( mesh, adj, listEdges);
        else
          getEdgesDelaunay( mesh, adj, listEdges);

        double x1,x2,y1,y2;
        MMG2D_sethsbColor(&ps, color);   
        MMG2D_setLineWidth(&ps, 0.00001);
       // cout << "loop over edges " << listEdges.size() << endl;
        for (unsigned int i = 0; i<listEdges.size(); i++)
        {
            //~ if ((listEdges[i].x>lMean/2 && listEdges[i].y>lMean/2 && listEdges[i].x<1-lMean/2 && listEdges[i].y<1-lMean/2))
            if (fabs(listEdges[i].r-lMean)<lMean)
            {
                x1 = (listEdges[i].x+listEdges[i].r/2*cos(listEdges[i].theta))*ps.box_width+ps.point0[0];
                y1 = (listEdges[i].y+listEdges[i].r/2*sin(listEdges[i].theta))*ps.box_width+ps.point0[1];
                x2 = (listEdges[i].x-listEdges[i].r/2*cos(listEdges[i].theta))*ps.box_width+ps.point0[0];
                y2 = (listEdges[i].y-listEdges[i].r/2*sin(listEdges[i].theta))*ps.box_width+ps.point0[1];
                
                //~ x1 = (listEdges[i].x+listEdges[i].r/2*2*cos(pi/6)/3*cos(listEdges[i].theta+pi/2))*ps.box_width+ps.point0[0];
                //~ y1 = (listEdges[i].y+listEdges[i].r/2*2*cos(pi/6)/3*sin(listEdges[i].theta+pi/2))*ps.box_width+ps.point0[1];
                //~ x2 = (listEdges[i].x-listEdges[i].r/2*2*cos(pi/6)/3*cos(listEdges[i].theta+pi/2))*ps.box_width+ps.point0[0];
                //~ y2 = (listEdges[i].y-listEdges[i].r/2*2*cos(pi/6)/3*sin(listEdges[i].theta+pi/2))*ps.box_width+ps.point0[1];
                
                //~ x1 = (listEdges[i].x+listEdges[i].r*cos(listEdges[i].theta))*ps.box_width+ps.point0[0];
                //~ y1 = (listEdges[i].y+listEdges[i].r*sin(listEdges[i].theta))*ps.box_width+ps.point0[1];
                //~ x2 = (listEdges[i].x-listEdges[i].r*cos(listEdges[i].theta))*ps.box_width+ps.point0[0];
                //~ y2 = (listEdges[i].y-listEdges[i].r*sin(listEdges[i].theta))*ps.box_width+ps.point0[1];
                
                fprintf(ps.file, "%.6g %.6g %.6g %.6g line\n", x1, y1, x2, y2);
            }
        }
        
    
        //~ MMG2D_sethsbColor(&ps, color);   
        //~ MMG2D_setLineWidth(&ps, 0.00001);
        //~ for (int i = 1; i<= mesh->nt; i++)
        //~ { 
            //~ MMG2D_drawTriangle(mesh, &ps, i);
        //~ }
    }
    //~ for (int i = 0; i< gradAngle.size(); i++)
    //~ {
        //~ for(int j =0; j< gradAngle[i].size(); j++)
        //~ {
            //~ MMG2D_sethueColor(&ps, gradAngle[i][j]*8);
            //~ MMG2D_drawLine(mesh, &ps, i, adj[i][j]);
        //~ }
    //~ }
    bool pair = false;
    if (opt%3 == 0)
    {
        MMG2D_sethueColor(&ps, 0.);
        cout << "Ratio X over Y " << ratioXoY << endl;
        for (int i = 1; i<= mesh->np; i++)
        {
            if (!isBorder(mesh, i, 2*lMean, ratioXoY))
            {
                pair = false;
                if(adj[i].size()<6)
                {
                    for (unsigned int j = 0; j< adj[i].size(); j++)
                    {
                        if(adj[adj[i][j]].size()>6)
                        {
                            pair = true; 
                            MMG2D_sethueColor(&ps, 1./12);
                            MMG2D_setLineWidthRelative(&ps, lMean/4);
                            if (opt%7 == 0)
                                MMG2D_setdarknessColor(&ps, 0.);
                            else
                                MMG2D_setdarknessColor(&ps, 0.4);
                            MMG2D_drawLine(mesh, &ps, i, adj[i][j]);
                        }
                    }
                    MMG2D_sethueColor(&ps, 0.);
                    if (opt%7 == 0)
                        MMG2D_setdarknessColor(&ps, 0.);
                    else
                        MMG2D_setdarknessColor(&ps, 0.8);
                    MMG2D_setLineWidth(&ps, 1);
                    if (!pair)
                        MMG2D_drawCircle(mesh, &ps, i, lMean/1.8);
                    MMG2D_drawPoint(mesh, &ps, i, lMean/3);
                }
            }
        }
        MMG2D_sethueColor(&ps, 1./3);
        if (opt%7 == 0)
            MMG2D_setgrayColor(&ps, 1.);
        else
            MMG2D_setdarknessColor(&ps, 0.8);
        for (int i = 1; i<= mesh->np; i++)
        {
            if (!isBorder(mesh, i, 2*lMean, ratioXoY))
            {
                pair = false;
                if(adj[i].size()>6)
                {
                    for (unsigned int j = 0; j< adj[i].size(); j++)
                    {
                        if(adj[adj[i][j]].size()<6)
                            pair = true;
                    }
                    MMG2D_setLineWidth(&ps, 1);
                    if (!pair)
                        MMG2D_drawCircle(mesh, &ps, i, lMean/1.8);
                    MMG2D_drawPoint(mesh, &ps, i, lMean/3);
                }
            }
        }
    }
    if (opt%2==0)
    {
        color[0] = 0;
        color[1] = 0;
        color[2] = 0;
        MMG2D_sethsbColor(&ps, color);
        for (int i = 1; i<= mesh->np; i++)
        {
            if (!isBorder(mesh, i, 2*lMean, ratioXoY))
            {
                pair = false;
                if(pointType[i] == 1)
                {
                    for (unsigned int j = 0; j< adj[i].size(); j++)
                    {
                        if(connection[i][j])
                        { 
                            //~ Debug(cout<< "Dessin connection" << endl;)
                            MMG2D_setLineWidthRelative(&ps, lMean/4);
                            MMG2D_drawLine(mesh, &ps, i, adj[i][j]);
                        }
                    }
                    MMG2D_drawPoint(mesh, &ps, i, lMean/3);
                }
            }
        }
    }
    int nb_grains = 0, nb_truc = 0, moy_size = 0, max_size = 0, min_size = mesh->np;
    for (unsigned int num_grain = 0; num_grain < grains.size(); num_grain++)
    {
        if (grains[num_grain].size() > 30)
            nb_grains++;
    }
    srand (time(NULL));
    for (unsigned int num_grain = 0; num_grain < grains.size(); num_grain++)
    {
        if (grains[num_grain].size() > 30)
        {
            min_size = min_size<int(grains[num_grain].size())? min_size:int(grains[num_grain].size());
            max_size = max_size>int(grains[num_grain].size())? max_size:int(grains[num_grain].size());
            moy_size += grains[num_grain].size();
            nb_truc++;
            if ( opt%13 == 0)
            {
                //~ if (nb_truc%2==0)
                double p = (double) (rand())/RAND_MAX;
                MMG2D_sethueColor(&ps, p);
                p = (double) (rand())/RAND_MAX;
                MMG2D_setdarknessColor(&ps,p*0.9+0.1);
                //~ else
                    //~ MMG2D_setgrayColor(&ps, float(nb_truc)/nb_grains);
                //~ cout << "Taille du grain " <<  grains[num_grain].size() << endl;
                for (unsigned int i = 0; i < grains[num_grain].size(); i++)
                {
                    MMG2D_drawPoint(mesh, &ps, grains[num_grain][i], lMean/2.5);
                }
            }
        }
    }
    if (nb_grains>0)
      moy_size /= nb_grains;
    else
      moy_size = 0;
    //~ MMG2D_drawPoint(mesh, &ps, 2530, lMean/3);
    // test of adj structure, allows too see if verticies are stored in the right order
    //~ for( int i = 0; i<adj[3200].size(); i++)
    //~ {
        //~ MMG2D_sethueColor(&ps, float(i)/adj[3200].size());
        //~ MMG2D_drawPoint(mesh, &ps, adj[3200][i], lMean/3);
    //~ }
    cout << "vecHist" << endl;
    if (vecHist.size()< 6)
    {
        for (unsigned int i = 0; i < vecHist.size(); i++)
        {
            plotHistogram( 10+120*(i+(5.-vecHist.size())/2), 17, 107, 107, vecHist[i], &ps);
        }
    }
    cout << "vecHist ok" << endl;

    
    
    MMG2D_drawText(0.05, 1.14, 30, 0, const_cast<char*>("2D crystal analysis"), &ps);
    char buffer[512];
    sprintf(buffer, "Number of cylinders: %i", mesh->np);
    MMG2D_drawText(0.07, 1.10, 12, 0, buffer, &ps);
    sprintf(buffer, "Defect density: %3.1f%% ", float(ndefect*100.)/mesh->np);
    MMG2D_drawText(0.07, 1.075, 12, 0, buffer, &ps);
    sprintf(buffer, "Number of grain: %i ", nb_grains);
    MMG2D_drawText(0.07, 1.05, 12, 0, buffer, &ps);
    sprintf(buffer, "Cylinders per grain (mean [min,max]): %i [%i,%i]", moy_size, min_size, max_size);
    MMG2D_drawText(0.07, 1.025, 12, 0, buffer, &ps);
    
    MMG2D_closePostscript(&ps);
}


/**
 * \param mesh mmg mesh structure 
 * \param name name of the file to output
 * \param lMean mean distance between the nodes
 * \param partition unused...
 * 
 * Output post script file with nodes positions marqued with dots
 */
void plotpoint( MMG5_pMesh mesh, char* name, double lMean, const vector<vector <vector <int> > >& partition)
{
    //Declare postscript
    MMG2D_postscript ps;
    //Open postscript file
    MMG2D_openPostscript(&ps,name);
    double color[3];
    color[0] = 0;
    color[1] = 0;
    color[2] = 0;
    MMG2D_sethsbColor(&ps, color);
    MMG2D_setLineWidth(&ps, lMean/3);
    //~ int nx = partition.size();
    //~ int ny = partition[0].size();
    //~ for(int i = 0; i< nx; i++)
    //~ {
        //~ for (int j = 0; j<ny; j++)
        //~ {
            //~ MMG2D_sethueColor(&ps, float(rand()%50)/50);
            //~ for (int p1 = 0; p1<partition[i][j].size(); p1++)
            //~ {
                //~ MMG2D_drawPoint(mesh, &ps, partition[i][j][p1], lMean/3);
            //~ }
        //~ }
    //~ }
    for (int i = 1; i < mesh->np; i++)
    {
        
        MMG2D_drawPoint(mesh, &ps, i, lMean/3);
    }
    
    
    
    //~ int i = 15;
    //~ int j = 15;
    //~ for (int k = -1; k<2; k++)
    //~ {
        //~ for (int l = -1; l<2; l++)
        //~ {
            //~ MMG2D_sethueColor(&ps, float(rand()%100)/100);
            //~ for (int p2 = 0; p2<partition[(i+k+nx)%nx][(j+l+ny)%ny].size(); p2++)
            //~ {
                //~ 
                //~ MMG2D_drawPoint(mesh, &ps, partition[(i+k+nx)%nx][(j+l+ny)%ny][p2], lMean/3);
            //~ }
        //~ }
    //~ }
    MMG2D_closePostscript(&ps);
}
//~ void plotpoint2( MMG5_pMesh mesh, char* name, double lMean, const vector<vector <vector <int> > >& partition, const vector <int>& pointType, int et1, int et2)
//~ {
    //~ //Declare postscript
    //~ MMG2D_postscript ps;
    //~ //Open postscript file
    //~ MMG2D_openPostscript(&ps,name);
    //~ double color[3];
    //~ color[0] = 0;
    //~ color[1] = 0;
    //~ color[2] = 0;
    //~ MMG2D_sethsbColor(&ps, color);
    //~ MMG2D_setLineWidth(&ps, lMean/3);
    //~ int nx = partition.size();
    //~ int ny = partition[0].size();
    //~ for(int i = 0; i< nx; i++)
    //~ {
        //~ for (int j = 0; j<ny; j++)
        //~ {
            //~ MMG2D_sethueColor(&ps, float(rand()%100)/100);
            //~ for (int p1 = 0; p1<partition[i][j].size(); p1++)
            //~ {
                //~ MMG2D_drawPoint(mesh, &ps, partition[i][j][p1], lMean/3);
            //~ }
        //~ }
    //~ }
    //~ for (int i = 1; i < mesh->np; i++)
    //~ {
        //~ if (pointType[i] == 5)
            //~ MMG2D_sethueColor(&ps, 0);
        //~ else
            //~ MMG2D_sethsbColor(&ps, color);
        //~ 
        //~ MMG2D_drawPoint(mesh, &ps, i, lMean/3);
    //~ }
    //~ int i = et1;
    //~ int j = et2;
    //~ for (int k = -1; k<2; k++)
    //~ {
        //~ for (int l = -1; l<2; l++)
        //~ {
            //~ MMG2D_sethueColor(&ps, float(rand()%100)/100);
            //~ for (int p2 = 0; p2<partition[(i+k+nx)%nx][(j+l+ny)%ny].size(); p2++)
            //~ {
                //~ 
                //~ MMG2D_drawPoint(mesh, &ps, partition[(i+k+nx)%nx][(j+l+ny)%ny][p2], lMean/3);
            //~ }
        //~ }
    //~ }
    //~ MMG2D_closePostscript(&ps);
//~ }
