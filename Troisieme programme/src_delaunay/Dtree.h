#ifndef DTREE_H
#define DTREE_H


#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

class Dtree
{
public :
  Dtree();
  Dtree(int, double);
  Dtree(const Dtree&);
  ~Dtree();
  

  
  void addBranch();
  void addBranch(int, double);
  void addBranch(const Dtree&);
  void addBranch(const Dtree&, const std::vector <int>&);
  void addBranch(int, double, const std::vector <int>&);
  
  Dtree& getBranch(int);
  Dtree& getBranch(std::vector <int>);
  
  double getValue();
  
  int getNode();
  int getNode(int);
  int getNode(std::vector <int>);
  int getSize();
  
  void print();
  inline bool isInTree(int elem);

private :
  std::vector< Dtree* > *branch;
  int node;
  double value;
  
  int getNodeRec(std::vector <int> v);
  Dtree& getBranchRec(std::vector <int>& v);
  void printDtreeRec();

};

void reverseVect( std::vector<int>& vec);

inline bool Dtree::isInTree(int elem)
{
  if(elem == node)
    return true;
  else
  {
    for(unsigned int i = 0; i<branch->size(); i++)
    {
      if (((*branch)[i])->isInTree(elem))
        return true;
    }
  }
  return false;
}

inline double Dtree::getValue()
{
  return value;
}

inline int Dtree::getNode()
{
  return node;
}


#endif
