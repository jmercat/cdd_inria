/**
 * \file grains.h
 * \brief Partitionning of crystalline block copolymer organisation into
 * grains of similar properties (orientation, stress).
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */

#ifndef GRAINS_H
#define GRAINS_H


#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "Dtree.h"

#include "mmg/mmg2d/libmmg2d.h"

int getIndex( std::vector <int> vec, int elem);
void chainNeighbor(Dtree& chaine, Dtree& chaine0, const std::vector < std::vector <int> >& adj,  std::vector<int>& parcour);
void getchain(Dtree& chain, std::vector <int> parcour, const std::vector< std::vector <bool> >& connection, const std::vector < std::vector < int > >& adj);
std::vector <double> BurgerVector(MMG5_pMesh mesh, int node, const std::vector < std::vector <int> >& adj,  const std::vector< std::vector <bool> >& connection);
void dijkstraNext(Dtree& chain, const std::vector< std::vector <int> >& adj, const std::vector< std::vector<double> >& gradAngle, const int& elem0, int& elem, Dtree& chemin0, Dtree& chemin, double& distance, std::vector<int>& pathway, std::vector<int> pathwayTemp);
void dijkstraLoop(const std::vector< std::vector <bool> >& connection, const std::vector< std::vector <int> >& adj, const std::vector< int >& pointType, const std::vector< std::vector<double> >& gradAngle, Dtree& path, std::vector <int>& pathway, double& distance);
void addConnection(std::vector< std::vector <bool> >& connection, const std::vector< std::vector <int> >& adj, std::vector< int >& pointType, Dtree& path, std::vector <int> pathway);
void dijkstra(std::vector< std::vector <bool> >& connection, const std::vector< std::vector <int> >& adj, std::vector< int >& pointType, const std::vector< std::vector<double> >& gradAngle, int elem, double distanceMax);
void spreadGrain(const std::vector< std::vector <int> >& adj, const std::vector< int >& pointType, std::vector <int>& listPt, std::vector <int>& grain);
void getGrains(const std::vector< std::vector <int> >& adj, const std::vector< int >& pointType, std::vector< std::vector <int> >& grains);
void getGrainsList(const std::vector< std::vector <int> >& grains, const std::vector< int >& pointType, std::vector< int >& grainsList);
int whichGrain(const std::vector <int>& adjgr, const std::vector <double>& anglePt, std::vector <int> grainsList, int gr);
void convertBoundary(const std::vector< std::vector <int> >& adj, const std::vector< int >& pointType,  const std::vector <double>& anglePt, std::vector< std::vector <int> >& grains);
void boundaryGrains(std::vector< std::vector <bool> >& connection, const std::vector< std::vector <int> >& adj, std::vector< int >& pointType, const std::vector< std::vector<double> >& grad_sol, double gs_mean);
void getConnection(MMG5_pMesh mesh, const std::vector< std::vector<int> >& adj, std::vector< std::vector<bool> >& connection, const double& lMean , double ratioXoY);
void initConnection(const std::vector< std::vector<int> >& adj, std::vector< std::vector<bool> >& connection);

#endif
