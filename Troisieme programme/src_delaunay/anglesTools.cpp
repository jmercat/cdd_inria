/**
 * \file anglesTools.cpp
 * \brief Tools for orientation analysis of the organisation.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 * 
 * Functions computing angles for orientation analysis of the triangulation.
 * 
 */

#include <math.h>
#include <stdio.h>
#include <iostream>

#include "anglesTools.h"

#define pi 3.1415926
#ifndef min
#define min(a,b) ((a<b)? a:b)
#endif
#ifndef max
#define max(a,b) ((a>b)? a:b)
#endif
using namespace std;



/**
 * \param mesh
 * \param ntria index of the triangle in the mesh structure.
 * \return minimum angle between horizontal axis and edges of the triangle.
 * 
 */
double angleTr(MMG5_pMesh mesh, int ntria)
{
    double angle1, angle2, angle3;
    
    // get the vertex numbers of the triangle
    int npt1 = mesh->tria[ntria].v[0];
    int npt2 = mesh->tria[ntria].v[1];
    int npt3 = mesh->tria[ntria].v[2];
        
    // get the coordinates of the vertex
    double x1, y1, x2, y2, x3, y3;
    x1 = mesh->point[npt1].c[0];
    y1 = mesh->point[npt1].c[1];
    x2 = mesh->point[npt2].c[0];
    y2 = mesh->point[npt2].c[1];
    x3 = mesh->point[npt3].c[0];
    y3 = mesh->point[npt3].c[1];
        
    // get the angle of each side of the triangle
    angle1 = fmod(atan2((y2-y3),(x2-x3))+2*pi,pi/3);
    angle2 = fmod(atan2((y1-y3),(x1-x3))+2*pi,pi/3);
    angle3 = fmod(atan2((y1-y2),(x1-x2))+2*pi,pi/3);

    // get the minimum absolute value
    angle1 = min(min(angle1,angle2),angle3)*3./(pi);
    
    return angle1;
}

/**
 * \param mesh
 * \param npt1 index of the first vertex.
 * \param npt2 index of the second vertex.
 * \return angle between horizontal and given segment.
 * 
 * Returns the angle between the segment formed by the two vertices and 
 * the horizontal axis.
 * 
 */
double getAngleSegment(MMG5_pMesh mesh, int npt1, int npt2)
{
    double x, y;
    x = mesh->point[npt1].c[0] - mesh->point[npt2].c[0];
    y = mesh->point[npt1].c[1] - mesh->point[npt2].c[1];
    return atan2(y,x)+pi;
}

/**
 * \param mesh
 * \param adj adjacence table of the vertices.
 * \param angle \a angle[i] is the angle of the cell centered on
 *  the node of index i and defined by its adjacent vertices.
 * 
 * Fills the vector \a angle with the orientation angle of the nodes 
 * organisation. There are a lot of different ways to compute the global
 * angle of a polyhedra formed by the adjacent vertices of a node. Here 
 * one that smoothes the result was chosen. It is based on a normal 
 * calculation of the orientation of a regular hexagon. The results varies
 * a lot for different ways to measure the orientation.
 * 
 */
void getAngle(MMG5_pMesh mesh, vector < vector <int> > adj, vector<double>& angle)
{
  double angleTemp;
  double cosMean, sinMean;
  for (unsigned int i = 1; i< adj.size(); ++i)
  {
    cosMean = 0;
    sinMean = 0;
    for (unsigned int j = 0; j<adj[i].size(); ++j)
    {
      angleTemp = fmod(getAngleSegment(mesh, i, adj[i][j]),pi/3);
      cosMean += cos(6*angleTemp);
      sinMean += sin(6*angleTemp);
    }
      
    angle[i] = (atan2(sinMean,cosMean)+pi)/6;
    angle[i]/=pi/3;
    //~ if (angle[i] >1 || angle[i]<0)
      //~ cout << "Après " << angle[i]*180/pi << endl;
  }
  angle[0] = pi/3;
}

/**
 * \param mesh
 * \param adj adjacence table of the vertices.
 * \param gradAngle \a gradAngle[i][j] is the gradient of angle between nodes
 * i and \a adj[i][j].
 * 
 * Fills the vector \a gradAngle with the gradient of orientation angles
 * of the nodes organisation.
 * 
 */
void getGradAngle(MMG5_pMesh mesh, const vector< vector<int> >& adj, vector< vector<double> >& gradAngle, double& ga_mean)
{
  ga_mean = 0;
  gradAngle.resize(adj.size());
  int s, nb_mean = 0;
  double amin = 2*pi, amax = 0, a1, a2;
  for (unsigned int i = 0; i< gradAngle.size(); i++)
  {
    s = adj[i].size();
    gradAngle[i].resize(s);
    for(int j=0; j<s;j++)
    {
      a1 = getAngleSegment(mesh, i, adj[i][(s+j-1)%s]);
      a2 = getAngleSegment(mesh, adj[i][(j+1)%s],adj[i][j]);
      a2 = a1+a2;
      a1 = max(a1,a2-a1);
      a2 = a2 - a1;
      gradAngle[i][j] = fmod(min(a1-a2,a2+2*pi-a1),pi);
      a1 = getAngleSegment(mesh, i, adj[i][(j+1)%s]);
      a2 = getAngleSegment(mesh, adj[i][(s+j-1)%s],adj[i][j]);
      a2 = a1+a2;
      a1 = max(a1,a2-a1);
      a2 = a2 - a1;
      gradAngle[i][j] += fmod(min(a1-a2,a2+2*pi-a1),pi);
      gradAngle[i][j] /= 2*pi;
      if(s==6)
      {    
        amin = min(amin, gradAngle[i][j]);
        amax = max(amax, gradAngle[i][j]);
        ga_mean += gradAngle[i][j];
        nb_mean++;
      }
    }
  }
  ga_mean /= nb_mean;
  cout  << " Angles min et max " << amin << " " << amax << endl;

}



