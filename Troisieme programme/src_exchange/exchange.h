#ifndef EXCHANGE_H
#define EXCHANGE_H

typedef struct histogram
{
  std::vector <int> count;
  std::vector <double> levels, levelsMean;
  int countAll;
  double sigma, mu;
  std::string name;
} histogram;

struct imageExchange
{
  int numberOfPoints;
  int pxWidth;
  int pxHeight;
  double dimension;
  int frequency;
  int radius;
  double quality;
};

struct delaunayExchange
{
  int numberOfPoints;
  int numberOfPointsCenter;
  int numberOfDefects;
  int numberOfGrains;
  double meanDistance;
  double defectDensity;
  double powFit[2];
  double expFit[2];
  int numberOfTriangles;
  std::vector<histogram> vecHist;
};

struct allExchange
{
    imageExchange image;
    delaunayExchange delaunay;
};

#endif // EXCHANGE_H
