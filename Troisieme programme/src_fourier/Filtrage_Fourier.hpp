#ifndef FILTRAGE
#define FILTRAGE

#include <vector>
#include "CImg.h"
#include "../src_exchange/exchange.h"

void output_log(int nbpt, int width, int height, int bp, int freq, int radius);
cimg_library::CImg<short> load_AFM(char* name, double& realSize);
void recoverTilt(cimg_library::CImg<unsigned short>& sourceImage);
double imageQuality(cimg_library::CImg<unsigned short>& sourceImage);
int wave_freq(const cimg_library::CImg<unsigned short> &mag,int dl,int max_radius );
void wave_show(const cimg_library::CImg<unsigned short> img);
int max_freq(const cimg_library::CImg<unsigned short> &mag, int dl);
double bell(double x,double x0,int n);
void enflamme4(cimg_library::CImg<short>& img0, cimg_library::CImg<bool>& img, int i, int j, double(&coord)[2], double& nbp);
int find_center(cimg_library::CImg<short>& img0, cimg_library::CImg<bool>& img, std::vector < std::vector <double> >& cylinders);
cimg_library::CImg<unsigned short> fourier_filtering(cimg_library::CImg<unsigned short> img, int& freq, int fbande, double ratioXoY);
void  item_fourier_filtering(cimg_library::CImg<unsigned short>& img, int& freq, double ratioXoY);
void output_node(const std::vector< std::vector <double> >& cylinders, double size, char* filename,int h, int w);
bool fexist(const char *fileName);
cimg_library::CImg<bool> binarizeAdaptative(cimg_library::CImg<unsigned short>& img,unsigned short windowSize);
int mainFiltrage(int argc, char **argv, imageExchange& dataOut);

#endif
