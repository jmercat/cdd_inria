/**
 * \file Filtrage_Fourier.cpp
 * \brief Particle detection on AFM image of crystalline block copolymer.
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <cmath>
#include <cstdlib>
#include <locale.h>     /* struct lconv, setlocale, localeconv */

using namespace std;

// Image library used
#define cimg_use_png
#include "Filtrage_Fourier.hpp"
using namespace cimg_library;


/**
 * \param nbpt number of points in the output.
 * \param width width of the image.
 * \param height height of the image.
 * \param bp band pass width.
 * \param freq maximum frequency in the image: nearest neighbor distance.
 * \param radius average radius of the cylinders.
 * 
 * outputs some data in the log file.
 *
 */
void output_log(int nbpt, int width, int height, int bp, int freq, int radius)
{
    setlocale(LC_ALL, "en_US.UTF-8");
    ofstream log;
    log.open("log.log");
    
    log << nbpt << endl;
    log << width << endl;
    log << height << endl;
    log << bp << endl;
    log << freq << endl;
    log << radius << endl;
    log.close();
    
}


/**
 * \param name name of the input file.
 * \param realSize size of the image in nm.
 * \return image loaded from nanoscope file.
 *
 * Load a nanoscope image
 *
 */
CImg<short> load_AFM(char* name, double& realSize)
{
    setlocale(LC_ALL, "en_US.UTF-8");
    fstream file;
    file.open(name);
    string line;
    int offset, length, type, height, width, depth = 0,size;
    //~ double zSens;
    //~ while ( getline(file,line) && line.substr(0,14).compare("\\*Scanner list") != 0)
    //~ {}
		//~ while ( getline(file,line) && line.substr(0,13).compare("\\@Sens. Zsens") != 0)
    //~ {}
    //~ zSens = atof(line.substr(17,8).c_str());
    //~ cout << "Zsens: " << zSens <<  " nm/V" << endl;
    
    while ( getline(file,line) && line.substr(0,16).compare("\\*Ciao scan list") != 0)
    {}
		while ( getline(file,line) && line.substr(0,10).compare("\\Scan Size") != 0)
    {}
    size = atoi(line.substr(12,4).c_str());
    cout << "dimension lue : " << size <<  " nm" << endl;
    realSize = size;
    while ( getline(file,line) && line.substr(0,17).compare("\\*Ciao image list") != 0)
    {}
    while ( getline(file,line) && line.substr(0,12).compare("\\Data offset") != 0)
    {}
    offset = atoi(line.substr(14,15).c_str());
    cout << "offset lu : " << offset << endl;
    while ( getline(file,line) && line.substr(0,12).compare("\\Data length") != 0)
    {}
    length = atoi(line.substr(14,15).c_str());
    cout << "longueur lue : " << length << endl;
    while ( getline(file,line) && line.substr(0,12).compare("\\Bytes/pixel") != 0)
    {}
    type = atoi(line.substr(14,15).c_str());
    cout << "type lu : " << type << endl;
    while ( getline(file,line) && line.substr(0,11).compare("\\Samps/line") != 0)
    {}
    width = atoi(line.substr(13,15).c_str());
    cout << "largeur lue : " << width << " px" << endl;
    while ( getline(file,line) && line.substr(0,16).compare("\\Number of lines") != 0)
    {}
    height = atoi(line.substr(18,15).c_str());
    cout << "hauteur lue : " << height << " px" << endl;
    
    //~ while ( getline(file,line) && line.substr(0,10).compare("\\Scan Size") != 0)
    //~ {}
    //~ size = atoi(line.substr(12,1).c_str());
    //~ cout << "dimension lue : " << size*1000 <<  " nm" << endl;
    
    while ( getline(file,line) && line.substr(0,15).compare("\\*File list end") != 0)
    {
        if(line.substr(0,17).compare("\\*Ciao image list"))
            depth++;
    }
    
  if (type == 2)
	{
		CImg<short> image;
    image.load_raw(name,width,height,3,1,false,false,offset); 
		return image;
  }else
  {
		cout << "Error: while loading nanoscope image, incorrect data type" << endl;
		exit(0);
	} 
}


/**
 * \param sourceImage image to be modified.
 *
 * Recovers image tilt by substracting the mean plane to the image
 * lines are corrected too so that mean hight is achieved by every line
 *
 */
void recoverTilt(CImg<unsigned short>& sourceImage)
{
  int height=sourceImage.height();
  int width=sourceImage.width();
  
  // enhance data range so computation of mean plane is more accurate
  unsigned short sImin = sourceImage.min();
  sourceImage = (sourceImage - sImin)*(65535/(sourceImage.max()-sImin)); // normalize ???
  //~ sourceImage.display();
  
  // compute mean pixel values at 3 different quarter of the image
  double mean1=0, mean2=0, mean3=0;
  for (int i=0;i<width/2;i++)
  {
    for (int j=0;j<height/2;j++)
    {
      mean1+= sourceImage(i,j);
    }
    for (int j=height/2;j<height;j++)
    {
      mean2+= sourceImage(i,j);
    }
  }
  for (int i=width/2;i<width;i++)
  {
    for (int j=0;j<height/2;j++)
    {
      mean3+= sourceImage(i,j);
    }
  }
  
  double surface=(width/2)*(height/2);
  mean1/=surface;
  mean2/=surface;
  mean3/=surface;
  
  
  // Solve the system to find the mean plane
  CImg<double> m(3,3);
  CImg<double> sol(1,3);
  
  m(0,0) = 0.25*width;
  m(0,1) = 0.25*height;
  m(0,2) = mean1;
  m(1,0) = 0;
  m(1,1) = 0.5*height;
  m(1,2) = mean2-mean1;
  m(2,0) = 0.5*width;
  m(2,1) = 0;
  m(2,2) = mean3-mean1;
  
  sol(0) = m(1,1)*m(2,2)-m(2,1)*m(1,2);
  sol(1) = m(1,2)*m(2,0)-m(1,0)*m(2,2);
  sol(2) = m(1,0)*m(2,1)-m(1,1)*m(2,0);
  
  
  // compute last facor of plane definition
  double d;
  
  d = -sol(0)*m(0,0)-sol(1)*m(0,1)-sol(2)*m(0,2);
  
  
  // compute mean plane pixels
  CImg<unsigned short> sImage(width,height);
  for (int i=0;i<width;i++)
  {
    for (int j=0;j<height;j++)
    {
      sImage(i,j) = -((sol(0)*i+sol(1)*j+d)/sol(2));
    }
  }
  
  // substract mean plane from source image (mean plane mean value
  // is substracted to keep values in the range of short type)
  sourceImage -= (sImage-sImage.mean());
  
  
  //compute mean pixel value over each line and rescale the line 
  //to the mean pixel value of the whole image
  double meanAll = sourceImage.mean();
  double meanLine = 0;
  for (int j=0;j<height;j++)
  {
    meanLine = 0;
    for (int i=0;i<width;i++)
    {
      meanLine += sourceImage(i,j);
    }
    meanLine/=width;
    for (int i=0;i<width;i++)
    {
      sourceImage(i,j) = sourceImage(i,j)-(meanLine-meanAll);
    }
  }
  
}


/**
 * \param sourceImage image to be modified.
 *
 * Gives an indication of the image quality based on contrast and noise
 * NOT DONE YET
 *
 */
double imageQuality(CImg<unsigned short>& sourceImage)
{
  unsigned short max, min;
  max = sourceImage.max_min(min);

  cout << "Quality in contrast: " << ((double) (max-min))/USHRT_MAX << endl;

  return 0.;
}

/**
 * \param mag magnitude of fourier transform.
 * \param dl width of the circular area on which mean pixel value is performed.
 * \return frequency of the wave.
 *   
 * Returns the wave frequency using the fourier transform magnitude
 * mean value on two dl pixel circles on horizontal line.
 * 
 */

int wave_freq(const CImg<unsigned short> &mag,int dl,int max_radius )
{
  CImg<unsigned short> F2(mag);
  int r_mespp=0, r_mesp=0, r_mes=0, r_max=0,imax=0;
  int x0, y0;
  x0 = mag.width()/2;
  y0 = mag.height()/2;
  
  ofstream pic;
  pic.open("pic.dat");
  r_mes = 0;
  for (int j = -dl/2;j<dl/2;j++)
    for(int k=-dl/2; k<dl/2; k++)
    {
      r_mes += F2(x0+dl+j,y0+k);
      r_mes += F2(x0-dl+j,y0+k);
    } 
  r_mesp = r_mes;
  r_mespp = r_mes;
  r_max = 0;
  for (int i = dl; i < max_radius; i+=1)
  {
    r_mes = 0;
    
    for (int j = -dl/2;j<dl/2;j++)
      for(int k=-dl/2; k<dl/2; k++)
      {
        r_mes += F2(x0+i+j,y0+k);
        r_mes += F2(x0-i+j,y0+k);
      }
    
    if (r_max<r_mesp && r_mespp< r_mesp && r_mesp>r_mes)
    {
      r_max = r_mesp;
      imax = i-1;
    }
    //~ cout << r_mespp << "  " << r_mesp << "  " << r_mes << "  " << imax << endl;
    r_mespp = r_mesp;
    r_mesp = r_mes;
    
    pic << i << "    "<< r_mes << endl;
  }

  pic.close();
  return imax;
}

/**
 * \param img image (tilt must be corrected)
 *   
 * Computes the mean value of each vertical line to show horizontal 
 * ondulations.
 * 
 */
void wave_show(const CImg<unsigned short> img)
{
  int r_mean;
  
  ofstream waveOut;
  waveOut.open("wave.dat");
  vector <int> wave(img.width());
    
  for (int i = 0; i<img.width(); i+=1)
  {
    r_mean = 0;
    for (int j = 0; j<img.height(); j++)
    {
      r_mean += img(i,j);
    }
    waveOut << i << "    " << r_mean <<endl;
  }
  
  waveOut.close();
}



/**
 * \param mag magnitude of fourier transform.
 * \param dl width of the circular area on which mean pixel value is performed.
 * \return radius in pixel of the circle with the maximum mean pixel value.
 *
 * Returns the maximum frequency using the fourier transform magnitude
 * mean value on a dl pixel thick circle
 * 
 */

int max_freq(const CImg<unsigned short> &mag,int dl)
{
   CImg<unsigned short> F2(mag);
   int r_mes, r_max = 0, i_max = -1, nb_pt;
   int x, y, x0, y0;
   x0 = mag.width()/2;
   y0 = mag.height()/2;
   int max_radius = mag.width()<mag.height()? mag.width()/2-dl:mag.height()/2-dl;
   int nbPxl;

   for (int i = 20; i < max_radius; i+=1)
   {
       r_mes = 0;
       nb_pt = 0;
       nbPxl = 0;
       for(int j=-i;j<i;j++)
       {
           if (abs(j)>dl) 
           {
                x = x0-j;
                y = sqrt(i*i-j*j)+y0;
                for (int kx = 0;kx<dl;kx++)
                {
                  for (int ky = 0;ky<dl;ky++)
                    {
                    
                      if( F2(x+kx,y+ky)>0 ) nb_pt++;
                      r_mes += F2(x+kx,y+ky);
                      nbPxl++;
                      F2(x+kx,y+ky) = 0;
                    }
                }
                y = -sqrt(i*i-j*j)+y0;
                for (int kx = 0;kx<dl;kx++)
                {
                  for (int ky = 0;ky<dl;ky++)
                    {
                    
                      if( F2(x+kx,y+ky)>0 ) nb_pt++;
                      r_mes += F2(x+kx,y+ky);
                      nbPxl++;
                      F2(x+kx,y+ky) = 0;
                    }
                }
                y = y0-j;
                x = sqrt(i*i-j*j)+x0;
                for (int kx = 0;kx<dl;kx++)
                {
                  for (int ky = 0;ky<dl;ky++)
                    {
                    
                      if( F2(x+kx,y+ky)>0 ) nb_pt++;
                      r_mes += F2(x+kx,y+ky);
                      nbPxl++;
                      F2(x+kx,y+ky) = 0;
                    }
                }
                x = -sqrt(i*i-j*j)+x0;
                for (int kx = 0;kx<dl;kx++)
                {
                  for (int ky = 0;ky<dl;ky++)
                    {
                    
                      if( F2(x+kx,y+ky)>0 ) nb_pt++;
                      r_mes += F2(x+kx,y+ky);
                      nbPxl++;
                      F2(x+kx,y+ky) = 0;
                    }
                }
            }
       }
       F2 = mag;
       r_mes /= i;
       
       if (r_mes > r_max)
       {
           r_max = r_mes;
           i_max = i;
       }
   }
   //~ cout << "Maximum frequency " <<  i_max  << endl;
   return i_max;
}


/**
 * \param x value on which the function is computed.
 * \param x0 center of the bell, maximum value for x=x0.
 * \param n order of the bell.
 * \return radius in pixel of the circle with the maximum mean pixel value.
 *   
 * Compute the value of a bell shaped function centered on \a x0 with a slope
 * increasing with \a n.
 * 
 */
 
double bell(double x,double x0,int n)
{
    return 1./(1+pow(double(x)/x0,2*n));
}



/**
 * \param img image with black and white pixels.
 * \param i x-coordinate of the starting pixel.
 * \param j y-coordinate of the starting pixel.
 * \param coord[2] sum of the coordinates of burnt pixel.
 *   
 * Burns the black 4-neighbor recursively for a given pixel, record
 * number of pixel burnt in \a nbp and sum of their coordinates in \a coord
 * 
 */
void enflamme4(CImg<unsigned short>& img0, CImg<bool>& img, int i, int j, double(&coord)[2], double& nbp)
{
    img(i,j) = 1;
    double ponder=1./img0(i,j);
    ponder = (ponder>1.E-4) ? ponder:1.E-4;
    ponder = (ponder<1) ? ponder:1;
    coord[0] += double(i)*ponder;
    coord[1] += double(j)*ponder;
    nbp+=ponder;
    if (img(i+1,j)==0)
      enflamme4(img0,img, i+1, j,coord,nbp);
    if (img(i-1,j)==0)
      enflamme4(img0,img, i-1, j,coord,nbp);
    if (img(i,j+1)==0)
      enflamme4(img0,img, i, j+1,coord,nbp);
    if (img(i,j-1)==0)
      enflamme4(img0,img, i, j-1,coord,nbp);
}


//warning : changing img to white spots on black background is unused
/**
 * \param img0 image with only tilt recovery.
 * \param img image with black and white pixels.
 * \param i x-coordinate of the starting pixel.
 * \param j y-coordinate of the starting pixel.
 * \param coord[2] sum of the coordinates of burnt pixel.
 * \return number of black spots counted.
 * 
 * find center of every separate black spot changes img into a black
 * image with the centers marked in white (two spot are separate if
 * there is no black path through 4-neighbors)
 * 
 */
int find_center(CImg<unsigned short>& img0, CImg<bool>& img, vector < vector <double> >& cylinders)
{
    cylinders.clear();
    vector <double> cylinder(3,0);
    CImg<bool> img2(img.width(),img.height());
    double coord[2],nbp;
    int nb_center=0;
    //int x,y;
    img2.fill(0);
    cimg_for_borderXY(img,i,j,1)
    {
        img(i,j) = 1;
    }
    cimg_for_insideXY(img,i,j,1)
    {
        coord[0] = 0;
        coord[1] = 0;
        nbp = 0;
        if(img(i,j) == 0)
        {
            enflamme4(img0,img,i,j,coord,nbp);
            coord[0] /= nbp;
            coord[1] /= nbp;
            cylinder[0] = coord[0];
            cylinder[1] = coord[1];
            cylinder[2] = nbp;
            cylinders.push_back(cylinder);
            if (cylinder[0]<0 || cylinder[1]<0)
               cout << "Ahhhhaaaaaaaaaa!!!!!" << endl;
            //~ x = coord[0];
            //~ y = coord[1];
            //~ img2(x,y)=1;
            nb_center++;
        }
    }
    //~ img = img2;
    return nb_center;
}


/**
 * \param img image to be filtered.
 * \param freq output of maximum frequency found.
 * \param fband basis value for the filter's band width.
 * \param ratioXoY ratio of image size.
 * 
 * Fourier Filtering : smooth band-pass filter
 * automatic selection of passing frequency stored in freq
 * band width given by fbande. (inspired from CImg_demo)
 * 
 */
CImg<unsigned short> fourier_filtering(CImg<unsigned short> img, int& freq, int fbande, double ratioXoY)
{
//   max radius for filter, size of img has to be a power of 2
//  int rmax = pow(2,int(log((img.width()<img.height()? img.width():img.height()))/log(2)));
  
  // compute fast fourier transform
  CImgList<double> F = img.get_FFT();
  cimglist_apply(F,shift)(img.width()/2,img.height()/2,0,0,2);

	// compute magnitude of fourier transform
  const CImg<unsigned short> mag = ((F[0].get_pow(2) + F[1].get_pow(2)).sqrt() + 1).log().normalize(0,65535);
  CImg<unsigned short> mag2(mag);
  mag2.resize(mag.width()/ratioXoY,mag.height()*ratioXoY); //inverted ratio for frequency to be unidirectional
  CImg<double> mask(img.width(),img.height());
  CImg<double> mask2(img.width()/ratioXoY,img.height()*ratioXoY); //inverted ratio for next resizing to fit elliptic frequency
  int mi,mj,mi2,mj2;
  double r;
  CImgList<double> nF(F); 
  int dl = 10;
  freq = max_freq(mag2,3); // finds the frequency to select
  int imax = wave_freq(mag2, dl, freq/2);
  cout << "imax : " << imax << endl;
  
   // middle of the image
   mi2 = mask2.width()/2;
   mj2 = mask2.height()/2;

   mi = mag.width()/2;
   mj = mag.height()/2;
      
    fbande = (fbande + freq/2)/2; // bandpass larger with higher frequencies to capture more details

    cout << "Band-pass width : " << fbande << endl;
    mask2 = 0.;
    cimg_forXY(mask2,i,j) // compute smoothed mask on inverted ratio mask
    {
        r = sqrt((i-mi2)*(i-mi2)+(j-mj2)*(j-mj2));
        if(r > freq)
          mask2(i,j) = bell(r-freq,fbande/2,7);
        else
          mask2(i,j) = bell(r-freq,fbande/2,7);
    }
    mask = mask2;
    mask.resize(mag.width(),mag.height()); // resize to square -> the mask selects an elliptic band-pass
      // compute convolution
  cimglist_for(F,l) nF[l].mul(mask).shift(-mi,-mj,0,0,2);
  
  int x0, y0;
  x0 = mask2.width()/2;
  y0 = mask2.height()/2;
  mask2 = 0.;
  for (int j = -dl/2;j<dl/2;j++)
    for(int k=-dl/2; k<dl/2; k++)
    {
      mask2(x0+imax+j,y0+k) = 1;
      mask2(x0-imax+j,y0+k) = 1;
    }
  mask = mask2;
  mask.resize(mag.width(),mag.height());

  cimglist_for(F,l) F[l].mul(mask).shift(-mi,-mj,0,0,2);

  wave_show(F.FFT(true)[0].normalize(0,65535));
  cout<<"nF"<<endl;
  return nF.FFT(true)[0].normalize(0,65535);
}


/**
 * \param img image to be filtered.
 * \param freq output of maximum frequency found.
 * \param ratioXoY ratio of image size.
 * 
 * Fourier Filtering : dynamic band-pass filter selection from CImg_demo
 * 
 */
void  item_fourier_filtering(CImg<unsigned short>& img, int& freq, double ratioXoY)
{
  int rmin = 0;
  int rmax = img.width()<img.height()? img.width():img.height();
  int size_x = img.width()*ratioXoY, size_y = img.height()/ratioXoY;
  CImgList<float> F = img.get_FFT();
  cimglist_apply(F,shift)(img.width()/2,img.height()/2,0,0,2);
  const CImg<unsigned short> mag = ((F[0].get_pow(2) + F[1].get_pow(2)).sqrt() + 1).log().normalize(0,65535);
  CImgList<unsigned short> visu(img,mag);
  CImgDisplay disp(visu,"Fourier Filtering (L/R Click to set filter)");
  CImg<unsigned char> mask(img.width(),img.height(),1,1,1);
  CImg<unsigned char> maskVisu(mask);
  maskVisu.resize(size_x,size_y);
  CImg<unsigned short> img2(img);
  const unsigned short one[] = { 1 }, zero[] = { 0 }, white[] = { 65535 };
//  freq = max_freq(mag,3); // finds the frequency to select

//  cout << "Maximum frequency: " << freq << endl;
  
  while (!disp.is_closed() && !disp.is_keyQ() && !disp.is_keyESC() && !disp.is_keyENTER()) {
    disp.wait();
    const int
      xm = disp.mouse_x()*2*img.width()/disp.width() - img.width(),
      ym = disp.mouse_y()*img.height()/disp.height(),
      x = xm - img.width()/2,
      y = ym - img.height()/2;
    if (disp.button() && xm>=0 && ym>=0) {
      double ratioXoY2 = ratioXoY*ratioXoY;
      const int r = (int)cimg::max(0.0f,(double)std::sqrt((double)(x*x/ratioXoY2 + y*y*ratioXoY2)));
      if (disp.button()&1) rmax = r;
      if (disp.button()&2) rmin = r;
      if (rmin>=rmax) rmin = cimg::max(rmax - 1,0);
      mask.fill(0).draw_ellipse(mag.width()/2,mag.height()/2,rmax*ratioXoY,rmax/ratioXoY,0,one).
        draw_ellipse(mag.width()/2,mag.height()/2,rmin*ratioXoY,rmin/ratioXoY,0,zero);
      CImgList<float> nF(F);
      cimglist_for(F,l) nF[l].mul(mask).shift(-img.width()/2,-img.height()/2,0,0,2);
      visu[0] = nF.FFT(true)[0].normalize(0,65535);
      img2 = visu[0];
    }
    if (disp.is_resized()) disp.resize(disp.window_width(),disp.window_width()/2).display(visu);
    visu[1] = mag.get_mul(mask).draw_text(5,5,"Freq Min/Max = %d / %d",white,zero,0.6f,13,(int)rmin,(int)rmax);
    visu.display(disp);
  }
  freq = (rmax+rmin)/2;
  img = img2;
}



void  cropImage(CImg<unsigned short>& img)
{
  CImg<unsigned short> img2(img);
  unsigned short sImin = img2.min();
  img2 = (img2 - sImin)*(65535/(img2.max()-sImin)); // normalize
  CImg<unsigned short> visu(img2);
  CImgDisplay disp(visu,"Image crop (L/R Click to set crop)");
  CImg<unsigned char> mask(img2.width(),img2.height(),1,1,1);
  const unsigned short one[] = { 1 }, zero[] = { 0 }, white[] = { 65535 };
//  freq = max_freq(mag,3); // finds the frequency to select

  cout << "Crop Image" << endl;
  int xl=0, yt=0, yb=img2.height(), xr=img2.width();
  while (!disp.is_closed() && !disp.is_keyQ() && !disp.is_keyESC() && !disp.is_keyENTER())
  {
    disp.wait();
    const int
      xm = disp.mouse_x()*img.width()/disp.width(),
      ym = disp.mouse_y()*img.height()/disp.height();
    if (disp.button() && xm>=0 && ym>=0 && xm<img.width() && ym < img.height())
    {
      if (disp.button()&1)
      {
        xl = xm;
        yt = ym;
      }
      if (disp.button()&2)
      {
        xr = xm;
        yb = ym;
      }
      if (xl>xr)
      {
        xl = (xl+xr)/2-1;
        xr = (xl+xr)/2+1;
      }
      if (yt>yb)
      {
        yt = (yt+yb)/2-1;
        yb = (yt+yb)/2+1;
      }
      mask.fill(0).draw_rectangle(xl,yt,xr,yb,one);
      CImg<unsigned short> imgVisu(img2);
      imgVisu.mul(mask);
    }
    if (disp.is_resized()) disp.resize(disp.window_width(),disp.window_width()/2).display(visu);
    visu = img2.get_mul(mask);
    visu.display(disp);
  }
  xl = xl<0 ? 0:xl;
  xr = xr>=img.width() ? img.width()-1:xr;
  yt = yt<0 ? 0:yt;
  yb = yb>=img.height() ? img.height()-1:yb;
  img.crop(xl+2,yt+2,0,0,xr-2,yb-2,0,0,true);
}



/**
 * \param cylinders contains 2 coordinates and surface of nodes found on the image.
 * \param filename name of the output file.
 * \param h height of the image (\a coor[0] should be in [0,h]).
 * \param w width of the image (\a coor[1] should be in [0,w]).
 * 
 * output a file with .node extention containing number, coordinates and
 * surface of the nodes detected in the image.
 * 
 */
void output_node(const vector< vector <double> >& cylinders, double size, char* filename,int h, int w)
{
    setlocale(LC_ALL, "en_US.UTF-8");
    char name[512];
    strcpy(name,filename);
    ofstream file;
    file.open(strcat(name,".node"));
    int nb_point = cylinders.size();
    double ratioXoY = ((double) (w))/h;
    double ratioXoY2 = sqrt(ratioXoY);
    file << nb_point <<"  " << 2 <<"  " << size << "  " << ratioXoY2 << endl;
    double surface = h*w;
    double mhw = h>w ? h:w;
    double offsetx = ratioXoY>1. ? 0:((1.-(ratioXoY))/2);
    double offsety = ratioXoY<1. ? 0:((1.-(1/ratioXoY))/2);

    for (int i =0; i<nb_point; i++)
    {
        file << i << "  " << cylinders[i][0]/mhw+offsetx<< "  " << cylinders[i][1]/mhw+offsety << "  " << cylinders[i][2]/surface << endl;
    }
    file << "# end of file" << endl;
    file.close();
}


/**
 * \param fileName name of a file.
 * \return true if the file exists false otherwise.
 * 
 * tels if filName is an existing file
*/
bool fexist(const char *fileName)
{
    ifstream infile(fileName);
    return infile.good();
}


// not working at all
CImg<unsigned short> wiener(CImg<unsigned short>& img, double sigma)
{
    CImgList<double> F = img.get_FFT();
    double sigma2 = sigma*sigma;
    for (int k =0; k < F[0].width()*F[0].height(); k++)
    {
        double re = F[0](k), im = F[1](k);
        double mag2 = re*re + im*im;
        if(fabs(mag2+2*sigma2 < 1E-6))
        {
            F[0](k) = 1.;
            F[1](k) = 1.;
        }else
        {
            F[0](k) = re / (mag2+2*sigma2);
            F[1](k) = -im / (mag2+2*sigma2);
        }
    }
    return F.FFT(true)[0].normalize(0,65535);
}



/**
 * \param img image to be binarized.
 * \param windowSize size of the window used to compute local threshold.
 * \return black and white image.
 * 
 * outputs a binarized version of the input image \a img using local threshold
 * 
*/
CImg<bool> binarizeAdaptative(CImg<unsigned short>& img,unsigned short windowSize)
{
    CImg<bool> bimage(img.width(),img.height());
    
    // parameter setting the threshold
    double g = 0.20; // g = 1 : all white, g = 0 : all black 
    int Fmax = 65535;
    int threshold;
    // local maximum and minimum
    unsigned short Fxy_min = Fmax, Fxy_max = 0;
    
    
    // compute local maximum and minimum on a window around each pixel
    // set the threshold between the two values and add a black pixel to
    // bimage if the pixel value is greater than the threshold 
    cimg_for_insideXY(img,x,y,windowSize)
    {
        Fxy_min = Fmax;
        Fxy_max = 0;
        for(int i = -windowSize; i<=windowSize; i++)
        {
            for(int j = -windowSize; j<=windowSize; j++)
            {
                Fxy_min = Fxy_min<img(i+x,j+y)? Fxy_min:img(i+x,j+y);
                Fxy_max = Fxy_max>img(i+x,j+y)? Fxy_max:img(i+x,j+y);
            }
        }
        
        threshold =  g*Fxy_max + (1.-g)*Fxy_min;
        bimage(x,y) = (img(x,y)>threshold);
    }
    cimg_for_borderXY(img,x,y,windowSize)
    {
        bimage(x,y) = true;
    }
    return bimage;
}

/**
 * \param argc counts the number of argv parameters.
 * \param argv should get 7 parameters :
 * 				1 : name of the file to filter
 * 				2 : "b" if particles are white over black
 * 				3 : "m" if manual fft
 * 				4 : integer, bandwidth value basis
 * 				5 : "i" compressed image (openable with imagemagick)
 * 						"a" nanoscope image
 *        6 : "1" Recover tilt
 *        7 : index of the image slice to get
 * \return 0.
 * 
 * Calls every step for image filtering and extracting positions and 
 * surfaces. Outputs binarized image and data in a ".node" file.
 * 
*/
int mainFiltrage(int argc, char **argv, imageExchange& dataOut)
{
    setlocale(LC_ALL, "en_US.UTF-8");
    // get the name of the png picture to binarize
    char name[512],namet[512];
    bool fft_manuelle = false;
    int bandPass = 40;
    double realSize;
    if(argc>1)
    {
        strcpy(namet,argv[1]);
        strcpy(name,namet);
        while(!fexist(namet))
        {
            return -1;
            cout << "Nom du fichier a traiter : " ;
            cin >> namet;
            strcpy(name,namet);
            cout << endl;
        } 
    }else
    {
        while(!fexist(namet))
        {
            return -1;
            cout << "Nom du fichier a traiter : " ;
            cin >> namet;
            strcpy(name,namet);
            cout << endl;
        }       
    }
    cout << "Traitement de : " << name << endl;
    int ii = 1;
    while(argc>ii)
    {
      cout << "Parameter " << ii << "  " << argv[ii] << endl;
      ii++;
    }
    // check if automatic or manual fft is selected.
    if (argc>3)
    {
        if (strcmp(argv[3],"m") == 0)
        {
          fft_manuelle = true;
        }
    }
    
    // get bandwidth value basis
    if (argc>4)
    {
        bandPass = atoi(argv[4]);
        cout << " Bande passante utilisateur " << bandPass << endl;
    }
    
    // load image according to its type
    CImg<short> imageRaw;
    int nSlice;
    if (argc>7)
      nSlice = atoi(argv[7]);
    if (argc>5)
    {
        if (strcmp(argv[5],"i") == 0) // compressed image
        {
            //~ imageRaw.assign(name);
            imageRaw.load(name);
            dataOut.dimension = -1;
            nSlice = 0;
        }else if (strcmp(argv[5],"a") == 0) // nanoscope image
        {
            imageRaw = load_AFM(name, realSize);
            dataOut.dimension = realSize;
        }
    }else
        CImg<short> imageRaw(name);
    
    CImg<unsigned short> image = imageRaw.get_slice(nSlice)+32767;

    if (argc>8)
    {
      if (strcmp(argv[8],"1") == 0)
      {
        cropImage(image);
      }
    }


    CImg<bool> bimage(image.width(),image.height());
    CImg<unsigned char> bimage2(bimage);
    dataOut.pxHeight = image.height();
    dataOut.pxWidth = image.width();

    int freq;
    
    // inverts the image if the spots are white (if b is given)
    if( argc > 2)
    {
        if (strcmp(argv[2],"b") == 0)
        {
          image = -image;
          cout << "Inversion des couleurs" << endl;
        }
    }
    
    // supress the luminosity gradient due to the angle
    // supress also the horizontal lines due to line scan offset
    if (argc>6)
    { 
      if (strcmp(argv[6],"1") == 0) // compressed image
        {
            recoverTilt(image); 
        }
    }
    else
    {
      recoverTilt(image);
    }

    dataOut.quality = imageQuality(image);
    int size_x = image.width(), size_y = image.height();
    int rmax = image.width()>image.height()? image.width():image.height();
    rmax --;
    rmax |= rmax>>1;
    rmax |= rmax>>2;
    rmax |= rmax>>4;
    rmax |= rmax>>8;
    rmax |= rmax>>16;
    rmax++;

    double sizeXoverY = (double (image.width()))/image.height();
    double ratioXoY2 = sqrt(sizeXoverY);

    CImg<unsigned short> imageTiltR(image);

    image.resize(rmax ,rmax);

    //~ wave_show(image);  
    // compute a smooth band-pass Fourier filtering around an 
    // automaticaly detected or manualy selected frequency
    if (fft_manuelle)
        item_fourier_filtering(image,freq,ratioXoY2);
    else
        image = fourier_filtering(image,freq,bandPass,ratioXoY2);
    CImg<unsigned short> image2(image);
    image2.resize(rmax*ratioXoY2,rmax/ratioXoY2);
    cout << "Frequence selectionnee :  " << freq << endl;
    dataOut.frequency = freq;

    //~ cout <<"image.width=" << image.width()<< " " <<round(float(image.width())/(8*freq))<<endl;
    int radius= round( (sqrt((double) (image.width()*image.height())))/(8*freq));
    radius = radius>1.? radius:1.;

    CImg<double> mask(2*radius-1,2*radius-1);
    int radius_act;
    
    // helps making it less sensitive to the threshold 
    //~ image.sharpen(1000);
    //~ cout << "avant normalisation" << endl;
    unsigned short sImin = image2.min();
    image2 = (image2 - sImin)*(65535/(image2.max()-sImin)); // normalize

    //~ cout << "avant binarisation" << endl;
    bimage = binarizeAdaptative(image2,radius*3);
    
    // mask for spot opening algorithm
    cimg_forXY(mask,x,y)
    {
        radius_act =(x-radius+1)*(x-radius+1)+(y-radius+1)*(y-radius+1);
        if(radius_act<=radius*radius)
        {
            mask(x,y) = bell(radius_act,radius,3);
        }else
        {
            mask(x,y) = 0;
        }
    }
    cout << "Rayon du masque : " << radius << endl;
    dataOut.radius = radius;

    // opening algorithm to separate spots (degrades the real shape)
    bimage.dilate(mask).erode(mask);
    //~ bimage.erode(mask).dilate(mask);
        
    bimage2 = bimage;
    
    
    int nb_point;
    
    vector <vector <double> > cylinders;
        
    
    //output binarized and centers as png image
    //~ if(argc>1)
    //~ {
        //~ strcpy(name,argv[1]);
    //~ }else
    //~ {
        //~ strcpy(name,namet);
    //~ }
    
    string nametemp;
    nametemp = name;
    size_t found = nametemp.find_last_of(".");
    if (nametemp.size()-found<7)
      nametemp = nametemp.substr(0,found);
    found = nametemp.find_last_of("/");
    nametemp+="/"+nametemp.substr(found)+"_binarized.png";
    //cout << " 11111 " << nametemp << " index" << found << endl;
    imageTiltR.resize(bimage.width(),bimage.height());
    if (argc>5)
    {
        if (strcmp(argv[5],"i") == 0) // compressed image
        {
						// for correct representation of the image
            (bimage2).normalize(0,255).save_png(const_cast <char*> (nametemp.c_str()));
            // find centers of spots
            nb_point = find_center(imageTiltR, bimage.mirror('y'), cylinders);
        }else if (strcmp(argv[5],"a") == 0) // nanoscope image
        {						
						// for correct representation of the image
            (bimage2).mirror('x').rotate(180,0).normalize(0,255).save_png(const_cast <char*> (nametemp.c_str()));
            // find centers of spots
            nb_point = find_center(imageTiltR, bimage, cylinders);
        }
    }else //should not happen, default is compressed image
    {
        (bimage2).mirror('x').rotate(180,0).normalize(0,255).save_png(const_cast <char*> (nametemp.c_str()));
        nb_point = find_center(imageTiltR, bimage, cylinders);
    }
    


    nametemp = name;
    found = nametemp.find_last_of(".");
    if (nametemp.size()-found<7)
      nametemp = nametemp.substr(0,found);
    found = nametemp.find_last_of("/");
    nametemp+="/"+nametemp.substr(found)+"_image.png";
    //cout << " 11111 " << nametemp << " index" << found << endl;
    if (argc>5)
    {
        if (strcmp(argv[5],"i") == 0) // compressed image
        {
                        // for correct representation of the image
            (imageTiltR).normalize(0,255).save_png(const_cast <char*> (nametemp.c_str()));
            // find centers of spots
        }else if (strcmp(argv[5],"a") == 0) // nanoscope image
        {
                        // for correct representation of the image
            (imageTiltR).mirror('x').rotate(180,0).normalize(0,255).save_png(const_cast <char*> (nametemp.c_str()));
            // find centers of spots
        }
    }else //should not happen, default is compressed image
    {
        (imageTiltR).mirror('x').rotate(180,0).normalize(0,255).save_png(const_cast <char*> (nametemp.c_str()));
    }



    //~ if(argc>1)
    //~ {
        //~ strcpy(name,argv[1]);
    //~ }else
    //~ {
        //~ strcpy(name,namet);
    //~ }
    nametemp = name;
    found = nametemp.find_last_of(".");
    if (nametemp.size()-found<7)
      nametemp =  nametemp.substr(0,found);
    found = nametemp.find_last_of("/");
    nametemp+="/"+nametemp.substr(found);
    output_log(nb_point, image2.width(), image2.height(), bandPass, freq, radius);
    //output nodes coordinates in a .node file
    output_node(cylinders, dataOut.dimension, const_cast <char*> (nametemp.c_str()),image2.height(), image2.width());

    return 0;
}
