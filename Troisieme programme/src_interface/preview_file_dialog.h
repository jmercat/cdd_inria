#ifndef PREVIEW_FILE_DIALOG_H
#define PREVIEW_FILE_DIALOG_H

#include <QFileDialog>
#include <QComboBox>

class QLabel;

class PreviewFileDialog : public QFileDialog
{
    Q_OBJECT

public:
    explicit PreviewFileDialog(QWidget *parent = 0,
                               const QString & caption = QString(),
                               const QString & directory = QString(),
                               const QString & filter = QString());
    void setChannel(int);
signals:
    void channelChanged(int);
public slots:
    void setChannelSlot(int);
protected slots:
    void OnCurrentChanged(const QString & path);
protected:
    QLabel* mpPreview;
    void LoadAFMPreview(const QString & path, int layer, QPixmap &pixmap);
private:
    int m_imageChannel;
    QComboBox *m_imageChannelBox;
};

#endif // PREVIEW_FILE_DIALOG_H
