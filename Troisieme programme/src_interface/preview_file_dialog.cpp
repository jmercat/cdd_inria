/**
 * \file interface.cpp
 * \brief Graphical interface for crystal analysis
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 *
 * This file is a mix of codes from:
 * http://www.qtcentre.org/threads/33593-How-can-i-have-a-QFileDialog-with-a-preview-of-the-picture?p=293656#post293656
 * http://stackoverflow.com/questions/16987916/add-widgets-to-qfiledialog/16991667#16991667
 * http://stackoverflow.com/questions/20303882/qt-gui-what-is-the-fastest-way-to-load-a-binary-pixmap
 *
 */

#include "preview_file_dialog.h"
#include <QLabel>
#include <QGridLayout>
#include <iostream>
#include <QPainter>
#include <omp.h>

PreviewFileDialog::PreviewFileDialog(
    QWidget* parent,
    const QString & caption,
    const QString & directory,
    const QString & filter
    ) :
  QFileDialog(parent, caption, directory, filter)
{
  this->setOption(QFileDialog::DontUseNativeDialog,true);
  this->setMinimumWidth(1000);
  setObjectName("PreviewFileDialog");
  std::cout << "dossier " << directory.toStdString() << std::endl;
  this->selectFile(directory);
  QVBoxLayout* box = new QVBoxLayout();

  mpPreview = new QLabel(tr("Preview"), this);
  mpPreview->setAlignment(Qt::AlignCenter);
  mpPreview->setObjectName("labelPreview");
  mpPreview->setMinimumSize(512,512);
  box->addWidget(mpPreview);
  box->addStretch();

  // add to QFileDialog layout
  QGridLayout *layout = static_cast<QGridLayout*>(this->layout());

  QList< QPair<QLayoutItem*, QList<int> > > movedItems;
  for(int i = 0; i < layout->count(); i++)
  {
    int row, column, rowSpan, columnSpan;
    layout->getItemPosition(i,&row,&column,&rowSpan,&columnSpan);
    if (row > 2)
    {
      QList<int> list;
      list << row << column << rowSpan << columnSpan;
      movedItems << qMakePair(layout->takeAt(i),list);
      i--;
    }
  }
  for(int i = 0; i < movedItems.count(); i++)
  {
    layout->addItem(movedItems[i].first,
                    movedItems[i].second[0],
                    movedItems[i].second[1],
                    movedItems[i].second[2],
                    movedItems[i].second[3]
            );
  }

  layout->addItem(box,1,3,1,1);
  m_imageChannelBox = new QComboBox;

  m_imageChannelBox->addItem("Topography");
  m_imageChannelBox->addItem("Amplitude");
  m_imageChannelBox->addItem("Phase");


  layout->addWidget(m_imageChannelBox,2,3,1,1);

  m_imageChannel = 0;
  connect(this, SIGNAL(currentChanged(const QString&)), this, SLOT(OnCurrentChanged(const QString&)));
  connect(this->m_imageChannelBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setChannelSlot(int)));
  connect(this->m_imageChannelBox, SIGNAL(currentIndexChanged(int)), this, SIGNAL(channelChanged(int)));
}

void PreviewFileDialog::setChannel(int c)
{
  m_imageChannel = c;
  m_imageChannelBox->setCurrentIndex(c);
}

void PreviewFileDialog::setChannelSlot(int c)
{
  setChannel(c);
  m_imageChannelBox->setCurrentIndex(c);
  emit OnCurrentChanged(this->selectedFiles()[0]);
}

void PreviewFileDialog::OnCurrentChanged(const QString & path)
{
  QPixmap pixmap = QPixmap(path);
  if (pixmap.isNull()) {
    QByteArray afmImg;
    int layer = m_imageChannel;
    LoadAFMPreview(path, layer, pixmap);
  }
  if (pixmap.isNull()) {
    mpPreview->setText("not an image");
  } else {
    mpPreview->setPixmap(pixmap.scaled(mpPreview->width(), mpPreview->height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
  }
}

void PreviewFileDialog::LoadAFMPreview(const QString & path, int layer, QPixmap &pixmap)
{
  QFile imgFile(path);
  QString line;
  int offset,length,nx,ny;
  if(!imgFile.open(QIODevice::ReadOnly))
  {
//    std::cout << "Could not open file" << std::endl;
    return;
  }
  QRegExp afm("*.[0-9][0-9][0-9]");
  afm.setPatternSyntax(QRegExp::Wildcard);
  if (!afm.exactMatch(path))
  {
//    std::cout<< "Not an afm image" << std::endl;
    return;
  }
  for (int i = 0; i<layer+1; i++)
  {
    line = imgFile.readLine();
    while ( !imgFile.atEnd() &&  !line.startsWith("\\*Ciao image list"))
    {
      line = imgFile.readLine();
      if (line.startsWith("\\*File list end"))
      {
//        std::cout << "Reached end of file (e)" << std::endl;
        return;
      }
    }
  }
  while ( !imgFile.atEnd() &&  !line.startsWith("\\Data offset"))
  {
    line = imgFile.readLine();
    if (line.startsWith("\\*File list end"))
    {
//      std::cout << "Reached end of file (e)" << std::endl;
      return;
    }
  }
  if (imgFile.atEnd())
  {
//    std::cout << "Reached end of file (1)" << std::endl;
    return;
  }
  offset = line.section(' ',-1).toInt();
  while ( !imgFile.atEnd() &&  !line.startsWith("\\Data length"))
  {
    line = imgFile.readLine();
    if (line.startsWith("\\*File list end"))
    {
//      std::cout << "Reached end of file (e)" << std::endl;
      return;
    }
  }
  if (imgFile.atEnd())
  {
//    std::cout << "Reached end of file (2)" << std::endl;
    return;
  }
  length = line.section(' ',-1).toInt();

  while ( !imgFile.atEnd() &&  !line.startsWith("\\Samps/line"))
  {
    line = imgFile.readLine();
    if (line.startsWith("\\*File list end"))
    {
//      std::cout << "Reached end of file (e)" << std::endl;
      return;
    }
  }
  if (imgFile.atEnd())
  {
//    std::cout << "Reached end of file (3)" << std::endl;
    return;
  }
  nx = line.section(' ',-1).toInt();

  while ( !imgFile.atEnd() &&  !line.startsWith("\\Number of lines"))
  {
    line = imgFile.readLine();
    if (line.startsWith("\\*File list end"))
    {
//      std::cout << "Reached end of file (e)" << std::endl;
      return;
    }
  }
  if (imgFile.atEnd())
  {
//    std::cout << "Reached end of file (4)" << std::endl;
    return;
  }
  ny = line.section(' ',-1).toInt();

//  std::cout << "offset " << offset << " length " << length << std::endl;
  imgFile.reset();

  QByteArray afmImg(length,' ');
  afmImg = imgFile.readAll().mid(offset,length);
  imgFile.close();
  int fx = nx/512, fy = ny/512; //only a few pixels are read for efficiency (it is only a preview)
  fx = fx>0?fx:1;
  fy = fy>0?fy:1;
  QPixmap pixmapTemp(512,512);
  const qint16 *rawData = (const qint16*) afmImg.data();
  pixmap.fill(Qt::white);
  QPainter pixPaint(&pixmapTemp);
  QPoint q1;
  int Pmin=255,Pmax=0,Pval;
  // get the contrast

  for (int i=0;i<nx;i+=fx)
  {
    for(int j=0;j<ny;j+=fy)
    {
      q1.setX(i/fx);
      q1.setY(j/fy);
      Pval = (rawData[i*ny+j]+32767)*255/65535;
      Pmin = Pmin<Pval? Pmin:Pval;
      Pmax = Pmax>Pval? Pmax:Pval;
    }
  }
//  std::cout << Pmin << " " << Pmax << std::endl;
  // set the pixel to gray color with better contrast
  for (int i=0;i<nx;i+=fx)
  {
    for(int j=0;j<ny;j+=fy)
    {
      q1.setX(j/fx);
      q1.setY(i/fy);
      Pval = int ((double(rawData[i*ny+j]+32767)*255/65535-Pmin)*(double (Pmax)/(Pmax-Pmin)));
      pixPaint.setPen(QColor(Pval,Pval,Pval));

      pixPaint.drawPoint(q1);
    }
  }

  pixmap = pixmapTemp;
}
