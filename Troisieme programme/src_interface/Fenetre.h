#ifndef DEF_FENETRE
#define DEF_FENETRE

#include <QApplication>
#include <QtWidgets>
#include <QtGui>
#include <QByteArray>
#include <QPen>
#include <string>
#include <vector>
#include <thread>
#include "../libqcustomplot/qcustomplot.h"
#include "../src_exchange/exchange.h"
#include "preview_file_dialog.h"


#if defined(unix)        || defined(__unix)      || defined(__unix__) \
 || defined(linux)       || defined(__linux)     || defined(__linux__) \
 || defined(sun)         || defined(__sun) \
 || defined(BSD)         || defined(__OpenBSD__) || defined(__NetBSD__) \
 || defined(__FreeBSD__) || defined (__DragonFly__) \
 || defined(sgi)         || defined(__sgi) \
 || defined(__MACOSX__)  || defined(__APPLE__) \
 || defined(__CYGWIN__)
#define onlyUnix(a) a
#define onlyWindows(a)
#elif defined(_MSC_VER) || defined(WIN32)  || defined(_WIN32) || defined(__WIN32__) \
   || defined(WIN64)    || defined(_WIN64) || defined(__WIN64__)
#define onlyUnix(a)
#define onlyWindows(a) a
#endif
class HistoryGraphTab : public QGridLayout
{
   Q_OBJECT
  public:
    HistoryGraphTab();
    ~HistoryGraphTab();
    
    
  public slots:
    void plot();
    void clean();
    
  private:
    QVector<double> m_dataX,m_dataY;
    QCustomPlot *m_customPlot;
    QPushButton *m_plotButton;
    QPushButton *m_cleanButton;
    QInputDialog *m_typeGraph;
    QLabel *m_nHistLabel1;
    QLabel *m_nHistLabel2;
    QLabel *m_typeGraphLabel;
    QInputDialog *m_nHist;
};


class OutputImageTab : public QWidget
{
  Q_OBJECT
  
  public:
    OutputImageTab();
    ~OutputImageTab();
    bool loadFile(const QString &);
  public slots:
    void setData(allExchange data);
  private slots:
    //~ void open();
    //~ void saveAs();
    //~ void print();
    //~ void copy();
    //~ void paste();
    void zoomIn();
    void zoomOut();
    void normalSize();
    //~ void wheelEvent(QWheelEvent *event); //automatic call when scrolling
    //~ void fitToWindow();
    //~ void about();
    
  private:
    //~ void createActions();
    //~ void createMenus();
    //~ void updateActions();
    //~ bool saveFile(const QString &fileName);
    //~ void setImage(const QImage &newImage);
    allExchange m_inputData;
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);
//~ 
    //~ QImage image;
    QPushButton *m_zoomp, *m_zoomm;
    QPushButton *m_zoomn;
    QLabel *m_scaleFactorLabel;
    QImage image;
    QString *m_file;
    QGridLayout *m_gridLayout;
    QLabel *imageLabel;
    QScrollArea *scrollArea;
    double scaleFactor;
//~ 
    //~ #ifndef QT_NO_PRINTER
      //~ QPrinter printer;
    //~ #endif
//~ 
    //~ QAction *saveAsAct;
    //~ QAction *printAct;
    //~ QAction *copyAct;
    //~ QAction *zoomInAct;
    //~ QAction *zoomOutAct;
    //~ QAction *normalSizeAct;
};

class ResultTab : public QGridLayout
{
  Q_OBJECT
  public:
    ResultTab(QWidget *parent=0);
  public slots:
    void print(bool b);
    void setData(allExchange dataIn);

  signals:
    void sendFit(double,double,double,double);

  private:
    void plotHist();
    allExchange m_data;
    QLabel *m_name, *m_time, *m_nPoints, *m_defectDensity;
    QLabel *m_realDimension, *m_nGrains;
    QCustomPlot *m_plotVecHist;
    QCPMarginGroup *m_marginGroup;
    QFile *m_log;
    QImage image;
    QLabel *imageLabel;
    QScrollArea *scrollArea;
    double m_expFit[2], m_powFit[2];
    QCPBars **m_hist;
    QCPAxisRect **m_rect;
    int m_histSizeP;


};

class CorrelationGraphTab : public QGridLayout
{
  Q_OBJECT
  public:
    CorrelationGraphTab();
    ~CorrelationGraphTab();

    
    
  public slots:
    void plot(bool b);
    void save(QString fileName);
    void recvFit(double, double, double, double);

  private:
    QVector<double> m_dataX,m_dataG, m_dataG6, m_dataG6Exp, m_dataG6Pow;
    QCustomPlot *m_customPlotG;
    QCustomPlot *m_customPlotG6;
    QCPMarginGroup *m_marginGroup;
    QPushButton *m_exportButton;
    QComboBox *m_formatExport;
    std::thread *m_job;
    void plot(std::string);
    double m_expFit[2], m_powFit[2];
    void saveThread(QString fileName);
};

class ImageTab : public QGridLayout
{
  Q_OBJECT
  
  public:
    ImageTab();
    ~ImageTab();
  //~ private:
    QSlider *m_sliderBandPass;
    QComboBox *m_spotColor;
    QComboBox *m_imageChannel;
    QCheckBox *m_cropImage;
    QCheckBox *m_lineRecover;
    QCheckBox *m_manualFFT;
    QLabel *m_channelLabel;
    QLabel *m_spotLabel;
    QLabel *m_bandPassLabel;
    
  public slots:
    void disableSlider(int);
};

class CrystalTab : public QGridLayout
{
  Q_OBJECT
  
  public:
    CrystalTab();
    ~CrystalTab();
  //~ private:
    QComboBox *m_wireType;
    QComboBox *m_colorType;
    QLabel *m_wireLabel;
    QLabel *m_colorLabel;
    
    QCheckBox *m_grains;
    QCheckBox *m_grainsBoundary;
    QCheckBox *m_defects;
    
};

class Settings : public QWidget
{
  public:
    Settings();
    ~Settings();
  //~ private:
    QWidget *m_globalWidget;
    QTabWidget *m_tabs;
    QWidget *m_tab1, *m_tab2, *m_tab3, *m_tab4, *m_tab5, *m_tab6;
    ImageTab *m_imageLayout;
    CrystalTab *m_analysisLayout;
    CorrelationGraphTab *m_correlationLayout;
    ResultTab *m_resultLayout;
    OutputImageTab *m_outputImageWidget;
    HistoryGraphTab *m_historyGraphLayout;
    QHBoxLayout *m_outputImageLayout;
    //~ QGridLayout *m_correlationLayout;
};

class Buttons : public QHBoxLayout
{
  Q_OBJECT
  
  public:
    Buttons(QApplication *app, QSettings *settings);
    ~Buttons();
    // image options setters
    
  private:    
    allExchange m_inputData;
    QPushButton *m_quitButton, *m_computeButton;
    QSettings *m_settings;
    std::vector<std::string> optImage;
    std::vector<std::string> optDelaunay;
    bool setTypeImage();
    bool m_input;
    
  public slots:  
    void compute(QUrl *url);
    void compute();
    // set optImage
    void setInput(QString input);
    void setChannel(int channelIndex);
    void setSpotColor(int spotColorIndex);
    void setFFTBandPass(int bandPass);
    void setManualFFT(int isChecked);
    void setCropImage(int isChecked);
    void setLineRecover(int isChecked);
    // set optDelaunay
    void setWireType(int wireTypeIndex);
    void setColor(int colorIndex);
    void isGrains(int isShow);
    void isGrainsBoundary(int isShow);
    void isDefects(int isShow);
    void wrongInput();
    void rightInput();
  
  signals:
    void startCompute();
    void endCompute(bool hasComputed);
    void sendFileName(QString fileName);
    void sendData(allExchange data);
  
};

class Input : public QGridLayout
{
  Q_OBJECT
  
  public:
    Input(QSettings *settings);
    ~Input();
  //~ private:
    QLineEdit *m_path;
    QPushButton *m_fileButton;
    PreviewFileDialog *m_fileDialog;
    QLabel *m_pathLabel;
    QSettings *m_settings;
  public slots:
    void refreshTextToPath();
    void saveDirectory(QString path);
    void textToPath(QString path);
    void getChannel(int);
  signals:
    void pathChanged(QString path);
    void wrongInput();
    void rightInput();
  private:
    int m_imageChannel;
};



class Layout : public QWidget
{
  Q_OBJECT
  
  public:
    Layout(QApplication *app);
    ~Layout();
    
  private slots:
    void setImage(bool b);
    void showImage(int exitCode, QProcess::ExitStatus exitStatus);

  private:
    QVBoxLayout *m_globalLayout;
    Input *m_inputLayout;
    Settings *m_settingsWidget;
    Buttons *m_buttonsLayout;
    QSettings *m_settings;
    void setSettings();
    QProcess *m_process;

    void resizeEvent(QResizeEvent *event);
    double sizeFactorx, sizeFactory;
    //~ bool resizeWanted = true;
    
  protected:
    
   /**
    * this event is called when the mouse enters the widgets area during a drag/drop operation
    */
   void dragEnterEvent(QDragEnterEvent* event);
 
   /**
    * this event is called when the mouse moves inside the widgets area during a drag/drop operation
    */
   void dragMoveEvent(QDragMoveEvent* event);
 
   /**
    * this event is called when the mouse leaves the widgets area during a drag/drop operation
    */
   void dragLeaveEvent(QDragLeaveEvent* event);
 
   /**
    * this event is called when the drop operation is initiated at the widget
    */
   void dropEvent(QDropEvent* event);
};



#endif
