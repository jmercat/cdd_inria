/**
 * \file interface.cpp
 * \brief Graphical interface for crystal analysis
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */


#include <QApplication>
#include <QtWidgets>
#include "Fenetre.h"
#include <locale.h>     /* struct lconv, setlocale, localeconv */
//QMAKE_LIBDIR_FLAGS += -L. -L../src_delaunay/ -L../src_delaunay/mmg/src/common -I../src_delaunay/mmg/src/mmg -I../src_delaunay/mmg/src/mmg2d -L../src_fourier/ -Lrelease -Ldebug -lpthread -lfiltrage -O2 -L/usr/local/lib -ldelaunay -lmmg2d -I../src_delaunay/mmg/mmg2d
int main(int argc, char **argv)
{
  QApplication app(argc, argv);
  setlocale(LC_NUMERIC, "en_US");
  Layout layout(&app);
  layout.show();
  return app.exec();
}
