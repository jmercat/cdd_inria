/**
 * \file Fenetre.cpp
 * \brief Graphical interface for crystal analysis
 * \author Jean Mercat (Inria/UBordeaux)
 * \version 1
 * \copyright ???
 */


#include "Fenetre.h"
#include <iostream>
#include <QImageReader>
#include "../src_fourier/Filtrage_Fourier.hpp"
#include "../src_delaunay/delaunay.hpp"
#include "../src_delaunay/inOut.h"
#include <thread>

/**
 *
 * @param app application
 *
 * Sets the interface and connect signals
 *
 */
Layout::Layout(QApplication *app)
{
  m_settings = new QSettings("Inria","Interface");
  
  // create the layout and construct the 3 sub-layouts
  m_process = new QProcess;
  m_globalLayout = new QVBoxLayout;
  m_inputLayout = new Input(m_settings);
  m_settingsWidget = new Settings();
  m_buttonsLayout = new Buttons(app,m_settings);

  // set the 3 parts of the window : input file line, settings tabs, bottom buttons
  m_globalLayout->addLayout(m_inputLayout,1);
  m_globalLayout->addWidget(m_settingsWidget,2);
  m_globalLayout->addLayout(m_buttonsLayout,0);  

  setLayout(m_globalLayout);  
  
  // set the size of the window and fix ite(870,500);
  adjustSize();
  sizeFactorx = 30;
  sizeFactory = 110;
  resize(this->width(),this->height());
  
  
  // center the window on screen
  setGeometry(QStyle::alignedRect(Qt::LeftToRight,Qt::AlignCenter,
    size(),app->desktop()->availableGeometry()));
  
  setAcceptDrops(true);
  // connection between input and computing
  QWidget::connect(m_inputLayout,
    SIGNAL(pathChanged(QString)), m_buttonsLayout, SLOT(setInput(QString)));
    
  // connection between Image processing settings, input and computing
  QWidget::connect(m_settingsWidget->m_imageLayout->m_manualFFT,
    SIGNAL(stateChanged(int)), m_buttonsLayout, SLOT(setManualFFT(int)));

  QWidget::connect(m_settingsWidget->m_imageLayout->m_cropImage,
    SIGNAL(stateChanged(int)), m_buttonsLayout, SLOT(setCropImage(int)));
    
  QWidget::connect(m_settingsWidget->m_imageLayout->m_lineRecover,
    SIGNAL(stateChanged(int)), m_buttonsLayout, SLOT(setLineRecover(int)));
  
  QWidget::connect(m_settingsWidget->m_imageLayout->m_sliderBandPass,
    SIGNAL(valueChanged(int)), m_buttonsLayout, SLOT(setFFTBandPass(int)));
  
  QWidget::connect(m_settingsWidget->m_imageLayout->m_spotColor,
    SIGNAL(currentIndexChanged(int)), m_buttonsLayout, SLOT(setSpotColor(int)));

  QWidget::connect(m_settingsWidget->m_imageLayout->m_imageChannel,
    SIGNAL(currentIndexChanged(int)), m_buttonsLayout, SLOT(setChannel(int)));

  QWidget::connect(m_settingsWidget->m_imageLayout->m_imageChannel,
    SIGNAL(currentIndexChanged(int)), m_inputLayout->m_fileDialog, SLOT(setChannelSlot(int)));

  QWidget::connect(m_inputLayout->m_fileDialog, SIGNAL(channelChanged(int)),
   m_settingsWidget->m_imageLayout->m_imageChannel, SLOT(setCurrentIndex(int)));
    
  // connection between Crystal analysis settings and computing
  QWidget::connect(m_settingsWidget->m_analysisLayout->m_wireType,
    SIGNAL(currentIndexChanged(int)), m_buttonsLayout, SLOT(setWireType(int)));
  
  QWidget::connect(m_settingsWidget->m_analysisLayout->m_colorType,
    SIGNAL(currentIndexChanged(int)), m_buttonsLayout, SLOT(setColor(int)));
  
  QWidget::connect(m_settingsWidget->m_analysisLayout->m_grains,
    SIGNAL(stateChanged(int)), m_buttonsLayout, SLOT(isGrains(int)));
    
  QWidget::connect(m_settingsWidget->m_analysisLayout->m_grainsBoundary,
    SIGNAL(stateChanged(int)), m_buttonsLayout, SLOT(isGrainsBoundary(int)));
    
  QWidget::connect(m_settingsWidget->m_analysisLayout->m_defects,
    SIGNAL(stateChanged(int)), m_buttonsLayout, SLOT(isDefects(int)));
  
  // connection between Buttons and computing
  QWidget::connect(m_buttonsLayout,
  SIGNAL(startCompute()), m_inputLayout, SLOT(refreshTextToPath()));

  QWidget::connect(m_inputLayout,
  SIGNAL(wrongInput()), m_buttonsLayout, SLOT(wrongInput()));

  QWidget::connect(m_inputLayout,
  SIGNAL(rightInput()), m_buttonsLayout, SLOT(rightInput()));

  QWidget::connect(m_buttonsLayout, SIGNAL(endCompute(bool)),
  m_settingsWidget->m_correlationLayout, SLOT(plot(bool)));

  QWidget::connect(m_buttonsLayout, SIGNAL(endCompute(bool)), 
  m_settingsWidget->m_resultLayout, SLOT(print(bool)));
  
  QWidget::connect(m_buttonsLayout, SIGNAL(endCompute(bool)), 
  this, SLOT(setImage(bool)));

  QWidget::connect(m_buttonsLayout,SIGNAL(sendFileName(QString)),
  m_settingsWidget->m_correlationLayout, SLOT(save(QString)));
  
  QWidget::connect(m_process, SIGNAL(finished(int,QProcess::ExitStatus)), 
  this, SLOT(showImage(int,QProcess::ExitStatus)));

  // dataExchange connections
  QWidget::connect(m_buttonsLayout,SIGNAL(sendData(allExchange)),
  m_settingsWidget->m_resultLayout, SLOT(setData(allExchange)));

  QWidget::connect(m_buttonsLayout,SIGNAL(sendData(allExchange)),
  m_settingsWidget->m_outputImageWidget, SLOT(setData(allExchange)));

  setSettings();

 resize(1000,600);


}

Layout::~Layout()
{
  delete m_process;
}

/**
 *
 * @param event
 *
 * Allow resize event
 *
 */
void Layout::resizeEvent(QResizeEvent *event)
{
  m_settingsWidget->m_tabs->resize(event->size().width()-sizeFactorx,event->size().height()-sizeFactory);

  event->accept();
}

/**
 *
 * Set the settings to their previous values stored in m_settings.
 *
 */
void Layout::setSettings()
{
  m_inputLayout->m_path->setText(m_settings->value("input").toString());
  std::string path;
  path = m_settings->value("directory").toString().toStdString();
  path = path.substr(0,path.find_last_of("/"));
  m_inputLayout->m_fileDialog->setDirectory(path.c_str());

  m_settingsWidget->m_imageLayout->m_imageChannel->setCurrentIndex(m_settings->value("channel").toInt());
  m_settingsWidget->m_imageLayout->m_spotColor->setCurrentIndex(m_settings->value("spotColor").toInt());
  
  m_settingsWidget->m_imageLayout->m_sliderBandPass->setValue(m_settings->value("FFTBandPass").toInt());
  
  m_settingsWidget->m_imageLayout->m_lineRecover->setCheckState((Qt::CheckState)m_settings->value("lineRecover").toInt());
  m_settingsWidget->m_imageLayout->m_manualFFT->setCheckState((Qt::CheckState)m_settings->value("manualFFT").toInt());
  m_settingsWidget->m_imageLayout->m_cropImage->setCheckState((Qt::CheckState)m_settings->value("cropImage").toInt());


  m_settingsWidget->m_analysisLayout->m_grains->setCheckState((Qt::CheckState)m_settings->value("grains").toInt());
  m_settingsWidget->m_analysisLayout->m_grainsBoundary->setCheckState((Qt::CheckState)m_settings->value("grainsBoundary").toInt());
  m_settingsWidget->m_analysisLayout->m_defects->setCheckState((Qt::CheckState)m_settings->value("defects").toInt());

  m_settingsWidget->m_analysisLayout->m_wireType->setCurrentIndex(m_settings->value("wireType").toInt());
  m_settingsWidget->m_analysisLayout->m_colorType->setCurrentIndex(m_settings->value("color").toInt());

 
  // The following should not be necessary since the connections are made but it seems that some of them don't work...
  
  m_buttonsLayout->setChannel(m_settings->value("channel").toInt());
  m_buttonsLayout->setSpotColor(m_settings->value("spotColor").toInt());
  
  m_buttonsLayout->setFFTBandPass(m_settings->value("FFTBandPass").toInt());
  
  m_buttonsLayout->setLineRecover(m_settings->value("lineRecover").toInt());
  m_buttonsLayout->setManualFFT(m_settings->value("manualFFT").toInt());
  m_buttonsLayout->setCropImage(m_settings->value("cropImage").toInt());
  
  m_buttonsLayout->isGrains(m_settings->value("grains").toInt());
  m_buttonsLayout->isGrainsBoundary(m_settings->value("grainsBoundary").toInt());
  m_buttonsLayout->isDefects(m_settings->value("defects").toInt());
  
  m_buttonsLayout->setColor(m_settings->value("color").toInt());
  m_buttonsLayout->setWireType(m_settings->value("wireType").toInt());
  
}
/**
 *
 * @param event
 *
 * Allows drag and drop.
 *
 */
void Layout::dragEnterEvent(QDragEnterEvent* event)
{
 // if some actions should not be usable, like move, this code must be adopted
 event->acceptProposedAction();
}

/**
 *
 * @param event
 *
 * Allows drag and drop.
 *
 */
void Layout::dragMoveEvent(QDragMoveEvent* event)
{
 // if some actions should not be usable, like move, this code must be adopted
 event->acceptProposedAction();
}

/**
 *
 * @param event
 *
 * Allows drag and drop.
 *
 */
void Layout::dragLeaveEvent(QDragLeaveEvent* event)
{
 event->accept();
}

/**
 *
 * @param event
 *
 * Allows drag and drop. Get the path of the droped file and compute.
 *
 */
void Layout::dropEvent(QDropEvent *event)
{
  if (event->mimeData()->hasUrls())
  { 
    QList<QUrl> urlList = event->mimeData()->urls();
    for(QList<QUrl>::iterator l = urlList.begin(); l != urlList.end(); ++l)
    { 
      QUrl url = *l;
      QString path = url.toString();
      onlyWindows(int index = path.indexOf("C:/");
      path = path.remove(0,index);)
      onlyUnix(int index = path.indexOf("/home");
      path = path.remove(0,index);)
      m_inputLayout->m_path->setText(path);
      //~ std::cout << url.toLocalFile().toStdString() << std::endl;
      m_buttonsLayout->rightInput();
      m_buttonsLayout->compute(&url);
      
    }
    event->acceptProposedAction();
  }else
    event->ignore();
  
}

/**
 *
 * @param exitCode
 * @param exitStatus
 *
 * Load image in the m_outputImageWidget of m_settingsWidget.
 *
 */
void Layout::showImage(int exitCode, QProcess::ExitStatus exitStatus)
{
  std::cout << "exitCode " << exitCode << " exit Status " << exitStatus << std::endl;
  QString fileName =  m_inputLayout->m_path->text();
    
  int index = fileName.lastIndexOf(".");
  if(fileName.length()-index<7)
    fileName = fileName.left(index);
  index = fileName.lastIndexOf("/");
  QString fileNameTemp = fileName.right(fileName.size()-index);
  if (fileName.lastIndexOf(fileNameTemp)==fileName.indexOf(fileNameTemp))
  {
    fileName += fileName.right(fileName.size()-index);
    std::cout << "add folder" << std::endl;
  }
  onlyWindows(index = fileName.indexOf("C:/");)
  onlyUnix(index = fileName.indexOf("/home");)
  fileName = fileName.remove(0,index);
  m_settingsWidget->m_outputImageWidget->loadFile(fileName+"_out.png");
}

/**
 *
 * @param b computation ended
 *
 * Convert output post script image to png (this takes some time and is therefore done in background)
 *
 */
void Layout::setImage(bool b)
{
  if (b)
  {
    QString fileName =  m_inputLayout->m_path->text();
    int index = fileName.lastIndexOf(".");
    if(fileName.length()-index<7)
      fileName = fileName.left(index);
    index =fileName.length()- fileName.lastIndexOf("/");
//    std::cout << "index " << index << std::endl;
    QString nameTemp = fileName.right(index);
    if (fileName.lastIndexOf(nameTemp)==fileName.indexOf(nameTemp))
      fileName = fileName+fileName.right(index);
    onlyWindows(QString processName = "gswin64c.exe -dBATCH -dNOPAUSE -r300 -sDEVICE=png16m -sOutputFile=\""
            +fileName+"_out.png\" \""+fileName+"_out.ps\"";)
    onlyUnix(QString processName = "gs -dBATCH -dNOPAUSE -r300 -sDEVICE=png16m -sOutputFile=\""
            +fileName+"_out.png\" \""+fileName+"_out.ps\"";)
    m_process->start(processName);

  }
}

/**
 *
 * @param settings
 *
 * Constructor, set input settings values and connections.
 *
 */
Input::Input(QSettings *settings)
{
  m_settings = settings;
  m_path = new QLineEdit();
  m_pathLabel = new QLabel("File:");
  m_imageChannel = settings->value("channel").toInt();
  this->addWidget(m_pathLabel, 0,0);
  this->addWidget(m_path, 0,1);
  m_fileButton = new QPushButton("...");
  this->addWidget(m_fileButton,0,2);
  QString filter;
  filter.push_back("*.jpg ");
  filter.push_back("*.png ");
  filter.push_back("*.tiff ");
  filter.push_back("*.tif ");
  filter.push_back("*.jpeg ");
  filter.prepend("*.raw ");
  filter.prepend("*.ppm ");
  filter.prepend("*.bmp ");
  filter.prepend("*.dlm ");
  filter.push_back("*.JPG ");
  filter.push_back("*.JPEG ");
  filter.push_back("*.PNG ");
  filter.push_back("*.TIFF ");
  filter.push_back("*.TIF ");
  filter.prepend("*.RAW ");
  filter.prepend("*.PPM ");
  filter.prepend("*.BMP ");
  filter.prepend("*.DLM ");
  filter.push_back("*.node ");
  for (int i = 0; i<1000; i++)
  {
    filter.push_back("*.");
    filter.push_back(QString("%1").arg(i,3,10, QChar('0')));
    filter.push_back(' ');
  }
  m_fileDialog = new PreviewFileDialog(0,tr("File to process"),m_settings->value("fileName").toString(),filter);
  
  QWidget::connect(m_fileButton, SIGNAL(clicked()), m_fileDialog, SLOT(open()));
  QWidget::connect(m_fileDialog, SIGNAL(fileSelected(QString)), m_path, SLOT(setText(QString)));
  QWidget::connect(m_fileDialog, SIGNAL(fileSelected(QString)), this, SLOT(saveDirectory(QString)));
 
}

Input::~Input()
{}

/**
 *
 * Set path text in input line
 *
 */
void Input::refreshTextToPath()
{
  textToPath(m_path->text());
  m_settings->setValue("fileName",m_path->text());
}

void Input::getChannel(int c)
{
  m_imageChannel = c;
  m_fileDialog->setChannel(c);
}

/**
 *
 * @param path path to file to compute
 *
 * Set path for next computations.
 *
 */
void Input::textToPath(QString path)
{
  QString path0;
  QUrl url;
  onlyWindows(int index = path.indexOf("C:/");
  path = path.remove(0,index);)
  onlyUnix(int index = path.indexOf("/home");
  path = path.remove(0,index);)
  url = QUrl::fromUserInput(path,QDir::currentPath());
  if(url.isLocalFile())
  {
    path = url.toLocalFile();
    if (path != path0)
    {
      if (isfexist(const_cast <char *> (path.toStdString().c_str())))
      {
        m_path->setText(path);
        emit rightInput();
        emit pathChanged(path);
      }else
      {
        std::cout << "Given path does not exist " << path.toStdString() << std::endl;
        QString errMessage = "Input error, file "+path+" could not be found";
        QMessageBox::information(0,"error",errMessage);
        emit wrongInput();
      }
    }
  }
}

/**
 *
 * @param path to file
 *
 * Save path in settings variable (kept for next launch).
 *
 */
void Input::saveDirectory(QString path)
{
  m_settings->setValue("directory", path);
}

/**
 *
 * @param app application
 * @param settings saved settings
 *
 * Constructor for compute and quit buttons.
 *
 */
Buttons::Buttons(QApplication *app,QSettings *settings)
{
  m_settings = settings;

  // set the vector of parameters to pass to the image processing and analysis functions
  optImage.resize(9);
  optDelaunay.resize(4);
  optImage[2] = "n";
  optImage[3] = "a";
  optImage[4] = "40";
  optImage[5] = "a";
  optImage[6] = "1";
  optImage[7] = "0";
  optImage[8] = "0";

  optDelaunay[2] = "a";
  optDelaunay[3] = "1";

  // set the buttons and connect them
  m_quitButton = new QPushButton("Quit");
  m_computeButton = new QPushButton("Compute");
  
  this->addWidget(m_computeButton);
  this->addWidget(m_quitButton);
  QWidget::connect(m_computeButton, SIGNAL(clicked()), this, SIGNAL(startCompute()));
  QWidget::connect(m_quitButton, SIGNAL(clicked()), app, SLOT(quit()));
  QWidget::connect(m_computeButton, SIGNAL(clicked()), this, SLOT(compute()));
  

}

Buttons::~Buttons()
{}

/**
 * Store the result of the verification of the file path input
 */
void Buttons::wrongInput()
{
  m_input = false;
}

/**
 * Store the result of the verification of the file path input
 */
void Buttons::rightInput()
{
  m_input = true;
}

/**
 * @param colorIndex index of the chosen item in the combobox
 *
 * Set the parameters to pass to the analysis function with
 * the user's choice: color the output by which value.
 *
 */
void Buttons::setColor(int colorIndex)
{
  int opt3 = std::stoi(optDelaunay[3]);
  opt3 = opt3>0? opt3:1;
  switch (colorIndex)
  {
    case 0:
      if (opt3%7!=0)
        opt3*=7;
      optDelaunay[2] = "a";
      break;
    case 1:
      if (colorIndex == 1)
      {
        while(opt3%7==0)
          opt3/=7;
      }
      break;
    case 2:
      if (opt3%7!=0)
        opt3*=7;
      optDelaunay[2] = "l";
      break;
    case 3:
      if (opt3%7!=0)
        opt3*=7;
      optDelaunay[2] = "d01";
      break;
    case 4:
      if (opt3%7!=0)
        opt3*=7;
      optDelaunay[2] = "d2";
      break;
    case 5:
      if (opt3%7!=0)
        opt3*=7;
      optDelaunay[2] = "d0";
      break;
    case 6:
      if (opt3%7!=0)
        opt3*=7;
      optDelaunay[2] = "d1";
      break;
  }
  m_settings->setValue("color", colorIndex);
  optDelaunay[3] = std::to_string(opt3);
}

/**
 *
 * @param wireTypeIndex index chosen in the combobox
 *
 * Set the analysis parameters with the user's choice:
 * set wire type to delaunay or voronoï.
 *
 */
void Buttons::setWireType(int wireTypeIndex)
{
  int opt3 = std::stoi(optDelaunay[3]);  
  opt3 = opt3>0? opt3:1;
  if (opt3%5 !=0)
    opt3*=5;
  if (wireTypeIndex == 0)
  {
    while(opt3%11==0)
      opt3/=11;

  }else if(wireTypeIndex == 1)
  {
    if (opt3%11!=0)
      opt3*=11;
  }else
  {
    while(opt3%5==0)
      opt3/=5;
    while(opt3%11==0)
      opt3/=11;
  }
  m_settings->setValue("wireType", wireTypeIndex);
  optDelaunay[3] = std::to_string(opt3);
}

/**
 *
 * @param isShow user's choice in checkbox
 *
 * Set the analysis parameters with the user's choice.
 * Set if grains should be shown or not (color nodes randomly with same color for same grain)
 *
 */
void Buttons::isGrains(int isShow)
{
  int opt3 = std::stoi(optDelaunay[3]);
  opt3 = opt3<1? 1:opt3;
  if (isShow)
  {
    if (opt3%13!=0)
      opt3*=13;
  }else
  {    
    while(opt3%13==0)
      opt3/=13;
  }
  m_settings->setValue("grains",isShow);
  optDelaunay[3] = std::to_string(opt3);   
}

/**
 *
 * @param isShow user's choice in checkbox
 *
 * Set the analysis parameters with the user's choice.
 * Set if grain's boundary should be shown.
 *
 */
void Buttons::isGrainsBoundary(int isShow)
{
  int opt3 = std::stoi(optDelaunay[3]);
  opt3 = opt3<1? 1:opt3;
  if (isShow)
  {
    if (opt3%2!=0)
      opt3*=2;
  }else
  {    
    while(opt3%2==0)
      opt3/=2;
  }
  m_settings->setValue("grainsBoundary",isShow);  
  optDelaunay[3] = std::to_string(opt3);   
}

/**
 *
 * @param isShow user's choice in checkbox
 *
 * Set the analysis parameters with the user's choice.
 * Set if defects should be shown.
 *
 */
void Buttons::isDefects(int isShow)
{
  int opt3 = std::stoi(optDelaunay[3]);
  opt3 = opt3<1? 1:opt3;
  if (isShow)
  {
    if (opt3%3!=0)
      opt3*=3;
  }else
  {    
    while(opt3%3==0)
      opt3/=3;
  }
  m_settings->setValue("defects",isShow);
  optDelaunay[3] = std::to_string(opt3);  
}


/**
 *
 * @param input path to the file to read (image)
 *
 */
void Buttons::setInput(QString input)
{
  optImage[1] = input.toStdString();
  m_settings->setValue("input",input);
}

/**
 *
 * @param channel index chosen by user in combobox
 *
 * Set the image processing parameters with the user's choice.
 * Set the image channel (layer) to read (useful only if image is in AFM Nanoscope format).
 *
 */
void Buttons::setChannel(int channel)
{
  optImage[7] = std::to_string(channel);
  m_settings->setValue("channel",channel);
}

/**
 *
 * @param spotColorIndex index of the user's choice in the combobox
 *
 * Set the image processing parameters with the user's choice.
 * Tells to the image processing function wether the dots are dark
 * on a lighter background or light on a draker background (if so,
 * colors are inverted)
 *
 */
void Buttons::setSpotColor(int spotColorIndex)
{
  if (spotColorIndex == 0)
    optImage[2] = "n";
  else
    optImage[2] = "b";
  m_settings->setValue("spotColor",spotColorIndex);
}


/**
 *
 * @param bandPass value the user has set on the slider
 *
 * Set the image processing parameters with the user's choice.
 * Set the width of the band pass filter in the image processing function.
 * (should be narrow for poor quality images and larger for good quality)
 *
 */
void Buttons::setFFTBandPass(int bandPass)
{
  optImage[4] = std::to_string(bandPass);
  m_settings->setValue("FFTBandPass",bandPass);
}


/**
 *
 * @param isChecked  user's choice in checkbox
 *
 * Set the image processing parameters with the user's choice.
 * Tells the image processing function if it should select the
 * band pass filter itself or show the image and FFT to the user
 * for him to interactively set it (FFTBandPass is then deactivated).
 *
 */
void Buttons::setManualFFT(int isChecked)
{
  if(isChecked)
    optImage[3] = "m";
  else
    optImage[3] = "a";
  m_settings->setValue("manualFFT",isChecked);
}


/**
 *
 * @param isChecked  user's choice in checkbox
 *
 * Set the image processing parameters with the user's choice.
 * Tells the image processing function if it should select the
 * band pass filter itself or show the image and FFT to the user
 * for him to interactively set it (FFTBandPass is then deactivated).
 *
 */
void Buttons::setCropImage(int isChecked)
{
  if(isChecked)
    optImage[8] = "1";
  else
    optImage[8] = "0";
  m_settings->setValue("cropImage",isChecked);
}

/**
 *
 * @param isChecked user's choice in checkbox
 *
 * Set the image processing parameters with the user's choice.
 * Tells the image processing function if it should
 * use the line and tilt recovery algorithm or not.
 * (it is better with it if images come from a noisy AFM image
 * and without for very clean images or other than AFM)
 *
 */
void Buttons::setLineRecover(int isChecked)
{
  if(isChecked)
    optImage[6] = "1";
  else
    optImage[6] = "0";
  m_settings->setValue("lineRecover",isChecked);
}


/**
 *
 * Throw computation.
 *
 */
void Buttons::compute()
{
  if (m_input)
  {
    QUrl url(QString::fromStdString("file:"+optImage[1]),QUrl::TolerantMode);
    optImage[1]=url.toLocalFile().toStdString();
    if (optImage[1].size()>2)
    {
      if (url.isLocalFile())
        compute(&url);
      else
      {
        std::cout << "t:Not computing " << optImage[1] << " do not exist" <<  std:: endl;
        emit endCompute(false);
      }
    }else
    {
         std::cout << "n:Not computing " << optImage[1] << " do not exist" <<  std:: endl;
      emit endCompute(false);
    }
  }
}

/**
 *
 * @return true if file is a node file, false if not (should be an image)
 *
 * Set the image processing parameters with the type of file given.
 *
 */
bool Buttons::setTypeImage()
{
  allExchange clearInputData;
  m_inputData = clearInputData;
  std::string::iterator it = optImage[1].end()-1;
  while(!isalnum(optImage[1].at(it-optImage[1].begin())))
  {
    optImage[1].erase(it);
    it--;
  }
  
  size_t found = optImage[1].find_last_of(".");
  std::string ext;
  
  if (optImage[1].size()-found<7)
    ext =  optImage[1].substr(found+1);
  
  bool isNumber = true;
  std::cout << "extension " << std::endl;
  
  for (unsigned int i = 0; i<ext.length(); i++)
  {
//    std::cout << ext[i] << "  " << isdigit(ext[i])<< std::endl;
    isNumber = isNumber && isdigit(ext[i]);
  } 
  if (isNumber)
  {
    optImage[5] = "a";
    return false;
  }
  else
  {
    optImage[5] = "i";
    optDelaunay[1] = optImage[1];
//    std::cout << "Node " << (ext=="node") << std::endl;
    return (ext=="node");
  }
}

/**
 *
 * @param url path to the input file
 *
 * Create a folder for the output if needed.
 * Throw computations with the given input file.
 *
 */
void Buttons::compute(QUrl *url)
{
  allExchange clearInputData;
  m_inputData = clearInputData;
  if (m_input)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);
    char **optImageTemp;
    int nOptImage = optImage.size();
//    std::cout << "nOptImage " << nOptImage << std::endl;
    optImageTemp = new char*[nOptImage];
    optImageTemp[0] = new char [nOptImage*512];
    for (int i=1; i<nOptImage; i++)
    {
      optImageTemp[i] = optImageTemp[i-1]+512;
    }

    char **optDelaunayTemp;
    int nOptDelaunay = optDelaunay.size();
    optDelaunayTemp = new char*[nOptDelaunay];
    optDelaunayTemp[0] = new char [nOptDelaunay*512];
    for (int i=1; i<nOptDelaunay; i++)
    {
      optDelaunayTemp[i] = optDelaunayTemp[i-1]+512;
    }

    strcpy(optImageTemp[1],optImage[1].c_str());
    if (url->isLocalFile())
    {
      std::cout << "Computing " << url->toLocalFile().toStdString() << std::endl;
      optImage[1] = url->toLocalFile().toStdString();

      bool isNode;

      isNode = setTypeImage();
      QStringList nameTemp;
      size_t found = optImage[1].find_last_of(".");
      if (optImage[1].size()-found<7)
        nameTemp << (optImage[1].substr(0,found)).c_str();
      else
        nameTemp << optImage[1].c_str();

      if (!isNode)
      {
        QProcess process;
        onlyUnix
        (
          process.start("mkdir",nameTemp);
          process.waitForFinished();
        )
        onlyWindows
        (
          nameTemp[0] = "\""+nameTemp[0]+"\"";
          system(("MD "+nameTemp[0].toStdString()).c_str());
        )
        for (int i=1; i<nOptImage; i++)
        {
          strcpy(optImageTemp[i],optImage[i].c_str());
        }
        std::cout << "Image analysis " << "  "
          << optImageTemp[1] << "  "
          << optImageTemp[2] << "  "
          << optImageTemp[3] << "  "
          << optImageTemp[4] << "  "
          << optImageTemp[5] << "  "
          << optImageTemp[6] << "  "
          << optImageTemp[7] << "  "
          << optImageTemp[8] << "  "
          << std::endl;

        m_settings->setValue("directory", optImage[1].c_str());

        if( mainFiltrage(nOptImage,optImageTemp,m_inputData.image)==-1)
        {
          QApplication::restoreOverrideCursor();
          QString errMessage = "Image processing error : "+QString::fromStdString(optImage[1])+" could not be found";
          QMessageBox::information(0,"error",errMessage);
          QApplication::setOverrideCursor(Qt::WaitCursor);
        }

        optDelaunay[1] = optImage[1];
        size_t found = optDelaunay[1].find_last_of(".");
        if (optDelaunay[1].size()-found<7)
          optDelaunay[1] =  optDelaunay[1].substr(0,found);
        found = optDelaunay[1].find_last_of("/");
        optDelaunay[1] = optDelaunay[1]+"/"+optDelaunay[1].substr(found);
        optDelaunay[1]+=".node";
      }

      for (int i=1; i<nOptDelaunay; i++)
      {
        strcpy(optDelaunayTemp[i],optDelaunay[i].c_str());
      }
      if (isNode)
      {
        m_inputData.image.dimension = -1;
        int index = nameTemp[0].lastIndexOf("/");
        QString folderName = nameTemp[0].right(nameTemp[0].size()-index);
        if (nameTemp[0].lastIndexOf(folderName)==nameTemp[0].indexOf(folderName))
        {
          onlyUnix
          (
            QProcess process;
            process.start("mkdir",nameTemp);
            process.waitForFinished();
          )
          onlyWindows
          (
            nameTemp[0] = "\""+nameTemp[0]+"\"";
            system(("MD "+nameTemp[0].toStdString()).c_str());
          )
        }
        for (int i=1; i<nOptDelaunay; i++)
        {
          strcpy(optDelaunayTemp[i],optDelaunay[i].c_str());
        }
        m_settings->setValue("directory", optDelaunay[1].c_str());
        std::cout << optDelaunayTemp[1] << std::endl;
      }
      mainDelaunay(4,optDelaunayTemp,m_inputData);
      emit sendData(m_inputData);
      emit endCompute(true);
      emit sendFileName(nameTemp[0]);
    }else
    {
      std::cout << "Not computing " << optImage[1] << " do not exist" <<  std:: endl;
      emit endCompute(false);
    }
    QApplication::restoreOverrideCursor();
    delete[] optDelaunayTemp[0];
    delete[] optDelaunayTemp;
    delete[] optImageTemp[0];
    delete[] optImageTemp;
  }
}

/**
 *
 * Constructor for the central widget containing the tabs.
 * It is called settings because it was first intended to be used only
 * for input of user's settings but tabs for the output visualisaton have been added.
 *
 */
Settings::Settings()
{
  //~ m_globalWidget = new QWidget;
  //~ this->addWidget(m_globalWidget);
  this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_tabs = new QTabWidget(this);
  this->setMinimumSize(500,300);
  m_tabs->setMinimumSize(500,300);
  m_tabs->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  m_tab1 = new QWidget;
  m_tab2 = new QWidget;
  m_tab3 = new QWidget;
  m_tab4 = new QWidget;
  m_tab5 = new QWidget;
  m_tab6 = new QWidget;
  
  //~ m_tab1->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  //~ m_tab2->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  //~ m_tab3->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  //~ m_tab4->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  m_imageLayout = new ImageTab;
  m_analysisLayout = new CrystalTab;
  m_resultLayout = new ResultTab(this);
  m_correlationLayout = new CorrelationGraphTab;
  m_outputImageWidget = new OutputImageTab;
  m_outputImageLayout = new QHBoxLayout;
  m_historyGraphLayout = new HistoryGraphTab;
  
  m_outputImageLayout->addWidget(m_outputImageWidget);
  
  m_tab1->setLayout(m_imageLayout);
  m_tab2->setLayout(m_analysisLayout);
  m_tab3->setLayout(m_resultLayout);
  m_tab4->setLayout(m_correlationLayout);
  m_tab5->setLayout(m_outputImageLayout);
  m_tab6->setLayout(m_historyGraphLayout);
  
  m_tabs->addTab(m_tab1, "Image processing");
  m_tabs->addTab(m_tab2, "Crystal analysis");
  m_tabs->addTab(m_tab3, "Histograms");
  m_tabs->addTab(m_tab4, "Correlations");
  m_tabs->addTab(m_tab5, "Enhanced image");
  m_tabs->addTab(m_tab6, "History graph");
  
  m_tabs->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  //~ m_tabs->adjustSize();

  QWidget::connect(m_resultLayout, SIGNAL(sendFit(double,double,double,double)),
  m_correlationLayout, SLOT(recvFit(double,double,double,double)));

}

Settings::~Settings()
{}


/**
 *
 * Constructor for correlations graphs tab.
 *
 */
CorrelationGraphTab::CorrelationGraphTab()
{
  m_customPlotG = new QCustomPlot;
  m_customPlotG6 = new QCustomPlot;
  m_marginGroup = new QCPMarginGroup(m_customPlotG6);
  
  addWidget(m_customPlotG,0,0);
  addWidget(m_customPlotG6,1,0);
  
  m_customPlotG->addGraph();
  m_customPlotG6->addGraph();
  m_customPlotG6->addGraph();
  m_customPlotG6->addGraph();

  m_customPlotG->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
  m_customPlotG6->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

  m_customPlotG->setLocale(QLocale(QLocale::English,QLocale::UnitedStates));
  m_customPlotG6->setLocale(QLocale(QLocale::English,QLocale::UnitedStates));

  m_job = NULL;

  QWidget::connect(m_customPlotG6->xAxis, SIGNAL(rangeChanged(QCPRange)), m_customPlotG->xAxis, SLOT(setRange(QCPRange)));
  //~ QWidget::connect(m_customPlotG->xAxis, SIGNAL(rangeChanged(QCPRange)), m_customPlotG6->xAxis, SLOT(setRange(QCPRange)));
  m_expFit[0]=0;
  m_expFit[1]=0;
  m_powFit[0]=0;
  m_powFit[1]=0;
}
CorrelationGraphTab::~CorrelationGraphTab()
{
  if (m_job)
  {
    m_job->join();
    delete m_job;
  }
}

/**
 *
 * @param b true if computation ended correctly.
 *
 * Plot the correlations graphs using text output file.
 *
 */
void CorrelationGraphTab::plot(bool b)
{
  //~ std::cout << "Signal endCompute recieved " << b << std::endl;
  if (b)
  {
    std::string path = "orientationalDistrib.dat";
    if (fexist(const_cast<char*>(path.c_str())))
    {
      plot(path);
    }
  }
  std::cout << "End of computation" << std::endl;
}

/**
 *
 * @param path input file's path.
 *
 * Plot the correlations graphs using text output file.
 *
 */
void CorrelationGraphTab::plot(std::string path)
{
  if (fexist(const_cast<char*>(path.c_str())))
  {
    QFile file(path.c_str());
    if(!file.open(QIODevice::ReadOnly)) 
    {
      QMessageBox::information(0,"error",file.errorString());
    }
    
    QTextStream in(&file);
    m_dataX.clear();
    m_dataG.clear();
    m_dataG6.clear();
    m_dataG6Exp.clear();
    m_dataG6Pow.clear();
    while(!in.atEnd())
    {
      QString line = in.readLine();
      QStringList fields = line.split("    ");
      m_dataX.push_back(fields[0].toDouble());
      m_dataG.push_back(fields[2].toDouble());
      m_dataG6.push_back(fields[1].toDouble());
      m_dataG6Exp.push_back(fields[3].toDouble());
      m_dataG6Pow.push_back(fields[4].toDouble());
      //~ std::cout<<fields[0].toDouble()<<"    "<<fields[1].toDouble()<<std::endl;
    }
    
    m_customPlotG->graph(0)->setData(m_dataX, m_dataG);
    //~ m_customPlotG->xAxis->setLabel("Normalized interparticle distance (r/p)");
    m_customPlotG->yAxis->setLabel("Positional");
    //~ m_customPlotG->rescaleAxes();
    m_customPlotG->xAxis->setRange(0,12);
    m_customPlotG->yAxis->setRange(0,7);
    
    QPen pen;
    m_customPlotG6->legend->setVisible(true);
    QFont legendFont;
    legendFont.setPointSize(7);
    m_customPlotG6->legend->setFont(legendFont);
    m_customPlotG6->legend->setIconSize(15,10);
  
   
    
    m_customPlotG6->graph(0)->setData(m_dataX, m_dataG6);
    m_customPlotG6->graph(0)->setName("g6");
    
    m_customPlotG6->graph(1)->setData(m_dataX, m_dataG6Exp);
    m_customPlotG6->graph(1)->setName("g6 exp fit "+QString::number(m_expFit[0])+"exp(-x/"+QString::number(-1./m_expFit[1])+")");
    pen.setColor(Qt::red); 
    pen.setWidth(2);
    pen.setStyle(Qt::DotLine);
    m_customPlotG6->graph(1)->setPen(pen);
    
    m_customPlotG6->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignBottom);
    m_customPlotG6->graph(2)->setData(m_dataX, m_dataG6Pow);
    m_customPlotG6->graph(2)->setName("g6 power fit "+QString::number(m_powFit[0])+"x^"+QString::number(m_powFit[1]));
    pen.setColor(Qt::green);
    pen.setWidth(2);
    pen.setStyle(Qt::DotLine);
    m_customPlotG6->graph(2)->setPen(pen);
    
    
    m_customPlotG6->xAxis->setLabel("Normalized interparticle distance (r/p)");
    m_customPlotG6->yAxis->setLabel("Orientational");
    m_customPlotG6->yAxis->setScaleType(QCPAxis::stLogarithmic);
    m_customPlotG6->yAxis->setScaleLogBase(10);
    m_customPlotG6->yAxis->setSubTickCount(10);
    m_customPlotG6->xAxis->setRange(0,12);
    m_customPlotG6->yAxis->setRange(0.001,1);
    m_customPlotG6->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    m_customPlotG6->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^
    
    m_customPlotG6->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, m_marginGroup);
    m_customPlotG->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, m_marginGroup);

    
    m_customPlotG6->replot();
    m_customPlotG->replot();
  }
}


/**
 *
 * @param fileName path to the output file
 *
 * Save correlations graphs in png files. Use another thread to
 * run in background because it may take some time.
 *
 */
void CorrelationGraphTab::save(QString fileName)
{
  m_job = new std::thread (&CorrelationGraphTab::saveThread,this,fileName);
}

/**
 *
 * @param fileName path to the output file
 *
 * Save correlations graphs in png files. Use another thread to
 * run in background because it may take some time.
 *
 */
void CorrelationGraphTab::saveThread(QString fileName)
{
    int index = fileName.lastIndexOf(".");
    if(fileName.length()-index<7)
      fileName = fileName.left(index);
    index = fileName.lastIndexOf("/");
     QProcess cp;
    onlyUnix
    (
      QString nameTemp = fileName.right(fileName.size()-index);
      if (fileName.lastIndexOf(nameTemp)==fileName.indexOf(nameTemp))
        fileName = fileName+"/"+nameTemp;
      index = fileName.lastIndexOf("/");
      cp.start("cp orientationalDistrib.dat \""+fileName.left(index)+"/orientationalDistrib.txt\"");
      cp.waitForFinished();
    )
    onlyWindows
    (
      QString nameTemp = (fileName.right(fileName.size()-index)).left(fileName.size()-index-1);
      if (fileName.lastIndexOf(nameTemp)==fileName.indexOf(nameTemp))
        fileName = fileName.left(fileName.size()-1).right(fileName.size()-2)+nameTemp;
      index = fileName.lastIndexOf("/");
      system(("copy orientationalDistrib.dat \""+fileName.left(index)+"/orientationalDistrib.csv\"").toStdString().c_str());
    )
    std::cout << "Saving correlation " << fileName.toStdString() << std::endl;
//    m_customPlotG6->savePdf(fileName+"_G6.pdf",true,800,300);
//    m_customPlotG->savePdf(fileName+"_G.pdf",true,800,300);
    std::cout << "Correlation saved"<< std::endl;
}


/**
 *
 * @param expFit0 a parameter of exponential fit (a*exp(b*x))
 * @param expFit1 b parameter of exponential fit
 * @param powFit0 a parameter of power law fit (a*x^b)
 * @param powFit1 b parameter of power law fit
 *
 * Get the output values from fit function.
 *
 */
void CorrelationGraphTab::recvFit(double expFit0, double expFit1, double powFit0, double powFit1)
{

  m_expFit[0] = (1000*expFit0-fmod(1000*expFit0,1.))/1000;
  expFit1 = -1./expFit1;
  m_expFit[1] = (1000*expFit1-fmod(1000*expFit1,1.))/1000;
  m_powFit[0] = (1000*powFit0-fmod(1000*powFit0,1.))/1000;
  m_powFit[1] = (1000*powFit1-fmod(1000*powFit1,1.))/1000;

  m_customPlotG6->graph(1)->setName("g6 exp fit "+QString::number(m_expFit[0])+"exp(-x/"+QString::number(m_expFit[1])+")");
  m_customPlotG6->graph(2)->setName("g6 power fit "+QString::number(m_powFit[0])+"x^"+QString::number(m_powFit[1]));

  m_customPlotG6->replot();
  m_customPlotG->replot();
}


/**
 *
 * Constructor for image processing settings tab
 *
 */
ImageTab::ImageTab()
{
  
  m_sliderBandPass = new QSlider(Qt::Horizontal);
  m_sliderBandPass->setValue(40);
  m_spotColor = new QComboBox;
  m_imageChannel = new QComboBox;
  m_cropImage = new QCheckBox("Crop image before processing");
  m_manualFFT = new QCheckBox("Manual setting of FFT band-pass filter");
  m_lineRecover = new QCheckBox("Recover tilt and smooth line noise");
  
  m_spotColor->addItem("Dark");
  m_spotColor->addItem("Light");
  
  m_imageChannel->addItem("Topography");
  m_imageChannel->addItem("Amplitude");
  m_imageChannel->addItem("Phase");
  
  m_channelLabel = new QLabel("Image channel:");
  addWidget(m_channelLabel,0,0);
  addWidget(m_imageChannel,0,1); 
  
  m_spotLabel = new QLabel("Spots color:");
  addWidget(m_spotLabel,1,0);
  addWidget(m_spotColor,1,1); 
  
  m_bandPassLabel = new QLabel("FFT band-pass:");
  addWidget(m_bandPassLabel,2,0);
  addWidget(m_sliderBandPass,2,1);
  
  addWidget(m_manualFFT,3,0,1,2);
  addWidget(m_cropImage,4,0,1,2);
  addWidget(m_lineRecover,5,0,1,2);
  
  QWidget::connect(m_manualFFT, SIGNAL(stateChanged(int)), this, SLOT(disableSlider(int)));
  
}

ImageTab::~ImageTab()
{}

/**
 *
 * @param d user's choice in checkbox for manualFFT
 *
 * If manualFFT is chosen, the band pass slider is useless. To show it to the user,
 * it is deactivated.
 *
 */
void ImageTab::disableSlider(int d)
{
  if(d)
  {
    m_bandPassLabel->setDisabled(true);
    m_sliderBandPass->setDisabled(true);
  }else
  {
    m_bandPassLabel->setDisabled(false);
    m_sliderBandPass->setDisabled(false);
  }
}


/**
 *
 * Constructor for analysis settings tab.
 *
 */
CrystalTab::CrystalTab()
{
  m_wireType = new QComboBox;
  m_colorType = new QComboBox;
  
  m_wireType->addItem("Delaunay");
  m_wireType->addItem("Voronoï");
  m_wireType->addItem("None");

  m_wireLabel = new QLabel("Wire type:");
  this->addWidget(m_wireLabel,0,0);
  this->addWidget(m_wireType,0,1);
  
  
  
  m_colorType->addItem("Orientation");
  m_colorType->addItem("None");
  m_colorType->addItem("Lennard-Jones potential gradient");
  m_colorType->addItem("Expansion (x+y)");
  m_colorType->addItem("Shear deformation");
  m_colorType->addItem("x deformation");
  m_colorType->addItem("y deformation");
  
  
  m_colorLabel = new QLabel("Color by:");
  this->addWidget(m_colorLabel,1,0);
  this->addWidget(m_colorType,1,1);
  
  this->setColumnStretch(1,1);
  
  m_grains = new QCheckBox("Show grains");
  this->addWidget(m_grains,2,0,1,2);
  
  m_grainsBoundary = new QCheckBox("Show grains' boundaries");
  this->addWidget(m_grainsBoundary,3,0,1,2);
  
  m_defects = new QCheckBox("Show defects");
  this->addWidget(m_defects,4,0,1,2);
  
}

CrystalTab::~CrystalTab()
{}



/**
 *
 * @param parent parent widget
 *
 * Constructor of result tab. It has been called result tab because it used to be the
 * only tab for output.
 * This tab shows numbered data and histograms.
 *
 */
ResultTab::ResultTab(QWidget *parent)
{
  imageLabel = new QLabel(parent);
  scrollArea = new QScrollArea(parent);

  imageLabel->setBackgroundRole(QPalette::Base);
  imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  imageLabel->setScaledContents(true);

  scrollArea->setBackgroundRole(QPalette::Dark);
  scrollArea->setWidget(imageLabel);
  scrollArea->setVisible(false);
  m_hist = NULL;
  m_rect = NULL;
  m_histSizeP = 0;
  m_name = new QLabel(parent);
  m_time = new QLabel(parent);
  m_nPoints = new QLabel(parent);
  m_defectDensity = new QLabel(parent);
  m_nGrains = new QLabel(parent);
  m_realDimension = new QLabel(parent);
  m_log = new QFile("log.log",parent);
  m_plotVecHist = new QCustomPlot;
  m_marginGroup = new QCPMarginGroup(m_plotVecHist);
  addWidget(m_nPoints,1,1);
  addWidget(m_defectDensity,2,1);
  addWidget(m_nGrains,1,2);
  addWidget(m_realDimension,2,2);
  addWidget(m_plotVecHist,3,1,2,2);
  //  addWidget(scrollArea,3,1,1,2);
  setRowStretch(3,6);
  setColumnStretch(1,50);
  setColumnStretch(2,50);

  setRowStretch(0,1);
  setRowStretch(4,1);

  setColumnStretch(4,1);
  setColumnStretch(0,1);

}
/**
 *
 * @param b true if computations ended correctly
 *
 * Print the numbered data.
 *
 */
void ResultTab::print(bool b)
{
  if (b)
  {
    if (m_log->open(QIODevice::ReadOnly | QIODevice::Text))
    {
      QTextStream in(m_log);
      QString line;

      line = in.readLine();
      m_nPoints->setText("Number of cylinders: "+ QString::number(m_data.delaunay.numberOfPoints));
      m_nGrains->setText("Number of grains: "+ QString::number(m_data.delaunay.numberOfGrains));
      if (m_data.image.dimension>0)
        m_realDimension->setText("Image total width: "+ QString::number( m_data.image.dimension) +"nm");
      else
        m_realDimension->setText(" ");
      line = in.readLine();
      line = in.readLine();
      line = in.readLine();
      line = in.readLine();
      line = in.readLine();
      line = in.readLine();

      line = in.readLine();
      double density = line.toDouble();
      //      density=(density*1000-fmod(density*1000,1.))/10;
      line.setNum(m_data.delaunay.defectDensity,'g');
      m_defectDensity->setText("Defect density : " +QString::number(m_data.delaunay.defectDensity,'g',3)+"%");
      line = in.readLine();
      QStringList fields = line.split("    ");
      m_expFit[0] = fields[0].toDouble();
      m_expFit[1] = fields[1].toDouble();
      line = in.readLine();
      fields = line.split("    ");
      m_powFit[0] = fields[0].toDouble();
      m_powFit[1] = fields[1].toDouble();
    }
    if(!m_log->remove())
      m_log->close();
    emit sendFit(m_expFit[0],m_expFit[1],m_powFit[0],m_powFit[1]);
  }
}


/**
 *
 * @param dataIn data set from computing functions (image processing and analysis)
 *
 * This stores the results of the computation and plots the histograms.
 *
 */
void ResultTab::setData(allExchange dataIn)
{
  m_data = dataIn;
  plotHist();
}

/**
 *
 * Plots the histograms.
 *
 */
void ResultTab::plotHist()
{
  // remove automatically or previously set plots and delete the m_hist[i] and m_rect[i]
  m_plotVecHist->clearPlottables();
  m_plotVecHist->plotLayout()->clear();

  int sizeVecHist = m_data.delaunay.vecHist.size();

  m_hist = new QCPBars*[sizeVecHist];
  m_rect = new QCPAxisRect*[sizeVecHist];

  for (int i = 0; i<sizeVecHist;i++)
  {
    // create chart object
    m_rect[i] = new QCPAxisRect(m_plotVecHist,false);
    m_rect[i]->addAxes(QCPAxis::atBottom|QCPAxis::atLeft);
    m_rect[i]->setMarginGroup(QCP::msBottom,m_marginGroup);
    m_hist[i] = new QCPBars(m_rect[i]->axis(QCPAxis::atBottom),m_rect[i]->axis(QCPAxis::atLeft));
    m_plotVecHist->plotLayout()->addElement(1,i,m_rect[i]);
    m_plotVecHist->addPlottable(m_hist[i]);
  }
  m_histSizeP = sizeVecHist;
  for (int i = 0; i<sizeVecHist;i++)
  {
    // set names and colors:
    QPen pen;
    pen.setWidth(1.2);
    pen.setColor(QColor(0, 0, 255));
    m_hist[i]->setPen(pen);
    m_hist[i]->setName(QString::fromStdString(m_data.delaunay.vecHist[i].name));
    m_plotVecHist->plotLayout()->addElement(0, i, new QCPPlotTitle(m_plotVecHist, m_hist[i]->name()));

    // prepare x axis
    int nLevels =m_data.delaunay.vecHist[i].count.size();
    QVector<double> ticks(nLevels);
    QVector<double> labelsValues(nLevels);
    QVector<QString> labels(nLevels);
    //    ticks = QVector<double>::fromStdVector(m_data.delaunay.vecHist[i].levelsMean);

    for(int j =0; j<nLevels;j++)
    {
      ticks[j]=j+1;
      labelsValues[j]=m_data.delaunay.vecHist[i].levelsMean[j];
      labels[j] = QString::number(labelsValues[j],'g',2);
      //      std::cout<<m_data.delaunay.vecHist[i].levelsMean[j]<<std::endl;
    }

    //set x axis
    m_rect[i]->axis(QCPAxis::atBottom)->setTickVector(ticks);
    m_rect[i]->axis(QCPAxis::atBottom)->setTickVectorLabels(labels);
    m_rect[i]->axis(QCPAxis::atBottom)->setTickLabelRotation(60);
    m_rect[i]->axis(QCPAxis::atBottom)->setSubTickCount(0);
    m_rect[i]->axis(QCPAxis::atBottom)->setTickLength(0, 4);
    m_rect[i]->axis(QCPAxis::atBottom)->setRange(0,nLevels+1);


    // Add data
    QVector<double> data;
    double dataMax =0;
    double dataj;
    for(unsigned int j =0; j<m_data.delaunay.vecHist[i].count.size(); j++)
    {
      if (m_data.delaunay.vecHist[i].countAll>0)
      {
        dataj = floor(((double) (m_data.delaunay.vecHist[i].count[j]))/m_data.delaunay.vecHist[i].countAll*100);
        data.push_back(dataj);
        dataMax = dataMax>dataj? dataMax:dataj;
      }else
      {
        data.push_back(0);
      }
    }

    // prepare y axis
    m_rect[i]->axis(QCPAxis::atLeft)->setRange(0,dataMax);
    m_rect[i]->axis(QCPAxis::atLeft)->grid()->setSubGridVisible(true);

    m_hist[i]->setData(ticks,data);
    m_hist[i]->keyAxis()->setAutoTicks(false);
    m_hist[i]->keyAxis()->setAutoTickLabels(false);
    m_hist[i]->keyAxis()->setTickVectorLabels(labels);
    m_hist[i]->keyAxis()->setTickLabelRotation(60);
    //    m_hist[i]->keyAxis()->setTickVector(ticks);
    m_hist[i]->keyAxis()->setSubTickCount(0);
    m_hist[i]->valueAxis()->grid()->setVisible(true);
  }
  //////// for gaussian fit of the second histogram (the fit values are wrong) //////
  //  int nLevels =m_data.delaunay.vecHist[2].count.size();
  //  QCPGraph *gaussFit = m_plotVecHist->addGraph(m_rect[2]->axis(QCPAxis::atBottom),m_rect[2]->axis(QCPAxis::atLeft));
  //  gaussFit->addToLegend();
  //  QVector<double> data;
  //  double dataj,sigma=m_data.delaunay.vecHist[2].sigma, mu=m_data.delaunay.vecHist[2].mu;
  //// /m_data.delaunay.vecHist[2].countAll*100
  //  std::cout << "sigma " << sigma << " mu " << mu << std::endl;

  //  QVector<double> ticks(nLevels);
  //  QVector<double> labelsValues(nLevels);
  //  double coef=0,vp,va;
  //  vp = m_data.delaunay.vecHist[2].levelsMean[1];
  //  for(int j =2; j<nLevels-1;j++)
  //  {
  //    va = m_data.delaunay.vecHist[2].levelsMean[j];
  //    coef+= 1./(va-vp);
  //    vp = va;
  //  }
  //  coef /= nLevels-1;
  //  mu -= m_data.delaunay.vecHist[2].levelsMean[1];
  //  mu *= coef*1.4; // value is 40% smaller than normal
  //  sigma *= coef;
  //  sigma = sqrt(sigma);
  //  mu += 2;
  //  std::cout << " coef " << coef << " sigma " << sigma << " mu " << mu << std::endl;
  //  for(int j =0; j<nLevels;j++)
  //  {
  //    ticks[j]=j;
  //    labelsValues[j]=m_data.delaunay.vecHist[2].levelsMean[j];
  //  }
  //  for(int i =0; i<nLevels;i++)
  //  {
  //      dataj = 100./(sigma*sqrt(2*3.14157))*exp(-(i-mu)*(i-mu)/(2*sigma*sigma));
  //      data.push_back(dataj);
  //  }
  //  gaussFit->addData(ticks,data);

  m_plotVecHist->replot();

  delete[] m_hist;
  delete[] m_rect;
}


/**
 *
 * Constructor of the tab showing the output image from the analysis function.
 *
 */
OutputImageTab::OutputImageTab()
{
  m_gridLayout = new QGridLayout;
  imageLabel = new QLabel;
  scrollArea = new QScrollArea;
  m_zoomm = new QPushButton("-");
  m_zoomp= new QPushButton("+");
  m_zoomn = new QPushButton("Reset");
  m_scaleFactorLabel = new QLabel;
  scaleFactor = 0.35;
  
  imageLabel->setBackgroundRole(QPalette::Base);
  imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  imageLabel->setScaledContents(true);
  
  scrollArea->setBackgroundRole(QPalette::Dark);
  scrollArea->setWidget(imageLabel);
  scrollArea->setVisible(false);
  
  m_gridLayout->addWidget(m_zoomp,0,0);
  m_gridLayout->addWidget(m_zoomm,0,1);
  m_gridLayout->addWidget(m_zoomn,1,0,1,2);
  m_gridLayout->addWidget(m_scaleFactorLabel,2,0,1,2);
  m_gridLayout->addWidget(scrollArea,0,2,3,3);
  m_gridLayout->setColumnStretch(2,1);
  m_gridLayout->setRowStretch(2,1);
  setLayout(m_gridLayout);
  
  QWidget::connect(m_zoomp, SIGNAL(clicked()), this, SLOT(zoomIn()));
  QWidget::connect(m_zoomm, SIGNAL(clicked()), this, SLOT(zoomOut()));
  QWidget::connect(m_zoomn, SIGNAL(clicked()), this, SLOT(normalSize()));
  
  //~ resize(QGuiApplication::primaryScreen()->availableSize() * 3 / 5);
}

OutputImageTab::~OutputImageTab()
{}


/**
 *
 * @param data data set by computing functions (image processing and analysis)
 *
 * This stores the results of the computing functions.
 *
 */
void OutputImageTab::setData(allExchange data)
{
  m_inputData = data;
}


/**
 *
 * @param fileName path to the file to show
 * @return true if image loaded correctly
 */
bool OutputImageTab::loadFile(const QString &fileName)
{
  QImageReader reader(fileName);
  QRect rect(39.,555.,2400.,2400.);
  reader.setClipRect(rect);
  std::cout << "Open " << fileName.toStdString() << std::endl;
  image = reader.read();
  if (image.isNull()) 
  {
    QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                             tr("Cannot load %1: %2")
                             .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
    return false;
  }
  imageLabel->setAlignment(Qt::AlignCenter);
  imageLabel->setPixmap(QPixmap::fromImage(image));
  scaleFactor = .70;

  scrollArea->setVisible(true);
  
  scaleImage(scaleFactor);
  return true;
}

//~ void OutputImageTab::wheelEvent(QWheelEvent *event)
//~ {
  //~ if (!image.isNull())
  //~ {
    //~ if (event->angleDelta().y()>0 && scaleFactor<20)
      //~ zoomIn();
    //~ else if (event->angleDelta().y()<0 && scaleFactor>0.1)
      //~ zoomOut();
    //~ event->accept();
  //~ }
//~ }

/**
 *
 * Zoom in on the image.
 *
 */
void OutputImageTab::zoomIn()
{
  scaleImage(1.25);
}

/**
 *
 * Zoom out on the image.
 *
 */
void OutputImageTab::zoomOut()
{
  scaleImage(0.8);
}


/**
 *
 * reset zoom on the image.
 *
 */
void OutputImageTab::normalSize()
{
  scaleFactor = .21;
  scaleImage(1.);
}

/**
 *
 * @param factor zoom factor
 *
 * Zoom in or out on the image.
 *
 */
void OutputImageTab::scaleImage(double factor)
{

  if (!image.isNull() && ((factor<=1. && scaleFactor>0.1) || (factor >= 1. && scaleFactor<3)))
  {
    Q_ASSERT(imageLabel->pixmap());
    scaleFactor *= factor;
    imageLabel->resize(scaleFactor * imageLabel->pixmap()->size());

    adjustScrollBar(scrollArea->horizontalScrollBar(), factor);
    adjustScrollBar(scrollArea->verticalScrollBar(), factor);
  }
  QString scaleFactorLabel = "Scale: ";
  scaleFactorLabel += QString::number(scaleFactor,'g',4);
  m_scaleFactorLabel->setText(scaleFactorLabel);
  //~ zoomInAct->setEnabled(scaleFactor < 3.0); //replace with desactivation of zoom
  //~ zoomOutAct->setEnabled(scaleFactor > 0.333);
}


/**
 *
 * @param scrollBar
 * @param factor zoom factor
 *
 * Set scrollbar so the zoom does not move it.
 *
 */
void OutputImageTab::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
  scrollBar->setValue(int(factor * scrollBar->value()
                          + ((factor - 1) * scrollBar->pageStep()/2)));
}


/**
 *
 * Constructor of the hystory graph tab. It alows the user to plot
 * values from previous computations.
 *
 */
HistoryGraphTab::HistoryGraphTab()
{
  m_customPlot = new QCustomPlot;
  m_plotButton = new QPushButton("Plot");
  m_cleanButton = new QPushButton("Clean history");
  m_typeGraphLabel = new QLabel("Data to plot:");
  
  m_typeGraph = new QInputDialog;
  m_typeGraph->setContentsMargins(0,0,0,0);
  m_typeGraph->setOption(QInputDialog::NoButtons);
  //~ m_typeGraph->setInputMode(QInputDialog::IntInput);
  //~ m_typeGraph->setOption(QInputDialog::UseListViewForComboBoxItems);
  m_typeGraph->setLabelText("Data to plot:");
  
  
  m_nHist = new QInputDialog;
  m_nHist->setContentsMargins(0,0,0,0);
  m_nHist->setOption(QInputDialog::NoButtons);
  m_nHist->setInputMode(QInputDialog::IntInput);
  m_nHist->setLabelText("Number of values:");
  m_nHist->setIntRange(1,10000);
  
  
  //~ std::cout << "ok1" << std::endl;
  QStringList items;
  items << "Defects" << "Ksi_6" << "Cylinders" << "exp fit 1"
        << "exp fit 2" << "pow fit 1" << "pow fit 2";
  //~ m_typeGraph->addItem("Defects");
  //~ m_typeGraph->addItem("Ksi_6");
  //~ m_typeGraph->addItem("Cylinders");
  //~ m_typeGraph->addItem("exp fit 1");
  //~ m_typeGraph->addItem("exp fit 2");
  //~ m_typeGraph->addItem("pow fit 1");
  //~ m_typeGraph->addItem("pow fit 2");
  //~ m_typeGraph->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
  m_typeGraph->setComboBoxItems(items);
    //~ std::cout << "ok2" << std::endl;  
  //~ setColumnStretch(0,1);
  //~ setColumnStretch(4,5);
  //~ addWidget(m_typeGraphLabel,0,0,1,2);
  addWidget(m_typeGraph,1,0,1,2);
  addWidget(m_nHist,2,0,1,2);
  addWidget(m_plotButton,4,0);
  addWidget(m_cleanButton,4,1);
  addWidget(m_customPlot,0,2,6,1);
  setRowStretch(4,10);
  setColumnStretch(2,20);
  //~ std::cout << "ok3" << std::endl;  
  m_customPlot->addGraph();

  m_customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
  
  QWidget::connect(m_plotButton, SIGNAL(clicked()), this, SLOT(plot()));
  QWidget::connect(m_cleanButton, SIGNAL(clicked()), this, SLOT(clean()));
  
}
HistoryGraphTab::~HistoryGraphTab()
{}

/**
 *
 * Clean the file where data from previous computations are stored.
 *
 */
void HistoryGraphTab::clean()
{
  QString question = "This will destroy all analysis history, is it what you want to do?";
  if(QMessageBox::question(0,"Are you sure?",question)==16384)
  {
    QProcess rm;
    onlyUnix(rm.start("rm history.dat");)
    onlyWindows(rm.start("del history.dat");)
    rm.waitForFinished();
  }
}

/**
 *
 * Plot the graph of previous computations with settings from the user.
 *
 */
void HistoryGraphTab::plot()
{
  // file read here is created by delaunay.cpp around line 410
  // informations are as follow : number of defects, ksi6 parameter, number of cylinders, expFit(a*exp(b*x)) a, b, powFit(a*x^b) a, b 
  std::string path = "history.dat";
  if (fexist(const_cast<char*>(path.c_str())))
  {
     QStringList items;
    items << "Defects" << "Ksi_6" << "Cylinders" << "exp fit 1"
        << "exp fit 2" << "pow fit 1" << "pow fit 2";
    int opt = items.indexOf(m_typeGraph->textValue());
    int nLines = m_nHist->intValue();
    if ( nLines>0)
    {
      QFile file(path.c_str());
      if(!file.open(QIODevice::ReadOnly)) 
      {
        QMessageBox::information(0,"error",file.errorString());
      }
      
      QTextStream in(&file);
      m_dataX.clear();
      m_dataY.clear();
      QString *lines;
      lines = new QString[nLines];
      int start = 0;
      int nLineRead = 0;
      while(!in.atEnd())
      {
        lines[start] = in.readLine();
        start = (start+1)%nLines;
        nLineRead++;
      }
      
      double miny=10.e10,maxy=-1,value;
      int nLineMin = nLineRead<nLines? nLineRead:nLines;
      for (int i=0; i<nLineMin; i++)
      { 
        QStringList fields = lines[(start+i)%nLineMin].split("    ");
        m_dataX.push_back(i);
        value = fields[opt].toDouble();
        maxy = maxy>value? maxy:value;
        miny = miny>value? value:miny;
        m_dataY.push_back(value); 
      }
      m_customPlot->graph(0)->setData(m_dataX, m_dataY);
      m_customPlot->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
      switch (opt)
      {
        case 0:
          m_customPlot->yAxis->setLabel("Defect density in %");
          break;
        case 1:
          m_customPlot->yAxis->setLabel("Ksi_6 in f(x)=a*exp(-d*x/ksi_6)");
          break;
        case 2:
          m_customPlot->yAxis->setLabel("Number of cylinders");
          break;
        case 3:
          m_customPlot->yAxis->setLabel("a in f(x)=a*exp(b*x) fit");
          break;
        case 4:
          m_customPlot->yAxis->setLabel("b in f(x)=a*exp(b*x) fit");
          break;
        case 5:
          m_customPlot->yAxis->setLabel("a in f(x)=a*x^b fit");
          break;
        case 6:
          m_customPlot->yAxis->setLabel("b in f(x)=a*x^b fit");
          break;
      }
    
      m_customPlot->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
      m_customPlot->yAxis->setNumberPrecision(1);
      m_customPlot->xAxis->setLabel("Index of analysis");
      m_customPlot->xAxis->setRange(0,nLineMin);
      miny = miny<0? miny*1.02:miny*0.98;
      maxy = maxy<0? maxy*0.98:maxy*1.02;
      m_customPlot->yAxis->setRange(miny,maxy);
      m_customPlot->replot();
      delete[] lines;
    }
  }
}
