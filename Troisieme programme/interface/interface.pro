RC_FILE = interface.rc
TARGET = interface
QT += widgets core gui printsupport

win32 {
  # Copy required DLLs to output directory
  # hack from http://stackoverflow.com/questions/8391964/how-to-copy-qt-runtime-dlls-to-project-output
  CONFIG(debug, debug|release) {
      Qt5Cored.commands = copy /Y %QTDIR%\\bin\\Qt5Cored.dll debug
      Qt5Cored.target = debug/Qt5Cored.dll
      Qt5Guid.commands = copy /Y %QTDIR%\\bin\\Qt5Guid.dll debug
      Qt5Guid.target = debug/Qt5Guid.dll
      Qt5Widgetsd.commands = copy /Y %QTDIR%\\bin\\Qt5Widgetsd.dll debug
      Qt5Widgetsd.target = debug/Qt5Widgetsd.dll
      Qt5PrintSupportd.commands = copy /Y %QTDIR%\\bin\\Qt5PrintSupportd.dll debug
      Qt5PrintSupportd.target = debug/Qt5PrintSupportd.dll

      QMAKE_EXTRA_TARGETS += Qt5Cored Qt5Guid Qt5Widgetsd Qt5PrintSupportd
      PRE_TARGETDEPS += debug/Qt5Cored.dll debug/Qt5Guid.dll debug/Qt5Widgetsd.dll debug/Qt5PrintSupportd.dll
  } else:CONFIG(release, debug|release) {
      QMAKE_CXXFLAGS += /O2 -openmp
      Qt5Core.commands = copy /Y %QTDIR%\\bin\\Qt5Core.dll release
      Qt5Core.target = release/Qt5Core.dll
      Qt5Gui.commands = copy /Y %QTDIR%\\bin\\Qt5Gui.dll release
      Qt5Gui.target = release/Qt5Gui.dll
      Qt5Widgets.commands = copy /Y %QTDIR%\\bin\\Qt5Widgets.dll release
      Qt5Widgets.target = release/Qt5Widgets.dll
      Qt5PrintSupport.commands = copy /Y %QTDIR%\\bin\\Qt5PrintSupport.dll release
      Qt5PrintSupport.target = release/Qt5PrintSupport.dll
      QMAKE_LFLAGS += -openmp
      QMAKE_EXTRA_TARGETS += Qt5Core Qt5Gui Qt5Widgets Qt5PrintSupport
      PRE_TARGETDEPS += release/Qt5Core.dll release/Qt5Gui.dll release/Qt5Widgets.dll release/Qt5PrintSupport.dll
  } else {
      error(Unknown set of dependencies.)
  }

  LIBS += -lkernel32 -luser32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luuid
  DEFINES += cimg_use_png
  INCLUDEPATH += ../src_fourier/zlib ../src_fourier/png
}

unix {
  LIBS += -L/usr/X11R6/lib -lm -lpthread -lX11 -fopenmp
  QMAKE_CXXFLAGS += -std=c++11 -fopenmp
  QMAKE_LFLAGS += -fopenmp
  QMAKE_CC += -std=c99
  DEFINES += POSIX cimg_use_png
  INCLUDEPATH += ../src_fourier/zlib ../src_fourier/png
}

HEADERS += \
    ../src_fourier/CImg.h \
    ../src_fourier/Filtrage_Fourier.hpp \
    ../src_delaunay/adj.h \
    ../src_delaunay/anglesTools.h \
    ../src_delaunay/DataSol.h \
    ../src_delaunay/debug.h \
    ../src_delaunay/delaunay.hpp \
    ../src_delaunay/domainTools.h \
    ../src_delaunay/Dtree.h \
    ../src_delaunay/Edge.h \
    ../src_delaunay/grains.h \
    ../src_delaunay/histogram.h \
    ../src_delaunay/inOut.h \
    ../src_delaunay/levmarq.h \
    ../src_delaunay/MMG2D_plottops.h \
    ../src_delaunay/potential.h \
    ../src_delaunay/radialDistrib.h \
    ../src_delaunay/vectorTools.h \
    ../src_interface/Fenetre.h \
    ../libqcustomplot/qcustomplot.h \
    ../src_delaunay/mmg/mmg2d/libmmg2d.h \
    ../src_delaunay/mmg/mmg2d/libmmg2df.h \
    ../src_delaunay/mmg/common/chrono.h \
    ../src_delaunay/mmg/common/eigenv.h \
    ../src_delaunay/mmg/common/libmmgcommon.h \
    ../src_delaunay/mmg/common/libmmgtypes.h \
    ../src_delaunay/mmg/common/libmmgtypesf.h \
    ../src_delaunay/mmg/common/librnbg.h \
    ../src_delaunay/mmg/common/mmgcommon.h \
    ../src_fourier/png/png.h \
    ../src_fourier/png/pngconf.h \
    ../src_fourier/png/pngdebug.h \
    ../src_fourier/png/pnginfo.h \
    ../src_fourier/png/pnglibconf.h \
    ../src_fourier/png/pngpriv.h \
    ../src_fourier/png/pngstruct.h \
    ../src_fourier/zlib/crc32.h \
    ../src_fourier/zlib/deflate.h \
    ../src_fourier/zlib/gzguts.h \
    ../src_fourier/zlib/infcodes.h \
    ../src_fourier/zlib/inffast.h \
    ../src_fourier/zlib/inffixed.h \
    ../src_fourier/zlib/inflate.h \
    ../src_fourier/zlib/inftrees.h \
    ../src_fourier/zlib/trees.h \
    ../src_fourier/zlib/zconf.h \
    ../src_fourier/zlib/zconf.in.h \
    ../src_fourier/zlib/zlib.h \
    ../src_fourier/zlib/zutil.h \
    ../src_delaunay/linearFit.h \
    ../src_exchange/exchange.h \
    ../src_interface/preview_file_dialog.h

SOURCES += \
    ../src_fourier/Filtrage_Fourier.cpp \
    ../src_delaunay/adj.cpp \
    ../src_delaunay/anglesTools.cpp \
    ../src_delaunay/DataSol.cpp \
    ../src_delaunay/delaunay.cpp \
    ../src_delaunay/domainTools.cpp \
    ../src_delaunay/Dtree.cpp \
    ../src_delaunay/Edge.cpp \
    ../src_delaunay/grains.cpp \
    ../src_delaunay/histogram.cpp \
    ../src_delaunay/inOut.cpp \
    ../src_delaunay/MMG2D_plottops.c \
    ../src_delaunay/potential.cpp \
    ../src_delaunay/radialDistrib.cpp \
    ../src_delaunay/vectorTools.cpp \
    ../src_delaunay/levmarq.c \
    ../src_interface/Fenetre.cpp \
    ../src_interface/interface.cpp \
    ../libqcustomplot/qcustomplot.cpp \
    ../src_delaunay/mmg/mmg2d/API_functions_2d.c \
    ../src_delaunay/mmg/mmg2d/API_functionsf_2d.c \
    ../src_delaunay/mmg/mmg2d/boulep_2d.c \
    ../src_delaunay/mmg/mmg2d/bucket_2d.c \
    ../src_delaunay/mmg/mmg2d/cendel_2d.c \
    ../src_delaunay/mmg/mmg2d/cenrad_2d.c \
    ../src_delaunay/mmg/mmg2d/chkmsh_2d.c \
    ../src_delaunay/mmg/mmg2d/colpoi_2d.c \
    ../src_delaunay/mmg/mmg2d/delone_2d.c \
    ../src_delaunay/mmg/mmg2d/enforcement_2d.c \
    ../src_delaunay/mmg/mmg2d/evalgeom_2d.c \
    ../src_delaunay/mmg/mmg2d/hash_2d.c \
    ../src_delaunay/mmg/mmg2d/inout_2d.c \
    ../src_delaunay/mmg/mmg2d/length_2d.c \
    ../src_delaunay/mmg/mmg2d/libmmg2d.c \
    ../src_delaunay/mmg/mmg2d/libmmg2d_tools.c \
    ../src_delaunay/mmg/mmg2d/libmmg2d_toolsf.c \
    ../src_delaunay/mmg/mmg2d/libmmg2df.c \
    ../src_delaunay/mmg/mmg2d/lissmet_2d.c \
    ../src_delaunay/mmg/mmg2d/locate_2d.c \
    ../src_delaunay/mmg/mmg2d/mmg2d0.c \
    ../src_delaunay/mmg/mmg2d/mmg2d1.c \
    ../src_delaunay/mmg/mmg2d/mmg2d2.c \
    ../src_delaunay/mmg/mmg2d/mmg2d6.c \
    ../src_delaunay/mmg/mmg2d/mmg2d9.c \
    ../src_delaunay/mmg/mmg2d/optlap_2d.c \
    ../src_delaunay/mmg/mmg2d/optlen_2d.c \
    ../src_delaunay/mmg/mmg2d/quality_2d.c \
    ../src_delaunay/mmg/mmg2d/queue_2d.c \
    ../src_delaunay/mmg/mmg2d/scalem_2d.c \
    ../src_delaunay/mmg/mmg2d/simred_2d.c \
    ../src_delaunay/mmg/mmg2d/solmap_2d.c \
    ../src_delaunay/mmg/mmg2d/split_2d.c \
    ../src_delaunay/mmg/mmg2d/swapar_2d.c \
    ../src_delaunay/mmg/mmg2d/variadic_2d.c \
    ../src_delaunay/mmg/mmg2d/zaldy_2d.c \
    ../src_delaunay/mmg/common/anisomovpt.c \
    ../src_delaunay/mmg/common/anisosiz.c \
    ../src_delaunay/mmg/common/API_functions.c \
    ../src_delaunay/mmg/common/API_functionsf.c \
    ../src_delaunay/mmg/common/bezier.c \
    ../src_delaunay/mmg/common/boulep.c \
    ../src_delaunay/mmg/common/chrono.c \
    ../src_delaunay/mmg/common/eigenv.c \
    ../src_delaunay/mmg/common/hash.c \
    ../src_delaunay/mmg/common/intmet.c \
    ../src_delaunay/mmg/common/isosiz.c \
    ../src_delaunay/mmg/common/librnbg.c \
    ../src_delaunay/mmg/common/mettools.c \
    ../src_delaunay/mmg/common/mmg.c \
    ../src_delaunay/mmg/common/quality.c \
    ../src_delaunay/mmg/common/scalem.c \
    ../src_delaunay/mmg/common/tools.c \
    ../src_fourier/png/png.c \
    ../src_fourier/png/pngerror.c \
    ../src_fourier/png/pngget.c \
    ../src_fourier/png/pngmem.c \
    ../src_fourier/png/pngpread.c \
    ../src_fourier/png/pngread.c \
    ../src_fourier/png/pngrio.c \
    ../src_fourier/png/pngrtran.c \
    ../src_fourier/png/pngrutil.c \
    ../src_fourier/png/pngset.c \
    ../src_fourier/png/pngtrans.c \
    ../src_fourier/png/pngwio.c \
    ../src_fourier/png/pngwrite.c \
    ../src_fourier/png/pngwtran.c \
    ../src_fourier/png/pngwutil.c \
    ../src_fourier/zlib/adler32.c \
    ../src_fourier/zlib/compress.c \
    ../src_fourier/zlib/crc32.c \
    ../src_fourier/zlib/deflate.c \
    ../src_fourier/zlib/gzclose.c \
    ../src_fourier/zlib/gzlib.c \
    ../src_fourier/zlib/gzread.c \
    ../src_fourier/zlib/gzwrite.c \
    ../src_fourier/zlib/infback.c \
    ../src_fourier/zlib/inffast.c \
    ../src_fourier/zlib/inflate.c \
    ../src_fourier/zlib/inftrees.c \
    ../src_fourier/zlib/trees.c \
    ../src_fourier/zlib/uncompr.c \
    ../src_fourier/zlib/zutil.c \
    ../src_delaunay/linearFit.cpp \
    ../src_exchange/exchange.cpp \
    ../src_interface/preview_file_dialog.cpp

DISTFILES += \
    interface.ico \
    interface.rc \
