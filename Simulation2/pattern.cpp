#include "boost/multi_array.hpp"
#include <cassert>
#include <random>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <omp.h>

using namespace boost;
using namespace std;

void start(struct timeval* tm1)
{
  gettimeofday(tm1, NULL);
}

void stop(struct timeval* tm1)
{
  struct timeval tm2;
  gettimeofday(&tm2, NULL);

  unsigned long long t = 1000 * (tm2.tv_sec - (*tm1).tv_sec) + (tm2.tv_usec - (*tm1).tv_usec) / 1000;
  printf("%llu ms\n", t);
}

int main ()
{
  // Time computation
  struct timeval t0;
  start(&t0);
  // Create a 2D array (with 2 values at each node so kind of 3D)
  typedef multi_array<double, 3> array_2D;
  typedef multi_array<double, 2> array_2D1;
  // Define the type of the index
  typedef array_2D::index index;
  unsigned int dim = 1000;
  // Define the Solution matrix
  array_2D A(extents[dim][dim][2]);


  // Define constants of the problem
  double alpha = 2.5;
  double beta = 1.1;
  double gamma = 1.05;
  array_2D1 D(extents[2][2]);
  D[0][0]=0.2;
  D[1][1]=0.2;
  D[0][1]=0.4;
  D[1][0]=0.05;
  double tau = 0.04;  
  double tTot = 100000/tau;
  double dh = 0.25;
  double dh2 = dh*dh;
  
  // Boundary conditions
  bool isPeriodic = false;
  
  // Initial conditions
  double epsilon = 7.E-1;
  struct timeval tm;
  gettimeofday(&tm, NULL);
  default_random_engine generator(tm.tv_usec);
  uniform_real_distribution<double> distribution(-epsilon,epsilon);
  double epsilonU, epsilonV;
  double valueTemp = sqrt(alpha*alpha*beta*gamma - alpha*alpha*gamma*gamma);
  for(index i = 0; i<dim; ++i)
  {
    for(index j = 0; j<dim; ++j)
    {
      epsilonU = distribution(generator);
      epsilonV = distribution(generator);
      A[i][j][0] = 1 - valueTemp/beta+epsilonU;
      A[i][j][1] = valueTemp/(alpha*gamma)+epsilonV;
      //~ if ((i-dim/2)*(i-dim/2)+(j-dim/2)*(j-dim/2)<dim/2)
        //~ A[i][j][1] = 0.2;
      //~ else
        //~ A[i][j][1] = 0.;
    }
  }
  array_2D B(A);
  ofstream image;
  //~ image.open("image.dat");
  //~ for(index i = 0; i<dim; ++i)
  //~ {
    //~ for(index j = 0; j<dim; ++j)
    //~ {
      //~ image << A[i][j][0] << "    ";
    //~ }
    //~ image << endl;
  //~ }
  //~ image.close();
  
  // Storage of coefficient
  double u,v,u2,v2;
  double lapU, lapV;
  double coef;
  // Main loop
  for(int t = 0; t<tTot; t++)
  {
    // Loop inside the domain
    if (t%(int (tTot/1000))==0)
    {
      cout << ((1000*t)/tTot) << "/1000" << endl;
      stop(&t0);
    }
    #pragma omp parallel for shared(A,B) private(u,v,u2,v2,lapU,lapV,coef)
    for(index i = 1; i<dim-1; ++i)
    {
      for(index j = 1; j<dim-1; ++j)
      {
        u = A[i][j][0];
        v = A[i][j][1];
        u2 = u*u;
        v2 = v*v;
        coef = (u2*v)/(u2+v2);
        
        lapU = (A[i+1][j][0]-2*u+A[i-1][j][0]);
        lapU += (A[i][j+1][0]-2*u+A[i][j-1][0]);
        lapU /= dh2;
        
        lapV = (A[i+1][j][1]-2*v+A[i-1][j][1]);
        lapV += (A[i][j+1][1]-2*v+A[i][j-1][1]);
        lapV /= dh2;
        
        B[i][j][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
        B[i][j][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
      }
    }
  
    // Boundary conditions /////////////////////////////////////////////
    index ib;
    index jb;
    
    // i borders
    ib = 0;
    for(index j = 1; j<dim-1; ++j)
    {
      u = A[ib][j][0];
      v = A[ib][j][1];
      u2 = u*u;
      v2 = v*v;
      coef = (u2*v)/(u2+v2);
      
      
      if (isPeriodic)
      {
        lapU = (A[ib+1][j][0]-2*u+A[dim-1][j][0]);
        lapU += (A[ib][j+1][0]-2*u+A[ib][j-1][0]);
        lapU /= dh2;
        
        lapV = (A[ib+1][j][1]-2*v+A[dim-1][j][1]);
        lapV += (A[ib][j+1][1]-2*v+A[ib][j-1][1]);
        lapV /= dh2;
      }
      else
      {
        lapU = (A[ib+1][j][0]-u);
        lapU += (A[ib][j+1][0]-2*u+A[ib][j-1][0]);
        lapU /= dh2;
         
        lapV = (A[ib+1][j][1]-v);
        lapV += (A[ib][j+1][1]-2*v+A[ib][j-1][1]);
        lapV /= dh2;
      }
      B[ib][j][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
      B[ib][j][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    }
    ib = dim-1;
    for(index j = 1; j<dim-1; ++j)
    {
      u = A[ib][j][0];
      v = A[ib][j][1];
      u2 = u*u;
      v2 = v*v;
      coef = (u2*v)/(u2+v2);
      
      
      if (isPeriodic)
      {
        lapU = (A[0][j][0]-2*u+A[ib-1][j][0]);
        lapU += (A[ib][j+1][0]-2*u+A[ib][j-1][0]);
        lapU /= dh2;
        
        lapV = (A[0][j][1]-2*v+A[ib-1][j][1]);
        lapV += (A[ib][j+1][1]-2*v+A[ib][j-1][1]);
        lapV /= dh2;
      }
      else
      {
        lapU = (-u+A[ib-1][j][0]);
        lapU += (A[ib][j+1][0]-2*u+A[ib][j-1][0]);
        lapU /= dh2;
        
        lapV = (-v+A[ib-1][j][1]);
        lapV += (A[ib][j+1][1]-2*v+A[ib][j-1][1]);
        lapV /= dh2;
      }
      
      B[ib][j][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
      B[ib][j][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    }
    
    // j borders
    jb = 0;
    for(index i = 1; i<dim-1; ++i)
    {
      u = A[i][jb][0];
      v = A[i][jb][1];
      u2 = u*u;
      v2 = v*v;
      coef = (u2*v)/(u2+v2);
      
      
      if (isPeriodic)
      {
        lapU = (A[i+1][jb][0]-2*u+A[i-1][jb][0]);
        lapU += (A[i][jb+1][0]-2*u+A[i][dim-1][0]);
        lapU /= dh2;
        
        lapV = (A[i+1][jb][1]-2*v+A[i-1][jb][1]);
        lapV += (A[i][jb+1][1]-2*v+A[i][dim-1][1]);
        lapV /= dh2;
      }
      else
      {
        lapU = (A[i+1][jb][0]-2*u+A[i-1][jb][0]);
        lapU += (A[i][jb+1][0]-u);
        lapU /= dh2;

        lapV = (A[i+1][jb][1]-2*v+A[i-1][jb][1]);
        lapV += (A[i][jb+1][1]-v);
        lapV /= dh2;
      }
      
      B[i][jb][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
      B[i][jb][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    }
    jb = dim-1;
    for(index i = 1; i<dim-1; ++i)
    {
      u = A[i][jb][0];
      v = A[i][jb][1];
      u2 = u*u;
      v2 = v*v;
      coef = (u2*v)/(u2+v2);
      

      if (isPeriodic)
      {
        lapU = (A[i+1][jb][0]-2*u+A[i-1][jb][0]);
        lapU += (A[i][0][0]-2*u+A[i][jb-1][0]);
        lapU /= dh2;
        
        lapV = (A[i+1][jb][1]-2*v+A[i-1][jb][1]);
        lapV += (A[i][0][1]-2*v+A[i][jb-1][1]);
        lapV /= dh2;
      }
      else
      {
        lapU = (A[i+1][jb][0]-2*u+A[i-1][jb][0]);
        lapU += (-u+A[i][jb-1][0]);
        lapU /= dh2;
        
        lapV = (A[i+1][jb][1]-2*v+A[i-1][jb][1]);
        lapV += (-v+A[i][jb-1][1]);
        lapV /= dh2;
      }
      
      B[i][jb][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
      B[i][jb][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    }
    
    // corners
    ib = 0;
    jb = 0;
    u = A[ib][jb][0];
    v = A[ib][jb][1];
    
    if (isPeriodic)
    {
      lapU = (A[ib+1][jb][0]-2*u+A[dim-1][jb][0]);
      lapU += (A[ib][jb+1][0]-2*u+A[ib][dim-1][0]);
      lapU /= dh2;

      lapV = (A[ib+1][jb][1]-2*v+A[dim-1][jb][1]);
      lapV += (A[ib][jb+1][1]-2*v+A[ib][dim-1][1]);
      lapV /= dh2;
    }
    else
    {
      lapU = (A[ib+1][jb][0]-u);
      lapU += (A[ib][jb+1][0]-u);
      lapU /= dh2;

      lapV = (A[ib+1][jb][1]-v);
      lapV += (A[ib][jb+1][1]-v);
      lapV /= dh2;
    }
    
    B[ib][jb][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
    B[ib][jb][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    
    ib = 0;
    jb = dim-1;
    u = A[ib][jb][0];
    v = A[ib][jb][1];

    if (isPeriodic)
    {
      lapU = (A[ib+1][jb][0]-2*u+A[dim-1][jb][0]);
      lapU += (A[ib][0][0]-2*u+A[ib][jb-1][0]);
      lapU /= dh2;

      lapV = (A[ib+1][jb][1]-2*v+A[dim-1][jb][1]);
      lapV += (A[ib][0][1]-2*v+A[ib][jb-1][1]);
      lapV /= dh2;
    }
    else
    {
      lapU = (A[ib+1][jb][0]-u);
      lapU += (-u+A[ib][jb-1][0]);
      lapU /= dh2;

      lapV = (A[ib+1][jb][1]-v);
      lapV += (-v+A[ib][jb-1][1]);
      lapV /= dh2;
    }
    B[ib][jb][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
    B[ib][jb][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    
    ib = dim-1;
    jb = 0;
    u = A[ib][jb][0];
    v = A[ib][jb][1];

    if (isPeriodic)
    {
      lapU = (A[0][jb][0]-2*u+A[ib-1][jb][0]);
      lapU += (A[ib][jb+1][0]-2*u+A[ib][dim-1][0]);
      lapU /= dh2;

      lapV = (A[0][jb][1]-2*v+A[ib-1][jb][1]);
      lapV += (A[ib][jb+1][1]-2*v+A[ib][dim-1][1]);
      lapV /= dh2;
    }
    else
    {
      lapU = (-u+A[ib-1][jb][0]);
      lapU += (A[ib][jb+1][0]-u);
      lapU /= dh2;

      lapV = (-v+A[ib-1][jb][1]);
      lapV += (A[ib][jb+1][1]-v);
      lapV /= dh2;
    }
    B[ib][jb][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
    B[ib][jb][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    
    ib = dim-1;
    jb = dim-1;
    u = A[ib][jb][0];
    v = A[ib][jb][1];

    if (isPeriodic)
    {
      lapU = (A[0][jb][0]-2*u+A[ib-1][jb][0]);
      lapU += (A[ib][0][0]-2*u+A[ib][jb-1][0]);
      lapU /= dh2;

      lapV = (A[0][jb][1]-2*v+A[ib-1][jb][1]);
      lapV += (A[ib][0][1]-2*v+A[ib][jb-1][1]);
      lapV /= dh2;
    }
    else
    {
      lapU = (-u+A[ib-1][jb][0]);
      lapU += (-u+A[ib][jb-1][0]);
      lapU /= dh2;

      lapV = (-v+A[ib-1][jb][1]);
      lapV += (-v+A[ib][jb-1][1]);
      lapV /= dh2;
    }
    B[ib][jb][0] = u+ tau*(u*(1-u)-alpha*coef+D[0][0]*lapU+D[0][1]*lapV);
    B[ib][jb][1] = v+ tau*(beta*coef-gamma*v+D[1][0]*lapU+D[1][1]*lapV);
    
    //~ swap(A,B);
    A = B;
  }
  
  // Printing output
  
  image.open("image2.dat");
  for(index i = 0; i<dim; ++i)
  {
    for(index j = 0; j<dim; ++j)
    {
      image << A[i][j][1] << "    ";
    }
    image << endl;
  }
  image.close();
  stop(&t0);
  return 0;
}

